package fr.dark40k.outlookdispatcher;



import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.gui.FastFolderAssignementJPanel;
import fr.dark40k.outlookdispatcher.gui.HistoryFolderAssignementJPanel;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.SelectionChangeEvent;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.SelectionChangeListener;
import fr.dark40k.outlookdispatcher.gui.folder.FolderAssignementJPanel;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;
import fr.dark40k.outlookdispatcher.util.ScreenSize;
import fr.dark40k.util.persistantparameters.ParamGroup;

public class DispatcherFrame extends JFrame {

	private final Dispatcher dispatcher;

	private final ParamGroup dispatcherDisplayParams;
	private final String dispatcherSizedDisplayParamsName;
	private final ParamGroup dispatcherSizedDisplayParams;
	private final ParamGroup dispatchFolderViewPanelParams;
	private final ParamGroup dispatchFolderAssignementPanelParams;

	final DispatchFolderViewPanel dispatchFolderViewPanel;
	final FolderAssignementJPanel folderAssignementPanel;
	final FastFolderAssignementJPanel fastFolderAssignementPanel;
	final HistoryFolderAssignementJPanel historyFolderAssignementPanel;

	JCheckBoxMenuItem chckbxmntmSelectionnerConversations;

	private JPanel contentPane;
	private JSplitPane splitPane_1;
	private JSplitPane splitPane_2;
	private JScrollPane scrollPaneFastFolder;
	private JPanel panel;
	private JButton btnDeplacerSelection;
	private JPanel panel_1;
	private JMenuBar menuBar;
	private JMenu mnFichier;
	private JMenuItem mntmQuitter;
	private JMenu mnParameters;
	private JMenu mnActions;
	private JMenuItem mntmActualiser;
	private JMenuItem mntmTransferer;
	private JSeparator separator;
	private JMenuItem mntmAfficherConsole;
	private JSeparator separator_1;
	private JMenuItem mntmChangerDossier;
	private JMenuItem mntmRepertoiresExclus;
	private JMenuItem mntmMotsExclus;
	private JSeparator separator_2;
	private JMenuItem mntmReinitDossiersOutlook;
	private JMenuItem mntmAddressesMailsExclues;
	private JMenuItem mntmDestinationPermanentes;
	private JMenuItem mntmRecRepertoiresExclus;
	private JMenuItem mntmSupprimer;
	private JMenuItem mntmDefTrashFolder;
	private JSeparator separator_3;
	private JMenuItem mntmUndo;
	private JMenuItem mntmRedo;
	private JTabbedPane tabbedPane;
	private JScrollPane scrollPaneHistory;
	protected JMenuItem mntmDirectLinks;
	protected JMenuItem mntmEraseAllFolders;
	protected JMenuItem mntmDossiers;
	protected JSeparator separator_4;
	protected JSeparator separator_5;
	protected JSeparator separator_6;
	protected JMenuItem mntmTrainClassifier;
	protected JSeparator separator_7;
	protected JMenuItem mntmParamtres;


	// ===========================================================================================================
	//
	// Initialisation
	//
	// ===========================================================================================================

	public DispatcherFrame(Dispatcher dispatcher) {

		this.dispatcher=dispatcher;

		//
		// param�tres
		//
		dispatcherDisplayParams=Dispatcher.properties.displayParams.getSubParamGroup("dispatcherFrame");

		dispatcherSizedDisplayParamsName=calculateCurrentDispatcherFrameParamsName();
		dispatcherSizedDisplayParams=dispatcherDisplayParams.getSubParamGroup(dispatcherSizedDisplayParamsName);

		dispatchFolderViewPanelParams=Dispatcher.properties.displayParams.getSubParamGroup("dispatchFolderViewPanel");

		dispatchFolderAssignementPanelParams=Dispatcher.properties.displayParams.getSubParamGroup("dispatchFolderAssignementPanel");

		//
		// Creation des entites
		//
		dispatchFolderViewPanel = new DispatchFolderViewPanel(
				dispatcher,
				(OlFolderEMail folder)->{return dispatcher.doCreateSubFolder(folder);},
				dispatchFolderViewPanelParams);

		folderAssignementPanel = new FolderAssignementJPanel(dispatchFolderAssignementPanelParams);

		fastFolderAssignementPanel = new FastFolderAssignementJPanel();

		historyFolderAssignementPanel = new HistoryFolderAssignementJPanel(dispatcher.database.moveToFolderHistory);

		//
		//Initialisation de l'interface graphique
		//

		initGUI();

		//
		// Configuration des elements
		//

		// Fenetre d'affichage

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				dispatcher.doSaveAndExit();
			}
		});

		restoreWindowConfiguration();

		// Menu

		chckbxmntmSelectionnerConversations.setSelected(Dispatcher.properties.selectConversation);

		// Transfer de la selection de la table vers les objet de selection de dossier

		dispatchFolderViewPanel.addSelectionChangeListener( new SelectionChangeListener() {
			@Override
			public void selectionChanged(SelectionChangeEvent evt) {
				ToSortEmailGroup emailDataGroup = dispatchFolderViewPanel.getSelectedEmailDataGroup();
				fastFolderAssignementPanel.setEmailDataGroup(emailDataGroup);
				historyFolderAssignementPanel.setEmailDataGroup(emailDataGroup);
				folderAssignementPanel.setEmailDataGroup(emailDataGroup);
			}
		});
		dispatchFolderViewPanel.setConversationSelectionMode(Dispatcher.properties.selectConversation);

		// PopupMenu pour l'affichage complet des repertoires

		folderAssignementPanel.folderPopupMenu.addPopupChckBox(
				"Corbeille",
				(OlFolder folder) -> { return  Dispatcher.properties.trashDestinationAddress.equals(folder.getEntryID()); } ,
				(OlFolder folder) -> { Dispatcher.properties.trashDestinationAddress=folder.getEntryID(); folderAssignementPanel.forceRefresh(null);});

		folderAssignementPanel.folderPopupMenu.addPopupChckBox(
				"Repertoire exclus",
				(OlFolder folder) -> { return  DispatchDatabase.properties.excludedDirectories.contains(folder.getEntryID()); } ,
				(OlFolder folder) -> { DispatchDatabase.properties.excludedDirectories.switchParam(folder.getEntryID()); folderAssignementPanel.forceRefresh(null);});

		folderAssignementPanel.folderPopupMenu.addPopupChckBox(
				"Repertoire exclus recursif",
				(OlFolder folder) -> { return  DispatchDatabase.properties.excludedRecDirectories.contains(folder.getEntryID()); } ,
				(OlFolder folder) -> { DispatchDatabase.properties.excludedRecDirectories.switchParam(folder.getEntryID()); folderAssignementPanel.forceRefresh(null);});

		folderAssignementPanel.folderPopupMenu.addPopupChckBox(
				"Repertoire scann�",
				(OlFolder folder) -> { return  DispatchDatabase.properties.scanDirectories.contains(folder.getEntryID()); } ,
				(OlFolder folder) -> { DispatchDatabase.properties.scanDirectories.switchParam(folder.getEntryID()); folderAssignementPanel.forceRefresh(null); });

		folderAssignementPanel.folderPopupMenu.addPopupChckBox(
				"Repertoire permanent",
				(OlFolder folder) -> { return  Dispatcher.properties.permanentDestinationAddresses.contains(folder.getEntryID()); } ,
				(OlFolder folder) -> { Dispatcher.properties.permanentDestinationAddresses.switchParam(folder.getEntryID()); folderAssignementPanel.forceRefresh(null); });

		folderAssignementPanel.folderPopupMenu.addPopupSeparator();

		folderAssignementPanel.folderPopupMenu.addPopupCommand("Copier ID => clipboard", (OlFolder folder) -> { Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(folder.getEntryID()),null); });
		folderAssignementPanel.folderPopupMenu.addPopupCommand("Renommer repertoire", (OlFolder folder) -> { dispatcher.doRenameFolder(folder); });
		folderAssignementPanel.folderPopupMenu.addPopupCommand("Cr�er sous-repertoire", (OlFolder folder) -> { dispatcher.doCreateSubFolder(folder);});
		folderAssignementPanel.folderPopupMenu.addPopupCommand("Editer description", (OlFolder folder) -> { dispatcher.doEditFolderDescription(folder); });
		folderAssignementPanel.folderPopupMenu.addPopupCommand("Recalculer Recursif", (OlFolder folder) -> { dispatcher.doActualiserSousDossiers(folder); });
//		folderAssignementPanel.popupMenu.addPopupCommand("Afficher description", (OlFolder folder) -> { dispatcher.doEditFolderRawDescription(folder); });

	}

	//
	// Assignation de la base de donn�e
	//

	public void setDatabase(DispatchDatabase newDatabase) {
		dispatchFolderViewPanel.setDatabase(newDatabase);
		folderAssignementPanel.setDatabase(newDatabase);
	}

	public void setTransferProgressMessage(String msg) {

		if (msg!=null)
				btnDeplacerSelection.setText(msg);
		else
				btnDeplacerSelection.setText("Deplacer selection");

	}

	// ===========================================================================================================
	//
	// Sauvegarde et restauration de l'affichage
	//
	// ===========================================================================================================

	public void restoreWindowConfiguration() {

		//
		// Chargement des param�tres ind�pendants de la taille d'ecran
		//

		// objets enfants
		dispatchFolderViewPanel.restoreWindowConfiguration();
		folderAssignementPanel.restoreWindowConfiguration();

		// tabbedpane
		applyIfNotNull(dispatcherDisplayParams.getSubParamInteger("tabbedPane",null).getValue(), (Integer v) -> {tabbedPane.setSelectedIndex(v);});

		//
		// Chargement des param�tres li�s � la taille d'ecran
		//

		// verifie que le jeu de param�tres charg� correspond bien � la taille de l'ecran
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        if (dispatcherSizedDisplayParams.getSubParamInteger("screen_height", -1).getValue()!=virtualBounds.height) return;
        if (dispatcherSizedDisplayParams.getSubParamInteger("screen_width", -1).getValue()!=virtualBounds.width) return;

        // taille de la fenetre
		setBounds(
				dispatcherSizedDisplayParams.getSubParamInteger("win_x",null).getValue(),
				dispatcherSizedDisplayParams.getSubParamInteger("win_y",null).getValue(),
				dispatcherSizedDisplayParams.getSubParamInteger("win_width",null).getValue(),
				dispatcherSizedDisplayParams.getSubParamInteger("win_height",null).getValue() );

		// separateurs
		applyIfNotNull(dispatcherSizedDisplayParams.getSubParamInteger("split1",null).getValue(), (Integer v) -> {splitPane_1.setDividerLocation(v);});
		applyIfNotNull(dispatcherSizedDisplayParams.getSubParamInteger("split2",null).getValue(), (Integer v) -> {splitPane_2.setDividerLocation(v);});

	}

	public void saveWindowConfiguration()  {

		//
		// Sauvegarde des param�tres ind�pendants de la taille d'ecran
		//

		// objets enfants
		dispatchFolderViewPanel.saveWindowConfiguration();
		folderAssignementPanel.saveWindowConfiguration();

		// tabbedpane
        dispatcherDisplayParams.getSubParamInteger("tabbedPane", null).setValue(tabbedPane.getSelectedIndex());

		// verifie que le jeu de param�tres utilis� correspond bien � la taille de l'ecran
		if (!dispatcherSizedDisplayParamsName.equals(calculateCurrentDispatcherFrameParamsName())) return;

		//
		// Sauvegarde des param�tres li�s � la taille d'ecran
		//

        // taille de l'ecran
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        dispatcherSizedDisplayParams.getSubParamInteger("screen_height",null).setValue(virtualBounds.height);
        dispatcherSizedDisplayParams.getSubParamInteger("screen_width",null).setValue(virtualBounds.width);

        // taille de la fenetre
        dispatcherSizedDisplayParams.getSubParamInteger("win_x",null).setValue(getX());
        dispatcherSizedDisplayParams.getSubParamInteger("win_y",null).setValue(getY());
        dispatcherSizedDisplayParams.getSubParamInteger("win_width",null).setValue(getWidth());
        dispatcherSizedDisplayParams.getSubParamInteger("win_height",null).setValue(getHeight());

		// separateurs
        dispatcherSizedDisplayParams.getSubParamInteger("split1", null).setValue(splitPane_1.getDividerLocation());
        dispatcherSizedDisplayParams.getSubParamInteger("split2", null).setValue(splitPane_2.getDividerLocation());

	}

	// ===========================================================================================================
	//
	// Divers
	//
	// ===========================================================================================================

	private static void applyIfNotNull(Integer value, Consumer<Integer> command) {
		if (value!=null) command.accept(value);
	}

	private static String calculateCurrentDispatcherFrameParamsName() {
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        return "display"+virtualBounds.width+"x"+virtualBounds.height;
	}

	// ===========================================================================================================
	//
	// Affichage
	//
	// ===========================================================================================================

	private void initGUI() {

		setBounds(100, 100, 1189, 747);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.10);
		contentPane.add(splitPane_1, BorderLayout.CENTER);

		splitPane_1.setRightComponent(dispatchFolderViewPanel);

		splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.1);
		splitPane_1.setLeftComponent(splitPane_2);
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);

		panel = new JPanel();
		splitPane_2.setRightComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));

		folderAssignementPanel.setBorder(new TitledBorder(null, "Repertoires messagerie", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(folderAssignementPanel);

		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Commandes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_1, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 119, 0 };
		gbl_panel_1.rowHeights = new int[] { 23, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		btnDeplacerSelection = new JButton("Deplacer selection");
		btnDeplacerSelection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doMoveSelection();
			}
		});
		GridBagConstraints gbc_btnDeplacerSelection = new GridBagConstraints();
		gbc_btnDeplacerSelection.ipady = 10;
		gbc_btnDeplacerSelection.fill = GridBagConstraints.BOTH;
		gbc_btnDeplacerSelection.gridx = 0;
		gbc_btnDeplacerSelection.gridy = 0;
		panel_1.add(btnDeplacerSelection, gbc_btnDeplacerSelection);

						tabbedPane = new JTabbedPane(JTabbedPane.TOP);
						splitPane_2.setLeftComponent(tabbedPane);

								scrollPaneFastFolder = new JScrollPane();
								tabbedPane.addTab("Selection filtr�e", null, scrollPaneFastFolder, null);
								scrollPaneFastFolder.setViewportView(fastFolderAssignementPanel);

								scrollPaneHistory = new JScrollPane();
								tabbedPane.addTab("Repertoires r�cents", null, scrollPaneHistory, null);
								scrollPaneHistory.setViewportView(historyFolderAssignementPanel);

		menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);

		mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);

		mntmActualiser = new JMenuItem("Actualiser Outlook");
		mntmActualiser.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		mnFichier.add(mntmActualiser);
		mntmActualiser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doActualiser();
			}
		});

		mntmReinitDossiersOutlook = new JMenuItem("Reinitialiser Dossiers Outlook");
		mntmReinitDossiersOutlook.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doActualiserDossiers();
			}
		});
		separator_5 = new JSeparator();
		mnFichier.add(separator_5);
		mntmTrainClassifier = new JMenuItem("Train Classifier");
		mntmTrainClassifier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doTrainClassifier();
			}
		});
		mnFichier.add(mntmTrainClassifier);
		separator_6 = new JSeparator();
		mnFichier.add(separator_6);
		mnFichier.add(mntmReinitDossiersOutlook);

		mntmEraseAllFolders = new JMenuItem("Effacer tous dossiers Outlook");
		mntmEraseAllFolders.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEraseAllFolders();
			}
		});
		mnFichier.add(mntmEraseAllFolders);

		separator_1 = new JSeparator();
		mnFichier.add(separator_1);

		mntmQuitter = new JMenuItem("Quitter");
		mntmQuitter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DispatcherFrame.this.dispatchEvent(new WindowEvent(DispatcherFrame.this, WindowEvent.WINDOW_CLOSING));
			}
		});
		mnFichier.add(mntmQuitter);

		mnParameters = new JMenu("Parametres");
		menuBar.add(mnParameters);

		chckbxmntmSelectionnerConversations = new JCheckBoxMenuItem("Selectionner conversations");
		chckbxmntmSelectionnerConversations.setSelected(true);
		chckbxmntmSelectionnerConversations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doChangeConversationSelectionMode();
			}
		});
		mnParameters.add(chckbxmntmSelectionnerConversations);

		mntmRepertoiresExclus = new JMenuItem("Dossiers exclus");
		mntmRepertoiresExclus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditExcludedDirectories();
			}
		});

		separator_2 = new JSeparator();
		mnParameters.add(separator_2);

		mntmDefTrashFolder = new JMenuItem("Corbeille");
		mntmDefTrashFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditTrashDirectory();
			}
		});
		mnParameters.add(mntmDefTrashFolder);

		mntmDirectLinks = new JMenuItem("RegExp globales");
		mntmDirectLinks.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditDirectLinks();
			}
		});
		mnParameters.add(mntmDirectLinks);

				mntmMotsExclus = new JMenuItem("Mots exclus");
				mntmMotsExclus.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dispatcher.doEditExcludedWords();
					}
				});
				mnParameters.add(mntmMotsExclus);

				mntmAddressesMailsExclues = new JMenuItem("Addresses mails exclues");
				mntmAddressesMailsExclues.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dispatcher.doEditExcludedAddresses();
					}
				});
				mnParameters.add(mntmAddressesMailsExclues);
		separator_4 = new JSeparator();
		mnParameters.add(separator_4);

		mntmChangerDossier = new JMenuItem("Dossiers scann\u00E9s");
		mnParameters.add(mntmChangerDossier);
		mntmChangerDossier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditScanDirectories();
			}
		});
		mnParameters.add(mntmRepertoiresExclus);

		mntmRecRepertoiresExclus = new JMenuItem("Dossiers recursifs exclus");
		mntmRecRepertoiresExclus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditExcludedRecDirectories();
			}
		});
		mnParameters.add(mntmRecRepertoiresExclus);

						mntmDestinationPermanentes = new JMenuItem("Destination permanentes");
						mntmDestinationPermanentes.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								dispatcher.doEditPermanentDestinationDirectories();
							}
						});
						mnParameters.add(mntmDestinationPermanentes);

		separator = new JSeparator();
		mnParameters.add(separator);

		mntmAfficherConsole = new JMenuItem("Afficher Console");
		mntmAfficherConsole.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doShowEventConsole(false);
			}
		});
		mntmParamtres = new JMenuItem("Param\u00E8tres");
		mntmParamtres.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditParameters();
			}
		});
		mntmDossiers = new JMenuItem("Gestion Dossiers");
		mntmDossiers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doEditFolders();
			}
		});
		mnParameters.add(mntmDossiers);
		mnParameters.add(mntmParamtres);
		separator_7 = new JSeparator();
		mnParameters.add(separator_7);
		mnParameters.add(mntmAfficherConsole);

		mnActions = new JMenu("Actions");
		menuBar.add(mnActions);

		mntmTransferer = new JMenuItem("Transferer");
		mntmTransferer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doMoveSelection();
			}
		});

		mntmSupprimer = new JMenuItem("Supprimer");
		mntmSupprimer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doMoveToTrashSelection();
			}
		});

		mntmUndo = new JMenuItem("Undo") {
			@Override
			public String getToolTipText(MouseEvent event) {
				return StaticUndoManager.getUndoPresentationName();
			}
		};
		mntmUndo.setToolTipText("vide");
		mntmUndo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doUndo();
			}
		});
		mntmUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
		mnActions.add(mntmUndo);

		mntmRedo = new JMenuItem("Redo") {
			@Override
			public String getToolTipText(MouseEvent event) {
				return StaticUndoManager.getRedoPresentationName();
			}
		};
		mntmRedo.setToolTipText("vide");
		mntmRedo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatcher.doRedo();
			}
		});
		mntmRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));
		mnActions.add(mntmRedo);

		separator_3 = new JSeparator();
		mnActions.add(separator_3);
		mntmSupprimer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		mnActions.add(mntmSupprimer);
		mnActions.add(mntmTransferer);
	}

}
