package fr.dark40k.outlookdispatcher.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;

import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.OriginList.Origin;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;

public class EmailDataGroupFolderWeightDisplayDialog {

//	private final static Logger LOGGER = LogManager.getLogger();

	public static void showDialog(ToSortEmailGroup emailDataGroup) {

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 13));

		tabbedPane.addTab("Total",buildGlobalTextArea(emailDataGroup.getPotentialFoldersList(), emailDataGroup.getComonDestFolderId()));

		Iterator<ToSortEmailSingle> emailDataIterator = emailDataGroup.getEmailsDataIterator();
		int count=0;
		while (emailDataIterator.hasNext()) {
			count++;
			ToSortEmailSingle emailData = emailDataIterator.next();
			String title = count+" "+emailData.getEMail().getSubject();
			if (title.length()>23) title=title.substring(0,20)+"...";
			tabbedPane.addTab(title,buildGlobalTextArea(emailData.getPotentialFoldersList(),emailDataGroup.getComonDestFolderId()));
		}

		JOptionPane.showMessageDialog(null, tabbedPane, "Justification score", JOptionPane.INFORMATION_MESSAGE);

//		LOGGER.printf(Level.INFO, "%s", "Score pour repertoire\n" + log);

	}

	private static JComponent buildGlobalTextArea(PotentialFoldersList potFolderList, String comonDestinationFolderId) {

		JTextPane textArea = new JTextPane();
		textArea.setContentType("text/html");
		textArea.setEditable(false);

		// wrap a scrollpane around it
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(800, 450));

		String msg = "";

		for (WeightedDestFolder potFolder : potFolderList) {
			msg += buildHTMLList(potFolder,potFolder.folder.getEntryID().equals(comonDestinationFolderId));
		}

		textArea.setText("<html><body><p style='width: 500px;'>" + msg + "</p></body></html>");

		textArea.setCaretPosition(0);

		return scrollPane;

	}

	private static String buildHTMLList(WeightedDestFolder potFolder, boolean boldTitle) {

		String subMsg = String.format("%s => %,.0f", potFolder.folder.getName(), potFolder.weight);

		if (boldTitle) subMsg = "<b>" + subMsg + "</b>";

		// Construit la liste compact�e des contributeurs au poids
		LinkedHashMap<String, Origin> compactedOrigin = new LinkedHashMap<String, Origin>();
		for (Origin origin : potFolder.getOrigin()) {
			// System.out.println("origin="+origin);
			Origin storedOrigin = compactedOrigin.get(origin.getName());
			if (storedOrigin == null)
				compactedOrigin.put(origin.getName(), new Origin(origin,1.0));
			else
				storedOrigin.add(origin);
		}

		// Construit une liste des entr�e et effectue son tri
		List<Map.Entry<String, Origin>> sortedOrigins = new ArrayList<Map.Entry<String, Origin>>(compactedOrigin.entrySet());

		Collections.sort(sortedOrigins, new Comparator<Map.Entry<String, Origin>>() {
			@Override
			public int compare(Map.Entry<String, Origin> entry1, Map.Entry<String, Origin> entry2) {
				return -Double.compare(entry1.getValue().weight , entry2.getValue().weight);
			}
		});

		String subMsg2 = "";
		for (Map.Entry<String, Origin> origin : sortedOrigins) {
			String line = String.format("%3.2f x %s => %,.0f", origin.getValue().count, origin.getValue().name, origin.getValue().weight);
			subMsg2 += "<li>" + line + "</li>";
		}

		return "<p>" + subMsg + "<ul>" + subMsg2 + "</ul>" + "</p>";

	}


}
