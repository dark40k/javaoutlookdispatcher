package fr.dark40k.outlookdispatcher.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.AutoUpdateFolderParam;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.AutoUpdateFolderParam.AutoUpdateFolderModeEnum;

public class UpdateFoldersDescriptionsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private final JRefreshModeComboBox defaultAutoKeywordsBlockComboBox = new JRefreshModeComboBox();
	private final JRefreshModeComboBox defaultAdressBlockComboBox = new JRefreshModeComboBox();
	private final  JRefreshModeComboBox defaultConversationBlockComboBox = new JRefreshModeComboBox();

	private final  JRefreshModeComboBox excludedAutoKeywordsBlockComboBox = new JRefreshModeComboBox();
	private final  JRefreshModeComboBox excludedAdressBlockComboBox = new JRefreshModeComboBox();
	private final  JRefreshModeComboBox excludedConversationBlockComboBox = new JRefreshModeComboBox();

	private final  JRefreshModeComboBox recExcludedAutoKeywordsBlockComboBox = new JRefreshModeComboBox();
	private final  JRefreshModeComboBox recExcludedAdressBlockComboBox = new JRefreshModeComboBox();
	private final  JRefreshModeComboBox recExcludedConversationBlockComboBox = new JRefreshModeComboBox();

	private Boolean exitStatus = null;

	/**
	 * Create the dialog.
	 */
	public UpdateFoldersDescriptionsDialog() {
		initGUI();
		setAutoUpdateFolderMode(new AutoUpdateFolderParam());
	}

	/**
	 *
	 */
	public void setAutoUpdateFolderMode(AutoUpdateFolderParam param) {

		defaultAutoKeywordsBlockComboBox.setUpdateValue(param.defaultAutoKeywordsBlock);
		defaultAdressBlockComboBox.setUpdateValue(param.defaultAdressBlock);
		defaultConversationBlockComboBox.setUpdateValue(param.defaultConversationBlock);

		excludedAutoKeywordsBlockComboBox.setUpdateValue(param.excludedAutoKeywordsBlock);
		excludedAdressBlockComboBox.setUpdateValue(param.excludedAdressBlock);
		excludedConversationBlockComboBox.setUpdateValue(param.excludedConversationBlock);

		recExcludedAutoKeywordsBlockComboBox.setUpdateValue(param.recExcludedAutoKeywordsBlock);
		recExcludedAdressBlockComboBox.setUpdateValue(param.recExcludedAdressBlock);
		recExcludedConversationBlockComboBox.setUpdateValue(param.recExcludedConversationBlock);
	}

	public AutoUpdateFolderParam getAutoUpdateFolderMode() {

		AutoUpdateFolderParam ret = new AutoUpdateFolderParam();

		ret.defaultAutoKeywordsBlock = defaultAutoKeywordsBlockComboBox.getUpdateValue();
		ret.defaultAdressBlock = defaultAdressBlockComboBox.getUpdateValue();
		ret.defaultConversationBlock = defaultConversationBlockComboBox.getUpdateValue();

		ret.excludedAutoKeywordsBlock = excludedAutoKeywordsBlockComboBox.getUpdateValue();
		ret.excludedAdressBlock = excludedAdressBlockComboBox.getUpdateValue();
		ret.excludedConversationBlock = excludedConversationBlockComboBox.getUpdateValue();

		ret.recExcludedAutoKeywordsBlock = recExcludedAutoKeywordsBlockComboBox.getUpdateValue();
		ret.recExcludedAdressBlock = recExcludedAdressBlockComboBox.getUpdateValue();
		ret.recExcludedConversationBlock = recExcludedConversationBlockComboBox.getUpdateValue();

		return ret;
	}


	/**
	 * @return status de la box, true=ok, false=cancel, null=en cours
	 */
	public Boolean getExistStatus() {
		return exitStatus;
	}

	/**
	 * Display gui
	 */
	private void initGUI() {
		setModal(true);
		setTitle("Mise \u00E0 jours des dossiers");
		setBounds(100, 100, 640, 188);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblProjets = new JLabel("Projets");
			GridBagConstraints gbc_lblProjets = new GridBagConstraints();
			gbc_lblProjets.insets = new Insets(0, 0, 5, 5);
			gbc_lblProjets.gridx = 0;
			gbc_lblProjets.gridy = 0;
			contentPanel.add(lblProjets, gbc_lblProjets);
		}
		{
			JLabel lblAdresses = new JLabel("Adresses");
			GridBagConstraints gbc_lblAdresses = new GridBagConstraints();
			gbc_lblAdresses.insets = new Insets(0, 0, 5, 5);
			gbc_lblAdresses.gridx = 1;
			gbc_lblAdresses.gridy = 0;
			contentPanel.add(lblAdresses, gbc_lblAdresses);
		}
		{
			JLabel lblConversations = new JLabel("Conversations");
			GridBagConstraints gbc_lblConversations = new GridBagConstraints();
			gbc_lblConversations.insets = new Insets(0, 0, 5, 5);
			gbc_lblConversations.gridx = 2;
			gbc_lblConversations.gridy = 0;
			contentPanel.add(lblConversations, gbc_lblConversations);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 0;
			gbc_comboBox.gridy = 1;
			contentPanel.add(defaultAutoKeywordsBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 1;
			contentPanel.add(defaultAdressBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 2;
			gbc_comboBox.gridy = 1;
			contentPanel.add(defaultConversationBlockComboBox, gbc_comboBox);
		}
		{
			JLabel lblRepertoireStandard = new JLabel("Repertoire standard");
			GridBagConstraints gbc_lblRepertoireStandard = new GridBagConstraints();
			gbc_lblRepertoireStandard.anchor = GridBagConstraints.WEST;
			gbc_lblRepertoireStandard.insets = new Insets(0, 0, 5, 0);
			gbc_lblRepertoireStandard.gridx = 3;
			gbc_lblRepertoireStandard.gridy = 1;
			contentPanel.add(lblRepertoireStandard, gbc_lblRepertoireStandard);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 0;
			gbc_comboBox.gridy = 2;
			contentPanel.add(excludedAutoKeywordsBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 2;
			contentPanel.add(excludedAdressBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 2;
			gbc_comboBox.gridy = 2;
			contentPanel.add(excludedConversationBlockComboBox, gbc_comboBox);
		}
		{
			JLabel lblRepertoireExclus = new JLabel("Repertoire exclus");
			GridBagConstraints gbc_lblRepertoireExclus = new GridBagConstraints();
			gbc_lblRepertoireExclus.anchor = GridBagConstraints.WEST;
			gbc_lblRepertoireExclus.insets = new Insets(0, 0, 5, 0);
			gbc_lblRepertoireExclus.gridx = 3;
			gbc_lblRepertoireExclus.gridy = 2;
			contentPanel.add(lblRepertoireExclus, gbc_lblRepertoireExclus);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 0;
			gbc_comboBox.gridy = 3;
			contentPanel.add(recExcludedAutoKeywordsBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 3;
			contentPanel.add(recExcludedAdressBlockComboBox, gbc_comboBox);
		}
		{
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 5);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 2;
			gbc_comboBox.gridy = 3;
			contentPanel.add(recExcludedConversationBlockComboBox, gbc_comboBox);
		}
		{
			JLabel lblRepertoireExclusRecursif = new JLabel("Repertoire exclus recursif");
			GridBagConstraints gbc_lblRepertoireExclusRecursif = new GridBagConstraints();
			gbc_lblRepertoireExclusRecursif.insets = new Insets(0, 0, 5, 0);
			gbc_lblRepertoireExclusRecursif.anchor = GridBagConstraints.WEST;
			gbc_lblRepertoireExclusRecursif.gridx = 3;
			gbc_lblRepertoireExclusRecursif.gridy = 3;
			contentPanel.add(lblRepertoireExclusRecursif, gbc_lblRepertoireExclusRecursif);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						exitStatus = true;
						UpdateFoldersDescriptionsDialog.this.setVisible(false);
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						exitStatus = false;
						UpdateFoldersDescriptionsDialog.this.setVisible(false);
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	private static class JRefreshModeComboBox extends JComboBox<String> {

		private final static String[] UPDATEOPTIONS = AutoUpdateFolderModeEnum.getNames();

		public JRefreshModeComboBox() {
			super(UPDATEOPTIONS);
			setSelectedItem(AutoUpdateFolderModeEnum.Recalculer.name());
		}

		public void setUpdateValue(AutoUpdateFolderModeEnum newValue) {
			setSelectedItem(newValue.name());
		}

		public AutoUpdateFolderModeEnum getUpdateValue() {
			return AutoUpdateFolderModeEnum.valueOfByName((String)getSelectedItem());
		}

	}


}
