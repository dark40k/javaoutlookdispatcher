package fr.dark40k.outlookdispatcher.gui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateEvent;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateListener;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.GUIUtilities;

public class FastFolderAssignementJPanel extends JPanel implements ListSelectionListener, UpdateListener {

	private static final String NOFOLDERMESSAGE="<Pas de repertoire>";

	private ToSortEmailGroup currentEmailDataGroup;

	private final DefaultListModel<FolderListItem> listModel=new DefaultListModel<FolderListItem>();

	private final JList<FolderListItem> folderList=new JList<FolderListItem>(listModel){
		@Override
		public String getToolTipText(MouseEvent event) {
		    Point p = event.getPoint();
		    int location = locationToIndex(p);
		    if (location<=0) return null;
			return getModel().getElementAt(location).folder.getPath("/").toString();
		  }
	};

    private class FolderListItem {
    	String title;
    	OlFolderEMail folder;
    	public FolderListItem(String title, OlFolderEMail folder) {
    		this.title=title;
    		this.folder=folder;
    	}
    	@Override
		public String toString() {
    		return title;
    	}
    }


	public FastFolderAssignementJPanel() {

		initGUI();
	}

	public void setEmailDataGroup(ToSortEmailGroup emailDataGroup) {

		if (emailDataGroup==null) {
			listModel.clear();
			folderList.removeListSelectionListener(this);
			if (currentEmailDataGroup.getDispatchDatabase()!=null)
				currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);
			currentEmailDataGroup=null;
			return;
		}

		if (currentEmailDataGroup !=null)
			if (currentEmailDataGroup.getDispatchDatabase()!=null)
				currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);

		folderList.removeListSelectionListener(this);
		folderList.clearSelection();

		listModel.clear();

		currentEmailDataGroup=emailDataGroup;

		if (emailDataGroup.mailCount()>0) listModel.add(0, new FolderListItem(NOFOLDERMESSAGE,null));

		for (WeightedDestFolder potFolder : currentEmailDataGroup.getPotentialFoldersList())
//			listModel.addElement(new FolderListItem(potFolder.folder.getName(),potFolder.folder));
			listModel.addElement(new FolderListItem(String.format("%s (%,.0f)", potFolder.folder.getName(),potFolder.weight),potFolder.folder));

		updateSelection();

		if (currentEmailDataGroup.getDispatchDatabase()!=null)
			currentEmailDataGroup.getDispatchDatabase().addUpdateListener(this);

	}

	private void updateSelection() {

		if (currentEmailDataGroup==null) return;

		String comonDestinationFolderId = currentEmailDataGroup.getComonDestFolderId();

		if (comonDestinationFolderId==null) {

			if (folderList.getSelectedIndex()<0) return;

			folderList.removeListSelectionListener(this);
			folderList.clearSelection();
			folderList.addListSelectionListener(this);
			return;
		}

		if (comonDestinationFolderId.equals(ToSortEmailGroup.NODESTINATIONFOLDERID)) {

			if (folderList.getSelectedIndex()==0) return;

			folderList.removeListSelectionListener(this);
			folderList.setSelectedIndex(0);
			folderList.addListSelectionListener(this);

			folderList.ensureIndexIsVisible(0);

			return;
		}

		OlFolderEMail comonFolder = (OlFolderEMail) OlConnector.getOlObject(comonDestinationFolderId);

		if (folderList.getSelectedIndex()>=0)
			if (comonFolder==folderList.getSelectedValue().folder)
				return;

		for (int index=0; index<listModel.size(); index++)
			if (comonFolder==listModel.get(index).folder) {

				folderList.removeListSelectionListener(this);
				folderList.setSelectedIndex(index);
				folderList.addListSelectionListener(this);

				folderList.ensureIndexIsVisible(index);

				return;
			}

		listModel.addElement(new FolderListItem(comonFolder.getName(),comonFolder));

		folderList.removeListSelectionListener(this);
		folderList.setSelectedIndex(listModel.getSize()-1);
		folderList.addListSelectionListener(this);

		folderList.ensureIndexIsVisible(listModel.getSize()-1);

	}

	private void initGUI() {

		setLayout(new BorderLayout(0, 0));
		folderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		add(folderList, BorderLayout.CENTER);

	}

	//
	// Selection listener
	//

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (currentEmailDataGroup==null) return;
		FolderListItem listItem = folderList.getSelectedValue();
		OlFolderEMail folder = listItem!=null?listItem.folder:null;

		currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);
		currentEmailDataGroup.setDestFolder(folder,true);
		currentEmailDataGroup.getDispatchDatabase().addUpdateListener(this);
	}

	//
	// Runnable tasks
	//


	private final Runnable runUpdateSelection = new Runnable() {
		@Override
		public void run() {
			updateSelection();
		}
	};

	private final Runnable runClearEmailDataGroup = new Runnable() {
		@Override
		public void run() {
			setEmailDataGroup(null);
		}
	};

	//
	// Database listeners
	//

	@Override
	public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData) {
		GUIUtilities.runEDT(runUpdateSelection);
	}

	@Override
	public void structureChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(runUpdateSelection);
	}

	@Override
	public void fullDatabaseChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(runClearEmailDataGroup);
	}

}
