package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;

public class EmailDataLink {
	
	private ToSortEmailSingle emailData;
	private String conversationID;
	
	public void setEmailData(ToSortEmailSingle emailData) {
		this.emailData = emailData;
		this.conversationID=(emailData!=null)?emailData.getEMail().getConversationID():null;
	}
	
	public ToSortEmailSingle getEmailData() {
		return emailData;
	}

	public String getConversationID(){
		return conversationID;
	}
	
}
