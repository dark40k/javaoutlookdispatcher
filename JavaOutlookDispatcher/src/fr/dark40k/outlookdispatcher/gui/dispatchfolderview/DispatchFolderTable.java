package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.INDIRECT_SELECTION_COLOR;
import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.INDIRECT_SELECTION_COLOR_WITH_POPUP;
import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.NODESTINATION_FOLDER_COLOR;
import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.POPUP_SELECTION_COLOR;
import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.UNREAD_COLOR;
import static fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderViewPanel.WITHDESTINATION_FOLDER_COLOR;

import java.awt.Component;
import java.awt.Font;
import java.time.LocalDateTime;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu.ColHeaderPopupMenu;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFlagStatusEnum;
import fr.dark40k.outlookdispatcher.util.EnhancedJTableHeader;

public class DispatchFolderTable extends JTable {

	final TableColumn[] columns;

	private final DispatchFolderTableRowFilter rowFilter;

	private final DispatchFolderViewPanel dispatchFolderViewPanel;

	public DispatchFolderTable(DispatchFolderViewPanel dispatchFolderViewPanel) {

		super(dispatchFolderViewPanel.model);

		this.dispatchFolderViewPanel=dispatchFolderViewPanel;

		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		columns=new TableColumn[columnModel.getColumnCount()];

		// Header des colones
		DispatchFolderTableEnhancedTableHeader enhancedTableHeader = new DispatchFolderTableEnhancedTableHeader(getColumnModel(), this);
		setTableHeader(enhancedTableHeader);

		// Tri des colonnes
		DispatchFolderTableRowSorter sorter = new DispatchFolderTableRowSorter(this);
		setRowSorter(sorter);

		// Filtrage des lignes
		rowFilter = new DispatchFolderTableRowFilter(this);
		sorter.setRowFilter(rowFilter);

		// affichage des colones de type date
		setDefaultRenderer(LocalDateTime.class, new LocalDateTimeCellRenderer());

		setDefaultRenderer(OlFlagStatusEnum.class, new OlFlagStatusEnumRenderer());
		setDefaultEditor(OlFlagStatusEnum.class, new OlFlagStatusEnumEditor());

		// initialisation de la largeur des colonnes
		for (int col=0; col<getColumnCount(); col++)
			getColumnModel().getColumn(col).setPreferredWidth(EmailTableColumns.getColumn(col).getPreferredWidth());

		// correction comportement anormaux (peut etre plus utile)
		putClientProperty("terminateEditOnFocusLost", true);

		// intialisation affichage
		setFont(new Font(getFont().getFontName(), getFont().getStyle(), 12));
		setRowHeight(18);

		ColHeaderPopupMenu.registerListener(this);
	}

	public DispatchFolderTableRowFilter getRowFilter() {
		return rowFilter;
	}

	public void AutoResizeAllColumns() {
		((EnhancedJTableHeader)getTableHeader()).doAutoResizeAll();
	}

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {

		Component c = null;

		try {
			c = super.prepareRenderer(renderer, row, column);
		} catch (Exception e) {
			return null;
		}

		ToSortEmailSingle emaildata;
		try {
			emaildata = ((DispatchMailTableModel) getModel()).getEMailData(convertRowIndexToModel(row));
		} catch (Exception e) {
			return c;
		}

		if (emaildata.getEMail().isUnRead())
			c.setFont(  c.getFont().deriveFont(Font.BOLD) );

		if (!isRowSelected(row)) {

			boolean seperatepopup = (dispatchFolderViewPanel.popupEmailDataGroup!=null)&&(dispatchFolderViewPanel.popupEmailDataGroup!=dispatchFolderViewPanel.selectedEmailDataGroup);
			boolean inpopup = (dispatchFolderViewPanel.popupEmailDataGroup != null) && (dispatchFolderViewPanel.popupEmailDataGroup.contains(emaildata));
			boolean inselection = (dispatchFolderViewPanel.selectedEmailDataGroup != null) && (dispatchFolderViewPanel.selectedEmailDataGroup.contains(emaildata));

			if (inpopup && seperatepopup)
				c.setBackground(POPUP_SELECTION_COLOR);
			else if (inselection)
				c.setBackground(seperatepopup?INDIRECT_SELECTION_COLOR_WITH_POPUP:INDIRECT_SELECTION_COLOR);
			else if (emaildata.getEMail().isUnRead())
				c.setBackground(UNREAD_COLOR);
			else if ((emaildata.isManualDest()) && (emaildata.getDestFolder() != null))
				c.setBackground(WITHDESTINATION_FOLDER_COLOR);
			else
				c.setBackground(NODESTINATION_FOLDER_COLOR);
		}

		return c;
	}

	//  Determine editor to be used by column
	@Override
	public TableCellEditor getCellEditor(int row, int column)
	{
		TableCellEditor editor = EmailTableColumns.getColumn(column).getColumnEditor();
		if (editor != null) return editor;

		return super.getCellEditor(row, column);
	}

	public void setColumnVisibility(int modelIndex, boolean state) {

		int viewIndex = convertColumnIndexToView(modelIndex);

		// verifie si viewindex>=0 (visible) et state sont bien différents
		if ((viewIndex >= 0) == state) return;

		if (viewIndex >= 0) {
			columns[modelIndex]=columnModel.getColumn(viewIndex);
			columnModel.removeColumn(columnModel.getColumn(viewIndex));
		}
		else
			columnModel.addColumn(columns[modelIndex]);

	}

}
