package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.util.LinkedHashMap;

import javax.swing.RowFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu.RowColFilter;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu.RowColFilterString;
import fr.dark40k.util.persistantparameters.ParamGroup;

public class DispatchFolderTableRowFilter extends RowFilter<DispatchMailTableModel, Object> {

	private final static Logger LOGGER = LogManager.getLogger();

	private final DispatchFolderTable table;

	private final LinkedHashMap<Integer, RowColFilter> rowColFilterList;

	public DispatchFolderTableRowFilter(DispatchFolderTable table) {
		this.table = table;
		rowColFilterList = new LinkedHashMap<Integer, RowColFilter>();
	}

	@Override
	public boolean include(Entry<? extends DispatchMailTableModel, ? extends Object> entry) {

		for (RowColFilter filter : rowColFilterList.values())
			if (!filter.include(entry))
				return false;

		return true;

	}

	public void setFilterAction(int popupCol) {

		LOGGER.info("Activation filtre.");
		LOGGER.debug("Colonne = "+EmailTableColumns.getColumn(popupCol).getHeaderDisplayName()+", Classe = "+EmailTableColumns.getColumn(popupCol).getColumnClass().getName());


		RowColFilter newFilter=null;

		switch (EmailTableColumns.getColumn(popupCol).getColumnClass().getName()) {
			case "javax.swing.Icon" :
				LOGGER.info("Classe non g�r�e, annulation.");
				return;

			default :
				LOGGER.info("Utilisation filtre par d�faut");
				newFilter = RowColFilterString.newFilterDialog(table.getTopLevelAncestor(),popupCol,rowColFilterList.get(popupCol));
		}

		if (newFilter==null) {
			LOGGER.info("Aucun filtre cr��, action annul�e.");
			return;
		}

		LOGGER.info("Application filtre");

		rowColFilterList.put(popupCol,newFilter);

		((DispatchFolderTableEnhancedTableHeader)table.getTableHeader()).updateHeaderInformation(popupCol," ("+newFilter+")","Filtre = \""+newFilter+"\"");

		((DispatchMailTableModel) table.getModel()).fireTableDataChanged();

	}

	public void clearFilterAction(int popupCol) {

		LOGGER.info("Annulation filtre.");

		rowColFilterList.remove(popupCol);

		((DispatchFolderTableEnhancedTableHeader) table.getTableHeader()).updateHeaderInformation(popupCol, null, null);

		((DispatchMailTableModel) table.getModel()).fireTableDataChanged();

	}

	public void restoreConfiguration(ParamGroup subParamGroup) {
		// TODO Auto-generated method stub

	}

	public void saveConfiguration(ParamGroup subParamGroup) {
		// TODO Auto-generated method stub

	}

}
