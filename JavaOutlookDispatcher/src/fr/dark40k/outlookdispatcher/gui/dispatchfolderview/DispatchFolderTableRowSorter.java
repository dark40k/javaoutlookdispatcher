package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SortOrder;
import javax.swing.table.TableRowSorter;

import fr.dark40k.util.persistantparameters.ParamGroup;

public class DispatchFolderTableRowSorter extends TableRowSorter<DispatchMailTableModel> {

	public DispatchFolderTableRowSorter(DispatchFolderTable table) {
		super((DispatchMailTableModel) table.getModel());
	}
	
	@Override
	public void toggleSortOrder(int column) {
		
		List<? extends SortKey> sortKeys = getSortKeys();
		
		SortOrder sortOrder = SortOrder.ASCENDING;
		if (sortKeys.size() > 0)
			if (sortKeys.get(0).getColumn()==column)
				if (sortKeys.get(0).getSortOrder()==SortOrder.ASCENDING)
					sortOrder=SortOrder.DESCENDING;
				else if (sortKeys.get(0).getSortOrder()==SortOrder.DESCENDING)
					sortOrder=SortOrder.UNSORTED;
		
		setSortOrder(column, sortOrder);
		
	}

	public void setSortOrder(int column, SortOrder sortOrder) {
		
		List<SortKey> newKeys = new ArrayList<SortKey>();
		
		newKeys.add(new SortKey(column, sortOrder));
		newKeys.add(new SortKey(9, SortOrder.DESCENDING)); // tri par conversation
		newKeys.add(new SortKey(5, SortOrder.DESCENDING)); // tri par date
		
		setSortKeys(newKeys);
		sort();
		
	}
	
	public int getSortColumn() {
		List<? extends SortKey> sortKeys = getSortKeys();
		if (sortKeys.size() <= 0) return -1;
		return sortKeys.get(0).getColumn();		
	}
	
	public SortOrder getSortOrder() {
		List<? extends SortKey> sortKeys = getSortKeys();
		if (sortKeys.size() <= 0) return null;
		return sortKeys.get(0).getSortOrder();		
	}
	
	public void saveConfiguration(ParamGroup paramGroup) {
		
		if (paramGroup==null) return;
		
		List<? extends SortKey> sortKeys = getSortKeys();
		
		if (sortKeys.size() <= 0) return;
		
		paramGroup.getSubParamInteger("sortedColumn",-1).setValue(getSortColumn());
		paramGroup.getSubParamInteger("sortedOrder",SortOrder.UNSORTED.ordinal()).setValue(getSortOrder().ordinal());
		
	}
	
	public void restoreConfiguration(ParamGroup paramGroup) {
		
		if (paramGroup==null) return;
		
		int column=paramGroup.getSubParamInteger("sortedColumn",-1).getValue();
		SortOrder sortOrder= SortOrder.values()[paramGroup.getSubParamInteger("sortedOrder",SortOrder.UNSORTED.ordinal()).getValue()];
		if (column>=0) setSortOrder(column, sortOrder);
		
	}
	
	
	
}
