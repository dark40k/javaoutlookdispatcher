package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;

import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchFolderTable;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.EmailTableColumns;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.EmailTableColumns.EmailTableColumn;

public class ColHeaderPopupMenu extends JPopupMenu {

	final DispatchFolderTable table;

	final JCheckBoxMenuItem[] chkBoxMenuItems;

	private int popupCol;

	public ColHeaderPopupMenu(DispatchFolderTable nodeTable) {

		table=nodeTable;

		JMenu dispMenu = new JMenu("Affichage colonnes");
		this.add(dispMenu);

		chkBoxMenuItems = new JCheckBoxMenuItem[EmailTableColumns.getColumnCount()];

		int index = 0;
		for (EmailTableColumn column : EmailTableColumns.getIterable()){

			JCheckBoxMenuItem anItem = new JCheckBoxMenuItem();

			anItem.setText(column.getHeaderDisplayName());

			// if (column.getHeaderIcon()!=null) anItem.setIcon(column.getHeaderIcon());

			final int columnIndex = index;
			anItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
						table.setColumnVisibility(columnIndex,chkBoxMenuItems[columnIndex].getState());
				}
			});

			dispMenu.add(anItem);
			chkBoxMenuItems[index++] = anItem;
		}

		this.add(new JSeparator());

		JMenuItem setFilterItem = new JMenuItem("Definir filtre");
		setFilterItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.getRowFilter().setFilterAction(popupCol);
			}
		});
		this.add(setFilterItem);

		JMenuItem clearFilterItem = new JMenuItem("Effacer filtre");
		clearFilterItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.getRowFilter().clearFilterAction(popupCol);
			}
		});
		this.add(clearFilterItem);



	}

	private void updateState() {
		for (int modelColumnIndex = 0; modelColumnIndex < table.getModel().getColumnCount(); modelColumnIndex++) {
			chkBoxMenuItems[modelColumnIndex].setState(table.convertColumnIndexToView(modelColumnIndex) >= 0);
		}
	}


	public static void registerListener(DispatchFolderTable nodeTable) {

		final ColHeaderPopupMenu popupMenu = new ColHeaderPopupMenu(nodeTable);

		final JTableHeader header = nodeTable.getTableHeader();

		header.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {

				if (SwingUtilities.isRightMouseButton(me)) {

					int dispCol = popupMenu.table.columnAtPoint(me.getPoint());
					popupMenu.popupCol = popupMenu.table.convertColumnIndexToModel(dispCol);

					popupMenu.updateState();
					popupMenu.show(header, me.getX(), me.getY());

				}
			}
		});
	}

}
