package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu;

import javax.swing.RowFilter.Entry;

import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchMailTableModel;

public abstract class RowColFilter {

	public abstract boolean include(Entry<? extends DispatchMailTableModel, ? extends Object> entry);

	@Override
	public abstract String toString();



}
