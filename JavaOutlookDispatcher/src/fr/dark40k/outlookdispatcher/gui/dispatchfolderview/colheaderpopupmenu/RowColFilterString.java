package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.colheaderpopupmenu;

import java.awt.Component;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.RowFilter.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.DispatchMailTableModel;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.EmailTableColumns;

public class RowColFilterString extends RowColFilter {

	private final static Logger LOGGER = LogManager.getLogger();

	private String filterString;

	private int col;

	private Pattern p;

	@Override
	public boolean include(Entry<? extends DispatchMailTableModel, ? extends Object> entry) {
		return p.matcher(entry.getStringValue(col)).find();
	}


	public RowColFilterString(int col, String filterString) {

		this.filterString=filterString;
		this.col=col;

		 p = Pattern.compile(filterString);

	}


	@Override
	public String toString() {
		return filterString;
	}


	public static RowColFilter newFilterDialog(Component parentComponent, int popupCol, RowColFilter oldFilter) {

		String newFilter = (String) JOptionPane.showInputDialog(parentComponent, "D�finir le filtre :\n" + "(?i)(chaine) => cherche chaine sans tenir compte de la casse.",
				"Definir filtre, colonne = " + EmailTableColumns.getColumn(popupCol).getHeaderDisplayName(), JOptionPane.PLAIN_MESSAGE, null, null,
				((oldFilter != null) && (oldFilter instanceof RowColFilterString)) ? ((RowColFilterString)oldFilter).filterString : "");

		if ((newFilter == null) || (newFilter.length() == 0)) {
			LOGGER.info("Action annull�e.");
			return null;
		}

		LOGGER.info("Filtre = " + newFilter);

		return new RowColFilterString(popupCol, newFilter);

	}

}
