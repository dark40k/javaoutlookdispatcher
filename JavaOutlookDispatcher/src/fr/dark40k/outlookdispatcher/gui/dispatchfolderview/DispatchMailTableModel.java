package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateEvent;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.GUIUtilities;


public class DispatchMailTableModel  extends AbstractTableModel implements DispatchDatabase.UpdateListener {

	private final static Logger LOGGER = LogManager.getLogger();

	DispatchDatabase database ;
	
	public void setDatabase(DispatchDatabase newDatabase) {
		if (database!=null) database.removeUpdateListener(this);
		database=newDatabase;
		if (newDatabase!=null) database.addUpdateListener(this);
		fireTableDataChanged();
	}
	
	public ToSortEmailSingle getEMailData(int row) {
		return database.getEMailData(row);
	}	

	@Override
	public int getColumnCount() {
		return EmailTableColumns.getColumnCount();
	}

	@Override
	public int getRowCount() {
		if (database==null) return 0;
		return database.getSize();
	}

	@Override
	public Object getValueAt(int row, int col) {
		return EmailTableColumns.getColumn(col).getColumnValue(database.getEMailData(row));
	}
	
	public String getToolTipText(int row, int col) {
		return EmailTableColumns.getColumn(col).getToolTipText(database.getEMailData(row));
	}
	
	@Override
    public String getColumnName(int col) {
        return EmailTableColumns.getColumn(col).getHeaderDisplayName();
    }
    
	@Override
    public Class<?> getColumnClass(int col) {
		return EmailTableColumns.getColumn(col).getColumnClass();
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return EmailTableColumns.getColumn(col).isEditable(database.getEMailData(row));
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		EmailTableColumns.getColumn(col).setValue(database.getEMailData(row), value);
		fireTableRowsUpdated(row, row);
	}

	public void setFolderAtRow(OlFolderEMail newFolder, int row) {
		database.getEMailData(row).setDestFolder(newFolder,true);
		fireTableRowsUpdated(row, row);
	}

	//
	// Runnable tasks
	//
	
	private final Runnable runFireTableDataChanged = new Runnable() {
		@Override
		public void run() {
			Long runDate = System.currentTimeMillis();
			LOGGER.debug("Firechanged - "+runDate);
			fireTableDataChanged();
			LOGGER.debug("UpdateDone - "+runDate);
		}
	};
	 	 
	//
	// Database changes
	//
	
	@Override
	public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData) {
		if (evt.getDatabaseUpdateStatus()) return;
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				int row = database.getIndex(emailData);
				fireTableRowsUpdated(row, row);
			}
		});
	}

	@Override
	public void structureChanged(UpdateEvent evt) {
		if (evt.getDatabaseUpdateStatus()) return;
		GUIUtilities.runEDT(runFireTableDataChanged);
	}

	@Override
	public void fullDatabaseChanged(UpdateEvent evt) {
		if (evt.getDatabaseUpdateStatus()) return;
		GUIUtilities.runEDT(runFireTableDataChanged);
	}

}
