package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableColumnModel;

import fr.dark40k.outlookdispatcher.Dispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateEvent;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.preview.EMailPreviewPanel;
import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.selfolderpopupmenu.FastFolderPopupMenu;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.GUIUtilities;
import fr.dark40k.outlookdispatcher.util.ScreenSize;
import fr.dark40k.util.persistantparameters.ParamGroup;

/**
 * @author Francois
 *
 */
public class DispatchFolderViewPanel extends JPanel implements DispatchDatabase.UpdateListener {

	public static final Color POPUP_SELECTION_COLOR = new Color(0,255,255); // light blue

	public static final Color DIRECT_SELECTION_COLOR = new Color(51,153,255); // dark blue
	public static final Color DIRECT_SELECTION_COLOR_WITH_POPUP = new Color(160,160,160); // dark grey

	public static final Color INDIRECT_SELECTION_COLOR = new Color(135,206,250); // light blue
	public static final Color INDIRECT_SELECTION_COLOR_WITH_POPUP = new Color(206,206,206); // light grey

	public static final Color UNREAD_COLOR = new Color(255, 240, 90);

	public static final Color NODESTINATION_FOLDER_COLOR = Color.white; // new Color(255,231,186);
	public static final Color WITHDESTINATION_FOLDER_COLOR = new Color(221, 255, 221); // new Color(255,231,186);

	private final ParamGroup dispatchFolderViewPanelParams;
	private String dispatchFolderViewSizedPanelParamsName;
	private ParamGroup dispatchFolderViewSizedPanelParams;

	protected final EMailPreviewPanel emailPreviewPanel = new EMailPreviewPanel();

	protected boolean selectConversations;

	protected final DispatchFolderTable table;
	protected final DispatchMailTableModel model;
	private JSplitPane splitPane;
	private JPanel panel;
	private JScrollPane scrollPane;

	private TitledBorder listScannedFolderTitle;
	private TitledBorder listSelectionAndContentCountTitle;

	private DispatchDatabase database;
	protected ToSortEmailGroup selectedEmailDataGroup;
	protected ToSortEmailGroup popupEmailDataGroup;

	private Function<OlFolderEMail, OlFolderEMail> createNewOlFolder;

	public DispatchFolderViewPanel(Dispatcher dispatcher, Function<OlFolderEMail, OlFolderEMail> createNewOlFolder, ParamGroup dispatchFolderViewPanelParams) {

		this.dispatchFolderViewPanelParams = dispatchFolderViewPanelParams;
		dispatchFolderViewSizedPanelParamsName=calculateCurrentDisplayParamsName();
		dispatchFolderViewSizedPanelParams=dispatchFolderViewPanelParams.getSubParamGroup(dispatchFolderViewSizedPanelParamsName);

		this.createNewOlFolder=createNewOlFolder;

		model = new DispatchMailTableModel();
		table = new DispatchFolderTable(this);

		initGUI();

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent listSelectionEvent) {
				if (listSelectionEvent.getValueIsAdjusting()) return;
		        updateSelectedRowsDisplay();
		        updatePreview();
			}
		});

		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent evt) {
				if (evt.getButton()==MouseEvent.BUTTON2)
					dispatcher.doMoveSelection();
				else if (evt.isPopupTrigger())
					updateSelectedRowAndShowPopupMenu(evt);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					ToSortEmailSingle mailData = model.getEMailData(table.convertRowIndexToModel(table.getSelectedRow()));
					mailData.getEMail().showMail();
				}
			}

		});

		table.setSelectionBackground(DIRECT_SELECTION_COLOR);
 	}

	public void setDatabase(DispatchDatabase newDatabase) {

		table.clearSelection();

		if (database!=null) database.removeUpdateListener(this);

		database=newDatabase;
		database.addUpdateListener(this);

		model.setDatabase(database);

		fullRefreshPanel();
	}

	public void fullRefreshPanel() {

		table.clearSelection();
		updateSelectedRowsDisplay();
		updatePreview();

		StringBuilder title = new StringBuilder();
		for(OlFolderEMail folder : database.getSearchedFolders() )
			title.append(folder.getName()+", ");

		if (title.length()>0) title.delete(title.length()-2, title.length());

		listScannedFolderTitle.setTitle("Emails dans : "+title.toString());

	}

	public void setConversationSelectionMode(boolean selected) {
		selectConversations=selected;
		updateSelectedRowsDisplay();
	}

	public ToSortEmailGroup getSelectedEmailDataGroup() {
		return selectedEmailDataGroup;
	}

	public Collection<ToSortEmailSingle> getEmailSelection() {

		HashSet<ToSortEmailSingle> emailDataListToMove = new HashSet<ToSortEmailSingle>();

		if (table.getSelectedRow()<0) return emailDataListToMove;

		for (int row : table.getSelectedRows())
			emailDataListToMove.add(model.getEMailData(table.convertRowIndexToModel(row)));

		return emailDataListToMove;
	}


	/**
	 * Renvoie la liste des emails selectionnes ettendue aux conversations si le mode conversation est actif. Autrement renvoie la selection simple.
	 *
	 * @return
	 */
	public Collection<ToSortEmailSingle> getExtendedEmailSelection() {

		if (!selectConversations) return getEmailSelection();

		HashSet<ToSortEmailSingle> emailDataListToMove = new HashSet<ToSortEmailSingle>();

		if (table.getSelectedRow()<0) return emailDataListToMove;

		for (int row = 0; row < table.getRowCount(); row++) {
			ToSortEmailSingle emailData = model.getEMailData(table.convertRowIndexToModel(row));
			if (selectedEmailDataGroup.contains(emailData))
				emailDataListToMove.add(emailData);
		}

		return emailDataListToMove;

	}



	//
	// update Selection
	//

	private void updateSelectedRowAndShowPopupMenu(MouseEvent e)  {

		int popupRow = table.rowAtPoint(e.getPoint());
		if (popupRow < 0) return;

		ToSortEmailSingle popupEmailData = model.getEMailData(table.convertRowIndexToModel(popupRow));

		if (selectConversations) {

			String popupConversationID = popupEmailData.getEMail().getConversationID();

			if (selectedEmailDataGroup.contains(popupEmailData)) {

				popupEmailDataGroup = getSelectedEmailDataGroup();

			} else {

				HashSet<ToSortEmailSingle> popupSet = new HashSet<ToSortEmailSingle>();
				for (int row = 0; row < table.getRowCount(); row++) {
					ToSortEmailSingle tmpEmailData = model.getEMailData(table.convertRowIndexToModel(row));
					if (popupConversationID.equals(tmpEmailData.getEMail().getConversationID()))
						popupSet.add(tmpEmailData);
				}
				popupEmailDataGroup = new ToSortEmailGroup(database, popupSet);

				table.setSelectionBackground(DIRECT_SELECTION_COLOR_WITH_POPUP);

			}

		} else {
			HashSet<ToSortEmailSingle> popupSet = new HashSet<ToSortEmailSingle>();
			popupSet.add(popupEmailData);
			popupEmailDataGroup = new ToSortEmailGroup(database, popupSet);
		}

		table.repaint();

		FastFolderPopupMenu popupMenu = new FastFolderPopupMenu(popupEmailDataGroup, createNewOlFolder);

		popupMenu.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuCanceled(PopupMenuEvent popupMenuEvent) {
				popupEmailDataGroup = null;
				table.setSelectionBackground(DIRECT_SELECTION_COLOR);
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent popupMenuEvent) {
				popupEmailDataGroup = null;
				table.setSelectionBackground(DIRECT_SELECTION_COLOR);
			}

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent popupMenuEvent) {
			}

		});

		popupMenu.show(e.getComponent(), e.getX(), e.getY());

	}

	private void updateSelectedRowsDisplay() {

		if (table.getSelectedRow() < 0) {

			selectedEmailDataGroup = new ToSortEmailGroup(database);

		} else {

			HashSet<ToSortEmailSingle> set = new HashSet<ToSortEmailSingle>();

			if (selectConversations) {

				LinkedHashSet<String> selectedDiscussions = new LinkedHashSet<String>();

				for (int row : table.getSelectedRows())
					selectedDiscussions.add(model.getEMailData(table.convertRowIndexToModel(row)).getEMail().getConversationID());

				for (int row = 0; row < table.getRowCount(); row++) {
					ToSortEmailSingle emailData = model.getEMailData(table.convertRowIndexToModel(row));
					if (selectedDiscussions.contains(emailData.getEMail().getConversationID()))
						set.add(emailData);
				}

			} else
				for (int row : table.getSelectedRows())
					set.add(model.getEMailData(table.convertRowIndexToModel(row)));

			selectedEmailDataGroup = new ToSortEmailGroup(database, set);
		}

		listSelectionAndContentCountTitle.setTitle(selectedEmailDataGroup.mailCount()+"/"+table.getRowCount());

		repaint();

        fireSelectionChangeEvent();
	}

	//
	// Update preview
	//

	public void updatePreview() {
		GUIUtilities.runEDT(runUpdateEMailDisplay);
	}

	private final Runnable runUpdateEMailDisplay = new Runnable() {
		@Override
		public void run() {
			if (table.getSelectedRow()<0)
				emailPreviewPanel.setEmail(null);
			else
				emailPreviewPanel.setEmail(model.getEMailData(table.convertRowIndexToModel(table.getSelectedRow())).getEMail());
		}
	};

	//
	// Database update changes follower
	//
	@Override
	public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData) {}

	@Override
	public void structureChanged(UpdateEvent evt) {}

	@Override
	public void fullDatabaseChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(runFullRefreshPanel);
	}

	private final Runnable runFullRefreshPanel = new Runnable() {
		@Override
		public void run() {
			fullRefreshPanel();
		}
	};

	//
	// Selection change event provider
	//

	private EventListenerList selectionListenerList = new EventListenerList();

	public class SelectionChangeEvent extends EventObject {
		public SelectionChangeEvent(Object source) {
			super(source);
		}
	}

	public interface SelectionChangeListener extends EventListener {
		public void selectionChanged(SelectionChangeEvent evt);
	}

	public void addSelectionChangeListener(SelectionChangeListener listener) {
		selectionListenerList.add(SelectionChangeListener.class, listener);
	}

	public void removeSelectionChangeListener(SelectionChangeListener listener) {
		selectionListenerList.remove(SelectionChangeListener.class, listener);
	}

	public void fireSelectionChangeEvent() {
		SelectionChangeEvent evt=new SelectionChangeEvent(this);
		Object[] listeners = selectionListenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SelectionChangeListener.class) {
				((SelectionChangeListener) listeners[i+1]).selectionChanged(evt);
			}
		}
	}

	//
	// Sauvegarde et restauration de l'affichage
	//

	public void restoreWindowConfiguration() {

		//
		// Chargement des param�tres ind�pendants de la taille d'ecran
		//

		ParamGroup tableParams = dispatchFolderViewPanelParams.getSubParamGroup("table");
		ParamGroup allColumnsParams = tableParams.getSubParamGroup("allColumns");

		int count = allColumnsParams.getSubParamInteger("count", -1).getValue();
		if (count<0) return;

		TableColumnModel columnModel = table.getColumnModel();
		table.createDefaultColumnsFromModel();

		// set column visibility
		for (String columnParamsName : allColumnsParams.listSubParamsNames(true) ) {

			if (!columnParamsName.startsWith("column")) continue;

			ParamGroup columnParams = allColumnsParams.getSubParamGroup(columnParamsName);
			table.setColumnVisibility(
					columnParams.getSubParamInteger("modelindex",null).getValue(),
					(columnParams.getSubParamInteger("index",-1).getValue())>=0
					);
		}

		// set column order & width
		for (String columnParamsName : allColumnsParams.listSubParamsNames(true) ) {

			if (!columnParamsName.startsWith("column")) continue;

			ParamGroup columnParams = allColumnsParams.getSubParamGroup(columnParamsName);

			int modelColumnIndex=columnParams.getSubParamInteger("modelindex",-1).getValue();
			int colIndex=columnParams.getSubParamInteger("index",-1).getValue();

			if (colIndex>=0) {
				table.setColumnVisibility(modelColumnIndex, true);
				int curColIndex = table.convertColumnIndexToView(modelColumnIndex);
				columnModel.moveColumn(curColIndex, colIndex);
				columnModel.getColumn(colIndex).setPreferredWidth(columnParams.getSubParamInteger("width",-1).getValue());
			}
		}

		// restauration du tri des colonnes
		((DispatchFolderTableRowSorter) table.getRowSorter()).restoreConfiguration(tableParams.getSubParamGroup("columnSorter"));
		table.getRowFilter().restoreConfiguration(tableParams.getSubParamGroup("rowFilter"));

		//
		// Chargement des param�tres li�s � la taille d'ecran
		//

		// verifie que le jeu de param�tres charg� correspond bien � la taille de l'ecran
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        if (dispatchFolderViewSizedPanelParams.getSubParamInteger("screen_height", -1).getValue()!=virtualBounds.height) return;
        if (dispatchFolderViewSizedPanelParams.getSubParamInteger("screen_width", -1).getValue()!=virtualBounds.width) return;

		// restauration des dimensions graphiques
		applyIfNotNull(dispatchFolderViewSizedPanelParams.getSubParamInteger("split1",null).getValue(), (Integer v) -> {splitPane.setDividerLocation(v);});

	}


	public void saveWindowConfiguration() {

		//
		// Sauvegarde des param�tres ind�pendants de la taille d'ecran
		//

		ParamGroup tableParams = dispatchFolderViewPanelParams.getSubParamGroup("table");
		ParamGroup allColumnsParams = tableParams.getSubParamGroup("allColumns");

		allColumnsParams.getSubParamInteger("count", -1).setValue(table.getColumnCount());

		TableColumnModel columnModel = table.getColumnModel();

		for (int modelColumnIndex = 0; modelColumnIndex < table.getModel().getColumnCount(); modelColumnIndex++) {

			int colIndex = table.convertColumnIndexToView(modelColumnIndex);

			ParamGroup columnParams = allColumnsParams.getSubParamGroup("column" + modelColumnIndex);

			columnParams.getSubParamInteger("modelindex", null).setValue(modelColumnIndex);
			columnParams.getSubParamInteger("index", null).setValue(colIndex);
			if (colIndex >= 0)
				columnParams.getSubParamInteger("width", null).setValue(columnModel.getColumn(colIndex).getWidth());

		}

		// backup du tri des colonnes
		((DispatchFolderTableRowSorter) table.getRowSorter()).saveConfiguration(tableParams.getSubParamGroup("columnSorter"));
		table.getRowFilter().saveConfiguration(tableParams.getSubParamGroup("rowFilter"));

		//
		// Sauvegarde des param�tres li�s � la taille d'ecran
		//

		// verifie que le jeu de param�tres utilis� correspond bien � la taille de l'ecran
		if (!dispatchFolderViewSizedPanelParamsName.equals(calculateCurrentDisplayParamsName())) return;

        // taille de l'ecran
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        dispatchFolderViewSizedPanelParams.getSubParamInteger("screen_height",null).setValue(virtualBounds.height);
        dispatchFolderViewSizedPanelParams.getSubParamInteger("screen_width",null).setValue(virtualBounds.width);

        // position du separateur
		dispatchFolderViewSizedPanelParams.getSubParamInteger("split1", null).setValue(splitPane.getDividerLocation());

	}

	private static String calculateCurrentDisplayParamsName() {
        Rectangle virtualBounds=ScreenSize.getScreenBonds();
        return "display"+virtualBounds.width+"x"+virtualBounds.height;
	}

	private static void applyIfNotNull(Integer value, Consumer<Integer> command) {
		if (value!=null) command.accept(value);
	}

	//
	// GUI
	//

	private void initGUI() {

		listScannedFolderTitle = new TitledBorder(null, "Emails dans : ", TitledBorder.LEADING, TitledBorder.TOP);
		listSelectionAndContentCountTitle = new TitledBorder(listScannedFolderTitle, "0/0", TitledBorder.TRAILING, TitledBorder.TOP);
		setBorder(listSelectionAndContentCountTitle);

		setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setResizeWeight(0.8);
		add(splitPane, BorderLayout.CENTER);

		panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		panel.add(scrollPane);

		scrollPane.setViewportView(table);

		splitPane.setRightComponent(emailPreviewPanel);

	}


}
