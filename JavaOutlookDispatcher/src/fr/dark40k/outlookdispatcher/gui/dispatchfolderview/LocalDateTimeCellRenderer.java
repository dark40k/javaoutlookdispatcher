package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.awt.Component;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class LocalDateTimeCellRenderer extends DefaultTableCellRenderer {

	public final static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm");

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {

		JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, rowIndex, columnIndex);

		if (value == null) {
			label.setText("");
			return label;
		}

		label.setText(((LocalDateTime) value).format(dtf));

		return label;
	}

}
