package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.table.TableCellEditor;

import fr.dark40k.outlookdispatcher.Dispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEMailSubject;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFlagStatusEnum;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class EmailTableColumns {

	private final static DateTimeFormatter LATESTDATEFORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");

	private final static Icon lockedSpecificIcon = new ImageIcon(EmailTableColumns.class.getResource("res/lock-go.png"));
	private final static Icon lockedIcon = new ImageIcon(EmailTableColumns.class.getResource("res/lock-5.png"));
	private final static Icon automaticIcon = null;

	private final static Icon readIcon = null; //new ImageIcon(EmailTableColumns.class.getResource("res/mail-mark-read-2.png"));
	private final static Icon unreadIcon = new ImageIcon(EmailTableColumns.class.getResource("res/mail_yellow.png"));


    private static ArrayList<EmailTableColumn> columns = new ArrayList<EmailTableColumn>();

    public static int getColumnCount() {
    	return columns.size();
    }

    public static EmailTableColumn getColumn(int index) {
    	return columns.get(index);
    }

    public static Iterable<EmailTableColumn> getIterable() {
    	return columns;
    }

	static {

		columns.add(new EmailTableColumn("M.", 20, "Verrouillage manuel") {
			@Override
			public Class<?> getColumnClass() {
				return Icon.class;
			}

			@Override
			public Icon getColumnValue(ToSortEmailSingle emailData) {

				Boolean manual = emailData.isManualDest();

				if (manual==null) throw new RuntimeException("Unexpected value : manual is null.");

				if (!manual) return automaticIcon;

				OlFolderEMail firstPotentialFolder = (emailData.getPotentialFoldersList().getCount()>0) ? emailData.getPotentialFoldersList().getFolder(0) : null;

				return (emailData.getDestFolder()==firstPotentialFolder) ? lockedIcon : lockedSpecificIcon;
			}
		});

		columns.add(new EmailTableColumn("L.", 20, "Icone du mail") {
			@Override
			public Class<?> getColumnClass() {
				return Icon.class;
			}

			@Override
			public Icon getColumnValue(ToSortEmailSingle emailData) {
				return (emailData.getEMail().isUnRead()) ? unreadIcon : readIcon;
			}
		});

		columns.add(new EmailTableColumn("T.", 20, "Etat de l'indicateur") {
			@Override
			public Class<?> getColumnClass() {
				return OlFlagStatusEnum.class;
			}

			@Override
			public OlFlagStatusEnum getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().status.getFlag();
			}

			@Override
			public boolean isEditable(ToSortEmailSingle emailData) {
				String from=emailData.getEMail().getFrom();
				if (from==null) return false;
				return from.length()>0;
			}

			@Override
			public void setValue(ToSortEmailSingle emailData, Object value) {
				Dispatcher.doSetStatusOfEMail(emailData.getEMail(), (OlFlagStatusEnum) value);
			}

		});

		columns.add(new EmailTableColumn("Objet", 400, "Objet du mail") {
			@Override
			public Class<?> getColumnClass() {
				return OlEMailSubject.class;
			}

			@Override
			public OlEMailSubject getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().getSubject();
			}

		});

		columns.add(new EmailTableColumn("De", 150, "Auteur de l'email") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().getFrom();
			}
		});

		columns.add(new EmailTableColumn("Envoy� le", 100, "Date d'envoi de l'email") {
			@Override
			public Class<?> getColumnClass() {
				return LocalDateTime.class;
			}

			@Override
			public LocalDateTime getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().getSentOn();
			}
		});

		columns.add(new EmailTableColumn("Emplacement", 50, "R�pertoire courant") {

			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().getParentName();
			}
		});

		columns.add(new EmailTableColumn("Destination", 200, "R�pertoire ou sera stock� le mail") {

			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {

				OlFolder destFolder = emailData.getDestFolder();

				if (destFolder == null)
					return "<Pas de repertoire selectionn�>";

				return emailData.getDestFolder().getName();

			}
		});

		columns.add(new EmailTableColumn("Cat�gories", 100, "Cat�gories du mail") {

			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {
				return emailData.getEMail().getCategories();
			}
		});

		columns.add(new EmailTableColumn("Conversation", 100, "Reference de conversation du mail") {

			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {
				String convId=emailData.getEMail().getConversationID();
				LocalDateTime latestdate = emailData.getDispatchDatabase().getConversationNewestTime(convId);
				if (latestdate==null) return convId;
				return latestdate.format(LATESTDATEFORMATTER)+" > "+convId;
			}
		});

		columns.add(new EmailTableColumn("Score", 25, "Score de la destination selectionn�e") {

			@Override
			public Class<?> getColumnClass() {
				return Double.class;
			}

			@Override
			public Double getColumnValue(ToSortEmailSingle emailData) {

				OlFolder destFolder = emailData.getDestFolder();

				if (destFolder == null)
					return -1.0;

				return Math.floor(emailData.getPotentialFoldersList().getScore( (OlFolderEMail) destFolder));
			}
		});

		columns.add(new EmailTableColumn("Destination compl�te", 200, "R�pertoire ou sera stock� le mail") {

			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(ToSortEmailSingle emailData) {

				OlFolder destFolder = emailData.getDestFolder();

				if (destFolder == null)
					return "<Pas de repertoire selectionn�>";

				return emailData.getDestFolder().getPath("/").toString();

			}
		});

	}

	public static abstract class EmailTableColumn {

		//
		// Internal enum methods
		//
		private String headerName;
		private int width;
		private String headerDescription;

		private EmailTableColumn(String displayName, int width, String description) {
			this.headerName = displayName;
			this.width=width;
			this.headerDescription=description;
		}

		//
		// Parametres d'en-tete
		//

		public String getHeaderDisplayName() {
			return headerName;
		}

		public int getPreferredWidth() {
			return width;
		};

		public String getHeaderDescription() {
			return headerDescription;
		}


		//
		// Methodes de comportement de la colonne (� surcharger au cas par cas)
		//

		public abstract Class<?> getColumnClass();

		public abstract Object getColumnValue(ToSortEmailSingle emailData);

		@SuppressWarnings("unused")
		public String getToolTipText(ToSortEmailSingle emailData) {
			return null;
		}

		public int getAlignement() {
			return JLabel.CENTER;
		}

		public boolean doApplyFont() {
			return false;
		}

		public int getColumn() {
			return columns.indexOf(this);
		}

		@SuppressWarnings("unused")
		public boolean isEditable(ToSortEmailSingle emailData) {
			return false;
		}

		public TableCellEditor getColumnEditor() {
			return null;
		}

		@SuppressWarnings("unused")
		public void setValue(ToSortEmailSingle emailData, Object value) {
			throw new RuntimeException("Column is not editable:" + headerDescription);
		}


	}

}
