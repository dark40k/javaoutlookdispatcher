package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.preview;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.time.format.DateTimeFormatter;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmailBody;

public class EMailPreviewPanel extends JPanel {

	public final static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm");

	private JPanel panel;
	private JLabel label_1;
	private JLabel label_From;
	private JLabel label_2;
	private JLabel label_To;
	private JPanel panel_1;
	private JScrollPane scrollPane;
	private JEditorPane editorPane;
	private JLabel label_0;
	private JLabel label_Subject;
	private JLabel label_3;
	private JLabel label_CC;
	private JLabel label_4;
	private JLabel label_Date;
	private JLabel label_5;
	private JLabel label_Categories;

	public EMailPreviewPanel() {
		initGUI();

        // add an html editor kit
        HTMLEditorKit kit = new HTMLEditorKit();
        editorPane.setEditorKit(kit);

        // add some styles to the html
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
        styleSheet.addRule("h1 {color: blue;}");
        styleSheet.addRule("h2 {color: #ff0000;}");
        styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");

        Document doc = kit.createDefaultDocument();
        editorPane.setDocument(doc);

	}

	public void setEmail(OlEmail email) {
	    SwingUtilities.invokeLater(new Runnable() {
	        @Override
			public void run() {
	        	displayEmail(email);
	        }
	    });
	}

	private OlEmail displayedEMail;
	private void displayEmail(OlEmail email) {

		if (displayedEMail == email) return;

		if (email==null) {
			editorPane.setContentType("text/plain");
			editorPane.setText("");
			label_Subject.setText("");
			label_From.setText("");
			label_To.setText("");
			label_CC.setText("");
			label_Date.setText("");
			return;
		}

		OlEmailBody body = email.getBody();
		switch (body.getFormat()) {
		case HTML:
			editorPane.setContentType("text/html");
			break;
		case PLAIN:
			editorPane.setContentType("text/plain");
			break;
		case RTF:
			editorPane.setContentType("text/rtf");
			break;
		case UNSPECIFIED:
			editorPane.setContentType("text/plain");
			break;
		}

		editorPane.setContentType("text/html");

		try {
			editorPane.setContentType("text/html");
			editorPane.setText(body.getContent());
		} catch (Exception e) {
			editorPane.setContentType("text/plain");
			editorPane.setText("Erreur : contenu non affichable");
		}

		editorPane.setCaretPosition(0);

		label_Subject.setText(email.getSubject().toString());
		label_Categories.setText(email.getCategories());
		label_From.setText(email.getFrom());
		label_To.setText(email.getTo());
		label_CC.setText(email.getCC());
		label_Date.setText(email.getSentOn().format(dtf));

	}


	private void initGUI() {
		setLayout(new BorderLayout(0, 0));

		panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 50, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);

		label_0 = new JLabel("Objet :");
		GridBagConstraints gbc_label_0 = new GridBagConstraints();
		gbc_label_0.anchor = GridBagConstraints.EAST;
		gbc_label_0.insets = new Insets(0, 0, 5, 5);
		gbc_label_0.gridx = 0;
		gbc_label_0.gridy = 0;
		panel.add(label_0, gbc_label_0);

		label_Subject = new JLabel("");
		GridBagConstraints gbc_label_Subject = new GridBagConstraints();
		gbc_label_Subject.gridwidth = 3;
		gbc_label_Subject.anchor = GridBagConstraints.WEST;
		gbc_label_Subject.insets = new Insets(0, 0, 5, 0);
		gbc_label_Subject.gridx = 1;
		gbc_label_Subject.gridy = 0;
		panel.add(label_Subject, gbc_label_Subject);

		label_5 = new JLabel("Categories :");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.EAST;
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 0;
		gbc_label_5.gridy = 1;
		panel.add(label_5, gbc_label_5);

		label_Categories = new JLabel("");
		label_Categories.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_label_Categories = new GridBagConstraints();
		gbc_label_Categories.anchor = GridBagConstraints.WEST;
		gbc_label_Categories.gridwidth = 3;
		gbc_label_Categories.insets = new Insets(0, 0, 5, 5);
		gbc_label_Categories.gridx = 1;
		gbc_label_Categories.gridy = 1;
		panel.add(label_Categories, gbc_label_Categories);

		label_1 = new JLabel("De :");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 2;
		panel.add(label_1, gbc_label_1);

		label_From = new JLabel("");
		GridBagConstraints gbc_label_De = new GridBagConstraints();
		gbc_label_De.anchor = GridBagConstraints.WEST;
		gbc_label_De.insets = new Insets(0, 0, 5, 5);
		gbc_label_De.gridx = 1;
		gbc_label_De.gridy = 2;
		panel.add(label_From, gbc_label_De);

		label_2 = new JLabel("A :");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 3;
		panel.add(label_2, gbc_label_2);

		label_To = new JLabel("");
		GridBagConstraints gbc_label_A = new GridBagConstraints();
		gbc_label_A.gridwidth = 3;
		gbc_label_A.insets = new Insets(0, 0, 5, 0);
		gbc_label_A.anchor = GridBagConstraints.WEST;
		gbc_label_A.gridx = 1;
		gbc_label_A.gridy = 3;
		panel.add(label_To, gbc_label_A);

		label_3 = new JLabel("Cc :");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.EAST;
		gbc_label_3.insets = new Insets(0, 0, 0, 5);
		gbc_label_3.gridx = 0;
		gbc_label_3.gridy = 4;
		panel.add(label_3, gbc_label_3);

		label_CC = new JLabel("");
		GridBagConstraints gbc_label_Cc = new GridBagConstraints();
		gbc_label_Cc.gridwidth = 3;
		gbc_label_Cc.anchor = GridBagConstraints.WEST;
		gbc_label_Cc.gridx = 1;
		gbc_label_Cc.gridy = 4;
		panel.add(label_CC, gbc_label_Cc);

		label_4 = new JLabel("Date :");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.EAST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 2;
		gbc_label_4.gridy = 2;
		panel.add(label_4, gbc_label_4);

		label_Date = new JLabel("");
		GridBagConstraints gbc_label_Date = new GridBagConstraints();
		gbc_label_Date.anchor = GridBagConstraints.WEST;
		gbc_label_Date.insets = new Insets(0, 0, 5, 0);
		gbc_label_Date.gridx = 3;
		gbc_label_Date.gridy = 2;
		panel.add(label_Date, gbc_label_Date);

		panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		panel_1.add(scrollPane);

		editorPane = new JEditorPane();
		editorPane.setEditable(false);
		editorPane.setContentType("text/html");
		scrollPane.setViewportView(editorPane);
	}

}
