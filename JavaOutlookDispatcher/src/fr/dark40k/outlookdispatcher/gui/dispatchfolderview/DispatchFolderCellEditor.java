package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;

public class DispatchFolderCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private static final String NOFOLDERMESSAGE="<Pas de repertoire>";
	
    private OlFolder editFolder;

    private class ComboItem {
    	String title;
    	OlFolder folder;
    	public ComboItem(String title, OlFolder folder) {
    		this.title=title;
    		this.folder=folder;
    	}
    	@Override
		public String toString() {
    		return title;
    	}
    }
    
    @Override
    public Object getCellEditorValue() {
        return this.editFolder;
    }
 
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    	
        DispatchMailTableModel model = (DispatchMailTableModel) table.getModel();
        
        ToSortEmailSingle emailData =  model.getEMailData(row);
                
        JComboBox<ComboItem> comboCountry = new JComboBox<ComboItem>();
         
        comboCountry.addItem(new ComboItem(NOFOLDERMESSAGE,null));
        if (emailData.getDestFolder()==null) 
        	comboCountry.setSelectedIndex(0);
        
		for (WeightedDestFolder aResult : emailData.getPotentialFoldersList()) {
            comboCountry.addItem(new ComboItem(aResult.folder.getName(),aResult.folder));
            if ((emailData.getDestFolder()!=null) && (emailData.getDestFolder().equals(aResult.folder))) {
            	comboCountry.setSelectedIndex(comboCountry.getItemCount()-1);
            	editFolder=aResult.folder;
            }
        }

		if ((emailData.getDestFolder()!=null) && (!emailData.getDestFolder().equals(editFolder))) {
			comboCountry.addItem(new ComboItem(emailData.getDestFolder().getName(),emailData.getDestFolder()));
			comboCountry.setSelectedIndex(comboCountry.getItemCount()-1);
		}
			
		
        comboCountry.addActionListener(this);
         
        if (isSelected) {
            comboCountry.setBackground(table.getSelectionBackground());
        } else {
            comboCountry.setBackground(table.getSelectionForeground());
        }
         
        return comboCountry;
    }
 
    @SuppressWarnings("unchecked")
	@Override
    public void actionPerformed(ActionEvent event) {
        JComboBox<OlFolder> comboCountry = (JComboBox<OlFolder>) event.getSource();
        this.editFolder = ((ComboItem) comboCountry.getSelectedItem()).folder;
        stopCellEditing();
    }
	
}
