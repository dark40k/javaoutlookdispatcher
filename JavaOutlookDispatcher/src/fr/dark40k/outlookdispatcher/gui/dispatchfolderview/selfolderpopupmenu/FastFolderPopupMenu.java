package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.selfolderpopupmenu;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Function;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.com.ComFailException;

import fr.dark40k.outlookdispatcher.Dispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.gui.EmailDataGroupFolderWeightDisplayDialog;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;

public class FastFolderPopupMenu extends JPopupMenu {

	private final static Logger LOGGER = LogManager.getLogger();

	static final int MAX_POTENTIAL_FOLDER = 5;

	static final String MOVEFOLDERMESSAGE="=> Deplacer Emails";
	static final Icon MOVEFOLDERICON = new ImageIcon(FastFolderPopupMenu.class.getResource("res/arrow-right-double-3.png"));

	static final String NOFOLDERMESSAGE="<Pas de repertoire>";

	static final String AUTOFOLDERMESSAGE = "Reinitialiser destination";
	static final Icon AUTOFOLDERICON = new ImageIcon(FastFolderPopupMenu.class.getResource("res/lock-open.png"));

	static final String MANUALFOLDERMESSAGE = "Verrouiller destination";
	static final Icon MANUALFOLDERICON = new ImageIcon(FastFolderPopupMenu.class.getResource("res/lock-5.png"));

	static final String SHOWSCORESOURCEMESSAGE = "Afficher scores";
	static final Icon SHOWSCORESOURCEICON = new ImageIcon(FastFolderPopupMenu.class.getResource("res/calculator-add.png"));

	protected final ToSortEmailGroup emailDataGroup;
	protected final StringHashSetField permanentDestinationAddressesParam;
	protected final Function<OlFolderEMail,OlFolderEMail> createNewOlFolder;


	public FastFolderPopupMenu(ToSortEmailGroup emailDataGroup, Function<OlFolderEMail,OlFolderEMail> createNewOlFolder) {

		this.emailDataGroup=emailDataGroup;
		this.permanentDestinationAddressesParam=Dispatcher.properties.permanentDestinationAddresses;
		this.createNewOlFolder=createNewOlFolder;

        String comonDestinationFolderId = emailDataGroup.getComonDestFolderId();

		JMenuItem emptyFolderItem = new JMenuItem(NOFOLDERMESSAGE);
        emptyFolderItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	emailDataGroup.setDestFolder(null,true);
            }
        });
	    if (comonDestinationFolderId!=null && comonDestinationFolderId.equals(ToSortEmailGroup.NODESTINATIONFOLDERID))
	    	emptyFolderItem.setIcon(JMenuOlFolder.SELECTEDICON);
        add(emptyFolderItem);


	    int potentialCounter=0;
		for (WeightedDestFolder potFolder : emailDataGroup.getPotentialFoldersList()) {
			add(new JMenuOlFolder(potFolder.folder,potFolder.folder.getName()+" ("+potFolder.weight+")",this));
	        potentialCounter++;
	        if (potentialCounter>MAX_POTENTIAL_FOLDER) break;
		}

		//
		// Ajoute la commande de visu des poids sur le repertoire selectionne.
		//

		if (comonDestinationFolderId != null) {
			JMenuItem showScoreSourceItem = new JMenuItem(SHOWSCORESOURCEMESSAGE);
			showScoreSourceItem.setIcon(SHOWSCORESOURCEICON);
			showScoreSourceItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					EmailDataGroupFolderWeightDisplayDialog.showDialog(emailDataGroup);
				}
			});
			add(new JSeparator());
			add(showScoreSourceItem);
		}

		//
		// Ajoute la commande pour recalculer en automatique
		//

		add(new JSeparator());

		JMenuItem manualFolderItem = new JMenuItem(MANUALFOLDERMESSAGE);
		manualFolderItem.setIcon(MANUALFOLDERICON);
		manualFolderItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LOGGER.info("Verrouiller repertoire");
				emailDataGroup.setManualDestFolder();
			}
		});
		add(manualFolderItem);

		JMenuItem autoFolderItem = new JMenuItem(AUTOFOLDERMESSAGE);
		autoFolderItem.setIcon(AUTOFOLDERICON);
		autoFolderItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LOGGER.info("Restaurer repertoire automatique");
				emailDataGroup.setAutoDestFolder();
			}
		});
		add(autoFolderItem);

		//
		// Ajoute la commande de d�placement.
		//

		int countMoveable = emailDataGroup.mailCountMoveable();
		if (countMoveable > 0) {
			JMenuItem moveFolderItem = new JMenuItem(MOVEFOLDERMESSAGE + " (" + countMoveable + ")");
			moveFolderItem.setIcon(MOVEFOLDERICON);
			moveFolderItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					LOGGER.info("Deplacer  messages.");
					JProgressDialog.showProgressDialog(
							Frame.getFrames()[0],
							"Mise � jours donn�es",
							"Lancement",
							(JProgressDialog dialog) -> {emailDataGroup.doMoveEmails(dialog);}
						);
				}
			});
			add(new JSeparator());
			add(moveFolderItem);
		}

		//
		// Ajoute les 5 plus recents et une visu sur les 5 suivants
		//

		add(new JSeparator());
		HistoryFolderMenu.add(this,this);


		//
		// Ajoute les repertoires de destination permanents.
		//

		add(new JSeparator());

		for (String folderLink : permanentDestinationAddressesParam ) {

			String folderID = folderLink;
			int index = folderID.indexOf(",");
			if (index>0) folderID = folderID.substring(0,index);


			OlFolderEMail folder;
			try {
				folder = (OlFolderEMail) OlConnector.getOlObject(folderID);

				if (folder != null) {
					String name = (index>0)?folderLink.substring(index+1):folder.getName();
					add(new JMenuOlFolder(folder,name,this));
				} else
					add(new JMenuItem("<Folder not found>"));


			} catch (ComFailException e) {
				LOGGER.warn("Repertoire scan non trouv� = "+folderLink);
				continue;
			}
		}

	}

}
