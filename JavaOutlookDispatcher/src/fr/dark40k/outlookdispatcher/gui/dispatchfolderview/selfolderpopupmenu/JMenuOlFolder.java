package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.selfolderpopupmenu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

class JMenuOlFolder extends JMenu {

	final static Color TOOL_BACKGROUND_COLOR = Color.white;
	final static Color TOOL_FOREGROUND_COLOR = Color.lightGray;

	final static Icon SELECTEDICON = new ImageIcon(JMenuOlFolder.class.getResource("res/dialog-ok-apply-4.png"));
	final static Icon CREATEICON = new ImageIcon(JMenuOlFolder.class.getResource("res/folder-add.png"));
	final static Icon PARENTFOLDERICON = new ImageIcon(JMenuOlFolder.class.getResource("res/folder-up.png"));

	private final String comonDestinationFolderId;

	public JMenuOlFolder(OlFolderEMail folder, String name, FastFolderPopupMenu fastFolderPopupMenu) {

		super(name);

		comonDestinationFolderId=fastFolderPopupMenu.emailDataGroup.getComonDestFolderId();

		if (comonDestinationFolderId!=null && comonDestinationFolderId.equals(folder.getEntryID()))
			setIcon(SELECTEDICON);

		addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {

				if (JMenuOlFolder.this.getItemCount() > 0) return;

				for (OlFolderEMail subfolder : folder)
					add(new JMenuOlFolder(subfolder, subfolder.getName(),fastFolderPopupMenu));

				add(new JSeparator());

				JMenuItem createSubFolder = new JMenuItem("Creer sous-repertoire");
				createSubFolder.setBackground(TOOL_BACKGROUND_COLOR);
				createSubFolder.setForeground(TOOL_FOREGROUND_COLOR);
				createSubFolder.setOpaque(true);
				createSubFolder.setIcon(CREATEICON);
				createSubFolder.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e1) {
						OlFolderEMail newFolder = fastFolderPopupMenu.createNewOlFolder.apply(folder);
						if (newFolder == null) return;
						fastFolderPopupMenu.emailDataGroup.setDestFolder(newFolder, true);
					}
				});
				add(createSubFolder);

				OlFolder parent = folder.getParentFolder();
				if (parent!=null)
					if (parent instanceof OlFolderEMail) {
						add(new JSeparator());
						JMenuItem item = add(new JMenuOlFolder((OlFolderEMail) parent, parent.getName(),fastFolderPopupMenu));
						item.setBackground(TOOL_BACKGROUND_COLOR);
						item.setForeground(TOOL_FOREGROUND_COLOR);
						item.setOpaque(true);
						item.setIcon(PARENTFOLDERICON);
					}
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource() != null) {
					fastFolderPopupMenu.emailDataGroup.setDestFolder(folder, true);
					fastFolderPopupMenu.setVisible(false);
				}
			}
		});
	}
}
