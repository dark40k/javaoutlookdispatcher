package fr.dark40k.outlookdispatcher.gui.dispatchfolderview.selfolderpopupmenu;

import java.util.Iterator;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

class HistoryFolderMenu {
	
	static final String HISTORYMESSAGE = "Repertoires r�cents";
	static final Icon HISTORYMESSAGEICON = new ImageIcon(HistoryFolderMenu.class.getResource("res/edit-history.png")); 

	static final String HISTORYMESSAGENEXT = "Suivants >>>";
	static final int MAXLINES = 15; 

	public static void add(JPopupMenu jmenu, FastFolderPopupMenu fastFolderPopupMenu) {
		HistoryFolderMenu historyFolderMenu = new HistoryFolderMenu(fastFolderPopupMenu);
		JMenuItem item = jmenu.add(historyFolderMenu.new JMenuNextHistoryFolder(HISTORYMESSAGE));
		item.setIcon(HISTORYMESSAGEICON);
	}
		
//	private final static Logger LOGGER = LogManager.getLogger();
	
	private final Iterator<OlFolderEMail> iterRecent;
	private final FastFolderPopupMenu fastFolderPopupMenu;

	private HistoryFolderMenu(FastFolderPopupMenu fastFolderPopupMenu) {
		
		this.fastFolderPopupMenu=fastFolderPopupMenu;
		iterRecent = fastFolderPopupMenu.emailDataGroup.getDispatchDatabase().moveToFolderHistory.iterator();

	}
	
	private void extendMenu(JMenu jmenu) {
		
		for (int i=0; i<MAXLINES; i++) {
			
			if (!iterRecent.hasNext()) return;
			
			OlFolderEMail histFolder = iterRecent.next();
			
			jmenu.add(new JMenuOlFolder(histFolder, histFolder.getName(), fastFolderPopupMenu));
			
		}
		
		jmenu.add(new JSeparator());
		
		jmenu.add(new JMenuNextHistoryFolder(HISTORYMESSAGENEXT));
		
	}

	
	//
	// Menu d'extension de la liste
	//
	
	private class JMenuNextHistoryFolder extends JMenu {

		public JMenuNextHistoryFolder(String text) {

			super(text);

			addMenuListener(new MenuListener() {
				@Override
				public void menuSelected(MenuEvent e) {

					if (JMenuNextHistoryFolder.this.getItemCount() > 0) return;

					extendMenu(JMenuNextHistoryFolder.this);

				}

				@Override
				public void menuDeselected(MenuEvent e) {
				}

				@Override
				public void menuCanceled(MenuEvent e) {
				}
			});

		}
	}
	

}
