package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import fr.dark40k.outlookdispatcher.gui.dispatchfolderview.EmailTableColumns.EmailTableColumn;
import fr.dark40k.outlookdispatcher.util.EnhancedJTableHeader;

public class DispatchFolderTableEnhancedTableHeader extends  EnhancedJTableHeader {

	public DispatchFolderTableEnhancedTableHeader(TableColumnModel cm, JTable table) {
		super(cm, table);

		for (EmailTableColumn column : EmailTableColumns.getIterable())
			setToolTipForColumn(column.getColumn(), column.getHeaderDescription());
	}

	public void updateHeaderInformation(int col, String addToTitle, String addToTooltip) {

		EmailTableColumn column = EmailTableColumns.getColumn(col);

		if (addToTooltip!=null)
			setToolTipForColumn(column.getColumn(),"<html>"+column.getHeaderDescription()+"<br>"+addToTooltip+"</html>");
		else
			setToolTipForColumn(column.getColumn(), column.getHeaderDescription());

		getColumnModel().getColumn(col).setHeaderValue(column.getHeaderDisplayName()+((addToTitle!=null)?" "+addToTitle:""));
		repaint();

	}

}
