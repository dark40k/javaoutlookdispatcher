package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableCellEditor;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFlagStatusEnum;

public class OlFlagStatusEnumEditor extends AbstractCellEditor implements TableCellEditor, ActionListener, PopupMenuListener {

	OlFlagStatusEnum status;

	Icon[] items = { OlFlagStatusEnumRenderer.noFlagIcon, OlFlagStatusEnumRenderer.flaggedIcon, OlFlagStatusEnumRenderer.completeIcon };
	JComboBox<Icon> comboStatus = new JComboBox<Icon>(items);

	@Override
	public OlFlagStatusEnum getCellEditorValue() {
		return status;
	}

	@Override
   public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    	
		status = (OlFlagStatusEnum) value;

		switch (status) {
			case olFlagComplete:
				comboStatus.setSelectedIndex(2);
				break;
			case olFlagMarked:
				comboStatus.setSelectedIndex(1);
				break;
			case olNoFlag:
				comboStatus.setSelectedIndex(0);
				break;
			default:
				break;
		}

		comboStatus.addActionListener(this);
		
		comboStatus.addPopupMenuListener(this);
		
      return comboStatus;
    }
 
    @Override
    public void actionPerformed(ActionEvent event) {
    	
    	switch (comboStatus.getSelectedIndex()) {
    	case -1:
    		break;
    	case 0:
    		status = OlFlagStatusEnum.olNoFlag;
    		break;
    	case 1:
    		status = OlFlagStatusEnum.olFlagMarked;
    		break;
    	case 2:
    		status = OlFlagStatusEnum.olFlagComplete;
    		break;
    	}
    	endEdition();
    }

	private void endEdition() {
		comboStatus.removeActionListener(this);
		comboStatus.removePopupMenuListener(this);
		fireEditingStopped();
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
	}

	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
		endEdition();
	}
	
    @Override
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) { return ((MouseEvent) anEvent).getClickCount() >= 2; }
		return true;
	}
}
