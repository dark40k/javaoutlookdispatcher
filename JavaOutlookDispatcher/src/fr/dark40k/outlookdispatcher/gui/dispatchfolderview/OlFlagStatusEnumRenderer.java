package fr.dark40k.outlookdispatcher.gui.dispatchfolderview;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFlagStatusEnum;

public class OlFlagStatusEnumRenderer extends DefaultTableCellRenderer {

	public final static Icon noFlagIcon = new ImageIcon(OlFlagStatusEnumRenderer.class.getResource("res/noFlag.png")); 
	public final static Icon completeIcon = new ImageIcon(OlFlagStatusEnumRenderer.class.getResource("res/task-complete.png")); 
	public final static Icon flaggedIcon = new ImageIcon(OlFlagStatusEnumRenderer.class.getResource("res/flag.png")); 
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
		JLabel label = (JLabel) super.getTableCellRendererComponent(table,  null,  isSelected,  hasFocus,  row,  column);
		
		if (value==null) return label;
		
		switch ((OlFlagStatusEnum)value) {
		case olFlagComplete:
			label.setIcon(completeIcon);
			break;
		case olFlagMarked:
			label.setIcon(flaggedIcon);
			break;
		case olNoFlag:
		default:
			label.setIcon(null);
		}
		
		return label;
	}

}
