package fr.dark40k.outlookdispatcher.gui.folder;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateEvent;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateListener;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.GUIUtilities;
import fr.dark40k.util.persistantparameters.ParamGroup;


public class FolderAssignementJPanel extends JPanel implements UpdateListener, TreeSelectionListener {

//	private final static Logger LOGGER = LogManager.getLogger();

	public static Icon refreshIcon = new ImageIcon(FolderAssignementJPanel.class.getResource("res/view-refresh-3.png"));

	//
	// Donn�es
	//

	private final ParamGroup dispatchFolderAssignementPanelParams;

	ToSortEmailGroup storedEmailDataGroup;

	private DispatchDatabase database;

	//
	// Composants
	//

	private final FolderTree tree = new FolderTree();

	private final JPanel filterPanel = new JPanel();

	public final FolderPopupMenu folderPopupMenu = new FolderPopupMenu();

	protected FilterTextField filterField;

	//
	// Constructeur
	//

	public FolderAssignementJPanel(ParamGroup dispatchFolderAssignementPanelParams) {

		this.dispatchFolderAssignementPanelParams=dispatchFolderAssignementPanelParams;


		filterField = new FilterTextField(this, dispatchFolderAssignementPanelParams.getSubParamGroup("filterField"));

		initGUI();

		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int row = tree.getClosestRowForLocation(e.getX(), e.getY());
					OlFolder olFolder = ((FolderTreeNode) tree.getPathForRow(row).getLastPathComponent()).getFolder();
					folderPopupMenu.show(e.getComponent(), e.getX(), e.getY(), olFolder);
				}
			}
		});

		folderPopupMenu.getPopupMenu().addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				tree.setHighlightedFolder(folderPopupMenu.getOlFolder());
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				tree.setHighlightedFolder(null);
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				tree.setHighlightedFolder(null);
			}

		});

		tree.addTreeSelectionListener(this);

		updateRootFolder();

	}

	//
	// Mise � jours
	//


	public void updateRootFolder() {
		ToSortEmailGroup tmpEmailData = storedEmailDataGroup;
		setEmailDataGroup(null);

		tree.updateTree(OlConnector.getRootFolder());

		tree.restoreCollapsedState(dispatchFolderAssignementPanelParams.getSubParamString("collapsedList", "").getValue());

		setEmailDataGroup(tmpEmailData);
	}

	public void setDatabase(DispatchDatabase newDatabase) {

		if (database!=null) database.removeUpdateListener(this);

		database = newDatabase;
		database.addUpdateListener(this);

		updateRootFolder();

	}

	public void forceRefresh() {
		forceRefresh(null);
	}

	public void forceRefresh(String saveCollapsedStateIn) {

		String saveCollapsedState = (saveCollapsedStateIn==null)?saveCollapsedState():saveCollapsedStateIn;

		TreePath selPath = tree.getSelectionPath();

		tree.updateTree(OlConnector.getRootFolder());
		filterField.updateFilter();

		if (selPath!=null) tree.setSelectionPath(selPath);

		tree.restoreCollapsedState(saveCollapsedState);
		tree.repaint();

	}

	public String saveCollapsedState() {
		return tree.saveCollapsedState();
	}


	//
	// Popup menu
	//


	//
	// Sauvegarde et restauration de l'affichage
	//

	public void restoreWindowConfiguration() {

		// restauration des repertoires repli�s
		dispatchFolderAssignementPanelParams.getSubParamString("collapsedList", "");
		tree.restoreCollapsedState(dispatchFolderAssignementPanelParams.getSubParamString("collapsedList", "").getValue());

		// restauration des parametres d'affichage du filtre
		filterField.restoreWindowConfiguration();

	}

	public void saveWindowConfiguration() {

		// sauvegarde des repertoires repli�s
		dispatchFolderAssignementPanelParams.getSubParamString("collapsedList", null).setValue(saveCollapsedState());

		// sauvegarde des parametres d'affichage du textField
		filterField.saveWindowConfiguration();

	}

	//
	// Utilitaires
	//

	public void setEmailDataGroup(ToSortEmailGroup emailDataGroup) {

		if (storedEmailDataGroup!=null)
			if (storedEmailDataGroup.getDispatchDatabase()!=null)
				storedEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);

		storedEmailDataGroup=emailDataGroup;

		if (emailDataGroup==null) {
			tree.removeTreeSelectionListener(this);
			tree.clearSelection();
			tree.addTreeSelectionListener(this);
			return;
		}

		if (storedEmailDataGroup.getDispatchDatabase()!=null)
			storedEmailDataGroup.getDispatchDatabase().addUpdateListener(this);

		updateSelectedFolder();

		tree.repaint();

	}

	private void updateSelectedFolder() {

		int row = -1;

		// Calcule si une destination commune existe
		String comonDestinationFolderId = (storedEmailDataGroup != null) ? storedEmailDataGroup.getComonDestFolderId() : null;

		// Affiche cette destination si elle existe
		if (comonDestinationFolderId!=null && !comonDestinationFolderId.equals(ToSortEmailGroup.NODESTINATIONFOLDERID)) {

			OlFolderEMail comonDestFolder = (OlFolderEMail) OlConnector.getOlObject(comonDestinationFolderId);

			row = findDisplayedRowFolder(comonDestFolder);

			if (row<0) {

				TreePath selPath =
						(comonDestFolder.getSubFoldersCount()>0)?
						buildPathForFolder(comonDestFolder):
						buildPathForFolder(comonDestFolder.getParentFolder());

				tree.expandPath(selPath);
				row = findDisplayedRowFolder(comonDestFolder);
			}

		}

		// reinitialise les destinations secondaires
		tree.subSelectedFolderList.clear();
		if (storedEmailDataGroup!=null)
			for ( WeightedDestFolder potDestination : storedEmailDataGroup.getPotentialFoldersList())
				tree.subSelectedFolderList.add(potDestination.folder);

		// modifie la ligne selectionnee s'il y a lieu
		if (row!=tree.getSelectionModel().getMinSelectionRow()) {

			tree.removeTreeSelectionListener(this);

			if (row>=0) {
				tree.setSelectionRow(row);
				tree.scrollRowToVisible(row);
			}
			else
				tree.clearSelection();

			tree.addTreeSelectionListener(this);
		}

	}

	private TreePath buildPathForFolder(OlFolder folder) {

		if (folder.getParentFolder()==null) {
			return new TreePath(tree.getModel().getRoot());
		}

		TreePath fatherPath = buildPathForFolder(folder.getParentFolder());

		FolderTreeNode fatherFolderTreeNode = (FolderTreeNode) fatherPath.getLastPathComponent();

		FolderTreeNode folderTreeNode=null;
		for (int i=0; i<fatherFolderTreeNode.getChildCount(); i++)
			if (fatherFolderTreeNode.getChildAt(i).getFolder().equals(folder))
				folderTreeNode = fatherFolderTreeNode.getChildAt(i);

		if (folderTreeNode==null) return null;

		return fatherPath.pathByAddingChild(folderTreeNode);
	}

	private int findDisplayedRowFolder(OlFolder folder) {
		for (int row=0; row<tree.getRowCount();row++) {
			OlFolder rowFolder = ((FolderTreeNode)tree.getPathForRow(row).getLastPathComponent()).getFolder();
			if (rowFolder.equals(folder)) return row;
		}
		return -1;
	}

	//
	// Recuperation des evenements de selection de l'arbre
	//

	@Override
	public void valueChanged(TreeSelectionEvent e) {

		if (storedEmailDataGroup==null) {
			if (tree.getSelectionCount()>0) {
				tree.removeTreeSelectionListener(this);
				tree.clearSelection();
				tree.addTreeSelectionListener(this);
			}
			return;
		}

		storedEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);

		if (tree.getSelectionCount()>0)
			storedEmailDataGroup.setDestFolder((OlFolderEMail) ((FolderTreeNode)tree.getSelectionPath().getLastPathComponent()).getFolder(),true);
		else
			storedEmailDataGroup.setDestFolder(null,true);

		storedEmailDataGroup.getDispatchDatabase().addUpdateListener(this);
	}

	//
	// Recuperation des evenements de la base de donn�e
	//

	@Override
	public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData) {
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				updateSelectedFolder();
			}
		});
	}

	@Override
	public void structureChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				setEmailDataGroup(null);
			}
		});
	}

	@Override
	public void fullDatabaseChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				setEmailDataGroup(null);
				updateRootFolder();
			}
		});
	}

	//
	// Inteface utilisateur
	//

	private void initGUI() {
		setLayout(new BorderLayout(0, 0));

		final JScrollPane scrollPane = new JScrollPane();
		add(scrollPane);

		tree.setShowsRootHandles(true);
		tree.setRootVisible(false);
		scrollPane.setViewportView(tree);
		{
			add(filterPanel, BorderLayout.NORTH);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{20, 49, 0};
			gbl_panel.rowHeights = new int[]{25, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			filterPanel.setLayout(gbl_panel);
			{
				final JButton updateButton = new JButton(refreshIcon);
				updateButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						filterField.setText("");
						forceRefresh();
					}
				});
				GridBagConstraints gbc_updateButton = new GridBagConstraints();
				gbc_updateButton.fill = GridBagConstraints.HORIZONTAL;
				gbc_updateButton.insets = new Insets(0, 0, 0, 5);
				gbc_updateButton.anchor = GridBagConstraints.NORTH;
				gbc_updateButton.gridx = 0;
				gbc_updateButton.gridy = 0;
				filterPanel.add(updateButton, gbc_updateButton);
			}
			{
				GridBagConstraints gbc_filterField = new GridBagConstraints();
				gbc_filterField.fill = GridBagConstraints.HORIZONTAL;
				gbc_filterField.gridx = 1;
				gbc_filterField.gridy = 0;
				filterPanel.add(filterField, gbc_filterField);
				filterField.setColumns(10);
			}
		}
	}

	public void setFilter(Predicate<FolderTreeNode> filter) {
		tree.setFilter(filter);
	}


}
