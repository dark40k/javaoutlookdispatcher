package fr.dark40k.outlookdispatcher.gui.folder;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.dark40k.util.persistantparameters.ParamGroup;

public class FilterTextField extends JTextField {

	private final FolderAssignementJPanel folderAssignementJPanel;

	// Parametres filtre

	boolean ignoreCase = true;
	boolean excludedVisibility = true;
	boolean recExcludedVisibility = true;

	// elements graphiques

	public final FilterPopupMenu filterPopupMenu = new FilterPopupMenu(this);
	private final ParamGroup filterFieldParamGroup;

	//
	// Constructeur
	//

	public FilterTextField(FolderAssignementJPanel folderAssignementJPanel, ParamGroup filterFieldParamGroup) {

		this.folderAssignementJPanel=folderAssignementJPanel;
		this.filterFieldParamGroup=filterFieldParamGroup;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					filterPopupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		getDocument().addDocumentListener(new DocumentListener() {
			  @Override
			public void changedUpdate(DocumentEvent e) {
				  folderAssignementJPanel.forceRefresh();
			  }
			  @Override
			public void removeUpdate(DocumentEvent e) {
				  folderAssignementJPanel.forceRefresh();
			  }
			  @Override
			public void insertUpdate(DocumentEvent e) {
				  folderAssignementJPanel.forceRefresh();
			  }});

	}

	//
	// Persistence de l'affichage
	//

	public void restoreWindowConfiguration() {

		ignoreCase = filterFieldParamGroup.getSubParamBoolean("ignoreCase", true).getValue();
		excludedVisibility = filterFieldParamGroup.getSubParamBoolean("excludedVisibility", true).getValue();
		recExcludedVisibility = filterFieldParamGroup.getSubParamBoolean("recExcludedVisibility", true).getValue();

		folderAssignementJPanel.forceRefresh();

	}

	public void saveWindowConfiguration() {
		filterFieldParamGroup.getSubParamBoolean("ignoreCase", true).setValue(ignoreCase);
		filterFieldParamGroup.getSubParamBoolean("excludedVisibility", true).setValue(excludedVisibility);
		filterFieldParamGroup.getSubParamBoolean("recExcludedVisibility", true).setValue(recExcludedVisibility);
	}

	//
	// Getters / Setters
	//

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
		folderAssignementJPanel.forceRefresh();
	}

	public void setExcludedVisibility(boolean excludedVisibility) {
		this.excludedVisibility = excludedVisibility;
		folderAssignementJPanel.forceRefresh();
	}

	public void setRecExcludedVisibility(boolean recExcludedVisibility) {
		this.recExcludedVisibility = recExcludedVisibility;
		folderAssignementJPanel.forceRefresh();
	}

	//
	// Filtre
	//

	public void updateFilter() {

		final Pattern pattern = calcPattern();

		setForeground((pattern!=null)?Color.black:Color.red);

		folderAssignementJPanel.setFilter(new Predicate<FolderTreeNode>() {

			private boolean exVis = excludedVisibility;
			private boolean recExVis = recExcludedVisibility;

			@Override
			public boolean test(FolderTreeNode node) {
				if ( (!exVis) && node.isExcluded()) return false;
				if ( (!recExVis) && node.isSonOfRecExcluded()) return false;
				if (pattern==null) return true;
				return pattern.matcher(node.getFolder().getPath("/")).find();
			}
		});

	}

	//
	// Outils
	//

	private Pattern calcPattern() {

		String text = getText().trim();

		if (text.isEmpty()) return null;

		if (ignoreCase) text="(?i)"+text;

		try {
				return Pattern.compile(text);
		} catch (PatternSyntaxException e) {}

		return null;

	}

}
