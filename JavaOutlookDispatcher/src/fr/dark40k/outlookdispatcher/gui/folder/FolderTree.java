package fr.dark40k.outlookdispatcher.gui.folder;

import java.util.HashSet;
import java.util.function.Predicate;

import javax.swing.JTree;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;

public class FolderTree extends JTree {

	final HashSet<OlFolder> subSelectedFolderList = new HashSet<>();
	private OlFolder highlightedFolder = null;

	private final FolderTreeModel folderTreeModel;

	//
	// Constructeur
	//

	public FolderTree() {
		super(new FolderTreeModel());
		folderTreeModel = ((FolderTreeModel)treeModel);
		setCellRenderer( new FolderTreeCellRenderer(this) );
	}

	//
	// Mise � jours
	//

	public void updateTree(OlFolder rootFolder) {
		folderTreeModel.updateTree(rootFolder);
	}

	public void setFilter(Predicate<FolderTreeNode> filter) {
		folderTreeModel.filterTree(filter);
	}

	public OlFolder getHighlightedFolder() {
		return highlightedFolder;
	}

	public void setHighlightedFolder(OlFolder highlightedFolder) {
		this.highlightedFolder = highlightedFolder;
		repaint();
	}

	//
	// Etat expanded/collapsed
	//

	public String saveCollapsedState() {

		StringBuilder sb = new StringBuilder();
	    for(int i =0 ; i < getRowCount(); i++){
	    	FolderTreeNode treeNode = (FolderTreeNode) getPathForRow(i).getLastPathComponent();

	        if (!treeNode.isLeaf())
		        if(isCollapsed(i)){
//		        	LOGGER.finest("saving collapsed="+((FolderTreeNode)tp.getLastPathComponent()).getFolder().getName());
		            sb.append(treeNode.getFolderId());
		            sb.append(",");
		        }
	    }

		return sb.toString();

	}

	public void restoreCollapsedState(String s) {

	    for(int i = 0 ; i<getRowCount(); i++){

	        FolderTreeNode treeNode=(FolderTreeNode) getPathForRow(i).getLastPathComponent();

//	        LOGGER.finest("expanding="+folder.getFolder().getName()+"("+folder.getChildCount()+")"+" row="+i+"/"+tree.getRowCount());

	        if (treeNode.isLeaf()) continue;

	        if (s.contains(treeNode.getFolderId())) {
//	        	LOGGER.finest(folder.getFolder().getName()+" => collapsed=" + folder.getFolderId());
	        	if (isExpanded(i)) collapseRow(i);

	        } else {

//		        LOGGER.finest("expanding row="+i);
	        	expandRow(i);

	        }
	    }

	}



}
