package fr.dark40k.outlookdispatcher.gui.folder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class FilterPopupMenu extends JPopupMenu {
	
	private JCheckBoxMenuItem chckbxmntmIgnorerCase;
	private JCheckBoxMenuItem chckbxmntmExclus;
	private JCheckBoxMenuItem chckbxmntmExclusRecursifs;
	
	private final FilterTextField filterTextField;
	
	public FilterPopupMenu(FilterTextField filterTextField) {
		
		this.filterTextField=filterTextField;
		
		initGUI();
		
		addPopupMenuListener(popupListener);
		
	}
	
	
	private PopupMenuListener popupListener = new PopupMenuListener() {
		@Override
		public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			chckbxmntmIgnorerCase.setSelected(filterTextField.ignoreCase);
			chckbxmntmExclus.setSelected(filterTextField.excludedVisibility);
			chckbxmntmExclusRecursifs.setSelected(filterTextField.recExcludedVisibility);
		}

		@Override
		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}

		@Override
		public void popupMenuCanceled(PopupMenuEvent e) {}
		
	};
	
	
	private void initGUI() {
		{
			chckbxmntmIgnorerCase = new JCheckBoxMenuItem("Ignorer la casse");
			chckbxmntmIgnorerCase.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					filterTextField.setIgnoreCase(chckbxmntmIgnorerCase.isSelected());
				}
			});
			add(chckbxmntmIgnorerCase);
		}
		
		add(new JSeparator());
		
		{
			chckbxmntmExclus = new JCheckBoxMenuItem("Exclus");
			chckbxmntmExclus.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					filterTextField.setExcludedVisibility(chckbxmntmExclus.isSelected());
				}
			});
			add(chckbxmntmExclus);
		}
		{
			chckbxmntmExclusRecursifs = new JCheckBoxMenuItem("Exclus recursifs");
			chckbxmntmExclusRecursifs.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					filterTextField.setRecExcludedVisibility(chckbxmntmExclusRecursifs.isSelected());
				}
			});
			add(chckbxmntmExclusRecursifs);
		}
	}
	
}
