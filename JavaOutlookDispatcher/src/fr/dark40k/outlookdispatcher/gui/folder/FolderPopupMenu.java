package fr.dark40k.outlookdispatcher.gui.folder;

import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;

public class FolderPopupMenu {

//	private final static Logger LOGGER = LogManager.getLogger();

	private final JPopupMenu  popupMenu = new JPopupMenu();
	private OlFolder olFolder;

	private final JMenuItem firstItem;

	public FolderPopupMenu() {

		firstItem = new JMenuItem();
		firstItem.setForeground(Color.white);
		firstItem.setBackground(Color.black);
		firstItem.setOpaque(true);
		popupMenu.add(firstItem);

		popupMenu.add(new JSeparator());

		setOlFolder(null);

	}

	//
	// Visualisation du popup
	//

	public void show(Component component, int x, int y, OlFolder newOlFolder) {
		setOlFolder(newOlFolder);
		popupMenu.show(component, x, y);
	}

	// Getter / Setters

	public OlFolder getOlFolder() {
		return olFolder;
	}

	public void setOlFolder(OlFolder olFolder) {
		firstItem.setText( (olFolder!=null) ? olFolder.getName() : "<empty>" );
		this.olFolder = olFolder;
	}

	public JPopupMenu getPopupMenu() {
		return popupMenu;
	}

	//
	// Extension du popup
	//

	public void addPopupCommand(String itemTitle, Consumer<OlFolder> doOnFolder) {
		JMenuItem item = new JMenuItem(itemTitle);
		item.addActionListener(ae -> {
			doOnFolder.accept(olFolder);
		});
		popupMenu.add(item);
	}

	public void addPopupChckBox(String itemTitle, Predicate<OlFolder> getStatus, Consumer<OlFolder> doOnFolder) {

		final JCheckBoxMenuItem item = new JCheckBoxMenuItem(itemTitle);
		item.addActionListener(ae -> {
			doOnFolder.accept(olFolder);
		});
		popupMenu.add(item);

		popupMenu.addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				if (!Arrays.asList(popupMenu.getComponents()).contains(item)) {
					popupMenu.removePopupMenuListener(this);
					return;
				}
				item.setSelected(getStatus.test(olFolder));
			}
			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {}
		});

	}

	public void addPopupSeparator() {
		popupMenu.add(new JSeparator());
	}



}
