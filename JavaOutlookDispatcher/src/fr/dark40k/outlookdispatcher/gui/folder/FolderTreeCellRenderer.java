package fr.dark40k.outlookdispatcher.gui.folder;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.dark40k.outlookdispatcher.util.CompoundIcon;

public class FolderTreeCellRenderer extends DefaultTreeCellRenderer {

	public static Icon searchIcon = new ImageIcon(FolderTreeCellRenderer.class.getResource("res/system-search-4.png"));
	public static Icon destinationIcon = new ImageIcon(FolderTreeCellRenderer.class.getResource("res/folder-go.png"));
	public static Icon trashIcon = new ImageIcon(FolderTreeCellRenderer.class.getResource("res/user-trash-full-3.png"));

	private FolderTree folderTree;

	public FolderTreeCellRenderer(FolderTree folderTree) {
		this.folderTree=folderTree;
	}

	@SuppressWarnings("null") // test inutile de icon==null
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean bSelected, boolean bExpanded, boolean bLeaf, int iRow, boolean bHasFocus) {

		Component c=super.getTreeCellRendererComponent(tree, value, bSelected, bExpanded, bLeaf, iRow, bHasFocus);

		FolderTreeNode folderTreeNode = (FolderTreeNode)value;

		if (folderTreeNode != null) {

			CompoundIcon icon = null;

			if (bSelected)
				applyDefaultBackground();
			else if (folderTreeNode.getFolder().equals(folderTree.getHighlightedFolder()))
				applyHighlightedBackground();
			else if (folderTree.subSelectedFolderList.contains(folderTreeNode.getFolder()))
				applySubSelectedBackground();
			else
				applyDefaultBackground();

			JLabel label = (JLabel) c;

			// Exclu recursif
			if (folderTreeNode.isRecExcluded()) {
				label.setText("<html><s>" + folderTreeNode.getFolder().getName() + "</s></html>");

			// Exclu simple
			} else if (folderTreeNode.isExcluded()) {
				label.setText("<html>" + folderTreeNode.getFolder().getName() + "</html>");

			// Descendant d'exclu recursif
			} else if (folderTreeNode.isSonOfRecExcluded()) {
				label.setText("<html><i>" + folderTreeNode.getFolder().getName() + "</i></html>");

			// Defaut
			} else {
				if (folderTreeNode.getFolder() != null) {
					if (folderTreeNode.isTitleExcluded())
						label.setText("<html>" + folderTreeNode.getFolder().getName() + "</html>");
					else
						label.setText("<html><b>" + folderTreeNode.getFolder().getName() + "</b></html>");
				}

				if (icon==null) icon=new CompoundIcon(); // test inutile (toujours vrai)
				icon.add(destinationIcon);

			}

			// repertoire scann�
			if (folderTreeNode.isScanned()) {
				if (icon==null) icon=new CompoundIcon();
				icon.add(searchIcon);

			}

			// repertoire poubelle
			if (folderTreeNode.isTrash()) {
				if (icon==null) icon=new CompoundIcon();
				icon.add(trashIcon);
			}

			label.setIcon(icon);

		}
		return c;
	}

	private void applyDefaultBackground() {
		this.setOpaque(false);
		this.setBackground(Color.white);
	}

	private void applyHighlightedBackground() {
		this.setOpaque(true);
		this.setBackground(Color.CYAN);
	}

	private void applySubSelectedBackground() {
		this.setOpaque(true);
		this.setBackground(Color.LIGHT_GRAY);
	}


}
