package fr.dark40k.outlookdispatcher.gui.folder;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.function.Predicate;

import javax.swing.tree.TreeNode;

import fr.dark40k.outlookdispatcher.Dispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class FolderTreeNode implements TreeNode {

//	private final static Logger LOGGER = LogManager.getLogger();

	private final OlFolder olFolder;

	private final FolderTreeNode parent;

	private final ArrayList<FolderTreeNode> children;

	private boolean isTitleExcluded;
	private boolean isRecExcluded;
	private boolean isSonOfRecExcluded;
	private boolean isExcluded;

	private boolean isScanned;
	private boolean isTrash;

	private boolean isVisible;

	public FolderTreeNode(OlFolder olFolder, FolderTreeNode parent) {

		this.olFolder=olFolder;

		this.parent=parent;

		isTitleExcluded =
				(olFolder instanceof OlFolderEMail)
				? OlFolderDataUtility.load((OlFolderEMail) olFolder).getKeywords().excludeTitle
				: false;

		isRecExcluded = DispatchDatabase.properties.excludedRecDirectories.contains(olFolder.getEntryID());
		isSonOfRecExcluded = isRecExcluded | ((parent != null) ? parent.isSonOfRecExcluded : false);
		isExcluded = DispatchDatabase.properties.excludedDirectories.contains(olFolder.getEntryID());

		isScanned = DispatchDatabase.properties.scanDirectories.contains(olFolder.getEntryID());
		isTrash = Dispatcher.properties.trashDestinationAddress.equals(olFolder.getEntryID());

		isVisible = true;

		children = new ArrayList<>();
		for (OlFolder subFolder : olFolder.getSubFolders())
			children.add(new FolderTreeNode(subFolder,this));

	}

	//
	// Visibility
	//

	public void setVisibility(boolean newStatus) {

		if (isVisible == newStatus) return;

		isVisible = newStatus;

		if (parent != null) {
			if (isVisible)
				parent.setVisibility(true);
		}

		if (!isVisible)
			for (FolderTreeNode child : children)
				child.setVisibility(false);

	}

	public boolean applyFilter(Predicate<FolderTreeNode> filter) {
		isVisible = filter.test(this);
		for (FolderTreeNode child : children)
			isVisible |= child.applyFilter(filter);
		return isVisible;
	}

	//
	// Information
	//

	public OlFolder getFolder(){
		return olFolder;
	}

	public String getFolderId(){
		if (olFolder==null) return null;
		return olFolder.getEntryID();
	}

	public boolean isRecExcluded() {
		return isRecExcluded;
	}

	public boolean isTitleExcluded() {
		return isTitleExcluded;
	}

	public boolean isExcluded() {
		return isExcluded;
	}

	public boolean isSonOfRecExcluded() {
		return isSonOfRecExcluded;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public boolean isScanned() {
		return isScanned;
	}

	public boolean isTrash() {
		return isTrash;
	}

	//
	// Interface TreeNode
	//

	@Override
	public FolderTreeNode getChildAt(int index) {
		return children.get(index);
	}

	@Override
	public int getChildCount() {
		if (olFolder==null) return -1;
		return children.size();
	}

	@Override
	public FolderTreeNode getParent() {
		return parent;
	}

	@Override
	public int getIndex(TreeNode node) {
		if (olFolder==null) return -1;
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	@Override
	public boolean isLeaf() {
		if (olFolder==null) return false;
		return (children.size()==0);
	}

	@Override
	public Enumeration<FolderTreeNode> children() {
        return new Enumeration<FolderTreeNode>() {
            int index=0;

            @Override
			public boolean hasMoreElements() {
                return index<getChildCount();
            }

            @Override
			public FolderTreeNode nextElement() {
                return getChildAt(index++);
            }
        };
	}

	public Iterable<FolderTreeNode> getChildren() {
		return children;
	}



}
