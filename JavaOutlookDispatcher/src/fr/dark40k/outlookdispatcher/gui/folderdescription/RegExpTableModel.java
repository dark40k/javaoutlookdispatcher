package fr.dark40k.outlookdispatcher.gui.folderdescription;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.RegExpFilter;

public class RegExpTableModel extends AbstractTableModel {

	private final static Icon errorIcon = new ImageIcon(RegExpTableModel.class.getResource("res/dialog-error-4.png"));
	private final static Icon okIcon = new ImageIcon(RegExpTableModel.class.getResource("res/dialog-ok-apply-4.png"));

	public final static String[] columnNames = {
			"Regexp filter", "Poids", "Desc.","Multi" ,"Title", "Body", "isValid"
	};

	private final static Class<?>[] columnTypes = new Class<?>[] {
			String.class, Double.class, String.class, Boolean.class, Boolean.class, Boolean.class, Icon.class
	};

	//
	// Donn�es
	//

	private boolean isValid;

	private ArrayList<RegExpFilter> regExpFilters;

	//
	// Constructeur
	//
	public RegExpTableModel() {
		regExpFilters=new ArrayList<RegExpFilter>();
		isValid=false;
	}

	public void setRegExpFilters(ArrayList<RegExpFilter> regExpFilters) {
		if (regExpFilters!=null) {
			this.regExpFilters=regExpFilters;
			isValid=true;
		} else {
			this.regExpFilters=new ArrayList<RegExpFilter>();
			isValid=false;
		}
		fireTableDataChanged();
	}

	//
	// Manipulation de la table
	//
	@Override
	public int getRowCount() {
		return regExpFilters.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0 : return regExpFilters.get(rowIndex).getRegExp();
			case 1 : return regExpFilters.get(rowIndex).score;
			case 2 : return regExpFilters.get(rowIndex).description;
			case 3 : return regExpFilters.get(rowIndex).multicatch;
			case 4 : return ((regExpFilters.get(rowIndex).applyTo & RegExpFilter.APPLY_TITLE)>0);
			case 5 : return ((regExpFilters.get(rowIndex).applyTo & RegExpFilter.APPLY_BODY)>0);
			case 6 : return (regExpFilters.get(rowIndex).isValid())?okIcon:errorIcon;
		}
		return null;
	}

	public String getToolTipText(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 6 : return formatTooltipTextToHTML(regExpFilters.get(rowIndex).getRegExpErrorMsg());
		}
		return null;
	}


	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return isValid && (columnIndex!=6);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0 : regExpFilters.get(rowIndex).setRegExp((String) aValue); break;
			case 1 : regExpFilters.get(rowIndex).score= (Double) aValue; break;
			case 2 : regExpFilters.get(rowIndex).description = (String) aValue; break;
			case 3 : regExpFilters.get(rowIndex).multicatch = (Boolean) aValue; break;
			case 4 : regExpFilters.get(rowIndex).updateApplyStatus(RegExpFilter.APPLY_TITLE, (Boolean) aValue); break;
			case 5 : regExpFilters.get(rowIndex).updateApplyStatus(RegExpFilter.APPLY_BODY, (Boolean) aValue); break;
		}
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	//
	// Colonnes
	//

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnTypes[columnIndex];
	}

	//
	// Manipulation des donn�es
	//

	public void addFilter() {
		if (!isValid) return;
		regExpFilters.add(new RegExpFilter());
		fireTableRowsInserted(regExpFilters.size()-1, regExpFilters.size()-1);
	}

	public void removeFilter(int row) {
		if (!isValid) return;
		regExpFilters.remove(row);
		fireTableRowsInserted(regExpFilters.size()-1, regExpFilters.size()-1);
	}

	//
	// utilitaires
	//
	private static String formatTooltipTextToHTML(String string) {
		return "<html><code>"+string.replace("\n", "<br>").replace(" ","&nbsp;")+"</html>";
	}

}
