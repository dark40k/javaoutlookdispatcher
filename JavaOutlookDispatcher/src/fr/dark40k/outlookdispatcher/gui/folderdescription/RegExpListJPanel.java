package fr.dark40k.outlookdispatcher.gui.folderdescription;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.RegExpFilter;

public class RegExpListJPanel extends JPanel {

	protected JPanel panel_1;
	protected JButton button;
	protected JButton button_2;

	protected RegExpTableModel tableModel;
	protected JScrollPane scrollPane;
	protected JTable table;

	public RegExpListJPanel() {

		tableModel = new RegExpTableModel();

		initGUI();

	}

	public void setRexExpFilters(ArrayList<RegExpFilter> regExpFilters) {
		tableModel.setRegExpFilters(regExpFilters);
	}

	private void addFilter() {
		tableModel.addFilter();
	}

	private void removeFilter() {
		int row = table.getSelectedRow();
		if (row<0) return;
		tableModel.removeFilter(table.convertRowIndexToModel(row));
	}


	private void initGUI() {
		setBorder(new TitledBorder(null, "Liste des Expressions Regulieres", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BorderLayout(0, 0));

		panel_1 = new JPanel();
		add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		button = new JButton("+");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFilter();
			}
		});
		panel_1.add(button);

		button_2 = new JButton("-");
		button_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeFilter();
			}
		});
		panel_1.add(button_2);

		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		table = new JTable(tableModel) {

            //Implement table cell tool tips.
            @Override
			public String getToolTipText(MouseEvent e) {

                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = table.convertRowIndexToModel(rowAtPoint(p));
                int colIndex = table.convertRowIndexToModel(columnAtPoint(p));

                tip = tableModel.getToolTipText(rowIndex, colIndex);

                return tip;
            }

		};
		scrollPane.setViewportView(table);

		table.getColumnModel().getColumn(0).setPreferredWidth(250);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(1).setMinWidth(50);
		table.getColumnModel().getColumn(2).setMinWidth(50);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(4).setPreferredWidth(50);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(5).setPreferredWidth(50);
		table.getColumnModel().getColumn(6).setResizable(false);
		table.getColumnModel().getColumn(6).setPreferredWidth(50);

	}

}
