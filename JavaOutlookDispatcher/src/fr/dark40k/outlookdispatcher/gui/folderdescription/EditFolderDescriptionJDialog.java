package fr.dark40k.outlookdispatcher.gui.folderdescription;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderData;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.VerticalScrollablePanel;

public class EditFolderDescriptionJDialog extends JDialog {

	//
	// Fast access command
	//

	public static void editDescription(OlFolderEMail olFolderEmail) {

		EditFolderDescriptionJDialog dialog = new EditFolderDescriptionJDialog(olFolderEmail);

		dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);

		dialog.setVisible(true);

	}

	//
	// Jdialog
	//

	private final VerticalScrollablePanel contentPanel = new VerticalScrollablePanel();


	protected JTextField nameTextField;

	protected JTextArea keywordsTextField;

	protected JCheckBox chckbxExcludeTitle;
	protected JCheckBox chckbxExcludeProjects;
	protected JCheckBox chckbxExcludeEmailAddresses;

	protected JTextArea projectsTextField;
	protected JTextArea emailsTextField;
	protected JTextArea conversationsTextField;

	protected JTextArea rawDescriptionTextArea;

	protected JTabbedPane tabbedPane;

	protected RegExpListJPanel regExpListJPanel = new RegExpListJPanel();

	//
	// Donn�es
	//

	private final OlFolderEMail olFolderEmail;

	private OlFolderData olFolderData;

	//
	// Constructor
	//

	public EditFolderDescriptionJDialog(OlFolderEMail olFolderEmail) {

		this.olFolderEmail=olFolderEmail;

		initGUI();

		resetChanges();

	}

	//
	// Commands
	//

	protected void doCancel() {
		setVisible(false);
	}

	protected void doOk() {

		switch (tabbedPane.getSelectedIndex()) {
			case 0:
				applyParameters();
				break;
			case 1:
				olFolderEmail.setDescription(rawDescriptionTextArea.getText());
				break;
		}

		setVisible(false);

	}

	protected void doDefault() {
		resetChanges();
	}

	protected void doAuto() {
		EMailAutoDispatcher.updateFolderDefinition(olFolderEmail);
		resetChanges();
	}

	//
	// Utilities
	//

	public void resetChanges() {

		// Recupere les donnees du repertoire
		olFolderData = OlFolderDataUtility.load(olFolderEmail);

		// affiche le nom du folder
		nameTextField.setText(olFolderEmail.getName());

		// liste des mots clefs + selection titre
		keywordsTextField.setText(olFolderData.getKeywords().list);
		chckbxExcludeTitle.setSelected(olFolderData.getKeywords().excludeTitle);
		chckbxExcludeProjects.setSelected(olFolderData.getInactiveAutoKeywords());
		chckbxExcludeEmailAddresses.setSelected(olFolderData.getInactiveEmailAdresses());

		// emails et conversations
		projectsTextField.setText(olFolderData.getAutoKeywords().exportString());
		emailsTextField.setText(olFolderData.getEmailAdresses().exportString());
		conversationsTextField.setText(olFolderData.getConversationIds().exportString());

		// regExps
		rawDescriptionTextArea.setText(olFolderEmail.getDescription());

		// Transfere les donn�es au panneau
		regExpListJPanel.setRexExpFilters(olFolderData.getRegExpFilters());

	}

	public void applyParameters() {

		if (!olFolderEmail.getName().equals(nameTextField.getText())) olFolderEmail.setName(nameTextField.getText());

		olFolderData.getKeywords().list=keywordsTextField.getText();
		olFolderData.getKeywords().excludeTitle=chckbxExcludeTitle.isSelected();
		olFolderData.setInactiveAutoKeywords(chckbxExcludeProjects.isSelected());
		olFolderData.setInactiveEmailAdresses(chckbxExcludeEmailAddresses.isSelected());

		olFolderData.getAutoKeywords().importString(projectsTextField.getText());
		olFolderData.getEmailAdresses().importString(emailsTextField.getText());
		olFolderData.getConversationIds().importString(conversationsTextField.getText());

		OlFolderDataUtility.save(olFolderData, olFolderEmail);

	}

	//
	// GUI
	//

	private void initGUI() {

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		regExpListJPanel.scrollPane.setPreferredSize(new Dimension(452, 150));

		setTitle("Edit folder :" + olFolderEmail.getName());

		setBounds(100, 100, 777, 581);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[] { 0, 0, 312, 47, 65, 0 };
			gbl_buttonPane.rowHeights = new int[] { 23, 0 };
			gbl_buttonPane.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
			gbl_buttonPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnDefault = new JButton("Default");
				btnDefault.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doDefault();
					}
				});
				GridBagConstraints gbc_btnDefault = new GridBagConstraints();
				gbc_btnDefault.anchor = GridBagConstraints.WEST;
				gbc_btnDefault.insets = new Insets(0, 0, 0, 5);
				gbc_btnDefault.gridx = 0;
				gbc_btnDefault.gridy = 0;
				buttonPane.add(btnDefault, gbc_btnDefault);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doOk();
					}
				});
				{
					JButton btnAuto = new JButton("Calculate");
					btnAuto.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doAuto();
						}
					});
					GridBagConstraints gbc_btnAuto = new GridBagConstraints();
					gbc_btnAuto.insets = new Insets(0, 0, 0, 5);
					gbc_btnAuto.gridx = 1;
					gbc_btnAuto.gridy = 0;
					buttonPane.add(btnAuto, gbc_btnAuto);
				}
				GridBagConstraints gbc_okButton = new GridBagConstraints();
				gbc_okButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_okButton.insets = new Insets(0, 0, 0, 5);
				gbc_okButton.gridx = 3;
				gbc_okButton.gridy = 0;
				buttonPane.add(okButton, gbc_okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				GridBagConstraints gbc_cancelButton = new GridBagConstraints();
				gbc_cancelButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_cancelButton.gridx = 4;
				gbc_cancelButton.gridy = 0;
				buttonPane.add(cancelButton, gbc_cancelButton);
			}
		}
		{
			tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			getContentPane().add(tabbedPane, BorderLayout.CENTER);
			{
				JPanel panel = new JPanel();
				tabbedPane.addTab("Param\u00E8tres", null, panel, null);
				panel.setLayout(new BorderLayout(0, 0));
				{
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
					panel.add(scrollPane, BorderLayout.CENTER);
					{
						scrollPane.setViewportView(contentPanel);
						GridBagLayout gbl_contentPanel = new GridBagLayout();
						gbl_contentPanel.columnWidths = new int[] { 0, 0, 0, 0 };
						gbl_contentPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
						gbl_contentPanel.columnWeights = new double[] { 1.0, 1.0, 0.0, Double.MIN_VALUE };
						gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
						contentPanel.setLayout(gbl_contentPanel);
						{
							JLabel lblNom = new JLabel("Nom repertoire :");
							GridBagConstraints gbc_lblNom = new GridBagConstraints();
							gbc_lblNom.anchor = GridBagConstraints.EAST;
							gbc_lblNom.insets = new Insets(0, 0, 5, 5);
							gbc_lblNom.gridx = 0;
							gbc_lblNom.gridy = 0;
							contentPanel.add(lblNom, gbc_lblNom);
						}
						{
							nameTextField = new JTextField();
							GridBagConstraints gbc_nameTextField = new GridBagConstraints();
							gbc_nameTextField.insets = new Insets(0, 0, 5, 5);
							gbc_nameTextField.fill = GridBagConstraints.HORIZONTAL;
							gbc_nameTextField.gridx = 1;
							gbc_nameTextField.gridy = 0;
							contentPanel.add(nameTextField, gbc_nameTextField);
							nameTextField.setColumns(10);
						}
						{
							chckbxExcludeTitle = new JCheckBox("Exclus");
							chckbxExcludeTitle.setToolTipText("Coch\u00E9 : Ne prends pas en compte le nom du repertoire pour les mots clefs.");
							GridBagConstraints gbc_chckbxAdd = new GridBagConstraints();
							gbc_chckbxAdd.anchor = GridBagConstraints.NORTHWEST;
							gbc_chckbxAdd.insets = new Insets(0, 0, 5, 0);
							gbc_chckbxAdd.gridx = 2;
							gbc_chckbxAdd.gridy = 0;
							contentPanel.add(chckbxExcludeTitle, gbc_chckbxAdd);
						}
						{
							JLabel lblFilter = new JLabel("Mots clefs manuels :");
							GridBagConstraints gbc_lblFilter = new GridBagConstraints();
							gbc_lblFilter.insets = new Insets(5, 0, 5, 5);
							gbc_lblFilter.anchor = GridBagConstraints.NORTHEAST;
							gbc_lblFilter.gridx = 0;
							gbc_lblFilter.gridy = 1;
							contentPanel.add(lblFilter, gbc_lblFilter);
						}
						{
							keywordsTextField = new JTextArea();
							keywordsTextField.setLineWrap(true);
							GridBagConstraints gbc_filterTextField = new GridBagConstraints();
							gbc_filterTextField.insets = new Insets(0, 0, 5, 5);
							gbc_filterTextField.fill = GridBagConstraints.HORIZONTAL;
							gbc_filterTextField.gridx = 1;
							gbc_filterTextField.gridy = 1;
							contentPanel.add(keywordsTextField, gbc_filterTextField);
							keywordsTextField.setColumns(10);
						}
						{
							JLabel lblProjets = new JLabel("Mots clefs auto. :");
							GridBagConstraints gbc_lblProjets = new GridBagConstraints();
							gbc_lblProjets.anchor = GridBagConstraints.NORTHEAST;
							gbc_lblProjets.insets = new Insets(5, 0, 5, 5);
							gbc_lblProjets.gridx = 0;
							gbc_lblProjets.gridy = 2;
							contentPanel.add(lblProjets, gbc_lblProjets);
						}
						{
							projectsTextField = new JTextArea();
							projectsTextField.setLineWrap(true);
							projectsTextField.setColumns(10);
							GridBagConstraints gbc_projectsTextField = new GridBagConstraints();
							gbc_projectsTextField.insets = new Insets(0, 0, 5, 5);
							gbc_projectsTextField.fill = GridBagConstraints.BOTH;
							gbc_projectsTextField.gridx = 1;
							gbc_projectsTextField.gridy = 2;
							contentPanel.add(projectsTextField, gbc_projectsTextField);
						}
						{
							chckbxExcludeProjects = new JCheckBox("Exclus");
							chckbxExcludeProjects.setToolTipText("Coch\u00E9 : Desactive la recherche et la prise en compte des mots clefs automatiques.");
							GridBagConstraints gbc_chckbxExcludeProjects = new GridBagConstraints();
							gbc_chckbxExcludeProjects.anchor = GridBagConstraints.NORTHWEST;
							gbc_chckbxExcludeProjects.insets = new Insets(0, 0, 5, 0);
							gbc_chckbxExcludeProjects.gridx = 2;
							gbc_chckbxExcludeProjects.gridy = 2;
							contentPanel.add(chckbxExcludeProjects, gbc_chckbxExcludeProjects);
						}
						{
							JLabel lblEmails = new JLabel("Addr. Emails :");
							GridBagConstraints gbc_lblEmails = new GridBagConstraints();
							gbc_lblEmails.insets = new Insets(5, 0, 5, 5);
							gbc_lblEmails.anchor = GridBagConstraints.NORTHEAST;
							gbc_lblEmails.gridx = 0;
							gbc_lblEmails.gridy = 3;
							contentPanel.add(lblEmails, gbc_lblEmails);
						}
						{
							emailsTextField = new JTextArea();
							emailsTextField.setLineWrap(true);
							GridBagConstraints gbc_emailsTextField = new GridBagConstraints();
							gbc_emailsTextField.insets = new Insets(0, 0, 5, 5);
							gbc_emailsTextField.fill = GridBagConstraints.HORIZONTAL;
							gbc_emailsTextField.gridx = 1;
							gbc_emailsTextField.gridy = 3;
							contentPanel.add(emailsTextField, gbc_emailsTextField);
							emailsTextField.setColumns(10);
						}
						{
							chckbxExcludeEmailAddresses = new JCheckBox("Exclus");
							GridBagConstraints gbc_chckbxExcludeEmailAddresses = new GridBagConstraints();
							gbc_chckbxExcludeEmailAddresses.anchor = GridBagConstraints.WEST;
							gbc_chckbxExcludeEmailAddresses.insets = new Insets(0, 0, 5, 0);
							gbc_chckbxExcludeEmailAddresses.gridx = 2;
							gbc_chckbxExcludeEmailAddresses.gridy = 3;
							contentPanel.add(chckbxExcludeEmailAddresses, gbc_chckbxExcludeEmailAddresses);
						}
						{
							JLabel lblConversationsids = new JLabel("ConversationsIds :");
							GridBagConstraints gbc_lblConversationsids = new GridBagConstraints();
							gbc_lblConversationsids.insets = new Insets(5, 0, 5, 5);
							gbc_lblConversationsids.anchor = GridBagConstraints.NORTHEAST;
							gbc_lblConversationsids.gridx = 0;
							gbc_lblConversationsids.gridy = 4;
							contentPanel.add(lblConversationsids, gbc_lblConversationsids);
						}
						{
							conversationsTextField = new JTextArea();
							conversationsTextField.setLineWrap(true);
							GridBagConstraints gbc_conversationsTextField = new GridBagConstraints();
							gbc_conversationsTextField.insets = new Insets(0, 0, 5, 5);
							gbc_conversationsTextField.fill = GridBagConstraints.BOTH;
							gbc_conversationsTextField.gridx = 1;
							gbc_conversationsTextField.gridy = 4;
							contentPanel.add(conversationsTextField, gbc_conversationsTextField);
							conversationsTextField.setColumns(10);
						}
						{
							GridBagConstraints gbc_regExpListJPanel = new GridBagConstraints();
							gbc_regExpListJPanel.gridwidth = 3;
							gbc_regExpListJPanel.fill = GridBagConstraints.BOTH;
							gbc_regExpListJPanel.gridx = 0;
							gbc_regExpListJPanel.gridy = 5;
							contentPanel.add(regExpListJPanel, gbc_regExpListJPanel);
						}
					}
				}
			}
			{
				JPanel panel = new JPanel();
				tabbedPane.addTab("Donn\u00E9es brutes", null, panel, null);
				panel.setLayout(new BorderLayout(0, 0));
				{
					JScrollPane scrollPane = new JScrollPane();
					panel.add(scrollPane, BorderLayout.CENTER);
					{
						rawDescriptionTextArea = new JTextArea();
						rawDescriptionTextArea.setLineWrap(true);
						scrollPane.setViewportView(rawDescriptionTextArea);
					}
				}
			}
		}
	}
}
