package fr.dark40k.outlookdispatcher.gui.editdirectlink;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.directfilter.DirectFilteredDirList;

public class EditDirectLinksJDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private boolean applyValue;

	private final EditDirectLinkTableModel tableModel;
	private final JTable table;

	//
	// Fast access command
	//

	public static void showDialog(String title) {

		EditDirectLinksJDialog dialog = new EditDirectLinksJDialog();

		dialog.setTitle(title);


		dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		dialog.setVisible(true);

		if (!dialog.applyValue) return;

		dialog.tableModel.applyPendingModifications();

	}

	//
	// Constructor
	//

	private EditDirectLinksJDialog() {

		tableModel = new EditDirectLinkTableModel(new DirectFilteredDirList(EMailAutoDispatcher.properties.directFiltersParamGroup));
		table = new JTable(tableModel);

		initGUI();

	}

	//
	// Commands
	//

	private void addFilter() {
		tableModel.addFilter();
	}

	private void removeFilter() {
		int row = table.getSelectedRow();
		if (row<0) return;
		tableModel.removeFilter(table.convertRowIndexToModel(row));
	}

	private void doCancel() {
		applyValue=false;
		setVisible(false);
	}

	private void doOk() {
		applyValue=true;
		setVisible(false);
	}

	private void doDefault() {
		tableModel.clearPendingModifications();
	}

	//
	// Utilities
	//

	//
	// GUI
	//

	private void initGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setBounds(100, 100, 834, 475);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new TitledBorder(null, "Liste des param�tres (1 param�tre par ligne)", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane, BorderLayout.CENTER);
			{
				table.getColumnModel().getColumn(0).setMinWidth(50);

				table.getColumnModel().getColumn(1).setPreferredWidth(250);
				table.getColumnModel().getColumn(1).setMinWidth(50);

				table.getColumnModel().getColumn(2).setMinWidth(50);

				table.getColumnModel().getColumn(3).setMinWidth(50);

				table.getColumnModel().getColumn(4).setResizable(false);
				table.getColumnModel().getColumn(4).setPreferredWidth(50);
				table.getColumnModel().getColumn(5).setResizable(false);
				table.getColumnModel().getColumn(5).setPreferredWidth(50);
				scrollPane.setViewportView(table);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.EAST);
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				JButton btnAdd = new JButton("Add filter");
				btnAdd.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						addFilter();
					}
				});
				panel.add(btnAdd);
			}
			{
				JButton btnRemove = new JButton("Remove filter");
				btnRemove.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						removeFilter();
					}
				});
				panel.add(btnRemove);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{0, 312, 47, 65, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnDefault = new JButton("Default");
				btnDefault.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doDefault();
					}
				});
				GridBagConstraints gbc_btnDefault = new GridBagConstraints();
				gbc_btnDefault.anchor = GridBagConstraints.WEST;
				gbc_btnDefault.insets = new Insets(0, 0, 0, 5);
				gbc_btnDefault.gridx = 0;
				gbc_btnDefault.gridy = 0;
				buttonPane.add(btnDefault, gbc_btnDefault);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doOk();
					}
				});
				GridBagConstraints gbc_okButton = new GridBagConstraints();
				gbc_okButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_okButton.insets = new Insets(0, 0, 0, 5);
				gbc_okButton.gridx = 2;
				gbc_okButton.gridy = 0;
				buttonPane.add(okButton, gbc_okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				GridBagConstraints gbc_cancelButton = new GridBagConstraints();
				gbc_cancelButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_cancelButton.gridx = 3;
				gbc_cancelButton.gridy = 0;
				buttonPane.add(cancelButton, gbc_cancelButton);
			}
		}
	}

}
