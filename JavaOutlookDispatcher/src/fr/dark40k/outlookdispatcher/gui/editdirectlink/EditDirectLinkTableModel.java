package fr.dark40k.outlookdispatcher.gui.editdirectlink;

import javax.swing.table.AbstractTableModel;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.directfilter.DirectFilteredDirList;

public class EditDirectLinkTableModel extends AbstractTableModel {
	
	public final static String[] columnNames = {
			"FolderID", "Regexp filter", "Poids", "Desc.", "Title", "Body"
	};

	private final static Class<?>[] columnTypes = new Class<?>[] {
			String.class, String.class, Double.class, String.class, Boolean.class, Boolean.class
	};
	
	//
	// Donn�es
	//
	
	private DirectFilteredDirList filterList;	

	//
	// Constructeur
	//
	public EditDirectLinkTableModel(DirectFilteredDirList filterList) {
		this.filterList=filterList;
	}

	//
	// Manipulation de la table
	//
	@Override
	public int getRowCount() {
		return filterList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0 : return filterList.get(rowIndex).getFolderId();
			case 1 : return filterList.get(rowIndex).getRegExpFormula();
			case 2 : return filterList.get(rowIndex).getWeight();
			case 3 : return filterList.get(rowIndex).getDescription();
			case 4 : return filterList.get(rowIndex).isApplyOnTitle();
			case 5 : return filterList.get(rowIndex).isApplyOnBody();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0 : filterList.get(rowIndex).setFolderId((String) aValue); return;
			case 1 : filterList.get(rowIndex).setRegExpFormula((String) aValue); return;
			case 2 : filterList.get(rowIndex).setWeight((Double) aValue); return;
			case 3 : filterList.get(rowIndex).setDescription((String) aValue); return;
			case 5 : filterList.get(rowIndex).setApplyOnTitle((Boolean) aValue); return;
			case 6 : filterList.get(rowIndex).setApplyOnBody((Boolean) aValue); return;
		}
	}
	
	//
	// Colonnes
	//

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	};
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnTypes[columnIndex];
	}

	//
	// Manipulation des donn�es
	//
	
	public void clearPendingModifications() {
		filterList.clearPendingModifications();
		fireTableDataChanged();
	}

	public void applyPendingModifications() {
		filterList.applyPendingModifications();
	}

	public void addFilter() {
		filterList.addNew();
		fireTableRowsInserted(filterList.size()-1, filterList.size()-1);
	}

	public void removeFilter(int row) {
		filterList.remove(row);
		fireTableRowsInserted(filterList.size()-1, filterList.size()-1);
	}
	
	
	
}
