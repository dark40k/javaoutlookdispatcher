package fr.dark40k.outlookdispatcher.gui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateEvent;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase.UpdateListener;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailGroup;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.utilities.MoveToFolderHistory;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.GUIUtilities;

public class HistoryFolderAssignementJPanel extends JPanel implements ListSelectionListener, UpdateListener, MoveToFolderHistory.UpdateListener {

	private final MoveToFolderHistory moveToFolderHistory;

	private ToSortEmailGroup currentEmailDataGroup;

	private final DefaultListModel<FolderListItem> listModel;

	private final JList<FolderListItem> folderList;

    private class FolderListItem {
    	String title;
    	OlFolderEMail folder;
    	public FolderListItem(String title, OlFolderEMail folder) {
    		this.title=title;
    		this.folder=folder;
    	}
    	@Override
		public String toString() {
    		return title;
    	}
    }

	public HistoryFolderAssignementJPanel(MoveToFolderHistory moveToFolderHistory) {

		this.moveToFolderHistory=moveToFolderHistory;

		listModel=new DefaultListModel<FolderListItem>();

		folderList=new JList<FolderListItem>(listModel){
			@Override
			public String getToolTipText(MouseEvent event) {
			    Point p = event.getPoint();
			    int location = locationToIndex(p);
			    
			    FolderListItem item;
				try {
					item = listModel.getElementAt(location);
				} catch (ArrayIndexOutOfBoundsException e) {
					return "";
				}
			    		
			    if (item==null) return "";
			    
				return item.folder.getPath("/").toString();
			  }
		};

		initGUI();

		updateListModel();

		moveToFolderHistory.addUpdateListener(this);

	}

	public void updateListModel() {

		setEmailDataGroup(null);

		listModel.clear();
		for (OlFolderEMail potFolder : moveToFolderHistory)
			if (potFolder!=null)
				listModel.addElement(new FolderListItem(potFolder.getName(),potFolder));


		setEmailDataGroup(currentEmailDataGroup);
	}

	public void setEmailDataGroup(ToSortEmailGroup emailDataGroup) {

		if (emailDataGroup==null) {

			folderList.removeListSelectionListener(this);
			folderList.clearSelection();

			if (currentEmailDataGroup !=null)
				if (currentEmailDataGroup.getDispatchDatabase() != null)
					currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);

			currentEmailDataGroup=null;
			return;
		}

		if (currentEmailDataGroup !=null)
			if (currentEmailDataGroup.getDispatchDatabase()!=null)
				currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);

		folderList.removeListSelectionListener(this);
		folderList.clearSelection();

		currentEmailDataGroup=emailDataGroup;

		updateSelection();

		if (currentEmailDataGroup.getDispatchDatabase()!=null)
			currentEmailDataGroup.getDispatchDatabase().addUpdateListener(this);

	}

	private void updateSelection() {

		if (currentEmailDataGroup==null) return;

		String comonDestinationFolderId = currentEmailDataGroup.getComonDestFolderId();

		// Pas de repertoire commun
		if (comonDestinationFolderId==null) {
			folderList.removeListSelectionListener(this);
			folderList.clearSelection();
			folderList.addListSelectionListener(this);
			return;
		}

		OlFolderEMail comonFolder = (comonDestinationFolderId.equals(ToSortEmailGroup.NODESTINATIONFOLDERID)) ? null : (OlFolderEMail) OlConnector.getOlObject(comonDestinationFolderId);

		// la selection est bonne => on touche a rien
		if (folderList.getSelectedIndex()>=0)
			if (comonFolder==folderList.getSelectedValue().folder)
				return;

		// Repertoire commun => verifie s'il est dans la liste
		for (int index=0; index<listModel.size(); index++)
			if (comonFolder==listModel.get(index).folder) {

				folderList.removeListSelectionListener(this);
				folderList.setSelectedIndex(index);
				folderList.addListSelectionListener(this);

				// Desactivé : penible car modifie la position de l'afficheur
				// folderList.ensureIndexIsVisible(index);

				return;
			}


		// Repertoire commun pas dans la liste
		folderList.removeListSelectionListener(this);
		folderList.clearSelection();
		folderList.addListSelectionListener(this);

	}

	private void initGUI() {

		setLayout(new BorderLayout(0, 0));
		folderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		add(folderList, BorderLayout.CENTER);

	}

	//
	// Selection listener
	//

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (currentEmailDataGroup==null) return;
		FolderListItem listItem = folderList.getSelectedValue();
		OlFolderEMail folder = listItem!=null?listItem.folder:null;

		currentEmailDataGroup.getDispatchDatabase().removeUpdateListener(this);
		currentEmailDataGroup.setDestFolder(folder,true);
		currentEmailDataGroup.getDispatchDatabase().addUpdateListener(this);
	}


	//
	// Runnable tasks
	//

	private final Runnable runUpdateSelection = new Runnable() {
		@Override
		public void run() {
			updateSelection();
		}
	};

	private final Runnable runClearEmailDataGroup = new Runnable() {
		@Override
		public void run() {
			setEmailDataGroup(null);
		}
	};

	private final Runnable runUpdateListModel = new Runnable() {
		@Override
		public void run() {
			updateListModel();
		}
	};

	//
	// Database listeners
	//

	@Override
	public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData) {
		GUIUtilities.runEDT(runUpdateSelection);
	}

	@Override
	public void structureChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(runUpdateSelection);
	}

	@Override
	public void fullDatabaseChanged(UpdateEvent evt) {
		GUIUtilities.runEDT(runClearEmailDataGroup);
	}

	//
	// History listeners
	//

	@Override
	public void historyChanged(MoveToFolderHistory.UpdateEvent evt) {
		GUIUtilities.runEDT(runUpdateListModel);
	}


}
