package fr.dark40k.outlookdispatcher.gui.folderlist;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.table.AbstractTableModel;

import fr.dark40k.outlookdispatcher.Dispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderData;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class FolderListTableModel extends AbstractTableModel {

	private final ArrayList<FolderData> folderList = new  ArrayList<FolderData>();

	private final ArrayList<String> permanentFolderList = new  ArrayList<String>();


	// =========================================================================================
	//
	// Constructeur
	//
	// =========================================================================================

	public FolderListTableModel() {	}

	// =========================================================================================
	//
	// Methodes
	//
	// =========================================================================================

	public void reset(OlFolder olRootFolderIn) {

		// reinitialise la table permanente
		permanentFolderList.clear();
		for (String folderLink : Dispatcher.properties.permanentDestinationAddresses )
			permanentFolderList.add(folderLink);


		// vide la table des repertoires
		folderList.clear();

		// rempli la table des repertoires avec les enfants
		ArrayDeque<OlFolder> scanPile = new ArrayDeque<OlFolder>();
		scanPile.add(olRootFolderIn);
		while (!scanPile.isEmpty()) {

			OlFolder olFolder = scanPile.pollFirst();

			for (OlFolder subFolder : olFolder.getSubFolders())
				scanPile.add(subFolder);

			if (olFolder instanceof OlFolderEMail)
				folderList.add(new FolderData((OlFolderEMail) olFolder));

		}

		// signale la mise � jours de la table
		fireTableDataChanged();

	}

	public void applyChanges() {

		ArrayList<String> scanFolderList = new ArrayList<String>();
		ArrayList<String> exFolderList = new ArrayList<String>();
		ArrayList<String> exRecFolderList = new ArrayList<String>();

		for (FolderData folderData : folderList ) {

			if (folderData.scanFolder) scanFolderList.add(folderData.getEntryID());

			if (folderData.excludedFolder) exFolderList.add(folderData.getEntryID());

			if (folderData.recExcludedFolder) exRecFolderList.add(folderData.getEntryID());

			boolean save = false;

			if (folderData.titleExcluded != folderData.olFolderData.getKeywords().excludeTitle) {
				folderData.olFolderData.getKeywords().excludeTitle=folderData.titleExcluded;
				save=true;
			}

			if (folderData.inactiveAutoKeywords != folderData.olFolderData.getInactiveAutoKeywords()) {
				folderData.olFolderData.setInactiveAutoKeywords(folderData.inactiveAutoKeywords);
				save=true;
			}

			if (save)
				OlFolderDataUtility.save(folderData.olFolderData, folderData.olFolderEmail);

		}

		DispatchDatabase.properties.scanDirectories.set(scanFolderList);
		DispatchDatabase.properties.excludedDirectories.set(exFolderList);
		DispatchDatabase.properties.excludedRecDirectories.set(exRecFolderList);
		Dispatcher.properties.permanentDestinationAddresses.set(permanentFolderList);

	}


	public FolderData getFolderData(int row) {
		return folderList.get(row);
	}


	// =========================================================================================
	//
	// Table model interface
	//
	// =========================================================================================

	@Override
	public int getRowCount() {
		return folderList.size();
	}

	@Override
	public int getColumnCount() {
		return FolderListTableColumns.getColumnCount();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return FolderListTableColumns.getColumn(columnIndex).getHeaderDisplayName();
	}

	public String getToolTipText(int row, int col) {
		return FolderListTableColumns.getColumn(col).getToolTipText(getFolderData(row));
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return FolderListTableColumns.getColumn(columnIndex).getColumnClass();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return FolderListTableColumns.getColumn(columnIndex).isEditable(getFolderData(rowIndex));
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return FolderListTableColumns.getColumn(columnIndex).getColumnValue(getFolderData(rowIndex));
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		FolderListTableColumns.getColumn(columnIndex).setValue(getFolderData(rowIndex),aValue);
	}

	// =========================================================================================
	//
	// Classe des donn�es
	//
	// =========================================================================================

	public class FolderData {

		private String levelName;
		private OlFolderEMail olFolderEmail;
		private OlFolderData olFolderData;

		private boolean scanFolder;
		private boolean excludedFolder;
		private boolean recExcludedFolder;
		private boolean titleExcluded;
		private boolean inactiveAutoKeywords;

		public FolderData(OlFolderEMail olFolderEmail) {

			this.olFolderEmail=olFolderEmail;
			olFolderData = OlFolderDataUtility.load(olFolderEmail);

			levelName = "";
			OlFolder parent = olFolderEmail.getParentFolder();
			while (parent!=null) {
				parent = parent.getParentFolder();
				levelName+="-> ";
			}
			levelName += olFolderEmail.getName();

			scanFolder = DispatchDatabase.properties.scanDirectories.contains(olFolderEmail.getEntryID());
			excludedFolder = DispatchDatabase.properties.excludedDirectories.contains(olFolderEmail.getEntryID());
			recExcludedFolder = DispatchDatabase.properties.excludedRecDirectories.contains(olFolderEmail.getEntryID());
			titleExcluded = olFolderData.getKeywords().excludeTitle;
			inactiveAutoKeywords = olFolderData.getInactiveAutoKeywords();

		}

		//
		// Acces lecture
		//

		public OlFolderEMail getOlFolderEMail() {
			return olFolderEmail;
		}

		public String getEntryID() {
			return olFolderEmail.getEntryID();
		}

		public String getLevelName() {
			return levelName;
		}

		public String getPath() {
			return olFolderEmail.getPath("/").toString();
		}

		public boolean isScannedFolder() {
			return scanFolder;
		}

		public boolean isExcludedFolder() {
			return excludedFolder;
		}

		public boolean isRecExcludedFolder() {
			return recExcludedFolder;
		}

		public boolean isTitleExcluded() {
			return titleExcluded;
		}

		public boolean isInactiveAutoKeywords() {
			return inactiveAutoKeywords;
		}

		public String getAutoKeywords() {
			return olFolderData.getAutoKeywords().exportString();
		}

		public String getName() {
			return olFolderEmail.getName();
		}

		public String getDescription() {
			return olFolderEmail.getDescription();
		}

		public String getKeywords() {
			return olFolderData.getKeywords().list;
		}

		public String getEmails() {
			return olFolderData.getEmailAdresses().exportString();
		}

		public String getConversationIds() {
			return olFolderData.getConversationIds().exportString();
		}

		public int getRegExpCount() {
			return olFolderData.getRegExpFilters().size();
		}

		public int getPermanentLinkPosition() {
			return permanentFolderList.indexOf(olFolderEmail.getEntryID());
		}

		//
		// Acces ecriture
		//

		public void setScanFolder(boolean scanFolder) {
			this.scanFolder = scanFolder;
			fireTableDataChanged();
		}

		public void setExcludedFolder(boolean excludedFolder) {
			this.excludedFolder = excludedFolder;
			fireTableDataChanged();
		}

		public void setRecExcludedFolder(boolean recFolderExcluded) {
			this.recExcludedFolder = recFolderExcluded;
			fireTableDataChanged();
		}

		public void setTitleExcluded(boolean titleExcluded) {
			this.titleExcluded = titleExcluded;
			fireTableDataChanged();
		}

		public void setInactiveAutoKeywords(boolean inactiveAutoKeywords) {
			this.inactiveAutoKeywords = inactiveAutoKeywords;
			fireTableDataChanged();
		}

		public void permFolderAdd() {
			permanentFolderList.add(olFolderEmail.getEntryID());
			fireTableDataChanged();
		}

		public void permFolderRemove() {
			permanentFolderList.remove(olFolderEmail.getEntryID());
			fireTableDataChanged();
		}

		public void permFolderUp() {
			if (permanentFolderList.size()<=1) return;
			int index = permanentFolderList.indexOf(olFolderEmail.getEntryID());
			if (index<=0) return;
			Collections.swap(permanentFolderList, index-1, index);
			fireTableDataChanged();
		}

		public void permFolderDown() {
			if (permanentFolderList.size()<=1) return;
			int index = permanentFolderList.indexOf(olFolderEmail.getEntryID());
			if (index>=(permanentFolderList.size()-1)) return;
			Collections.swap(permanentFolderList, index, index+1);
			fireTableDataChanged();
		}

	}

}
