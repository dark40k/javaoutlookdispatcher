package fr.dark40k.outlookdispatcher.gui.folderlist;

import java.awt.Component;
import java.awt.Font;
import java.util.function.Consumer;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fr.dark40k.outlookdispatcher.gui.folderlist.FolderListTableColumns.FolderListTableColumn;
import fr.dark40k.outlookdispatcher.gui.folderlist.FolderListTableModel.FolderData;
import fr.dark40k.outlookdispatcher.util.EnhancedJTableHeader;

public class FolderListTable extends JTable {

	public FolderListTable(FolderListTableModel tableModel) {
		super(tableModel);

		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		// Header des colones
		EnhancedJTableHeader enhancedTableHeader = new EnhancedJTableHeader(getColumnModel(), this);
		for (FolderListTableColumn column : FolderListTableColumns.getIterable())
			enhancedTableHeader.setToolTipForColumn(column.getColumn(), column.getHeaderDescription());
		setTableHeader(enhancedTableHeader);

		// Tri des colonnes
		setAutoCreateRowSorter(true);
		getRowSorter().setSortKeys(FolderListTableColumns.getDefaultSortKeys());

		// initialisation de la largeur des colonnes
		for (int col=0; col<getColumnCount(); col++)
			getColumnModel().getColumn(col).setPreferredWidth(FolderListTableColumns.getColumn(col).getPreferredWidth());

//		// correction comportement anormaux (peut etre plus utile)
//		putClientProperty("terminateEditOnFocusLost", true);

		// intialisation affichage
		setFont(new Font(getFont().getFontName(), getFont().getStyle(), 12));
		setRowHeight(18);

	}

	public FolderData[] getSelectedRowsFolderData() {
		int[] selRows = getSelectedRows();
		FolderData[] folderDataRows = new FolderData[selRows.length];
		for (int index = 0; index < selRows.length; index++) folderDataRows[index]=((FolderListTableModel)dataModel).getFolderData(convertRowIndexToModel(selRows[index]));
		return folderDataRows;
	}

	public Integer[] getModelSelectedRows() {
		int[] selRows = getSelectedRows();
		Integer[] modelRows = new Integer[selRows.length];
		for (int index = 0; index < selRows.length; index++) modelRows[index]=convertRowIndexToModel(selRows[index]);
		return modelRows;
	}

	public void applyToSelection(Consumer<FolderData> apply) {
		if (getSelectedRowCount()<=0) return;
		int[] modelRows = getSelectedRows().clone();
		for (int index = 0; index < modelRows.length; index++)
			apply.accept(((FolderListTableModel)dataModel).getFolderData(convertRowIndexToModel(modelRows[index])));
		for (int index = 0; index < modelRows.length; index++)
			getSelectionModel().addSelectionInterval(modelRows[index], modelRows[index]);
	}

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {

		Component c = super.prepareRenderer(renderer, row, column);

		if (c instanceof JLabel) {

			JLabel label = (JLabel) c;
			FolderListTableColumn columnObject = FolderListTableColumns.getColumn(convertColumnIndexToModel(column));
			FolderData folderData = ((FolderListTableModel)dataModel).getFolderData(convertRowIndexToModel(row));

			label.setHorizontalAlignment(columnObject.getAlignement());
			label.setToolTipText(columnObject.getToolTipText(folderData));

		}

		return c;
	}
}
