package fr.dark40k.outlookdispatcher.gui.folderlist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class EditFolderListDialog extends JDialog {

	//
	// Fast access command
	//

	public static boolean showDialog(OlFolder olRootFolder) {

		EditFolderListDialog dialog = new EditFolderListDialog(olRootFolder);

		dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);

		dialog.setVisible(true);

		return dialog.appliedChanges;

	}

	//
	// Jdialog
	//

	private boolean appliedChanges;
	private OlFolder olRootFolder;

	protected final FolderListTableModel tableModel = new FolderListTableModel();
	protected final FolderListTable table = new FolderListTable(tableModel);

	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public EditFolderListDialog(OlFolder olRootFolder) {

		this.appliedChanges=false;
		this.olRootFolder=olRootFolder;

		initGUI();

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		doReset();

	}

	/**
	 * Commandes
	 */

	public void doReset() {
		tableModel.reset(olRootFolder);
	}

	public void doClose(boolean applyChanges) {

		this.appliedChanges=applyChanges;

		if (applyChanges) {
			tableModel.applyChanges();
		}

		setVisible(false);
	}

	public void doSetScanFolder(boolean status) {
		table.applyToSelection(folderData -> {folderData.setScanFolder(status);});
	}

	public void doSetExcludedFolder(boolean status) {
		table.applyToSelection(folderData -> {folderData.setExcludedFolder(status);});
	}

	public void doSetRecExcludedFolder(boolean status) {
		table.applyToSelection(folderData -> {folderData.setRecExcludedFolder(status);});
	}

	public void doSetTitleExcluded(boolean status) {
		table.applyToSelection(folderData -> {folderData.setTitleExcluded(status);});
	}

	private void doPermFolderAdd() {
		Integer[] rows = table.getModelSelectedRows();
		for (int row : rows) tableModel.getFolderData(row).permFolderAdd();
	}

	private void doPermFolderRemove() {
		Integer[] rows = table.getModelSelectedRows();
		Arrays.sort(rows,Collections.reverseOrder());
		for (int row : rows) tableModel.getFolderData(row).permFolderRemove();
	}

	private void doPermFolderUp() {
		tableModel.getFolderData(table.convertRowIndexToModel(table.getSelectedRow())).permFolderUp();
	}

	private void doPermFolderDown() {
		tableModel.getFolderData(table.convertRowIndexToModel(table.getSelectedRow())).permFolderDown();
	}

	private void doFolderSaveXML() {

		//
		// Selection du fichier
		//

		JFileChooser chooser= new JFileChooser();

		int choice = chooser.showOpenDialog(this);

		if (choice != JFileChooser.APPROVE_OPTION) return;

		File chosenFile = chooser.getSelectedFile();

		//
		// Export des donn�es
		//

		var list = new ArrayList<OlFolderEMail>();

		var selRows = table.getSelectedRowsFolderData();
		for (int index = 0; index < selRows.length; index++)
			list.add(selRows[index].getOlFolderEMail());


		OlFolderDataUtility.backupFolders(chosenFile,list);

	}

	private void doFolderLoadXML() {

		//
		// Selection du fichier
		//

		JFileChooser chooser= new JFileChooser();

		int choice = chooser.showOpenDialog(this);

		if (choice != JFileChooser.APPROVE_OPTION) return;

		File chosenFile = chooser.getSelectedFile();

		//
		// Import des donn�es
		//

		OlFolderDataUtility.restoreFolders(chosenFile);

	}

	/**
	 * Create the GUI.
	 */

	private void initGUI() {
		setBounds(100, 100, 1000, 760);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			final JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.EAST);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{100, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				final JPanel panel_General = new JPanel();
				panel_General.setBorder(new TitledBorder(null, "General", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				GridBagConstraints gbc_panel_General = new GridBagConstraints();
				gbc_panel_General.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_General.insets = new Insets(0, 0, 5, 0);
				gbc_panel_General.gridx = 0;
				gbc_panel_General.gridy = 0;
				panel.add(panel_General, gbc_panel_General);
				GridBagLayout gbl_panel_General = new GridBagLayout();
				gbl_panel_General.columnWidths = new int[]{71, 0};
				gbl_panel_General.rowHeights = new int[]{23, 0};
				gbl_panel_General.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_General.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				panel_General.setLayout(gbl_panel_General);
				final JButton button = new JButton("Refresh");
				GridBagConstraints gbc_button = new GridBagConstraints();
				gbc_button.fill = GridBagConstraints.HORIZONTAL;
				gbc_button.anchor = GridBagConstraints.NORTH;
				gbc_button.gridx = 0;
				gbc_button.gridy = 0;
				panel_General.add(button, gbc_button);
				button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doReset();
					}
				});
			}
			{
			}
			{
				final JPanel panel_Scan = new JPanel();
				panel_Scan.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Scan", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_Scan = new GridBagConstraints();
				gbc_panel_Scan.insets = new Insets(0, 0, 5, 0);
				gbc_panel_Scan.fill = GridBagConstraints.BOTH;
				gbc_panel_Scan.gridx = 0;
				gbc_panel_Scan.gridy = 1;
				panel.add(panel_Scan, gbc_panel_Scan);
				GridBagLayout gbl_panel_Scan = new GridBagLayout();
				gbl_panel_Scan.columnWidths = new int[]{57, 0};
				gbl_panel_Scan.rowHeights = new int[]{23, 23, 0};
				gbl_panel_Scan.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_Scan.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
				panel_Scan.setLayout(gbl_panel_Scan);
				{
					final JButton button = new JButton("Set");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetScanFolder(true);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.anchor = GridBagConstraints.NORTH;
					gbc_button.insets = new Insets(0, 0, 5, 0);
					gbc_button.gridx = 0;
					gbc_button.gridy = 0;
					panel_Scan.add(button, gbc_button);
				}
				{
					final JButton button = new JButton("Clear");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetScanFolder(false);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.anchor = GridBagConstraints.NORTH;
					gbc_button.gridx = 0;
					gbc_button.gridy = 1;
					panel_Scan.add(button, gbc_button);
				}
			}
			{
				final JPanel panel_FolderExcluded = new JPanel();
				panel_FolderExcluded.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Exclu", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_FolderExcluded = new GridBagConstraints();
				gbc_panel_FolderExcluded.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_FolderExcluded.insets = new Insets(0, 0, 5, 0);
				gbc_panel_FolderExcluded.gridx = 0;
				gbc_panel_FolderExcluded.gridy = 2;
				panel.add(panel_FolderExcluded, gbc_panel_FolderExcluded);
				GridBagLayout gbl_panel_FolderExcluded = new GridBagLayout();
				gbl_panel_FolderExcluded.columnWidths = new int[]{57, 0};
				gbl_panel_FolderExcluded.rowHeights = new int[]{23, 23, 0};
				gbl_panel_FolderExcluded.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_FolderExcluded.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
				panel_FolderExcluded.setLayout(gbl_panel_FolderExcluded);
				{
					final JButton button = new JButton("Set");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetExcludedFolder(true);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.anchor = GridBagConstraints.NORTH;
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.insets = new Insets(0, 0, 5, 0);
					gbc_button.gridx = 0;
					gbc_button.gridy = 0;
					panel_FolderExcluded.add(button, gbc_button);
				}
				{
					final JButton button = new JButton("Clear");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetExcludedFolder(false);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.anchor = GridBagConstraints.NORTH;
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.gridx = 0;
					gbc_button.gridy = 1;
					panel_FolderExcluded.add(button, gbc_button);
				}
			}
			{
				final JPanel panel_RecFolderExcluded = new JPanel();
				panel_RecFolderExcluded.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Rec. ex.", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_RecFolderExcluded = new GridBagConstraints();
				gbc_panel_RecFolderExcluded.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_RecFolderExcluded.insets = new Insets(0, 0, 5, 0);
				gbc_panel_RecFolderExcluded.gridx = 0;
				gbc_panel_RecFolderExcluded.gridy = 3;
				panel.add(panel_RecFolderExcluded, gbc_panel_RecFolderExcluded);
				GridBagLayout gbl_panel_RecFolderExcluded = new GridBagLayout();
				gbl_panel_RecFolderExcluded.columnWidths = new int[]{57, 0};
				gbl_panel_RecFolderExcluded.rowHeights = new int[]{23, 23, 0};
				gbl_panel_RecFolderExcluded.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_RecFolderExcluded.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
				panel_RecFolderExcluded.setLayout(gbl_panel_RecFolderExcluded);
				{
					final JButton button = new JButton("Set");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetRecExcludedFolder(true);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.insets = new Insets(0, 0, 5, 0);
					gbc_button.gridx = 0;
					gbc_button.gridy = 0;
					panel_RecFolderExcluded.add(button, gbc_button);
				}
				{
					final JButton button = new JButton("Clear");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetRecExcludedFolder(false);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.gridx = 0;
					gbc_button.gridy = 1;
					panel_RecFolderExcluded.add(button, gbc_button);
				}
			}
			{
				final JPanel panel_TitleExcluded = new JPanel();
				panel_TitleExcluded.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Titre ex.", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_TitleExcluded = new GridBagConstraints();
				gbc_panel_TitleExcluded.insets = new Insets(0, 0, 5, 0);
				gbc_panel_TitleExcluded.fill = GridBagConstraints.BOTH;
				gbc_panel_TitleExcluded.gridx = 0;
				gbc_panel_TitleExcluded.gridy = 4;
				panel.add(panel_TitleExcluded, gbc_panel_TitleExcluded);
				GridBagLayout gbl_panel_TitleExcluded = new GridBagLayout();
				gbl_panel_TitleExcluded.columnWidths = new int[]{57, 0};
				gbl_panel_TitleExcluded.rowHeights = new int[]{23, 23, 0};
				gbl_panel_TitleExcluded.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_TitleExcluded.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
				panel_TitleExcluded.setLayout(gbl_panel_TitleExcluded);
				{
					final JButton button = new JButton("Set");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetTitleExcluded(true);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.insets = new Insets(0, 0, 5, 0);
					gbc_button.gridx = 0;
					gbc_button.gridy = 0;
					panel_TitleExcluded.add(button, gbc_button);
				}
				{
					final JButton button = new JButton("Clear");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doSetTitleExcluded(false);
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.gridx = 0;
					gbc_button.gridy = 1;
					panel_TitleExcluded.add(button, gbc_button);
				}
			}
			{
				final JPanel panel_PermFolder = new JPanel();
				panel_PermFolder.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Rep. Permanent", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_PermFolder = new GridBagConstraints();
				gbc_panel_PermFolder.insets = new Insets(0, 0, 5, 0);
				gbc_panel_PermFolder.fill = GridBagConstraints.BOTH;
				gbc_panel_PermFolder.gridx = 0;
				gbc_panel_PermFolder.gridy = 5;
				panel.add(panel_PermFolder, gbc_panel_PermFolder);
				GridBagLayout gbl_panel_PermFolder = new GridBagLayout();
				gbl_panel_PermFolder.columnWidths = new int[]{57, 0};
				gbl_panel_PermFolder.rowHeights = new int[]{23, 23, 0, 0, 0};
				gbl_panel_PermFolder.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_PermFolder.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panel_PermFolder.setLayout(gbl_panel_PermFolder);
				{
					final JButton btnAdd = new JButton("Add");
					btnAdd.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doPermFolderAdd();
						}
					});
					GridBagConstraints gbc_btnAdd = new GridBagConstraints();
					gbc_btnAdd.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnAdd.insets = new Insets(0, 0, 5, 0);
					gbc_btnAdd.gridx = 0;
					gbc_btnAdd.gridy = 0;
					panel_PermFolder.add(btnAdd, gbc_btnAdd);
				}
				{
					final JButton btnRemove = new JButton("Remove");
					btnRemove.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doPermFolderRemove();
						}
					});
					GridBagConstraints gbc_btnRemove = new GridBagConstraints();
					gbc_btnRemove.insets = new Insets(0, 0, 5, 0);
					gbc_btnRemove.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnRemove.gridx = 0;
					gbc_btnRemove.gridy = 1;
					panel_PermFolder.add(btnRemove, gbc_btnRemove);
				}
				{
					final JButton btnUp = new JButton("Up");
					btnUp.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doPermFolderUp();
						}
					});
					GridBagConstraints gbc_btnUp = new GridBagConstraints();
					gbc_btnUp.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnUp.insets = new Insets(0, 0, 5, 0);
					gbc_btnUp.gridx = 0;
					gbc_btnUp.gridy = 2;
					panel_PermFolder.add(btnUp, gbc_btnUp);
				}
				{
					final JButton btnDown = new JButton("Down");
					btnDown.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doPermFolderDown();
						}
					});
					GridBagConstraints gbc_btnDown = new GridBagConstraints();
					gbc_btnDown.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnDown.gridx = 0;
					gbc_btnDown.gridy = 3;
					panel_PermFolder.add(btnDown, gbc_btnDown);
				}
			}
			{
				JPanel panel_Backup = new JPanel();
				panel_Backup.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Backup Folders", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				GridBagConstraints gbc_panel_Backup = new GridBagConstraints();
				gbc_panel_Backup.insets = new Insets(0, 0, 5, 0);
				gbc_panel_Backup.fill = GridBagConstraints.BOTH;
				gbc_panel_Backup.gridx = 0;
				gbc_panel_Backup.gridy = 6;
				panel.add(panel_Backup, gbc_panel_Backup);
				GridBagLayout gbl_panel_Backup = new GridBagLayout();
				gbl_panel_Backup.columnWidths = new int[]{57, 0};
				gbl_panel_Backup.rowHeights = new int[]{23, 23, 0};
				gbl_panel_Backup.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_Backup.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
				panel_Backup.setLayout(gbl_panel_Backup);
				{
					JButton button = new JButton("Load XML");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doFolderLoadXML();
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.insets = new Insets(0, 0, 5, 0);
					gbc_button.gridx = 0;
					gbc_button.gridy = 0;
					panel_Backup.add(button, gbc_button);
				}
				{
					JButton button = new JButton("Save XML");
					button.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							doFolderSaveXML();
						}
					});
					GridBagConstraints gbc_button = new GridBagConstraints();
					gbc_button.fill = GridBagConstraints.HORIZONTAL;
					gbc_button.gridx = 0;
					gbc_button.gridy = 1;
					panel_Backup.add(button, gbc_button);
				}
			}
		}
		{
			final JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane, BorderLayout.CENTER);
			{
				scrollPane.setViewportView(table);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Appliquer");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doClose(true);
					}
				});
				okButton.setActionCommand("Ok");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				final JButton cancelButton = new JButton("Annuler");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doClose(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
