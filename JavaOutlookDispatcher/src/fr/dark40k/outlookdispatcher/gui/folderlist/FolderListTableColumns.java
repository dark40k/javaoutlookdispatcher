package fr.dark40k.outlookdispatcher.gui.folderlist;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.TableCellEditor;

import fr.dark40k.outlookdispatcher.gui.folderlist.FolderListTableModel.FolderData;

public class FolderListTableColumns {

    private static ArrayList<FolderListTableColumn> columns = new ArrayList<FolderListTableColumn>();

    public static int getColumnCount() {
    	return columns.size();
    }

	public static List<SortKey> getDefaultSortKeys() {

		List<SortKey> newKeys = new ArrayList<SortKey>();

		for (int col = 0; col < columns.size(); col++)
			if (columns.get(col).defaultOrder != null)
				newKeys.add(new SortKey(col, columns.get(col).defaultOrder));

		return newKeys;
	}

    public static FolderListTableColumn getColumn(int index) {
    	return columns.get(index);
    }

    public static Iterable<FolderListTableColumn> getIterable() {
    	return columns;
    }

	static {

		columns.add(new FolderListTableColumn("Name", 300, "Nom du repertoire") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getLevelName();
			}
		});

		columns.add(new FolderListTableColumn("Scan", 50, "Rep. scann�") {
			@Override
			public Class<?> getColumnClass() {
				return Boolean.class;
			}

			@Override
			public Boolean getColumnValue(FolderData folderData) {
				return folderData.isScannedFolder();
			}

			@Override
			public boolean isEditable(FolderData folderData) {
				return true;
			}

			@Override
			public void setValue(FolderData folderData, Object value) {
				folderData.setScanFolder((boolean) value);;
			}
		});

		columns.add(new FolderListTableColumn("Exclu", 50, "Rep. Exclu") {
			@Override
			public Class<?> getColumnClass() {
				return Boolean.class;
			}

			@Override
			public Boolean getColumnValue(FolderData folderData) {
				return folderData.isExcludedFolder();
			}

			@Override
			public boolean isEditable(FolderData folderData) {
				return true;
			}

			@Override
			public void setValue(FolderData folderData, Object value) {
				folderData.setExcludedFolder((boolean) value);;
			}
		});

		columns.add(new FolderListTableColumn("Rec. ex.", 50, "Rep. Exclu recursivement") {
			@Override
			public Class<?> getColumnClass() {
				return Boolean.class;
			}

			@Override
			public Boolean getColumnValue(FolderData folderData) {
				return folderData.isRecExcludedFolder();
			}

			@Override
			public boolean isEditable(FolderData folderData) {
				return true;
			}

			@Override
			public void setValue(FolderData folderData, Object value) {
				folderData.setRecExcludedFolder((boolean) value);;
			}
		});

		columns.add(new FolderListTableColumn("Direct", 50, "Ordre du repertoire en acces direct.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public int getAlignement() {
				return JLabel.CENTER;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				int n = folderData.getPermanentLinkPosition();
				if (n<0) return "";
				return Integer.toString(n);
			}

			@Override
			public String getToolTipText(FolderData folderData) {
				return null;
			}

		});

		columns.add(new FolderListTableColumn("RegExp", 50, "Nombre de r�gles.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public int getAlignement() {
				return JLabel.CENTER;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				int n = folderData.getRegExpCount();
				if (n==0) return "";
				return Integer.toString(n);
			}

			@Override
			public String getToolTipText(FolderData folderData) {
				return null;
			}

		});

		columns.add(new FolderListTableColumn("Titre ex.", 50, "Titres exclus") {
			@Override
			public Class<?> getColumnClass() {
				return Boolean.class;
			}

			@Override
			public Boolean getColumnValue(FolderData folderData) {
				return folderData.isTitleExcluded();
			}

			@Override
			public boolean isEditable(FolderData folderData) {
				return true;
			}

			@Override
			public void setValue(FolderData folderData, Object value) {
				folderData.setTitleExcluded((boolean) value);;
			}
		});

		columns.add(new FolderListTableColumn("Mots clefs", 100, "Mots clefs additionnels au repertoire.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getKeywords();
			}

		});


		columns.add(new FolderListTableColumn("AutoKW ex.", 50, "Mots clefs automatiques exclus") {
			@Override
			public Class<?> getColumnClass() {
				return Boolean.class;
			}

			@Override
			public Boolean getColumnValue(FolderData folderData) {
				return folderData.isInactiveAutoKeywords();
			}

			@Override
			public boolean isEditable(FolderData folderData) {
				return true;
			}

			@Override
			public void setValue(FolderData folderData, Object value) {
				folderData.setInactiveAutoKeywords((boolean) value);;
			}
		});

		columns.add(new FolderListTableColumn("AutoKW", 100, "Mots clefs r�cup�r�s automatiquement.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getAutoKeywords();
			}

		});

		columns.add(new FolderListTableColumn("Emails", 100, "Emails.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getEmails();
			}

		});

		columns.add(new FolderListTableColumn("Conversations", 100, "Conversations.") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getConversationIds();
			}

		});

		columns.add(new FolderListTableColumn("Chemin", 200, "Chemin du repertoire",SortOrder.ASCENDING) {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getPath();
			}

		});

		columns.add(new FolderListTableColumn("Nom", 300, "Nom du repertoire") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getName();
			}
		});

		columns.add(new FolderListTableColumn("ID", 200, "ID du repertoire") {
			@Override
			public Class<?> getColumnClass() {
				return String.class;
			}

			@Override
			public String getColumnValue(FolderData folderData) {
				return folderData.getEntryID();
			}
		});

	}

	@SuppressWarnings("unused")
	public static abstract class FolderListTableColumn {

		//
		// Internal enum methods
		//
		private String headerName;
		private int width;
		private String headerDescription;
		private SortOrder defaultOrder;

		private FolderListTableColumn(String displayName, int width, String description) {
			this(displayName,width,description,null);
		}

		private FolderListTableColumn(String displayName, int width, String description, SortOrder defaultOrder) {
			this.defaultOrder = defaultOrder;
			this.headerName = displayName;
			this.width=width;
			this.headerDescription=description;
		}

		//
		// Parametres d'en-tete
		//

		public String getHeaderDisplayName() {
			return headerName;
		}

		public int getPreferredWidth() {
			return width;
		};

		public String getHeaderDescription() {
			return headerDescription;
		}


		//
		// Methodes de comportement de la colonne (� surcharger au cas par cas)
		//

		public abstract Class<?> getColumnClass();

		public abstract Object getColumnValue(FolderData folderData);

		public String getToolTipText(FolderData folderData) {
			Object o = getColumnValue(folderData);
			if (o==null) return null;
			return getColumnValue(folderData).toString();
		}

		public int getAlignement() {
			return JLabel.LEFT;
		}

		public boolean doApplyFont() {
			return false;
		}

		public int getColumn() {
			return columns.indexOf(this);
		}

		public boolean isEditable(FolderData folderData) {
			return false;
		}

		public TableCellEditor getColumnEditor() {
			return null;
		}

		public void setValue(FolderData folderData, Object value) {
			throw new RuntimeException("Column is not editable:" + headerDescription);
		}


	}

}
