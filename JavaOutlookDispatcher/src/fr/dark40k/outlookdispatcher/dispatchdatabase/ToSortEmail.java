package fr.dark40k.outlookdispatcher.dispatchdatabase;

import org.jdom2.Element;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public abstract class ToSortEmail {
	
	protected final DispatchDatabase dispatchDatabase;

	protected PotentialFoldersList potentialFoldersList = new PotentialFoldersList();

	//
	// Constructeur
	//
	
	ToSortEmail(DispatchDatabase dispatchDatabase) {
		this.dispatchDatabase = dispatchDatabase;
		potentialFoldersList = new PotentialFoldersList();
	}
	
	//
	// Import/Export XML
	//
	
	public ToSortEmail(DispatchDatabase dispatchDatabase, Element elt) {
		this.dispatchDatabase = dispatchDatabase;
		potentialFoldersList = new PotentialFoldersList(elt);
	}

	public Element exportToXML() {
		Element elt = new Element("EmailBaseData");
		elt.addContent(potentialFoldersList.exportToXML());
		return elt;
	}
	
	//
	// Getters/Setters
	//
	
	abstract public void setDestFolder(OlFolderEMail newFolder, boolean isManualDestintation);

	abstract public void setAutoDestFolder();
	abstract public void setManualDestFolder();

	public PotentialFoldersList getPotentialFoldersList() {
		return potentialFoldersList;
	}

	public DispatchDatabase getDispatchDatabase() {
		return this.dispatchDatabase;
	}

	abstract public Boolean isManualDest();

	
}