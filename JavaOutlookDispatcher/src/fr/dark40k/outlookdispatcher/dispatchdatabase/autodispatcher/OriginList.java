package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;

public class OriginList implements Iterable<OriginList.Origin> {

	private ArrayList<Origin> originList = new ArrayList<Origin>();

	//
	// Constructeur
	//

	public OriginList() {
	}

	//
	// Methodes d'acces
	//
	public void add(double newWeight, String newOrigin) {
		originList.add(new Origin(1.0,newOrigin,newWeight));
	}

	public void add(OriginList originList2) {
		add(originList2,1.0);
	}

	public void add(OriginList originList2, double coef) {
		for (Origin origin : originList2.originList)
			originList.add(new Origin(origin,coef));
	}

	@Override
	public Iterator<OriginList.Origin> iterator() {
		return originList.iterator();
	}

	//
	// Import/Export XML
	//

	public OriginList(Element fatherElt) {
		Element elt = fatherElt.getChild("OriginList");
		originList.clear();
		for (Element subElt : elt.getChildren())
			originList.add(new Origin(subElt));
	}

	public Element exportToXML() {
		Element elt = new Element("OriginList");
		for (Origin origin:originList)
			elt.addContent(origin.exportToXML());
		return elt;
	}

	//
	// Sous classe pour enregistrer les donn�es
	//

	public static class Origin {

		public double count;
		public String name;
		public double weight;

		//
		// Constructeur
		//

		public Origin(double count, String name, double weight) {

			if (name==null) throw new RuntimeException("name cannot be null");

			this.count=count;
			this.name=name;
			this.weight=weight;
		}

		public Origin(Origin origin, double coef) {
			this.count=origin.count*coef;
			this.name=origin.name;
			this.weight=origin.weight*coef;
		}

		//
		// Getters/Setters
		//

		public double getCount() {
			return count;
		}

//		public void setCount(double count) {
//			this.count = count;
//		}

		public String getName() {
			return name;
		}

//		public void setName(String name) {
//			this.name = name;
//		}

		public double getWeight() {
			return weight;
		}

//		public void setWeight(double weight) {
//			this.weight = weight;
//		}



		//
		// Import/Export XML
		//

		public Origin(Element subElt) {
			count = Double.parseDouble(subElt.getAttributeValue("count"));
			name = subElt.getAttributeValue("name");
			weight = Double.parseDouble(subElt.getAttributeValue("weight"));
		}

		public Element exportToXML() {
			Element elt = new Element("Origin");
			elt.setAttribute("count", Double.toString(count));
			elt.setAttribute("name", name);
			elt.setAttribute("weight", Double.toString(weight));
			return elt;
		}

		@Override
		public String toString() {
			return count+" x "+name+" ("+weight+")";
		}

		public void add(Origin origin) {
			count+=origin.count;
			weight+=origin.weight;
		}

	}

}
