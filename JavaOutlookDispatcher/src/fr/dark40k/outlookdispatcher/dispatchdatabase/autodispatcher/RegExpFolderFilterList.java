package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderData;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.RegExpFilter;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class RegExpFolderFilterList {

	private ArrayList<RegExpFolderFilter> regExpFolderFilters = new ArrayList<RegExpFolderFilter>();

	//
	// Edite la liste de filtres
	//

	public void clear() {
		regExpFolderFilters.clear();
	}

	public void addFolder(OlFolderEMail folderEmail, OlFolderData dispatcherData) {
		for ( RegExpFilter filter : dispatcherData.getRegExpFilters() )
			if (filter.isValid())
				regExpFolderFilters.add(new RegExpFolderFilter(filter, folderEmail));
	}

	//
	// Construit la r�ponse
	//

	public PotentialFoldersList buildPotentialFolderList(SimplifiedMail simplifiedMail) {

		PotentialFoldersList list = new PotentialFoldersList();

		for (RegExpFolderFilter filter : regExpFolderFilters)
			list.addWeightedDestFolder(filter.weightedDestFolder, filter.countMatches(simplifiedMail));

		return list;

	}

	//
	// Classe interne pour stocker les filtres
	//

	private static class RegExpFolderFilter {

		private WeightedDestFolder weightedDestFolder;

		public Pattern regExpPattern;

		public boolean applyOnTitle;
		public boolean applyOnBody;
		public boolean multicatch;

		public RegExpFolderFilter(RegExpFilter filter, OlFolderEMail folderEmail) {

			this.regExpPattern = filter.getPattern();

			weightedDestFolder = new WeightedDestFolder(folderEmail, filter.score, (filter.description!=null)?filter.description:filter.toString());

			this.multicatch=filter.multicatch;

			this.applyOnTitle = (filter.applyTo & RegExpFilter.APPLY_TITLE)>0;
			this.applyOnBody = (filter.applyTo & RegExpFilter.APPLY_BODY)>0;

		}

		public int countMatches(SimplifiedMail simplifiedMail) {

			int count = 0;

			if (applyOnTitle) {
				Matcher  matcher = regExpPattern.matcher(simplifiedMail.getRawTitle());
				if (multicatch) {
					while (matcher.find()) count++;
				} else {
					if (matcher.find()) count++;
				}
			}

			if (applyOnBody) {
				Matcher  matcher = regExpPattern.matcher(simplifiedMail.getRawBody());
				if (multicatch) {
					while (matcher.find()) count++;
				} else {
					if (matcher.find()) count++;
				}
			}

			return count;
		}

	}


}
