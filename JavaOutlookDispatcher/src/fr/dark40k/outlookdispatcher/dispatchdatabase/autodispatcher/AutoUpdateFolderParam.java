package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;


public class AutoUpdateFolderParam {

	public enum AutoUpdateFolderModeEnum {

		Recalculer("Recalculer"),
		Effacer("Effacer"),
		Laisser("Laisser");

		private String name;

		private AutoUpdateFolderModeEnum(String name) {
			this.name = name;
		}

		public static AutoUpdateFolderModeEnum valueOfByName(String name) {
			if (name != null)
				for (AutoUpdateFolderModeEnum testVal : values()) {
					if (testVal.name.equals(name)) {
					return testVal;
					}
				}
			throw new IllegalArgumentException("Ce nom ne correspond n'est pas valide. name=" + name);
		}

		public static String[] getNames() {
			AutoUpdateFolderModeEnum[] states = values();
		    String[] names = new String[states.length];

		    for (int i = 0; i < states.length; i++) {
		        names[i] = states[i].name();
		    }

		    return names;
		}

	}

	public AutoUpdateFolderModeEnum defaultAutoKeywordsBlock = AutoUpdateFolderModeEnum.Recalculer;
	public AutoUpdateFolderModeEnum defaultAdressBlock = AutoUpdateFolderModeEnum.Recalculer;
	public AutoUpdateFolderModeEnum defaultConversationBlock = AutoUpdateFolderModeEnum.Recalculer;

	public AutoUpdateFolderModeEnum excludedAutoKeywordsBlock = AutoUpdateFolderModeEnum.Recalculer;
	public AutoUpdateFolderModeEnum excludedAdressBlock = AutoUpdateFolderModeEnum.Recalculer;
	public AutoUpdateFolderModeEnum excludedConversationBlock = AutoUpdateFolderModeEnum.Recalculer;

	public AutoUpdateFolderModeEnum recExcludedAutoKeywordsBlock = AutoUpdateFolderModeEnum.Laisser;
	public AutoUpdateFolderModeEnum recExcludedAdressBlock = AutoUpdateFolderModeEnum.Laisser;
	public AutoUpdateFolderModeEnum recExcludedConversationBlock = AutoUpdateFolderModeEnum.Laisser;

}
