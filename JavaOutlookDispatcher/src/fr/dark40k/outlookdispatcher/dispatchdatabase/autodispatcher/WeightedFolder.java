package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class WeightedFolder {

	public OlFolderEMail folder;
	
	public double weight;
	
	public WeightedFolder(OlFolderEMail folder, double newWeight) {
		this.folder=folder;
		weight=newWeight;
	}
	
}
