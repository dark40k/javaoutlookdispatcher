package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import org.jdom2.Element;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class WeightedDestFolder implements Comparable<WeightedDestFolder>, Cloneable {

	public final static String ORIGIN_SEPARATOR=";";

	public OlFolderEMail folder;

	public double weight;

	private final OriginList originList;

	//
	// Constructeur
	//

	public WeightedDestFolder(OlFolderEMail folder, double newWeight, String origin) {
		
		if (folder==null) throw new RuntimeException("Folder is null");

		this.folder=folder;
		weight=newWeight;
		originList = new OriginList();
		originList.add(newWeight,origin);
	}

	public WeightedDestFolder(WeightedDestFolder potFolder, double coef) {
		folder=potFolder.folder;
		weight=potFolder.weight*coef;
		originList = new OriginList();
		originList.add(potFolder.originList,coef);
	}

	//
	// Import/Export XML
	//

	public WeightedDestFolder(Element elt) {
		folder = (OlFolderEMail) OlConnector.getOlObject(elt.getAttributeValue("folder"));
		weight = Double.parseDouble(elt.getAttributeValue("weight"));
		originList = new OriginList(elt);
	}

	public Element exportToXML() {
		Element elt = new Element("WeightedFolder");
		elt.setAttribute("folder",folder.getEntryID());
		elt.setAttribute("weight", Double.toString(weight));
		elt.addContent(originList.exportToXML());
		return elt;
	}

	//
	// Divers
	//

	public OriginList getOrigin() {
		return originList;
	}

	public void inc(double newWeight, String newOrigin) {
		weight+=newWeight;
		originList.add(newWeight,newOrigin);
	}

	public void inc(WeightedDestFolder weightedFolder) {
		weight+=weightedFolder.weight;
		originList.add(weightedFolder.originList);
	}

	public void inc(WeightedDestFolder weightedFolder, double coef) {
		weight+=weightedFolder.weight*coef;
		originList.add(weightedFolder.originList,coef);
	}

	@Override
	public boolean equals(Object object) {
		if (object==null) return false;
		if (!(object instanceof WeightedDestFolder)) return false;
		if (folder==null) return (((WeightedDestFolder) object).folder==null);
		return folder.equals(((WeightedDestFolder) object).folder);
	}

	@Override
	public int compareTo(WeightedDestFolder o) {
		return Double.compare(o.weight, weight);
	}

	@Override
	public WeightedDestFolder clone() {
		return new WeightedDestFolder(this,1.0);
	}

//	//
//	// Interne
//	//
//
//	private final static Pattern PARENTHESIS_PATTERN=Pattern.compile("(\\()([^(]*)(\\))");
//
//
//	private static String calcOrigin(double newWeight, String origin) {
//		return origin+" ("+newWeight+")";
//	}
//	private static String multiOrigin(String origin, double coef) {
//		return PARENTHESIS_PATTERN.matcher(origin).replaceAll("($2 x"+String.format("%1.2f", coef)+")");
//	}

}