package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.fields.ParamGroupField;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;
import fr.dark40k.util.persistantproperties.tags.PropertyAlias;
import fr.dark40k.util.persistantproperties.tags.PropertyRootClassAlias;

@PropertyRootClassAlias("autodispatcher")
public final class EMailAutoDispatcherProperties extends Property {

	@PropertyAlias("excludedWords")
	public final StringHashSetField excludedWordsParam = new StringHashSetField(new String[] { "pour", "for", "des", "from", "and", "with", "avec" });

	@PropertyAlias("excludedAdresses")
	public final StringHashSetField excludedAddressesParam = new StringHashSetField(new String[] { ".*@cedrat-tec.com$" });

//	@PropertyAlias("classifierModelFile")
//	public String classifierModelFileParam = "classifier.model";

	@PropertyAlias("classifierArffFile")
	public String classifierArffFileParam = "classifier.arff";

	@PropertyAlias("directFilters")
	public ParamGroupField directFiltersParamGroup = new ParamGroupField();

	@PropertyAlias("maxAdressesNbr")
	public Integer maxAdressesNbrParam = 0; //stocke toutes les adresses

	@PropertyAlias("maxConversationNbr")
	public Integer maxConversationNbrParam = 20;

	@PropertyAlias("conversationNote")
	public Double conversationNoteParam = 100000.0;

	@PropertyAlias("CTECCodeNote")
	public Double CTECCodeNoteParam = 10000.0;

	@PropertyAlias("emailAddressNote")
	public Double emailAddressNoteParam = 100.0;

	@PropertyAlias("wordTitleNote")
	public Double wordTitleNoteParam = 10.0;

	@PropertyAlias("bodyNoteCoeficient")
	public Double bodyCoeficientParam = 0.1;


}
