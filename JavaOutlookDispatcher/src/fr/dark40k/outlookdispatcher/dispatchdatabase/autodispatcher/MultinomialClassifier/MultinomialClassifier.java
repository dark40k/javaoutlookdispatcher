package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.MultinomialClassifier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.com.Dispatch;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.SimplifiedMail;
import fr.dark40k.outlookdispatcher.outlookconnector.DispUtil;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlObjectClassEnum;
import fr.dark40k.outlookdispatcher.util.ActiveDirectory;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomialUpdateable;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class MultinomialClassifier {

	private final static Logger LOGGER = LogManager.getLogger();

	private static final int WORDSTOKEEP = 1000000;

	// // nombre maximum d'adresses dans un mail pour le prendre en compte
	// private static final int MAX_ADRESSES_NBR = 10;

	/* The training data gathered so far. */
	private Instances m_Data = null;

	/* The filter used to generate the word counts. */
	private StringToWordVector m_Filter = new StringToWordVector();

	/* The actual classifier. */
	private Classifier m_Classifier = new NaiveBayesMultinomialUpdateable();

	/* Whether the model is up to date. */
	private boolean m_UpToDate;

	/**
	 * Constructs empty training dataset.
	 */
	public MultinomialClassifier() {

		// Set the tokenizer
		// NGramTokenizer tokenizer = new NGramTokenizer();
		// tokenizer.setNGramMinSize(1);
		// tokenizer.setNGramMaxSize(1);
		// tokenizer.setDelimiters("\\W");

		WordTokenizer tokenizer = new WordTokenizer();
		tokenizer.setDelimiters(" ");

		m_Filter.setTokenizer(tokenizer);
		m_Filter.setWordsToKeep(WORDSTOKEEP);
		m_Filter.setDoNotOperateOnPerClassBasis(true);
		m_Filter.setLowerCaseTokens(true);
		m_Filter.setTFTransform(true);
		m_Filter.setIDFTransform(true);
		// m_Filter.setStopwords(stopwords);

	}

	private void buildClassifier() throws Exception {

		LOGGER.info("Reconstruction classifier.");

		// Initialize filter and tell it about the input format.
		m_Filter.setInputFormat(m_Data);

		// Generate word counts from the training data.
		LOGGER.debug("Traitement des donn�es");
		Instances filteredData = Filter.useFilter(m_Data, m_Filter);

		// construction du mod�le
		LOGGER.debug("Construction du mod�le");
		m_Classifier.buildClassifier(filteredData);

		m_UpToDate = true;

	}

	public void load() {

		try {
			m_UpToDate = false;
			m_Data = null;

			File arffFile = ActiveDirectory.getAbsolute(new File(EMailAutoDispatcher.properties.classifierArffFileParam));

			if (!arffFile.exists()) {
				LOGGER.warn("Classifier d�sactiv� - Fichier non trouv� (base non scann�e?) : "+arffFile.getAbsolutePath());
				return;
			}

			LOGGER.debug("Chargement fichier .arrff => " + arffFile.getAbsolutePath());

			ArffLoader loader = new ArffLoader();
			loader.setFile(arffFile);

			m_Data = loader.getDataSet();
			m_Data.setClassIndex(m_Data.numAttributes() - 1);

			buildClassifier();

		} catch (Exception e) {
			LOGGER.warn("Echec chargement", e);
		}

	}

	public void save() {

		try {
			File arffFile = ActiveDirectory.getAbsolute(new File(EMailAutoDispatcher.properties.classifierArffFileParam));
			if (arffFile != null) {
				LOGGER.debug("Sauvegarde fichier .arrff => " + arffFile.getAbsolutePath());
				ArffSaver saver = new ArffSaver();
				saver.setInstances(m_Data);
				saver.setFile(arffFile);
				saver.writeBatch();
			}
		} catch (IOException e) {
			LOGGER.warn("Echec sauvegarde", e);
		}

	}

	/**
	 * Updates model using the given training message.
	 */
	public void updateData(SimplifiedMail simplifiedMail, String classValue) throws Exception {

		// Make message into instance.
		Instance instance = makeInstance(simplifiedMail, m_Data);

		// Set class value for instance.
		instance.setClassValue(classValue);

		// Add instance to training data.
		m_Data.add(instance);
		m_UpToDate = false;
	}

	/**
	 * Classifies a given message.
	 */
	public PotentialFoldersList classifyMessage(SimplifiedMail simplifiedMail) {

		try {

//			LOGGER.debug("Classification du mail : " + olEmail.getSubject());

			// Check whether classifier has been built.
			if (m_Data==null) {
//				LOGGER.debug("Donn�es non initialis�es (null).");
				return null;
			}

			if ((m_Data==null)||(m_Data.numInstances() == 0)) {
				LOGGER.warn("Donn�es insuffisantes.");
				return null;
			}

			// Check whether classifier and filter are up to date.
			if (!m_UpToDate) buildClassifier();

			// Make separate little test set so that message
			// does not get added to string attribute in m_Data.
			Instances testset = m_Data.stringFreeStructure();

			// Make message into test instance.
			Instance instance = makeInstance(simplifiedMail, testset);

			// Filter instance.
			m_Filter.input(instance);
			Instance filteredInstance = m_Filter.output();

			// calcul les probabilit� de chaque repertoire
			Classification classification = new Classification(filteredInstance,m_Classifier,m_Data.classAttribute());

			return classification.exportClassification();

		} catch (Exception e) {
			LOGGER.warn("Echec classification.", e);
		}

		return null;
	}

	/**
	 * Method that converts a text message into an instance.
	 */
	private static Instance makeInstance(SimplifiedMail simplifiedMail, Instances dataIn) {

		// Create instance of length two.
		Instance instance = new DenseInstance(2);

		// Set value for message attribute
		Attribute messageAtt = dataIn.attribute("Message");
		instance.setValue(messageAtt, messageAtt.addStringValue(simplifiedMail.getGlobalText()));

		// Give instance access to attribute information from the dataset.
		instance.setDataset(dataIn);

		return instance;
	}

	public void train(OlFolder olRootFolder, int depth, JProgressDialog progressDialog) {

		// Demarrage
		LOGGER.info("Debut apprentissage donn�es existantes.");
		progressDialog.setAuto("Debut apprentissage donn�es existantes.");

		// intialise la classification
		String nameOfDataset = "MessageClassificationProblem";

		// Create vector of attributes.
		ArrayList<Attribute> attributes = new ArrayList<Attribute>(2);

		// Add attribute for holding messages.
		attributes.add(new Attribute("Message", (ArrayList<String>) null));

		// Cr�e la liste potentielle des repertoires de destination
		// stocke les repertoires dans l'attribut "Class"
		LOGGER.debug("Listing des repertoires de destination");
		ArrayList<String> classValues = new ArrayList<String>(1000);
		applyToAllDirectories(olRootFolder, olFolder -> classValues.add(formatFolderName(olFolder)));
		int foldersCount = classValues.size();
		attributes.add(new Attribute("Class", classValues));

		// Create dataset with initial capacity of 1000, and set index of class.
		m_Data = new Instances(nameOfDataset, attributes, 1000);
		m_Data.setClassIndex(m_Data.numAttributes() - 1);

		// Scan repertoires pour apprentissage des contenus
		LOGGER.debug("Debut scan des mails dans les repertoires");

		// Initialise l'afficheur
		progressDialog.setManual("Nettoie les definitions des repertoires", 0, 1, foldersCount);

		// realise le scan
		applyToAllDirectories(olRootFolder, olFolder -> trainFolder(olFolder, depth, progressDialog));

		// traitement des mails
		LOGGER.debug("Traite les donn�es recup�r�es.");
		Instances filteredData = null;
		try {

			// sauvegarde des donn�es d'entr�e du mod�le
			progressDialog.setAuto("Sauvegarde fichier .arff");
			save();

			// Initialize filter and tell it about the input format.
			m_Filter.setInputFormat(m_Data);

			// Generate word counts from the training data.
			filteredData = Filter.useFilter(m_Data, m_Filter);

			// construction du mod�le
			LOGGER.debug("Construction du mod�le");
			progressDialog.setAuto("Construction du mod�le");

			m_Classifier.buildClassifier(filteredData);

//			// sauvegarde du mod�le
//			File modelFile = ActiveDirectory.getAbsolute(new File(EMailAutoDispatcher.properties.classifierModelFileParam));
//			if (modelFile != null) {
//				LOGGER.debug("Sauvegarde du mod�le => " + modelFile.getAbsolutePath());
//				progressDialog.setAuto("Sauvegarde du mod�le => " + modelFile.getAbsolutePath());
//				weka.core.SerializationHelper.write(modelFile.getAbsolutePath(), m_Classifier);
//			}

			// verification du mod�le
			LOGGER.debug("Verification du mod�le");
			progressDialog.setAuto("Verification du mod�le");

			Evaluation eval = new Evaluation(filteredData);
			eval.crossValidateModel(m_Classifier, filteredData, 4, new Random(1));

			LOGGER.info(eval.toSummaryString());
			LOGGER.info(eval.toClassDetailsString());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		LOGGER.info("Termin�.");

	}

	//
	// Methodes internes
	//

	public static String formatFolderName(OlFolder olFolder) {
		return olFolder.getEntryID() + " " + olFolder.getName();
	}
	public static String getFolderID(String attributeName) {
		if (attributeName==null) return null;
 		int index=attributeName.indexOf(" ");
 		if (index<0) return attributeName;
		return attributeName.substring(0, index);
	}

	private static void applyToAllDirectories(OlFolder startFolder, Consumer<OlFolderEMail> toApply) {

		ArrayDeque<OlFolder> scanPile = new ArrayDeque<OlFolder>();
		scanPile.add(startFolder);
		while (!scanPile.isEmpty()) {

			// recupere la tete de pile
			OlFolder olFolder = scanPile.pollFirst();

			// ignore le repertoire s'il est exclu recursif
			if (DispatchDatabase.properties.excludedRecDirectories.contains(olFolder.getEntryID()))
				continue;

			// ajoute les enfants
			for (OlFolderEMail subFolder : olFolder.getSubFolders())
				scanPile.add(subFolder);

			// bloque le traitement si le repertoire n'est pas de type OlFolderEMail ou s'il est exclu
			if ((!(olFolder instanceof OlFolderEMail)) || (DispatchDatabase.properties.excludedDirectories.contains(olFolder.getEntryID())))
				continue;

			// traite le repertoire
			toApply.accept((OlFolderEMail) olFolder);

		}

	}

	private void trainFolder(OlFolderEMail olFolderEMail, int depth, JProgressDialog progressDialog) {

		// affichage avancement
		progressDialog.set("Training : " + olFolderEMail.getName());

		// recupere la liste des mails
		Dispatch itemsD = DispUtil.get(olFolderEMail.getDispatch(), "Items");

		// limite le nombre de mails � extraire
		int countMax = depth;
		int count = DispUtil.getInt(itemsD, "Count");
		int iter = (int) Math.max(1, Math.floor(Math.sqrt(count) / Math.sqrt(countMax)));

		for (int mailIndex = 1; mailIndex <= Math.min(count, countMax); mailIndex += iter) {

			Dispatch mailItemD = DispUtil.call(itemsD, "Item", mailIndex);

			OlObjectClassEnum typeEnum = OlObjectClassEnum.getClass(mailItemD);
			if (typeEnum == OlObjectClassEnum.olMail) {

				String mailEntryID = DispUtil.getString(mailItemD, "entryID");

				OlEmail email = (OlEmail) OlConnector.getOlObject(mailEntryID);

				if (email == null)
					email = new OlEmail(mailEntryID);

				// Make message into instance.
				try {

					progressDialog.set("Training : " + olFolderEMail.getName() + " - " + email.getSubject());

					// insere le message dans la table d'aprentissage
					updateData(new SimplifiedMail(email), formatFolderName(olFolderEMail));

				} catch (Exception e1) {
					LOGGER.warn("Erreur lors du transfert du message. ID = " + email.getEntryID());
					LOGGER.warn(e1);
				}

				// libere la memoire
				email.forceSafeRelease();

			} else {
				// incr�mente le plafond car l'objet n'est pas utilisable.
				countMax++;
			}

			mailItemD.safeRelease();
		}

		itemsD.safeRelease();

		// affiche l'avancement
		progressDialog.incManualProgress();

	}

}
