package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.MultinomialClassifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;

public class Classification {

	private final static Logger LOGGER = LogManager.getLogger();

	private final static Integer CLASSIFICATIONSIZE=5;
	private final static Double BASESCORE=100.0;
	private final static Double MINPROBA=1e-10;

	private final ArrayList<ClassProba> probaList;

	public Classification(Instance instance, Classifier classifier, Attribute attribute) {

		double[] probability = {};
		int size = (CLASSIFICATIONSIZE > 0) ? Math.min(attribute.numValues(), CLASSIFICATIONSIZE) : attribute.numValues();
		int indexMax=size-1;
		probaList = new ArrayList<ClassProba>(size);

		try {
			probability = classifier.distributionForInstance(instance);
			LOGGER.debug("maxproba="+Arrays.stream(probability).max().getAsDouble());

			for (int index=0; index<size; index++)
				probaList.add(new ClassProba(attribute.value(index), probability[index]));

			revSortProbaList();

			if (CLASSIFICATIONSIZE<=0) return;

			double minproba = probaList.get(indexMax).proba;

			for (int index=size; index<attribute.numValues(); index++)
				if (probability[index]>minproba) {
					probaList.set(indexMax, new ClassProba(attribute.value(index), probability[index]));
					revSortProbaList();
					minproba=probaList.get(indexMax).proba;
				}

		} catch (Exception e) {
			LOGGER.warn("Erreur de creation ",e);
		}

		if (LOGGER.isDebugEnabled()) showLogStatus();
	}

	public void showLogStatus() {
		LOGGER.debug("Display status");
		for (ClassProba proba : probaList)
			LOGGER.debug("proba="+proba);
	}

	public PotentialFoldersList exportClassification() {


		PotentialFoldersList list = new PotentialFoldersList();

		for (int index = 0; (index < probaList.size()) && (probaList.get(index).proba > MINPROBA); index++) {
			
			OlFolderEMail oFolder = (OlFolderEMail) OlConnector.getOlObject(MultinomialClassifier.getFolderID(probaList.get(index).name));
			
			// Folder pas trouv� car peut-�tre n'existe plus => on laisse tomber.
			if (oFolder==null) {
				LOGGER.info("Skipping folder, ID not found, ID="+probaList.get(index).name);
				continue;
			}

			list.addWeightedDestFolder(
					new WeightedDestFolder(
							oFolder,
							Math.round(BASESCORE * Math.log10(probaList.get(index).proba / MINPROBA)),
							String.format("Classifier (rank=%d, p=%1.1e)", index, probaList.get(index).proba)));
		}

		return list;

	}

	//
	// Classes et methodes internes
	//

	private class ClassProba {

		String name;
		double proba;

		public ClassProba(String name,double proba) {
			this.name=name;
			this.proba=proba;
		}

		@Override
		public String toString() {
			return "[ "+proba+" => "+name+" ]";
		}

	}

	private void revSortProbaList() {
		probaList.sort(classProbaRevComparator);
	}

	// tri en ordre inverse : le plus grand en premier
	private final static ClassProbaRevComparator classProbaRevComparator = new ClassProbaRevComparator();
	public static class ClassProbaRevComparator implements Comparator<ClassProba> {
	    @Override
	    public int compare(ClassProba o1, ClassProba o2) {
	        return Double.compare(o2.proba, o1.proba);
	    }
	}


}
