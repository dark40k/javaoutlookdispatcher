package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.ctecfilter.CTecReformating;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmailBody;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmailBodyFormatEnum;
import net.htmlparser.jericho.Source;

public class SimplifiedMail {

	private static HashMap<String,SimplifiedMail> simplifiedMailList = new HashMap<String,SimplifiedMail>(2000);

	private String rawTitle;
	private String cleanedTitle;

	private OlEmailBody body;
	private String rawBody;
	private String cleanedBody;

	private String fromAdress;
	private ArrayList<String> toAdresses;
	private ArrayList<String> ccAdresses;

	private String conversationID;

	private String globalText;

	public static SimplifiedMail getSimplifiedMail(OlEmail olEmail) {

		SimplifiedMail simpleMail = simplifiedMailList.get(olEmail.getEntryID());
		if (simpleMail!=null) return simpleMail;

		simpleMail = new SimplifiedMail(olEmail);
		simplifiedMailList.put(olEmail.getEntryID(),simpleMail);

		return simpleMail;
	}


	public SimplifiedMail(OlEmail olEmail) {

		// Traitement du titre

		rawTitle = olEmail.getSubject().getCleaned();

		cleanedTitle = rawTitle;
		cleanedTitle = CTecReformating.cleanString1(cleanedTitle);
		cleanedTitle = CTecReformating.cleanString2(cleanedTitle);
		cleanedTitle = CTecReformating.cleanString3(cleanedTitle);

		// Traitement du corps du message

		body = olEmail.getBody();

		if (body.getFormat() == OlEmailBodyFormatEnum.HTML) {
			// nettoie le corps du message du code HTML si besoin
			Source source = new Source(body.getContent());
			source.fullSequentialParse();
			rawBody = source.getTextExtractor().setExcludeNonHTMLElements(true).toString();
		} else {
			// Utilise le corps brut si pas HTML
			rawBody = body.getContent();
		}

		cleanedBody = CTecReformating.cleanString(rawBody);

		// adresse emeteur s'ils ne sont pas trop nombreux

		fromAdress = CTecReformating.cleanString(olEmail.getFromAdress());
		if (!fromAdress.contains("@"))
			fromAdress = "";

		// adresse des destinataires s'ils ne sont pas trop nombreux

		toAdresses = new ArrayList<String>();
		if ((EMailAutoDispatcher.properties.maxAdressesNbrParam<=0)||(olEmail.getToAdressesCount() < EMailAutoDispatcher.properties.maxAdressesNbrParam))
			for (String address : olEmail.getToAdresses()) {
				String cleanAdress = CTecReformating.cleanString(address);
				if (cleanAdress.length() > 0)
					if (cleanAdress.contains("@"))
						toAdresses.add(cleanAdress);
			}

		// adresse des copies s'ils ne sont pas trop nombreux

		ccAdresses = new ArrayList<String>();
		if ((EMailAutoDispatcher.properties.maxAdressesNbrParam<=0)||(olEmail.getCcAdressesCount() < EMailAutoDispatcher.properties.maxAdressesNbrParam))
			for (String address : olEmail.getToAdresses()) {
				String cleanAdress = CTecReformating.cleanString(address);
				if (cleanAdress.length() > 0)
					if (cleanAdress.contains("@"))
						ccAdresses.add(cleanAdress);
			}

		// reference de conversation

		conversationID = olEmail.getConversationID();

		// texte global

		StringBuilder globalBuilder = new StringBuilder();

		globalBuilder.append(" ");
		globalBuilder.append(cleanedTitle);
		globalBuilder.append(" ");
		globalBuilder.append(CTecReformating.cleanString(olEmail.getFrom()));
		globalBuilder.append(" ");
		globalBuilder.append(fromAdress);
		globalBuilder.append(" ");

		for (String adress : toAdresses) {
			globalBuilder.append(adress);
			globalBuilder.append(" ");
		}

		for (String adress : ccAdresses) {
			globalBuilder.append(adress);
			globalBuilder.append(" ");
		}

		globalBuilder.append(cleanedBody);
		globalBuilder.append(" ");

		globalText = globalBuilder.toString();

	}

	@Override
	public String toString() {
		StringBuilder toString = new StringBuilder();
		toString.append("[Titre=");
		toString.append(cleanedTitle);
		toString.append(", FromAdress=");
		toString.append(fromAdress);
		toString.append(", To=");
		toString.append(toAdresses);
		toString.append(", Cc=");
		toString.append(ccAdresses);
		toString.append("]");
		return toString.toString();
	}

	public String getRawTitle() {
		return rawTitle;
	}

	public Iterable<String> getTitleWords() {
		return CTecReformating.getWords(cleanedTitle);
	}

	public String getCleanedTitle() {
		return cleanedTitle;
	}

	public Iterable<String> getBodyWords() {
		return CTecReformating.getWords(cleanedBody);
	}

	public String getCleanedBody() {
		return cleanedBody;
	}

	public String getFromAdress() {
		return fromAdress;
	}

	public Iterable<String> getToAdresses() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return toAdresses.listIterator();
			}
		};
	}

	public Iterable<String> getCcAdresses() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return ccAdresses.listIterator();
			}
		};
	}

	public String getConversationID() {
		return conversationID;
	}

	public String getGlobalText() {
		return globalText;
	}

	public String getRawBody() {
		return rawBody;
	}

}
