package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class PotentialFoldersList implements Iterable<WeightedDestFolder>, Cloneable {

	private final ArrayList<WeightedDestFolder> result = new ArrayList<WeightedDestFolder>();

	public PotentialFoldersList() {
	}

	//
	// Import/Export XML
	//

	public PotentialFoldersList(Element fatherElt) {
		Element elt = fatherElt.getChild("PotentialFoldersList");
		result.clear();
		for (Element subElt : elt.getChildren())
			result.add(new WeightedDestFolder(subElt));
	}

	public Element exportToXML() {
		Element elt = new Element("PotentialFoldersList");
		for (WeightedDestFolder folder:this)
			elt.addContent(folder.exportToXML());
		return elt;
	}

	//
	// Divers
	//

	@Override
	public String toString() {
		String strOut = "[";

		for (WeightedDestFolder folder : this) {
			strOut+=", " + folder.folder + " ( "+folder.weight+" ) ";
		}
		strOut=strOut.replaceFirst(WeightedDestFolder.ORIGIN_SEPARATOR,"");
		return strOut+"]";
	}

	public int getIndex(OlFolder folder) {
		for (int index=0; index < result.size(); index++)
			if (result.get(index).folder.equals(folder))
				return index;
		return -1;
	}

	public boolean contains(OlFolder folder) {
		return getIndex(folder)>=0;
	}

	public int getCount() {
		return result.size();
	}

	public double getScore(OlFolderEMail folder) {
		int index = getIndex(folder);
		if (index<0) return -1;
		return result.get(index).weight;
	}

	public OlFolderEMail getFolder(int index) {
		return result.get(index).folder;
	}

	public double getScore(int index) {
		return result.get(index).weight;
	}

	public void addWeightedFolders(ArrayList<WeightedFolder> weightedFolderList, String origin) {
		addWeightedFolders(weightedFolderList, 1, origin);
	}

	public void addWeightedFolders(ArrayList<WeightedFolder> weightedFolderList, double coef, String origin) {

		if (weightedFolderList==null) return;

		for (WeightedFolder weightedFolder : weightedFolderList) {

			int index = getIndex(weightedFolder.folder);

			if (index<0) {
				result.add(new WeightedDestFolder(weightedFolder.folder,weightedFolder.weight*coef,origin));
			} else {
				result.get(index).inc(weightedFolder.weight*coef,origin);
			}

			if (index<=0) continue;

			if (result.get(index).weight > result.get(index-1).weight) result.sort(null);
		}
	}

	@Override
	public Iterator<WeightedDestFolder> iterator() {
		return result.iterator();
	}

	public void addWeightedDestFolder(WeightedDestFolder weightedDestFolder) {
		if (weightedDestFolder==null) return;
		int index = this.result.indexOf(weightedDestFolder);
		if (index<0)
			result.add(weightedDestFolder.clone());
		else
			result.get(index).inc(weightedDestFolder);
		result.sort(null);
	}

	public void addWeightedDestFolder(WeightedDestFolder weightedDestFolder, long count) {

		if (weightedDestFolder==null) return;
		if (count==0) return;

		long remainCount=count;

		int index = this.result.indexOf(weightedDestFolder);

		if (index<0) {
			index = result.size();
			result.add(weightedDestFolder.clone());
			remainCount--;
		}

		WeightedDestFolder wdfolder = result.get(index);
		for (long count2=0; count2<remainCount; count2++) wdfolder.inc(weightedDestFolder);

		result.sort(null);
	}

	public void addPotentialFolderList(PotentialFoldersList potentialFoldersList) {
		if (potentialFoldersList==null) return;
		for (WeightedDestFolder potFolder : potentialFoldersList) {
			int index = this.result.indexOf(potFolder);
			if (index<0)
				result.add(potFolder.clone());
			else
				result.get(index).inc(potFolder);
		}
		result.sort(null);
	}

	public void addPotentialFolderList(PotentialFoldersList potentialFoldersList, double coef) {
		if (potentialFoldersList==null) return;
		for (WeightedDestFolder potFolder : potentialFoldersList) {
			int index = this.result.indexOf(potFolder);
			if (index<0)
				result.add(new WeightedDestFolder(potFolder,coef));
			else
				result.get(index).inc(potFolder,coef);
		}
		result.sort(null);
	}

	@Override
	public PotentialFoldersList clone() {

		PotentialFoldersList potClone = new PotentialFoldersList();

		for (WeightedDestFolder weightedDestFolder : this)
			potClone.result.add(weightedDestFolder.clone());

		return potClone;
	}

}