package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.directfilter;

import java.util.regex.Pattern;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.SimplifiedMail;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.util.persistantparameters.ParamGroup;

public class DirectFilteredDir {

	private String description;

	private String folderId;
	private double weight;

	private WeightedDestFolder weightedDestFolder;

	private String regExpFormula;
	private Pattern regExp;

	private boolean applyOnTitle;
	private boolean applyOnBody;

	public DirectFilteredDir(String xmlName, ParamGroup subParamGroup) {

		ParamGroup param = subParamGroup.getSubParamGroup(xmlName);

		description = param.getSubParamString("description", "<No description>").getValue();
		folderId = param.getSubParamString("folderId", "<No folder assigned>").getValue();
		weight = param.getSubParamDouble("weight", 0d).getValue();
		updateWeigthedDestFolder();

		regExpFormula = param.getSubParamString("regExpFormula", "").getValue();
		updateRegEx();

		applyOnTitle = param.getSubParamBoolean("applyOnTitle", true).getValue();
		applyOnBody = param.getSubParamBoolean("applyOnBody", false).getValue();

	}

	private void updateRegEx() {
		regExp = Pattern.compile(regExpFormula,Pattern.CASE_INSENSITIVE);
	}
	private void updateWeigthedDestFolder() {
		OlFolderEMail folder = (OlFolderEMail) OlConnector.getOlObject(folderId);
		if (folder==null) {
			weightedDestFolder=null;
			return;
		}
		weightedDestFolder = new WeightedDestFolder(folder, weight, description);
	}

	//
	// Methode pour calcul de destination
	//

	public boolean test(SimplifiedMail simplifiedMail) {

		if (applyOnTitle && (regExp.matcher(simplifiedMail.getRawTitle()).find())) return true;

		if (applyOnBody && (regExp.matcher(simplifiedMail.getRawBody()).find())) return true;

		return false;

	}

	public WeightedDestFolder getWeightedDestFolder() {
		return weightedDestFolder;
	}

	//
	// Sauvegarde du parametrage
	//

	public void saveAsNewParam(String xmlName, ParamGroup subParamGroup) {

		ParamGroup param = subParamGroup.getSubParamGroup(xmlName);

		param.getSubParamString("description", null).setValue(description);
		param.getSubParamString("folderId", null).setValue(folderId);
		param.getSubParamDouble("weight", null).setValue(weight);
		updateWeigthedDestFolder();

		param.getSubParamString("regExpFormula", null).setValue(regExpFormula);
		updateRegEx();

		param.getSubParamBoolean("applyOnTitle", null).setValue(applyOnTitle);
		param.getSubParamBoolean("applyOnBody", null).setValue(applyOnBody);

	}

	//
	// Edition des paramètres
	//

	/**
	 * @return the desc
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDescription(String desc) {
		this.description = desc;
	}

	/**
	 * @return the folderId
	 */
	public String getFolderId() {
		return folderId;
	}

	/**
	 * @param folderId the folderId to set
	 */
	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @return the regExpFormula
	 */
	public String getRegExpFormula() {
		return regExpFormula;
	}

	/**
	 * @param regExp the regExp to set
	 */
	public void setRegExpFormula(String regExpFormula) {
		this.regExpFormula = regExpFormula;
	}

	/**
	 * @return the applyOnTitle
	 */
	public boolean isApplyOnTitle() {
		return applyOnTitle;
	}

	/**
	 * @param applyOnTitle the applyOnTitle to set
	 */
	public void setApplyOnTitle(boolean applyOnTitle) {
		this.applyOnTitle = applyOnTitle;
	}

	/**
	 * @return the applyOnBody
	 */
	public boolean isApplyOnBody() {
		return applyOnBody;
	}

	/**
	 * @param applyOnBody the applyOnBody to set
	 */
	public void setApplyOnBody(boolean applyOnBody) {
		this.applyOnBody = applyOnBody;
	}

}
