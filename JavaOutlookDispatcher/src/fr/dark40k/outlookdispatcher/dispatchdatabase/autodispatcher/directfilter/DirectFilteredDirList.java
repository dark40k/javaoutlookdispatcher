package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.directfilter;

import java.util.ArrayList;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.SimplifiedMail;
import fr.dark40k.util.persistantparameters.ParamGroup;

public class DirectFilteredDirList {

	public final ParamGroup directFilteredDirListParams;
	public final static String DIRECT_FILTERED_DIRS_PARAMNAME="directFilterDirs";

	int currentVersion;

	private final ArrayList<DirectFilteredDir> filterList = new ArrayList<DirectFilteredDir>();

	public DirectFilteredDirList(ParamGroup directFilteredDirListParams) {

		this.directFilteredDirListParams = directFilteredDirListParams;

		clearPendingModifications();

	}

	public void clearPendingModifications() {

		currentVersion = directFilteredDirListParams.getVersion();

		filterList.clear();

		for (String subParamName : directFilteredDirListParams.listSubParamsNames(true))
			filterList.add(new DirectFilteredDir(subParamName,directFilteredDirListParams));

	}

	public void applyPendingModifications() {

		directFilteredDirListParams.clear();

		for (int index=0; index<filterList.size(); index++)
			filterList.get(index).saveAsNewParam("filter" + filterList.size(),directFilteredDirListParams);

		currentVersion = directFilteredDirListParams.getVersion();

	}


	public PotentialFoldersList buildPotentialFolderList(SimplifiedMail simplifiedMail) {

		if (currentVersion!=directFilteredDirListParams.getVersion()) clearPendingModifications();

		PotentialFoldersList list = new PotentialFoldersList();

		for (DirectFilteredDir filter : filterList)
			if (filter.test(simplifiedMail))
				list.addWeightedDestFolder(filter.getWeightedDestFolder());

		return list;

	}

	public int size() {
		return filterList.size();
	}

	//
	// Edition
	//

	public DirectFilteredDir addNew() {
		DirectFilteredDir newFilter = new DirectFilteredDir("filter" + filterList.size(), directFilteredDirListParams);
		filterList.add(newFilter);
		return newFilter;
	}

	public DirectFilteredDir get(int index) {
		return filterList.get(index);
	}

	public DirectFilteredDir remove(int index) {
		return filterList.remove(index);
	}

}
