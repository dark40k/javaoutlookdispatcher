package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class WordDictionnary {

	private LinkedHashMap<String,ArrayList<WeightedFolder>> dictionnary = new LinkedHashMap<String,ArrayList<WeightedFolder>>();

	public void pushKey(String key, OlFolderEMail folderEmail, double weight) {
		pushKey(key, new WeightedFolder(folderEmail, weight));
	}

	public void pushKey(String key, WeightedFolder folder) {

		ArrayList<WeightedFolder> listFolders = dictionnary.get(key);

		// la clef n'existe pas => creation de la clef
		if (listFolders==null) {
			listFolders = new ArrayList<WeightedFolder>();
			dictionnary.put(key,listFolders);
		}

		listFolders.add(folder);
	}

	public ArrayList<WeightedFolder> get(String key) {
		return dictionnary.get(key);
	}

	public double getTotalWeight(String key) {
		double total=0.0;
		for (WeightedFolder wf : dictionnary.get(key))
			total+=wf.weight;
		return total;
	}

	public Set<String> keySet() {
		return dictionnary.keySet();
	}



}
