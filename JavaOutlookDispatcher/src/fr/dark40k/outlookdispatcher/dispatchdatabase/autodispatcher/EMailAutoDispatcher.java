package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.AutoUpdateFolderParam.AutoUpdateFolderModeEnum;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.MultinomialClassifier.MultinomialClassifier;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.ctecfilter.CTecReformating;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.directfilter.DirectFilteredDirList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderData;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;

public class EMailAutoDispatcher {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static EMailAutoDispatcherProperties properties = new EMailAutoDispatcherProperties();

	private final static Pattern searchEmailAddressInStringPattern = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+");

	//
	// Parametres
	//

	private WordDictionnary globalWordDictionnary;
	private WordDictionnary foldersMailAdressDictionnary;
	private WordDictionnary foldersConversationDictionnary;

	private DirectFilteredDirList directFilteredDirList;
	private RegExpFolderFilterList regExpFolderFilterList;

	private MultinomialClassifier multiClassifier;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------

	public EMailAutoDispatcher(OlFolder olRootFolder, JProgressDialog progressDialog) {

		LOGGER.info("Construction du dispatcheur");
		progressDialog.setAuto("Construction du dispatcheur, recuperation des donn�es d'entree");

		// Charge la liste d'exclusion avec les mots sp�cifiques
		for (String word : properties.excludedWordsParam) CTecReformating.addStopWord(word);

		// initialise les dictionnaires
		WordDictionnary tmpAutoWordDictionnary = new WordDictionnary();
		globalWordDictionnary = new WordDictionnary();
		foldersMailAdressDictionnary = new WordDictionnary();
		foldersConversationDictionnary = new WordDictionnary();

		// initialise la liste des filtres directs (regexp stock� dans le fichier parametre global)
		directFilteredDirList = new DirectFilteredDirList(properties.directFiltersParamGroup);

		// intialise la liste indirecte (regexp stock� stock� dans le r�pertoire)
		regExpFolderFilterList = new RegExpFolderFilterList();

		// initialise le repartiteur multinomial
		multiClassifier = new MultinomialClassifier();
		multiClassifier.load();

		// construit la pile
		ArrayDeque<OlFolder> folderStack = new ArrayDeque<OlFolder>();
		folderStack.add(olRootFolder);

		// balaie les repertoires
		while (!folderStack.isEmpty()) {

			// met � jours la pile
			OlFolder folder = folderStack.pollFirst();

			// verifie si le repertoire n'est pas exclu en recursif
			if (DispatchDatabase.properties.excludedRecDirectories.contains(folder.getEntryID())) {
				continue;
			}

			// ajoute les sous-repertoire � la pile
			for (OlFolder subFolder : folder) {
				folderStack.add(subFolder);
			}

			// verifie si le repertoire est bien du type OlFolderEMail
			if (!(folder instanceof OlFolderEMail)) continue;
			OlFolderEMail folderEmail = (OlFolderEMail) folder;

			// Recupere les donnees du repertoire
			OlFolderData dispatcherData = OlFolderDataUtility.load(folderEmail);

			// Ajoute les r�gles locales
			regExpFolderFilterList.addFolder(folderEmail, dispatcherData);

			// construit le dictionnaire des conversations � partir de la description du repertoire
			for (String conversationId : dispatcherData.getConversationIds()) {
				foldersConversationDictionnary.pushKey(conversationId, folderEmail, properties.conversationNoteParam);
			}

			// verifie si le repertoire n'est pas exclu
			if (DispatchDatabase.properties.excludedDirectories.contains(folderEmail.getEntryID())) {
				continue;
			}

			// charge le dictionnaire des addresses mail et le dictionnaire global � partir des adresses mails du repertoire
			if (!dispatcherData.getInactiveEmailAdresses())
				for (String address : dispatcherData.getEmailAdresses()) {
					address = CTecReformating.cleanString(address);
					if (address.isEmpty()) continue;
					foldersMailAdressDictionnary.pushKey(address, folderEmail, properties.emailAddressNoteParam);
					globalWordDictionnary.pushKey(address, folderEmail, properties.emailAddressNoteParam);
				}

			// ajoute les mots sp�cifiques au dictionnaire global s'il y en a
			if ((dispatcherData.getKeywords().list != null) && (!dispatcherData.getKeywords().list.isEmpty()))
				for (String word : CTecReformating.getWords(CTecReformating.cleanString(dispatcherData.getKeywords().list)))
					if (!word.startsWith(CTecReformating.OPEN_CAR))
						globalWordDictionnary.pushKey(word, folderEmail, properties.wordTitleNoteParam);
					else
						globalWordDictionnary.pushKey(word, folderEmail, properties.CTECCodeNoteParam);

			// ajoute les mots du titre au dictionnaire global si le titre n'est pas exclu
			if (!dispatcherData.getKeywords().excludeTitle)
				for (String word : CTecReformating.getWords(CTecReformating.cleanString(folderEmail.getName())))
					if (!word.startsWith(CTecReformating.OPEN_CAR))
						globalWordDictionnary.pushKey(word, folderEmail, properties.wordTitleNoteParam);
					else
						globalWordDictionnary.pushKey(word, folderEmail, properties.CTECCodeNoteParam);

			// ajoute les mots clefs auto au dictionnaire global
			if (!dispatcherData.getInactiveAutoKeywords())
				dispatcherData.getAutoKeywords().forEach(
						(w,n)->{
							//globalWordDictionnary.pushKey(w, folderEmail, properties.CTECCodeNoteParam*n/dispatcherData.getAutoKeywords().getTotal());
								tmpAutoWordDictionnary.pushKey(w, folderEmail,n);
						});

		}

		// normalise l'amplitude des mots du dictionnaire auto avant de reventiler dans le dictionnaire global
		for (String word : tmpAutoWordDictionnary.keySet())
			if (globalWordDictionnary.get(word) == null) {
				double wfnorm = properties.CTECCodeNoteParam / tmpAutoWordDictionnary.getTotalWeight(word);
				for (WeightedFolder wf : tmpAutoWordDictionnary.get(word))
					globalWordDictionnary.pushKey(word, wf.folder, wf.weight * wfnorm);
			}

	}

	// -------------------------------------------------------------------------------------------
	//
	// Genere la liste des repertoires potentiels pour un mail
	//
	// -------------------------------------------------------------------------------------------

	/**
	 * Genere la liste des repertoires potentiels pour un mail
	 *
	 * @param olEmail
	 * @return
	 */
	public PotentialFoldersList findBestDestinationFolders(OlEmail olEmailIn) {

		//LOGGER.debug("Title="+olEmailIn.getSubject());
		//LOGGER.debug("tmp");

		SimplifiedMail simplifiedMail = SimplifiedMail.getSimplifiedMail(olEmailIn);
		PotentialFoldersList result = new PotentialFoldersList();

		// balaie les mots clefs du titre du mail et les testes sur le dictionnaire de mots
		for (String word : simplifiedMail.getTitleWords()) {
			//LOGGER.debug(word);
			result.addWeightedFolders(globalWordDictionnary.get(word), "Title : " + word);
		}

		// recherche l'adresse de l'emetteur du mail dans le dictionnaire
		// d'addresses
		String from = simplifiedMail.getFromAdress();
		result.addWeightedFolders(foldersMailAdressDictionnary.get(from), "From :" + from);

		// recherche l'adresse des destinataires s'ils ne sont pas trop nombreux
		// (spam ou mail general sinon)
		for (String address : simplifiedMail.getToAdresses())
			result.addWeightedFolders(foldersMailAdressDictionnary.get(address), "To :" + address);

		// recherche l'adresse des destinataires s'ils ne sont pas trop nombreux
		// (spam ou mail general sinon)
		for (String address : simplifiedMail.getCcAdresses())
			result.addWeightedFolders(foldersMailAdressDictionnary.get(address), "Cc :" + address);

		// recherche la conversation
		result.addWeightedFolders(foldersConversationDictionnary.get(simplifiedMail.getConversationID()), "Conversation");

		// balaie les mots du corps et les testes sur le dictionnaire de mots
		for (String word : simplifiedMail.getBodyWords())
			result.addWeightedFolders(globalWordDictionnary.get(word), properties.bodyCoeficientParam, "BodyWord : " + word);

		// application des r�gles particuli�res globales
		result.addPotentialFolderList(directFilteredDirList.buildPotentialFolderList(simplifiedMail));

		// application des r�gles particuli�res des repertoires
		result.addPotentialFolderList(regExpFolderFilterList.buildPotentialFolderList(simplifiedMail));

		// Utilisation du classifier
		result.addPotentialFolderList(multiClassifier.classifyMessage(simplifiedMail));

		return result;
	}

	/**
	 * Realise la mise � jours de la description d'un repertoire contenant un mail
	 *
	 * @param destFolder
	 * @param eMail
	 * @param excludedAddressesParam
	 */
	public static void updateFolderDescriptionAfterMove(OlFolderEMail destFolder, OlEmail eMail) {

		// Construit le filtre pour les adresses acceptables
		Pattern excludedAdressesFilterPattern = buildFilterPattern(properties.excludedAddressesParam);

		// Recupere les donnees du repertoire
		OlFolderData dispatcherFolderData = OlFolderDataUtility.load(destFolder);

		// Ajoute la conversation
		dispatcherFolderData.getConversationIds().add(eMail.getConversationID());

		// Ajoute les informations d'addresse
		if (!dispatcherFolderData.getInactiveEmailAdresses())
			addEmailAddressesToFolderData(eMail, dispatcherFolderData, excludedAdressesFilterPattern);

		// Ajoute les codes projets detect�s
		if (!dispatcherFolderData.getInactiveAutoKeywords())
			addAutoKeywordsToFolderData(SimplifiedMail.getSimplifiedMail(eMail), destFolder, dispatcherFolderData);

		// sauvegarde
		OlFolderDataUtility.save(dispatcherFolderData, destFolder);
	}

	/**
	 * Realise la mise � jours d'un repertoire � partir des mails contenus dans celui-ci
	 *
	 * @param olFolder
	 */
	public static void updateFolderDefinition(OlFolder olFolder) {

		LOGGER.info("Mise � jours de la d�finition du repertoire: " + olFolder.getName());

		// annule la mise � jours si le repertoire ne contient pas des mails
		if (!(olFolder instanceof OlFolderEMail)) {
			LOGGER.warn("Repertoire ne contient pas de mails, Mise � jours annul�e");
			return;
		}

		// effectue la mise � jours
		doUpdateFolderDefinition(
				(OlFolderEMail) olFolder, buildFilterPattern(properties.excludedAddressesParam),
				AutoUpdateFolderModeEnum.Recalculer,
				AutoUpdateFolderModeEnum.Recalculer,
				AutoUpdateFolderModeEnum.Recalculer);

		LOGGER.info("Mise � jours termin�e");
	}

	/**
	 * Realise la mise � jours d'un repertoire et de ses sous-repertoires � partir des mails contenus dans chacun
	 *
	 * @param olRootFolder
	 * @param updateParam
	 */
	public static void updateAllFoldersDefinitions(OlFolder olRootFolder, AutoUpdateFolderParam updateParam,
			JProgressDialog progressDialog) {

		LOGGER.info("Demarre mise � jours des repertoires.");
		long start = System.currentTimeMillis();

		// message utilisateur et initialisation afficheur progression
		int foldersCount = olRootFolder.folderAndSubFoldersStream().mapToInt(e -> 1).sum();
		LOGGER.debug("Compte des repertoire=" + foldersCount);
		progressDialog.setManual("Nettoie les definitions des repertoires", 0, 1, foldersCount);

		// construit le filtre pour exclure les adresses
		Pattern excludedAdressesFilterPattern = buildFilterPattern(properties.excludedAddressesParam);

		// fonction de filtrage et de nettoyage des repertoires exclus
		Predicate<OlFolder> excludedOlFolderFilter = (OlFolder olFolder) -> {

			// le repertoire n'est pas exclus recursif => sort du test avec valeur negative
			if (!DispatchDatabase.properties.excludedRecDirectories.contains(olFolder.getEntryID())) return false;

			// le repertoire est exclus recursif => applique la mise � jours au repertoire et � ses enfants
			olFolder.folderAndSubFoldersStream().forEach(
					folder -> {

						// affichage avancement
						progressDialog.set("MaJ Definition : " + olFolder.getName());
						progressDialog.incManualProgress();

						// effectue la mise � jours
						if (folder instanceof OlFolderEMail)
							doUpdateFolderDefinition(
									(OlFolderEMail) olFolder,
									excludedAdressesFilterPattern,
									updateParam.recExcludedAutoKeywordsBlock,
									updateParam.recExcludedAdressBlock,
									updateParam.recExcludedConversationBlock);
					});

			return true;

		};

		// fonction de mise � jours des repertoires, ne fait la mise � jours que si le repertoire contient des mails
		Consumer<OlFolder> updateFolder = (OlFolder olFolder) -> {

			// affichage avancement
			progressDialog.set("MaJ Definition : " + olFolder.getName());
			progressDialog.incManualProgress();

			// effectue la mise � jours
			if (olFolder instanceof OlFolderEMail) {

				OlFolderEMail olFolderEMail = (OlFolderEMail) olFolder;

				// cas d'un scan directory => nettoyage
				if (DispatchDatabase.properties.scanDirectories.contains(olFolder.getEntryID())) {
					doUpdateFolderDefinition(
							olFolderEMail,
							excludedAdressesFilterPattern,
							AutoUpdateFolderModeEnum.Effacer,
							AutoUpdateFolderModeEnum.Effacer,
							AutoUpdateFolderModeEnum.Effacer);
					return;
				}

				// cas d'un repertoire exclus => appliquer parametres d'update
				if (DispatchDatabase.properties.excludedDirectories.contains(olFolder.getEntryID())) {
					doUpdateFolderDefinition(
							olFolderEMail,
							excludedAdressesFilterPattern,
							updateParam.excludedAutoKeywordsBlock,
							updateParam.excludedAdressBlock,
							updateParam.excludedConversationBlock);
					return;
				}

				// cas d'un repertoire standard => appliquer parametres d'update
				doUpdateFolderDefinition(
						olFolderEMail,
						excludedAdressesFilterPattern,
						updateParam.defaultAutoKeywordsBlock,
						updateParam.defaultAdressBlock,
						updateParam.defaultConversationBlock);
				return;

			}
		};

		// effectue le scan et la mise a jours des repertoires
		olRootFolder.folderAndSubFoldersStream(excludedOlFolderFilter).forEach(updateFolder);

		LOGGER.info(String.format("Mise � jours repertoires termin�e, dur�e = %d",(System.currentTimeMillis()-start)/1000));

	}

	// -------------------------------------------------------------------------------------------
	//
	// Utilitaires
	//
	// -------------------------------------------------------------------------------------------

	/**
	 * Met � jour la description d'un repertoire � partir des mails qu'il contient
	 *
	 * @param olFolderEMail
	 *            repertoire � traiter
	 * @param excludedAdressesFilterPattern
	 *            filtre d'exclusion des adresses
	 */
	private static void doUpdateFolderDefinition(OlFolderEMail olFolderEMail, Pattern excludedAdressesFilterPattern, AutoUpdateFolderModeEnum addressBlockMode,
			AutoUpdateFolderModeEnum autoKeywordsBlockMode, AutoUpdateFolderModeEnum conversationBlockMode) {

		LOGGER.debug("MaJ repertoire :" + olFolderEMail.getName());

		// Recupere les donnees du repertoire
		OlFolderData dispatcherFolderData = OlFolderDataUtility.load(olFolderEMail);

		// efface les donn�es autokeyword si demand�
		if (autoKeywordsBlockMode != AutoUpdateFolderModeEnum.Laisser) {
			LOGGER.trace("=>Efface donn�es autoKeywords :" + olFolderEMail.getName());
			dispatcherFolderData.clearAutoKeywords();
		}

		// efface les donn�es addresse si demand�
		if (addressBlockMode != AutoUpdateFolderModeEnum.Laisser) {
			LOGGER.trace("=>Efface donn�es adresse :" + olFolderEMail.getName());
			dispatcherFolderData.clearEmailAdresses();
		}

		// efface les donn�es conversationId si demand�
		if (conversationBlockMode != AutoUpdateFolderModeEnum.Laisser) {
			LOGGER.trace("=>Efface donn�es conversation :" + olFolderEMail.getName());
			dispatcherFolderData.clearConversationIds();
		}

		// verifie s'il y a besoin de recalculer
		if (
				(addressBlockMode == AutoUpdateFolderModeEnum.Recalculer) ||
				(conversationBlockMode == AutoUpdateFolderModeEnum.Recalculer) ||
				(autoKeywordsBlockMode == AutoUpdateFolderModeEnum.Recalculer)
				) {

			//
			// Recalcul du repertoire
			//

			LinkedHashMap<String, LocalDateTime> conversationDates = new LinkedHashMap<String, LocalDateTime>();

			// balaye les emails dans le dossier et construit la liste des adresses
			for (OlEmail olEmail : olFolderEMail.listMails()) {

				// ajoute les adresses du mail
				if ((addressBlockMode == AutoUpdateFolderModeEnum.Recalculer)&&(!dispatcherFolderData.getInactiveEmailAdresses()))
					addEmailAddressesToFolderData(olEmail, dispatcherFolderData, excludedAdressesFilterPattern);

				// ajoute les mots clefs automatiques
				if ((autoKeywordsBlockMode == AutoUpdateFolderModeEnum.Recalculer)&&(!dispatcherFolderData.getInactiveAutoKeywords()))
					addAutoKeywordsToFolderData(new SimplifiedMail(olEmail), olFolderEMail, dispatcherFolderData);

				// ajoute la conversation et sa date si elle n'est pas enregistr�e ou si elle est ant�rieure
				LocalDateTime previousDate = conversationDates.get(olEmail.getConversationID());
				if ((previousDate == null) || previousDate.isBefore(olEmail.getSentOn()))
					conversationDates.put(olEmail.getConversationID(), olEmail.getSentOn());

				// libere l'objet EMail
				olEmail.forceSafeRelease();
			}

			// realise la mise � jours des conversations
			if (conversationBlockMode == AutoUpdateFolderModeEnum.Recalculer) {

				// enleve les conversation vide
				conversationDates.remove("");

				// Tri les conversations en commencant par la plus recente
				String[] orderedConversationList = conversationDates.entrySet().stream()
						.sorted((c1, c2) -> c2.getValue().compareTo(c1.getValue()))
						.map(x -> x.getKey())
						.toArray(size -> new String[size]);

				// redimensionne le tableau s'il est trop grand
				if (orderedConversationList.length > properties.maxConversationNbrParam)
					orderedConversationList = Arrays.copyOf(orderedConversationList, properties.maxConversationNbrParam);

				// stocke les conversation en ordre inverse
				for (int index = orderedConversationList.length - 1; index >= 0; index--)
					dispatcherFolderData.getConversationIds().add(orderedConversationList[index]);

			}
		}

		// sauvegarde
		OlFolderDataUtility.save(dispatcherFolderData,olFolderEMail);
		LOGGER.trace("MaJ repertoire termin�e :" + olFolderEMail.getName());

	}

	private static void addAutoKeywordsToFolderData(SimplifiedMail simplifiedMail, @SuppressWarnings("unused") OlFolderEMail destFolder, OlFolderData dispatcherFolderData) {

		// balaie les mots clefs du titre du mail et identifie les mots clefs majeurs pour les ajouter � la liste ou incrementer le compteur de 1 pour chaque mot
		for (String word : simplifiedMail.getTitleWords())
			if (word.startsWith(CTecReformating.OPEN_CAR))
				if (!word.contains("@"))
					dispatcherFolderData.getAutoKeywords().add(word,1);

		// balaie les mots du corps et identifie les mots clefs majeurs pour les ajouter � la liste ou incrementer le compteur de 1 pour chaque mot
		for (String word : simplifiedMail.getBodyWords())
			if (word.startsWith(CTecReformating.OPEN_CAR))
				if (!word.contains("@"))
					dispatcherFolderData.getAutoKeywords().add(word,1);

	}


	private static void addEmailAddressesToFolderData(OlEmail eMail, OlFolderData dispatcherFolderData, Pattern excludedAdressesFilterPattern) {

		// ajoute l'emeteur
		dispatcherFolderData.getEmailAdresses().add(formatEmailAddress(eMail.getFromAdress(), excludedAdressesFilterPattern));

		// ajoute les destinataires s'ils ne sont pas trop nombreux (spam ou mail general sinon)
		if ((EMailAutoDispatcher.properties.maxAdressesNbrParam<=0)||(eMail.getToAdressesCount() < properties.maxAdressesNbrParam))
			for (String address : eMail.getToAdresses())
			dispatcherFolderData.getEmailAdresses().add(formatEmailAddress(address, excludedAdressesFilterPattern));

		// ajoute les destinataires en copie s'ils ne sont pas trop nombreux (spam ou mail general sinon)
		if ((EMailAutoDispatcher.properties.maxAdressesNbrParam<=0)||(eMail.getCcAdressesCount() < properties.maxAdressesNbrParam))
			for (String address : eMail.getCcAdresses())
			dispatcherFolderData.getEmailAdresses().add(formatEmailAddress(address, excludedAdressesFilterPattern));

	}

	private static String formatEmailAddress(String address, Pattern excludedAdressesFilterPattern) {

		if (address == null) return null;

		String addressOut = address.toLowerCase().trim();

		// ne prends les adresses vides
		if (addressOut.isEmpty()) return null;

		// ne prends pas en compte les adresses mal format�es qui ne seraient pas d�tect�es de toute fa�on
		if (!searchEmailAddressInStringPattern.matcher(addressOut).find()) return null;

		// ne prends pas en compte les adresses deja presentes
		if (excludedAdressesFilterPattern.matcher(addressOut).find()) return null;

		return addressOut;

	}

	private static Pattern buildFilterPattern(StringHashSetField excludedAddressesParam) {
		// construit le filtre pour exclure les adresses
		String excludedAdressesFilter = "a^";
		for (String excludedAddress : excludedAddressesParam)
			excludedAdressesFilter += "|" + excludedAddress;
		return Pattern.compile(excludedAdressesFilter);
	}

}
