package fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.ctecfilter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CTecReformating {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static String OPEN_CAR = "(";
	public final static String CLOSE_CAR = ")";

	private final static Pattern searchSpecialCaractersInStringPattern = Pattern.compile("[^a-z0-9_.+\\-@]");
	private final static Pattern searchEmailAddressInStringPattern = Pattern.compile("[a-z0-9_.+\\-]+@[a-z0-9\\-]+\\.[a-z0-9\\-.]+");
	private final static Pattern searchSpecialCharactersInStringPattern = Pattern.compile("[_.+\\-@](?![^(]*\\))");

	private final static Pattern wordsPattern = Pattern.compile("((\\([^\\)]*\\))|([a-z0-9]{3,}))");

	private final static HashSet<String> stopWords = new HashSet<String>();
	static {
		importStopWords("stopwords-en.txt");
		importStopWords("stopwords-fr.txt");
		importStopWords("stopwords-ctec.txt");
	}

//	private final static String INTERN_ROOT = "rd=";
//	private final static String PROJECT_ROOT = "pr=";
//	private final static String OPORTUNITY_ROOT = "op=";

//	private final static Pattern INTERN_CODE_PATTERN = Pattern.compile("\\b(ar|br|aar|abr|arp|arg|arm)(\\d{4})\\b");
//	private final static Pattern PROJECT_CODE_PATTERN = Pattern.compile("\\b([ap]?[ab][esw])(\\d{4})[a-z]?\\b");
//	private final static Pattern OPORTUNITY_CODE_PATTERN = Pattern.compile("op(\\d{4})");

//	private final static String INTERN_ROOT_REP = OPEN_CAR + INTERN_ROOT + "$2" + CLOSE_CAR;
//	private final static String PROJECT_ROOT_REP = OPEN_CAR + PROJECT_ROOT + "$2" + CLOSE_CAR;
//	private final static String OPORTUNITY_ROOT_REP = OPEN_CAR + OPORTUNITY_ROOT + "$1" + CLOSE_CAR;

	private static enum CTecProjectCodes {
		
		OPORTUNITY_CODE("op(\\d{4})",OPEN_CAR + "op=" + "$1" + CLOSE_CAR),
		PROJECT_CODE("\\b([ap]?[ab][esw])(\\d{4})[a-z]?\\b",OPEN_CAR + "pr=" + "$2" + CLOSE_CAR), // projets standards aae, abe, aas, ...
		AR_CODE("\\b(ar|aar)(\\d{4})\\b",OPEN_CAR + "ar=" + "$2" + CLOSE_CAR),
		BR_CODE("\\b(br|abr)(\\d{4})\\b",OPEN_CAR + "br=" + "$2" + CLOSE_CAR),
		ARP_CODE("\\b(arp)(\\d{4})\\b",OPEN_CAR + "arp=" + "$2" + CLOSE_CAR),
		ARG_CODE("\\b(arg)(\\d{4})\\b",OPEN_CAR + "arg=" + "$2" + CLOSE_CAR),
		ARM_CODE("\\b(arm)(\\d{4})\\b",OPEN_CAR + "arm=" + "$2" + CLOSE_CAR);
		
		private Pattern searchPattern;
		private String repPattern;
		
		public String CodeReplace(String strIn) {
			return searchPattern.matcher(strIn).replaceAll(repPattern);
		}
		
		private CTecProjectCodes(String textPattern, String repPattern) {
			this.searchPattern=Pattern.compile(textPattern);
			this.repPattern=repPattern;
		}
		
	}
	
	public static String cleanString(String stringIn) {

		String stringOut = stringIn;

		stringOut = cleanString1(stringOut);
		stringOut = cleanString2(stringOut);
		stringOut = cleanString3(stringOut);

		return stringOut;

	}

	public static boolean testStopWord(String word) {
		return stopWords.contains(word);
	}

	/**
	 * Nettoie les characteres sp�ciaux (hors adresse email) et passe en minuscule
	 *
	 * @param stringIn Chaine � traiter
	 * @return Chaine trait�e
	 */
	public static String cleanString1(String stringIn) {

		String stringOut = stringIn;

		// supprime les caracteres regionaux
		stringOut= Normalizer.normalize(stringOut, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

		// minuscules
		stringOut = stringOut.toLowerCase().trim();

		return stringOut;
	}

	/**
	 * Isole les mails en parenthese et les codes speciaux CTEC
	 *
	 * @param stringIn Chaine � traiter
	 * @return Chaine trait�e
	 */
	public static String cleanString2(String stringIn) {

		String stringOut = stringIn;

		// enleve les caracteres sp�ciaux sauf ceux permettant codage adresse mail
		stringOut = searchSpecialCaractersInStringPattern.matcher(stringOut).replaceAll(" ");

		// detection des adresses mail
		stringOut = searchEmailAddressInStringPattern.matcher(stringOut).replaceAll("($0)");

		// supression des caracteres speciaux residuels
		stringOut = searchSpecialCharactersInStringPattern.matcher(stringOut).replaceAll(" ");

		// detection et remplacement des codes sp�ciaux CTEC
		
//		stringOut = INTERN_CODE_PATTERN.matcher(stringOut).replaceAll(INTERN_ROOT_REP);
//		stringOut = PROJECT_CODE_PATTERN.matcher(stringOut).replaceAll(PROJECT_ROOT_REP);
//		stringOut = OPORTUNITY_CODE_PATTERN.matcher(stringOut).replaceAll(OPORTUNITY_ROOT_REP);

		for ( CTecProjectCodes rep : CTecProjectCodes.values() )
			stringOut = rep.CodeReplace(stringOut);
		
		return stringOut;

	}

	/**
	 * Supprime les stopWords
	 *
	 * @param stringIn Chaine � traiter
	 * @return Chaine trait�e
	 */
	public static String cleanString3(String stringIn) {

		// decoupe la chaine
		Matcher matcher = wordsPattern.matcher(stringIn);

		// scanne et reconstruit la chaine sans les stopWords
		StringBuilder stringBuilder = new StringBuilder();
		while (matcher.find()) {
			String word = matcher.group(1);
			if (!stopWords.contains(word)) {
				stringBuilder.append(word);
				stringBuilder.append(" ");
			}
		}
		return stringBuilder.toString();

	}


	public static void addStopWords(Collection<? extends String> stopWordsList) {
		stopWords.addAll(stopWordsList);
	}

	public static void addStopWord(String word) {
		String cleanedWord = cleanString(word).trim();
		if (cleanedWord.length() > 0) stopWords.add(cleanedWord);
	}

	private static void importStopWords(String filename) {
		LOGGER.debug("Reading " + filename.toString());

		try (InputStream inputStream = CTecReformating.class.getResourceAsStream(filename)) {

			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}

			// StandardCharsets.UTF_8.name() > JDK 7
			StringTokenizer st = new StringTokenizer(result.toString("UTF-8"));
			while (st.hasMoreTokens())
				stopWords.add(st.nextToken());

		} catch (IOException e) {
			LOGGER.debug("Error reading : ", e);
		}

	}

	static String readFile(String path, Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	//
	// Utilities
	//

	public static Iterable<String> getWords(String string) {
		return new Iterable<String>() {
			private final StringTokenizer token = new StringTokenizer(string);
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					@Override
					public boolean hasNext() {
						return token.hasMoreTokens();
					}
					@Override
					public String next() {
						return token.nextToken();
					}
				};
			}
		};
	}

//	// Permet de tester la compilation
//
//	public static void main(String[] args)
//	{
////		String in =" AE2933 sdf <toto-@tot-gmail.com> qsdqsd francois.barillot@cedrat-tec.com qsd1 qsd qd qd31";
////		String in ="inovallee 38246 ptbl premises eric verbist mailto (ev@servotronic.be) envoye mardi octobre 2013 (francois.barillot@cedrat-tech.com) objet bekaert";
//		String in ="AE2933 inovallee 38246 AE2933_toto (ev@servotronic.be) envoye mardi octobre 2013 francois.barillot@cedrat-tech.com objet bekaert";
//		System.out.println(in);
//		System.out.println(cleanString(in));
//	}


}
