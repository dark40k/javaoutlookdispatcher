package fr.dark40k.outlookdispatcher.dispatchdatabase.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.util.SizedStack;
import fr.dark40k.util.persistantparameters.ParamPersistentGroup;
import fr.dark40k.util.persistantproperties.fields.ParamPersistentGroupField;

public class MoveToFolderHistory implements Iterable<OlFolderEMail> {

	private final static Logger LOGGER = LogManager.getLogger();

	private final static int MAXLISTSIZE=2500;
	private final static int MAXLISTDAYS=100;

	private final ParamPersistentGroupField moveToFolderHistoryParam;

	private final SizedStack<FolderItem> folderHistoryStack = new SizedStack<FolderItem>(MAXLISTSIZE);

	private final ArrayList<OlFolderEMail> sortedFolderList = new ArrayList<OlFolderEMail>();

	private boolean needUpdate;

	//
	// Constructeur
	//

	public MoveToFolderHistory(ParamPersistentGroupField moveToFolderHistory) {
		this.moveToFolderHistoryParam=moveToFolderHistory;
		needUpdate=true;
	}

	//
	// Recuperation de la liste des repertoires
	//

	@Override
	public Iterator<OlFolderEMail> iterator() {
		if (needUpdate) updateSortedFolderList();
		return sortedFolderList.iterator();
	}

	private void updateSortedFolderList() {

		sortedFolderList.clear();

		HashMap<OlFolderEMail, Integer> listFolderCount = new HashMap<OlFolderEMail, Integer>();

		for (FolderItem item : folderHistoryStack) {
			Integer counterFolder = listFolderCount.get(item.olFolder);
			listFolderCount.put(item.olFolder, 1 + ((counterFolder == null) ? 0 : counterFolder));
		}

		listFolderCount.entrySet().stream()
			.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
			.forEach(entry -> sortedFolderList.add(entry.getKey()));

		needUpdate = false;

	}

	//
	// Ajout d'une entr�e
	//

	public void push(OlFolderEMail olFolder) {

		if (olFolder==null) return;

		folderHistoryStack.push(new FolderItem(olFolder, LocalDateTime.now()));

		needUpdate = true;
		fireHistoryChangedEvent();
	}

	//
	// Sauvegarde / restauration des donn�es
	//

	public void loadParams() {

		LOGGER.info("Chargement de l'historique de mouvements");

		LocalDateTime now = LocalDateTime.now();

		folderHistoryStack.clear();

		for (String indexName : moveToFolderHistoryParam.listSubParamsNames(true) ) {

			ParamPersistentGroup folderData = moveToFolderHistoryParam.getSubParamPersistentGroup(indexName);

			String folderID = folderData.getSubParamString("id", null).getValue();
			String accessTimeString = folderData.getSubParamString("access", null).getValue();
			//String name = folderData.getSubParamString("name", null).getValue();

			OlFolderEMail folder = (OlFolderEMail) OlConnector.getOlObject(folderID);
			if (folder==null) {
				LOGGER.warn("Folder does not exist, skipping - ID="+folderID);
				continue;
			}

			LocalDateTime accessTime;
			try {
				accessTime = LocalDateTime.parse(accessTimeString);
			} catch (DateTimeParseException e) {
				LOGGER.warn("Cannot parse time, skipping - ID="+folderID+", Time="+accessTimeString);
				continue;
			}


			if (ChronoUnit.DAYS.between(accessTime, now )<=MAXLISTDAYS)
					folderHistoryStack.push(new FolderItem(folder, accessTime));

		}

		needUpdate = true;
		fireHistoryChangedEvent();
	}

	public void saveParams() {

		LOGGER.info("Sauvegarde de l'historique de mouvements");

		int index=0;
		LocalDateTime now = LocalDateTime.now();

		// Efface les anciens enregistrements
		moveToFolderHistoryParam.clear();

		for (FolderItem listItem : folderHistoryStack)

			if (ChronoUnit.DAYS.between(listItem.lastAccess, now) <= MAXLISTDAYS) {

				ParamPersistentGroup folderData = moveToFolderHistoryParam.getSubParamPersistentGroup("folder" + index);

				folderData.getSubParamString("id", null).setValue(listItem.olFolder.getEntryID());
				folderData.getSubParamString("access", null).setValue(listItem.lastAccess.toString());
				folderData.getSubParamString("name", null).setValue(listItem.olFolder.getName());

				index++;

			}

	}

	//
	// Sub class FolderItem
	//

	private class FolderItem implements Comparable<FolderItem> {

		OlFolderEMail olFolder;
		LocalDateTime lastAccess;

		public FolderItem(OlFolderEMail olFolder, LocalDateTime lastAccess) {
			this.olFolder=olFolder;
			this.lastAccess=lastAccess;
		}

		@Override
		public int compareTo(FolderItem o) {
			return o.lastAccess.compareTo(this.lastAccess);
		}

	}

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		public UpdateEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateListener extends EventListener {
		public void historyChanged(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}

	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}

	public void fireHistoryChangedEvent() {
		UpdateEvent evt = new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i + 1]).historyChanged(evt);
			}
		}
	}

}
