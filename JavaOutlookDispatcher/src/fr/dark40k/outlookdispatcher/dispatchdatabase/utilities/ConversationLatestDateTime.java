package fr.dark40k.outlookdispatcher.dispatchdatabase.utilities;

import java.time.LocalDateTime;
import java.util.HashMap;

import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class ConversationLatestDateTime {

	private final HashMap<String, LocalDateTime> listConversationsNewestLocalDateTime = new HashMap<String, LocalDateTime>();

	public LocalDateTime get(String conversationID) {
		return listConversationsNewestLocalDateTime.get(conversationID);
	}

	public void reset(Iterable<OlFolderEMail> searchFolderList) {
		listConversationsNewestLocalDateTime.clear();
		for (OlFolderEMail folder : searchFolderList)
			for (OlEmail email : folder.listMails()) {
				LocalDateTime conversationNewestLocalDateTime = listConversationsNewestLocalDateTime.get(email.getConversationID());
				if (conversationNewestLocalDateTime == null)
					listConversationsNewestLocalDateTime.put(email.getConversationID(), email.getSentOn());
				else if (conversationNewestLocalDateTime.isBefore(email.getSentOn()))
					listConversationsNewestLocalDateTime.put(email.getConversationID(), email.getSentOn());
			}
	}

}
