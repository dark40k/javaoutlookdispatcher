package fr.dark40k.outlookdispatcher.dispatchdatabase.utilities;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.util.persistantparameters.ParamGroup;
import fr.dark40k.util.persistantparameters.ParamPersistentGroup;
import fr.dark40k.util.persistantproperties.fields.ParamPersistentGroupField;

public class ManualDestinationFolders {

	private final static Logger LOGGER = LogManager.getLogger();

	public static final String NO_DESTINATION_FOLDER_ID = "";

	private final ParamPersistentGroupField manualDestFoldersParam;

	public ManualDestinationFolders(ParamPersistentGroupField manualDestFoldersParam) {
		this.manualDestFoldersParam = manualDestFoldersParam;
	}

	public void saveDestination(ArrayList<ToSortEmailSingle> emailDataList) {

		LOGGER.info("Sauvegarde des mouvements manuels");

		manualDestFoldersParam.clear();

		int count = 1;

		for (ToSortEmailSingle emailData : emailDataList)
			if (emailData.isManualDest()) {

				ParamGroup subGroup = manualDestFoldersParam.getSubParamGroup("ID" + count);

				subGroup.getSubParamString("emailID", null).setValue(emailData.getEMail().getEntryID());

				if (emailData.getDestFolder() != null)
					subGroup.getSubParamString("destinationID", null).setValue(emailData.getDestFolder().getEntryID());

				count++;
			}

	}

	public void restoreDestination(ArrayList<ToSortEmailSingle> emailDataList) {

		final LinkedHashMap<String, ToSortEmailSingle> mailDataList = new LinkedHashMap<String, ToSortEmailSingle>();
		for (ToSortEmailSingle emailData : emailDataList)
			mailDataList.put(emailData.getEMail().getEntryID(), emailData);

		for (String name : manualDestFoldersParam.listSubParamsNames(true)) {

			ParamPersistentGroup subGroup = manualDestFoldersParam.getSubParamPersistentGroup(name);

			String mailId = subGroup.getSubParamString("emailID", null).getValue();

			ToSortEmailSingle mailData = mailDataList.get(mailId);

			if (mailData == null)
				return;

			String destinationId = subGroup.getSubParamString("destinationID", NO_DESTINATION_FOLDER_ID).getValue();

			if (!destinationId.equals(NO_DESTINATION_FOLDER_ID)) {
				mailData.setDestFolder((OlFolderEMail) OlConnector.getOlObject(destinationId), true);
			} else {
				mailData.setDestFolder(null, true);
			}

		}

	}

}
