package fr.dark40k.outlookdispatcher.dispatchdatabase;

import java.util.HashSet;
import java.util.Iterator;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.WeightedDestFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;

public class ToSortEmailGroup extends ToSortEmail {

	public static final String NODESTINATIONFOLDERID = "";

	HashSet<ToSortEmailSingle> emailDataList = new HashSet<ToSortEmailSingle>();

	private Boolean isManualDestination  = null; // null => informations incohérentes ou groupe vide

	//
	// Constructors
	//

	// create empty EmailDataGroup
	public ToSortEmailGroup(DispatchDatabase dispatchDatabase) {
		super(dispatchDatabase);
	}

	public ToSortEmailGroup(DispatchDatabase dispatchDatabase, Iterable<ToSortEmailSingle> emailDataSet) {

		this(dispatchDatabase);

		if (!emailDataSet.iterator().hasNext()) return;

		boolean first = false;

		for (ToSortEmailSingle emailData : emailDataSet) {

			potentialFoldersList.addPotentialFolderList(emailData.potentialFoldersList);

			OlFolderEMail curentDestFolder = emailData.getDestFolder();
			if (curentDestFolder!=null)
				if (!potentialFoldersList.contains(curentDestFolder))
					potentialFoldersList.addWeightedDestFolder(new WeightedDestFolder(curentDestFolder,0,"Manual"));

			if (first) {
				isManualDestination=emailData.isManualDestination;
				first=true;
			} else {
				if (isManualDestination!=null)
					if (isManualDestination!=emailData.isManualDestination)
						isManualDestination=null;
			}

			emailDataList.add(emailData);

		}

	}

	//
	// Methods
	//

	public String getComonDestFolderId() {

		if (emailDataList.size()==0) return null;

		Iterator<ToSortEmailSingle> iter = emailDataList.iterator();

		OlFolderEMail destFolder = iter.next().destFolder;

		if (destFolder==null) {
			while(iter.hasNext()) {
				if (iter.next().destFolder!=null) return null;
			}
			return NODESTINATIONFOLDERID;
		}

		while (iter.hasNext()) {
			if (!destFolder.equals(iter.next().destFolder)) return null;
		}

		return destFolder.getEntryID();

	}

	@Override
	public void setDestFolder(OlFolderEMail newFolder, boolean isManual) {

		UndoableActionSetDestFolder undoableAction = new UndoableActionSetDestFolder(dispatchDatabase);

		isManualDestination = isManual;

		for (ToSortEmailSingle emailData : emailDataList) {

			if (emailData.isManualDestination==isManual) {
				if (emailData.destFolder==newFolder) continue;
				if ((emailData.destFolder!=null) && (emailData.destFolder.equals(newFolder))) continue;
			}

			undoableAction.addEmail(emailData);
			emailData.destFolder=newFolder;
			emailData.isManualDestination=isManual;
			dispatchDatabase.fireDestFolderChangedEvent(emailData);
		}

		if (undoableAction.getFoldersCount()>0) StaticUndoManager.addUndoableAction(undoableAction);

	}


	@Override
	public void setManualDestFolder() {

		UndoableActionSetDestFolder undoableAction = new UndoableActionSetDestFolder(dispatchDatabase);

		for (ToSortEmailSingle emailData : emailDataList) {

			// pas de changement si deja destination manuelle
			if (emailData.isManualDestination) continue;

			undoableAction.addEmail(emailData);
			emailData.isManualDestination=true;

			dispatchDatabase.fireDestFolderChangedEvent(emailData);
		}

		if (undoableAction.getFoldersCount()>0) StaticUndoManager.addUndoableAction(undoableAction);

	}

	@Override
	public void setAutoDestFolder() {

		UndoableActionSetDestFolder undoableAction = new UndoableActionSetDestFolder(dispatchDatabase);

		boolean isManual = false;

		for (ToSortEmailSingle emailData : emailDataList) {

			OlFolderEMail newFolder = (emailData.potentialFoldersList.getCount()>0) ? emailData.potentialFoldersList.getFolder(0) : null;

			if (emailData.isManualDestination==isManual) {
				if (emailData.destFolder==newFolder) continue;
				if ((emailData.destFolder!=null) && (emailData.destFolder.equals(newFolder))) continue;
			}

			undoableAction.addEmail(emailData);
			emailData.destFolder=newFolder;
			emailData.isManualDestination=isManual;
			dispatchDatabase.fireDestFolderChangedEvent(emailData);
		}

		if (undoableAction.getFoldersCount()>0) StaticUndoManager.addUndoableAction(undoableAction);

	}


	@Override
	public Boolean isManualDest() {
		return isManualDestination;
	}

	public boolean contains(ToSortEmailSingle emailData) {
		return emailDataList.contains(emailData);
	}

	public int mailCount() {
		return emailDataList.size();
	}

	public int mailCountMoveable() {
		int count=0;
		for (ToSortEmailSingle emailData : emailDataList) {
			if (emailData.destFolder!=null)
				count+=1;
		}
		return count;
	}

	public void doMoveEmails(JProgressDialog dialog) {
		dispatchDatabase.doMoveEmails(emailDataList,"Ranger Email",dialog);
	}

	public Iterator<ToSortEmailSingle> getEmailsDataIterator() {
		return emailDataList.iterator();
	}

}
