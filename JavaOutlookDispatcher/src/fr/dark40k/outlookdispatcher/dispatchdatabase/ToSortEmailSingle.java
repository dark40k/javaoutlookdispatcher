package fr.dark40k.outlookdispatcher.dispatchdatabase;

import org.jdom2.Element;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;

public class ToSortEmailSingle extends ToSortEmail {

	protected OlEmail email;
	
	protected OlFolderEMail destFolder;
	
	protected boolean isManualDestination;
	
	//
	// Constructeur
	//
	
	public ToSortEmailSingle(DispatchDatabase dispatchDatabase, OlEmail email) {
		super(dispatchDatabase);
		this.email=email;
	}
	
	//
	// Import/Export XML
	//
	
	public ToSortEmailSingle(DispatchDatabase dispatchDatabase, OlEmail email, Element elt) {
		super(dispatchDatabase,elt);
		
		String destFolderIDAttr = elt.getAttributeValue("destFolderID");
		destFolder = (destFolderIDAttr!=null) ? (OlFolderEMail) OlConnector.getOlObject(destFolderIDAttr) : null;
		
		this.email=email;
	}
	
	@Override
	public Element exportToXML() {
		Element elt = super.exportToXML();
		if (destFolder!=null) elt.setAttribute("destFolderID", destFolder.getEntryID());
		return elt;
	}


	//
	// Getters/Setters
	//
	
	public OlEmail getEMail() {
		return email;
	}
	
	public OlFolderEMail getDestFolder() {
		return destFolder;
	}

	@Override
	public void setDestFolder(OlFolderEMail newFolder, boolean isManual) {
		
		if (isManualDestination == isManual) {
			if (destFolder == newFolder)
				return;
			if ((destFolder != null) && (destFolder.equals(newFolder)))
				return;
		}
		
		isManualDestination=isManual;

		UndoableActionSetDestFolder undoableAction = new UndoableActionSetDestFolder(dispatchDatabase);
		undoableAction.addEmail(this);
		
		destFolder = newFolder;
		
		dispatchDatabase.fireDestFolderChangedEvent(this);
		
		StaticUndoManager.addUndoableAction(undoableAction);

	}
	
	@Override
	public void setAutoDestFolder() {
		if (potentialFoldersList.getCount()==0)
			setDestFolder(null,false);
		else
			setDestFolder(potentialFoldersList.getFolder(0),false);
	}
	
	@Override
	public void setManualDestFolder() {
		
		// pas de modification si deja manuel
		if (isManualDestination) return;
		
		UndoableActionSetDestFolder undoableAction = new UndoableActionSetDestFolder(dispatchDatabase);
		
		undoableAction.addEmail(this);
		
		isManualDestination=true;
		
		dispatchDatabase.fireDestFolderChangedEvent(this);
		
		StaticUndoManager.addUndoableAction(undoableAction);
		
	}

	@Override
	public Boolean isManualDest() {
		return isManualDestination;
	}
	
	
	
	//
	// Divers
	//
	
	public boolean destFolderEquals(OlFolder folder) {
		if (destFolder==folder) return true;
		if (destFolder==null) return false;
		return destFolder.equals(folder);
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof ToSortEmailSingle)) return false;
	    return email.equals(((ToSortEmailSingle) other).email);
	}



}