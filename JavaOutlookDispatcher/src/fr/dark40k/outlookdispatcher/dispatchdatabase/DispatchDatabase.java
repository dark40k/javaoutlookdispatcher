package fr.dark40k.outlookdispatcher.dispatchdatabase;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map.Entry;

import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.com.ComFailException;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.PotentialFoldersList;
import fr.dark40k.outlookdispatcher.dispatchdatabase.utilities.ConversationLatestDateTime;
import fr.dark40k.outlookdispatcher.dispatchdatabase.utilities.ManualDestinationFolders;
import fr.dark40k.outlookdispatcher.dispatchdatabase.utilities.MoveToFolderHistory;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;

public class DispatchDatabase {

	private final static Logger LOGGER = LogManager.getLogger();

	public static final String NODESTINATIONFOLDERID = "";

	//
	// Parametres
	//

	//
	// Donn�es
	//

	private boolean loaded=false;

	public final static DispatchDatabaseProperties properties = new DispatchDatabaseProperties();

	// marque si la base est en train de subir un train de modification
	private boolean modificationState;

	// liste des repertoires scannes
	private final LinkedHashSet<OlFolderEMail> searchFolderList = new LinkedHashSet<OlFolderEMail>();

	// liste des emails a classer
	private final ArrayList<ToSortEmailSingle> toSortEmailSingleList = new ArrayList<ToSortEmailSingle>();

	// Objets utiles

	private final ManualDestinationFolders manualDestinationFolders;

	private final ConversationLatestDateTime conversationLatestDateTime;

	public final MoveToFolderHistory moveToFolderHistory;

	// Objet pour execution bufferis�e
	public final BufferedMoveEmailMgr bufferedMoves;

	//
	// Constructeur
	//

	public DispatchDatabase() {

		loaded = false;

		modificationState = false;

		conversationLatestDateTime = new ConversationLatestDateTime();

		manualDestinationFolders = new ManualDestinationFolders(properties.manualDestFoldersParam);

		moveToFolderHistory = new MoveToFolderHistory(properties.moveToFolderHistory);

		bufferedMoves = new BufferedMoveEmailMgr(null);

	}

	//
	// Chargement et sauvegarde des donn�es
	//

	public void saveConfiguration() {

		LOGGER.info("Sauvegarde du param�trage.");
		if (!loaded) {
			LOGGER.info("La base n'a jamais �t� charg�e, preserve les donn�es ant�rieures.");
			return;
		}

		manualDestinationFolders.saveDestination(toSortEmailSingleList);
		moveToFolderHistory.saveParams();

	}

	public void loadConfiguration() {
		manualDestinationFolders.restoreDestination(toSortEmailSingleList);
		moveToFolderHistory.loadParams();
	}

	//
	// Database getters
	//

	public boolean getModificationState() {
		return modificationState;
	}

	public int getSize() {
		return toSortEmailSingleList.size();
	}

	public ToSortEmailSingle getEMailData(int row) {
		return toSortEmailSingleList.get(row);
	}

	public Iterable<ToSortEmailSingle> getEMailDataList() {
		return toSortEmailSingleList;
	}

	public int getIndex(ToSortEmailSingle emailData) {
		return toSortEmailSingleList.indexOf(emailData);
	}

	public Iterable<OlFolderEMail> getSearchedFolders() {
		return new Iterable<OlFolderEMail>() {
			@Override
			public Iterator<OlFolderEMail> iterator() {
				return searchFolderList.iterator();
			}
		};
	}

	public LocalDateTime getConversationNewestTime(String conversationID) {
		return conversationLatestDateTime.get(conversationID);
	}

	//
	// Raw database methodes - reserv�es pour undo/redo
	//

	void rawAddEMailDatasToDatabase(Collection<ToSortEmailSingle> addEmailDataList) {
		this.toSortEmailSingleList.addAll(addEmailDataList);
		conversationLatestDateTime.reset(searchFolderList);
		fireFullDatabaseChangeEvent();
	}

	void rawRemoveEMailDatasFromDatabase(Collection<ToSortEmailSingle> remEmailDataList) {
		this.toSortEmailSingleList.removeAll(remEmailDataList);
		conversationLatestDateTime.reset(searchFolderList);
		fireFullDatabaseChangeEvent();
	}


	//
	// Deplacement des emails
	//

	public void doMoveEmails(Collection<ToSortEmailSingle> emailDataListToMove, String title, JProgressDialog dialog) {
		doMoveEmails(emailDataListToMove, title, null, true, dialog);
	}

	public void doMoveEmails(Collection<ToSortEmailSingle> emailDataListToMove, String title, OlFolderEMail forceDestFolder, boolean learnFromMove, JProgressDialog dialog) {

		LOGGER.info("Deplacement mails, D�but, qt�="+emailDataListToMove.size());
		if (dialog!=null) dialog.setManual("Deplacement mails ("+emailDataListToMove.size()+")", 0, 0, emailDataListToMove.size());

		UndoableActionMoveEmails undoableAction = new UndoableActionMoveEmails(this, title);

		for (ToSortEmailSingle mailData : emailDataListToMove) {

			// cas ou il n'y a pas de destination => on ne fait rien
			if ((mailData.getDestFolder() == null)&&(forceDestFolder==null))
				continue;

			// recupere l'index du mail
			int row = toSortEmailSingleList.indexOf(mailData);
			if (row < 0)
				continue;

			// Stocke les informations pour faire le retour
			undoableAction.addMovement(mailData, mailData.getEMail().getParentId(), (forceDestFolder==null) ? mailData.getDestFolder().getEntryID() : forceDestFolder.getEntryID(), learnFromMove);

			// Met a jours la table des Emails
			toSortEmailSingleList.remove(row);

			// Met a jour l'historique des destination
			moveToFolderHistory.push((forceDestFolder==null) ? mailData.getDestFolder() : forceDestFolder);

			// Met � jours l'affichage
			if (dialog!=null) dialog.incManualProgress();

		}

		undoableAction.addToRunStack();

		StaticUndoManager.addUndoableAction(undoableAction);

		// TODO Trop lourd de tout recalculer!! + Erreur il faudrait utiliser toSortEmailSingleList
		conversationLatestDateTime.reset(searchFolderList);

		fireStructureChangedEvent();

		LOGGER.info("Deplacement mails, Termin�.");
	}


	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		private boolean databaseUpdateStatus;
		public UpdateEvent(Object source) {
			super(source);
			databaseUpdateStatus = getModificationState();
		}
		public boolean getDatabaseUpdateStatus() {
			return databaseUpdateStatus;
		}
	}

	public interface UpdateListener extends EventListener {
		public void destFolderChanged(UpdateEvent evt, ToSortEmailSingle emailData);

		public void structureChanged(UpdateEvent evt);

		public void fullDatabaseChanged(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}

	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}

	public void fireDestFolderChangedEvent(ToSortEmailSingle emailData) {
		UpdateEvent evt = new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i + 1]).destFolderChanged(evt, emailData);
			}
		}
	}

	public void fireStructureChangedEvent() {
		UpdateEvent evt = new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i + 1]).structureChanged(evt);
			}
		}
	}

	public void fireFullDatabaseChangeEvent() {
		UpdateEvent evt = new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i + 1]).fullDatabaseChanged(evt);
			}
		}
	}

	//
	// Construction de la base de donn�e
	//

	public void resetDatabase(boolean selectConversation, JProgressDialog progressDialog) {

		// Demarre la mise � jours de la base

		LOGGER.info("Rebuilding database - Start");

		modificationState = true;

		// Recharge les objets outlook

		progressDialog.setAuto("Reinitialisation des objets Outlook");
		OlConnector.refresh();

		// Recuperation des dossiers � scanner

		LOGGER.info("Recuperation dossiers a scanner");
		progressDialog.setAuto("Recuperation dossiers a scanner");

		ArrayList<OlFolderEMail> folderList = getScanFolderList();

		// construit la base des mails � traiter

		reBuildEmailDataList(folderList, progressDialog);

		// construit l'outils de r�partition

		EMailAutoDispatcher autoDispatcher = new EMailAutoDispatcher(OlConnector.getRootFolder(), progressDialog);

		// calcule la note de repartition et initialise les dossiers de destination

		if (selectConversation)
			buildPotentialFoldersListsByConversation(autoDispatcher, progressDialog);
		else
			buildPotentialFoldersLists(autoDispatcher, progressDialog);

		progressDialog.setAuto("Application des repertoires de destination");

		applyTopPotentialFolderToAllEmailsDestFolders();

		// Recupere les informations manuelles
		loadConfiguration();

		// Marque que la base a �t� charg�e
		loaded=true;

		// marque la fin de mise � jours de la base de donn�e

		modificationState=false;


		fireFullDatabaseChangeEvent();

		LOGGER.info("Rebuilding database - End");

	}

	private static ArrayList<OlFolderEMail> getScanFolderList() {

		ArrayList<OlFolderEMail> folderList = new ArrayList<OlFolderEMail>();

		for (String folderID : DispatchDatabase.properties.scanDirectories ) {

			OlFolderEMail folder;
			try {
				folder = (OlFolderEMail) OlConnector.getOlObject(folderID);
			} catch (ComFailException e) {
				folder=null;
			}

			if (folder!=null)
				folderList.add(folder);
			else
				LOGGER.error("Repertoire scan non trouv� = "+folderID);
		}

		return folderList;
	}

	private void reBuildEmailDataList(Collection<? extends OlFolderEMail> scanFolderList, JProgressDialog progressDialog) {

		LOGGER.info("Chargement de la base de donn�es");
		progressDialog.setAuto("Liste les mails � traiter 1/3");

		searchFolderList.clear();
		searchFolderList.addAll(scanFolderList);

		toSortEmailSingleList.clear();

		// evalue le nombre de mails � traiter et met � jours l'affichage de progression
		Integer mailTotalNbr = 0;
		for (OlFolderEMail folder : searchFolderList)
			mailTotalNbr += folder.getItemsCount(false);

		String msgBase = "Liste les mails � traiter ("+mailTotalNbr+" items) 2/3";

		progressDialog.setManual(msgBase, 0, 0, 100);

		// balaie les mails et cr�e les connexion
		Integer processedCount=0;
		for (OlFolderEMail folder : searchFolderList) {

			progressDialog.set(msgBase+", repertoire = "+folder.getName());
			LOGGER.info("Extraction mails, repertoire :"+folder.getPath("/").toString());

			Integer processedCountLocal=processedCount;
			Integer mailTotalNbrLocal = mailTotalNbr;
			for (OlEmail email : folder.listMails((i)->{progressDialog.setVal((int) 100.0*(processedCountLocal+i)/mailTotalNbrLocal);}))
				toSortEmailSingleList.add(new ToSortEmailSingle(this, email));

			processedCount+=folder.getItemsCount(false);

		}

		// MaJ affichage progression
		progressDialog.setAuto("Liste les mails � traiter ("+mailTotalNbr+" items) 3/3");

		// calcule la date de conversation les plus recentes
		conversationLatestDateTime.reset(searchFolderList);

	}

	private void buildPotentialFoldersLists(EMailAutoDispatcher eMailAutoDispatcher, JProgressDialog progressDialog) {

		LOGGER.info("Analyse des mails");
		progressDialog.setManual("Analyse les mails("+toSortEmailSingleList.size()+" mails)", 0, 0, toSortEmailSingleList.size());

		for (ToSortEmailSingle emailData : toSortEmailSingleList) {
			emailData.potentialFoldersList = eMailAutoDispatcher.findBestDestinationFolders(emailData.getEMail());
			progressDialog.incManualProgress();
		}

	}

	private void buildPotentialFoldersListsByConversation(EMailAutoDispatcher eMailAutoDispatcher, JProgressDialog progressDialog) {

		// construit la liste des conversations et des mails associ�s
		LOGGER.info("Construction liste des conversation");
		progressDialog.setManual("Construction liste des conversation.", 0, 0, toSortEmailSingleList.size());

		HashMap<String, HashSet<ToSortEmailSingle>> listConversations = new HashMap<String, HashSet<ToSortEmailSingle>>();

		for (ToSortEmailSingle emailData : toSortEmailSingleList) {

			String conversationID = emailData.getEMail().getConversationID();

			HashSet<ToSortEmailSingle> listMails = listConversations.get(conversationID);

			if (listMails == null) {
				listMails = new HashSet<ToSortEmailSingle>();
				listConversations.put(conversationID, listMails);
			}

			listMails.add(emailData);

			progressDialog.incManualProgress();

		}

		// scanne les conversations
		LOGGER.info("Analyse les conversations");
		progressDialog.setManual("Analyse les conversations ("+listConversations.size()+" conversations)", 0, 0, listConversations.size());

		for (Entry<String, HashSet<ToSortEmailSingle>> conversation : listConversations.entrySet()) {

			PotentialFoldersList potentialFoldersList = new PotentialFoldersList();

			for (ToSortEmailSingle emailData : conversation.getValue()) {
				PotentialFoldersList emailPotentialFoldersList = eMailAutoDispatcher.findBestDestinationFolders(emailData.getEMail());
				potentialFoldersList.addPotentialFolderList(emailPotentialFoldersList, 1.0/conversation.getValue().size());
			}

			for (ToSortEmailSingle emailData : conversation.getValue())
				emailData.potentialFoldersList = potentialFoldersList.clone();

			progressDialog.incManualProgress();
		}

	}

	private void applyTopPotentialFolderToAllEmailsDestFolders() {

		LOGGER.info("Application du chemin par defaut");

		for (ToSortEmailSingle emailData : toSortEmailSingleList)
			if (emailData.potentialFoldersList.getCount() == 0)
				emailData.setDestFolder(null, false);
			else
				emailData.setDestFolder(emailData.potentialFoldersList.getFolder(0), false);

	}


}
