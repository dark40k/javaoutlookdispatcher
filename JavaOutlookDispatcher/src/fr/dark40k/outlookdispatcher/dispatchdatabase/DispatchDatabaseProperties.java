package fr.dark40k.outlookdispatcher.dispatchdatabase;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.fields.ParamPersistentGroupField;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;
import fr.dark40k.util.persistantproperties.tags.PropertyAlias;
import fr.dark40k.util.persistantproperties.tags.PropertyRootClassAlias;

@PropertyRootClassAlias("dispatchDatabase")
public class DispatchDatabaseProperties extends Property {

	@PropertyAlias("scanDirectoriesID")
	public final StringHashSetField scanDirectories = new StringHashSetField();

	@PropertyAlias("excludedDirectoriesID")
	public final StringHashSetField excludedDirectories = new StringHashSetField();

	@PropertyAlias("excludedRecDirectoriesID")
	public final StringHashSetField excludedRecDirectories = new StringHashSetField();

	@PropertyAlias("manualDestFolders")
	public final ParamPersistentGroupField manualDestFoldersParam = new ParamPersistentGroupField();

	@PropertyAlias("destFolderHistory")
	public final ParamPersistentGroupField moveToFolderHistory = new ParamPersistentGroupField();


}
