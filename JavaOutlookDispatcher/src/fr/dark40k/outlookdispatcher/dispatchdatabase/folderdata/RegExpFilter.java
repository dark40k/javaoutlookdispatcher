package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

@Root(name = "RegExpFilter")
public class RegExpFilter {

	private final static Logger LOGGER = LogManager.getLogger();

	private final static Pattern ALLWAYS_FALSE_PATTERN = Pattern.compile("$^");

	public final static int APPLY_TITLE = 1;
	public final static int APPLY_BODY = 2;

	@Attribute
	private String regExp;

	@Attribute(required=false)
	public String description;

	@Attribute
	public double score;

	@Attribute
	public int applyTo;

	// indique si le score est proportionnel au nombre de reussite
	@Attribute
	public boolean multicatch;

	@Transient
	private transient Pattern regExpPattern;

	@Transient
	private transient String regExpErrorMsg;

	//
	// Getters and Setters
	//

	public String getRegExp() {
		return regExp;
	}

	public void setRegExp(String regExp) {
		this.regExp = regExp;
		compilePattern();
	}

	public String getRegExpErrorMsg() {
		return regExpErrorMsg;
	}

	public Pattern getPattern() {
		if (regExpPattern == null) compilePattern();
		return regExpPattern;
	}

	//
	// Methods
	//

	public boolean isValid() {
		if (regExpPattern == null) compilePattern();
		return (regExpPattern != ALLWAYS_FALSE_PATTERN);
	}

	public void updateApplyStatus(int filter, boolean status) {

		if (status) {
			applyTo = applyTo | filter;
		} else {
			applyTo = applyTo & ~filter;
		}

	}

	private void compilePattern() {

		if ((regExp==null)||(regExp.isEmpty())) {
			regExpErrorMsg="RegExp non d�finie (null)";
			regExpPattern = ALLWAYS_FALSE_PATTERN;
			return;
		}

		try {
			regExpPattern = Pattern.compile(regExp);
			regExpErrorMsg="Code valide.";
		} catch (PatternSyntaxException exception) {
			LOGGER.info("Regex pattern failed : "+exception.getMessage());
			regExpErrorMsg=exception.getMessage();
			regExpPattern = ALLWAYS_FALSE_PATTERN;
		}

	}




}
