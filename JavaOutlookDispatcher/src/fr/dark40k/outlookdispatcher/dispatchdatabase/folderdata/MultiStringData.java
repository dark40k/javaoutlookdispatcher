package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Iterator;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

@Root
@Convert(MultiStringData.ArrayDequeConverter.class)
public class MultiStringData implements Iterable<String> {

	private static final String SEPARATOR = ";";

	private ArrayDeque<String> data = new ArrayDeque<String>();

	// Longueur non limit�e si <= 0
	private long maxSize;

	public MultiStringData(long maxSize) {
		this.maxSize = maxSize;
	}

	public String exportString() {

		StringBuilder builder = new StringBuilder();
		for(String s : data) {
		    builder.append(SEPARATOR);
		    builder.append(s);
		}

		String desc = builder.toString();

		if (desc.length()>=SEPARATOR.length()) desc = desc.substring(SEPARATOR.length());

		return desc;
	}

	public void importString(String body) {
		data.clear();
		if (body==null) return;
		data.addAll(Arrays.asList(body.split(SEPARATOR)));
	}

	@Override
	public Iterator<String> iterator() {
		return data.iterator();
	}

	public int size() {
		return data.size();
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	public boolean contains(String string) {
		return data.contains(string);
	}

	public boolean add(String string) {

		if (string==null) return false;

		if (maxSize<=0) {
			if (data.contains(string)) return false;
			return data.add(string);
		}

		data.remove(string);
		data.add(string);

		if (data.size()>maxSize) data.removeFirst();

		return true;
	}

	public void clear() {
		data.clear();
	}

	//
	// Convertisseur custom pour la list de texte
	//

	public static class ArrayDequeConverter implements Converter<MultiStringData> {

		@Override
		public MultiStringData read(InputNode node) throws Exception {

			String str = node.getValue();

			MultiStringData data = new MultiStringData(0);

			if (str!=null)
				data.data.addAll(Arrays.asList(str.split(SEPARATOR)));

			data.maxSize=Long.parseLong(node.getAttribute("maxSize").getValue());

			return data;

		}

		@Override
		public void write(OutputNode node, MultiStringData value) throws Exception {

			StringBuilder builder = new StringBuilder();
			for(String s : value) {
			    builder.append(SEPARATOR);
			    builder.append(s);
			}

			String desc = builder.toString();

			if (desc.length()>=SEPARATOR.length()) desc = desc.substring(SEPARATOR.length());

			node.setValue(desc);

			node.setAttribute("maxSize",Long.toString(value.maxSize));

		}

	}

}
