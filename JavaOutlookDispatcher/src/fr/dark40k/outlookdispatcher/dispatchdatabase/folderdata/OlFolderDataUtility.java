package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.io.File;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class OlFolderDataUtility {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static int MAX_ADRESSES_NBR = 0;

	private final static Pattern searchBlockInDescriptionPattern= Pattern.compile("<"+OlFolderData.XML_NAME+".*</"+OlFolderData.XML_NAME+">",Pattern.DOTALL);

	private final static Serializer XML_MAPPER = new Persister(new AnnotationStrategy());

	//
	// Chargement et sauvegarde dans le repertoire Outlook
	//

	public static OlFolderData load(OlFolderEMail folderEmail) {

		LOGGER.trace("loading ="+folderEmail.getName());

		// recupere le block xml
		String xmlBlock=extractXmlBlockFromFolderDescription(folderEmail);

		// block vide
		if (xmlBlock==null) return new OlFolderData();

		// extrait les donnees
		OlFolderData olFolderData=importFolderDataFromXML(xmlBlock);

		// cree un bloc vierge par defaut si l'import n'a pas fonctionn�
		if (olFolderData==null) olFolderData = new OlFolderData();

		return olFolderData;

	}

	public static void save(OlFolderData olFolderData, OlFolderEMail folderEmail) {

		// genere le block
		String newBlock = exportFolderDataToXML(olFolderData);

		// sauve le block
		replaceXmlBlockInFolderDescription(folderEmail,newBlock);

	}

	//
	// Insertion / Extraction du bloc XML d'un repertoire
	//

	public static String extractXmlBlockFromFolderDescription(OlFolderEMail folderEmail) {

		// construit le matcheur pour trouver le template
		Matcher descriptionBlockMatcher = searchBlockInDescriptionPattern.matcher(folderEmail.getDescription());

		// renvoie le block s'il est trouv�, null sinon
		return  (descriptionBlockMatcher.find()) ? (descriptionBlockMatcher.group()) : null;

	}

	public static void replaceXmlBlockInFolderDescription(OlFolderEMail folderEmail, String xmlBlock) {

		// recupere la description actuelle
		String oldDescription=folderEmail.getDescription();

		// construit le builder
		StringBuilder newDescriptionBuilder = new StringBuilder();

		// construit le matcheur pour trouver le template
		Matcher addressInDescriptionMatcher = searchBlockInDescriptionPattern.matcher(oldDescription);

		// si template non trouve, ajoute le template � la description � la fin et reconstruit le matcheur
		if (addressInDescriptionMatcher.find()) {
			newDescriptionBuilder.append(oldDescription.substring(0,addressInDescriptionMatcher.start()));
			newDescriptionBuilder.append(xmlBlock);
			newDescriptionBuilder.append(oldDescription.substring(addressInDescriptionMatcher.end()));
		} else {
			newDescriptionBuilder.append(oldDescription);
			newDescriptionBuilder.append(xmlBlock);
		}

		//recupere la nouvelle definition
		String newDescription=newDescriptionBuilder.toString();

		// applique la modification de la description (si elle a chang�)
		if (!oldDescription.equals(newDescription)) {
			LOGGER.trace("Update description : "+folderEmail.getName()+" => "+newDescription);
			folderEmail.setDescription(newDescription);
		}

	}

	//
	// Import / export du bloc XML
	//

	public static OlFolderData importFolderDataFromXML(String xmlBlock) {

		LOGGER.trace("xmlBlock =\n"+xmlBlock);

		try {
			return XML_MAPPER.read(OlFolderData.class,xmlBlock);
		} catch (Exception e) {
			LOGGER.warn(e);
//			LOGGER.debug("Email block content=\r\n"+xmlBlock);
		}
		return null;

	}


	public static String exportFolderDataToXML(OlFolderData olFolderData) {

		StringWriter writer = new StringWriter();
		try {
			XML_MAPPER.write(olFolderData,writer);
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RuntimeException("Echec generation XML."); // stoppe le logiciel plutot que continuer avec une erreur.
		}
		return writer.toString();

	}


	//
	// Nettoyage
	//

	public static void resetAllFolders() {
		for (OlFolderEMail subFolderEmail : OlConnector.getRootFolder() )
			resetFolder(subFolderEmail,true);
	}

	public static void resetFolder(OlFolderEMail folderEmail, boolean recursive) {

		resetFolderBlock(folderEmail,"autodispatch2");
		resetFolderBlock(folderEmail,"autodispatch");
		resetFolderBlock(folderEmail,"conversation");

		if (recursive)
			for (OlFolderEMail subFolderEmail : folderEmail )
				resetFolder(subFolderEmail,true);

	}

	private static void resetFolderBlock(OlFolderEMail folderEmail,String title) {

		LOGGER.trace("Scanning <"+title+"> in "+folderEmail.getName());

		String oldDescription=folderEmail.getDescription();
		String newDescription=oldDescription.replaceAll("(?s)<"+title+">.*</"+title+">", "");
		newDescription=newDescription.replaceAll("(?s)<"+title+"/>", "");

		// applique la modification de la description (si elle a chang�)
		if (!oldDescription.equals(newDescription)) {
			LOGGER.trace("Clear description : "+folderEmail.getName()+" => "+newDescription);
			folderEmail.setDescription(newDescription);
		}

	}

	//
	// Backup / Restauration
	//

	public static void backupFolders(File file, Iterable<OlFolderEMail> list) {

		LOGGER.info("file="+file);

		var folderGroup = new OlFolderDataGroup();

		for (OlFolderEMail folder : list)
			folderGroup.addFolders(folder,false);

		try {
			XML_MAPPER.write(folderGroup,file);
		} catch (Exception e) {
			LOGGER.warn(e);
		}

		LOGGER.info("Done.");

	}

	public static void restoreFolders(File file) {

		LOGGER.info("file="+file);

		OlFolderDataGroup folderGroup = null;

		try {
			folderGroup=XML_MAPPER.read(OlFolderDataGroup.class,file);
		} catch (Exception e) {
			LOGGER.warn(e);
			LOGGER.info("Cancelling.");
			return;
		}

		folderGroup.replaceOlDataFolders();

		LOGGER.info("Done.");

	}


}
