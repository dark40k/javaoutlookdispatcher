package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Text;

public class KeywordsData {

	@Text(required=false)
	public String list;

	@Attribute(required=false)
	public boolean excludeTitle;

}
