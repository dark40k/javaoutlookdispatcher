package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

@Root
@Convert(CountedMultiStringData.LinkedHashMapConverter.class)
public class CountedMultiStringData {

	private static final String MAIN_SEPARATOR = ";";
	private static final String SEC_SEPARATOR = ",";

	private LinkedHashMap<String,Integer> data = new LinkedHashMap<String,Integer>();

	private int max;

	private int total;

	//
	// Methods
	//

	public String exportString() {
		return saveHashMapToString(data);
	}

	public void importString(String body) {
		loadHashMapFromString(body,data);
	}

	public void forEach(BiConsumer<? super String, ? super Integer> action) {
		data.forEach(action);
	}

	public boolean contains(String string) {
		return data.containsKey(string);
	}

	public Integer get(String key) {
		return data.get(key);
	}

	public Integer add(String string, int count) {

		if (string==null) return null;

		Integer curCount = data.get(string);

		if (curCount==null) curCount = 0;
		curCount+=count;

		data.put(string,curCount);

		if (curCount>max) max=curCount;
		total+=count;

		return curCount;
	}

	public void clear() {
		data.clear();
		max=0;
		total=0;
	}

	public int getMax() {
		return max;
	}

	public int getTotal() {
		return total;
	}

	//
	// Convertisseur custom pour la list de texte
	//

	public static class LinkedHashMapConverter implements Converter<CountedMultiStringData> {
		@Override
		public CountedMultiStringData read(InputNode node) throws Exception {
			CountedMultiStringData data = new CountedMultiStringData();
			data.data=loadHashMapFromString(node.getValue(), null);
			data.max=Integer.parseInt(node.getAttribute("max").getValue());
			data.total=Integer.parseInt(node.getAttribute("total").getValue());
			return data;
		}

		@Override
		public void write(OutputNode node, CountedMultiStringData value) throws Exception {
			node.setValue(saveHashMapToString(value.data));
			node.setAttribute("max",Integer.toString(value.max));
			node.setAttribute("total",Integer.toString(value.total));
		}
	}

	//
	// Fonctions utilitaires
	//

	public static String saveHashMapToString(LinkedHashMap<String,Integer> dataIn) {

		// Constitue une liste des entr�es
		List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(dataIn.entrySet());

		// Trie la liste des entr�es
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			// Comparing two entries by value
			@Override
			public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
				// Substracting the entries
				return entry2.getValue() - entry1.getValue();
			}
		});

		// Construit la liste ordonn�e des entr�es

		StringBuilder builder = new StringBuilder();

		for (Map.Entry<String, Integer> l : list) {
			builder.append(MAIN_SEPARATOR);
			builder.append(l.getKey());
			builder.append(SEC_SEPARATOR);
			builder.append(l.getValue());
		}

		String stringOut = builder.toString();

		// Finalise la chaine : Enl�ve le 1er s�parateur qui est en trop si la liste n'est pas vide

		if (stringOut.length()>0) stringOut = stringOut.substring(MAIN_SEPARATOR.length());

		return stringOut;
	}

	public static LinkedHashMap<String,Integer> loadHashMapFromString(String body, LinkedHashMap<String,Integer> dataOut) {

		if (dataOut!=null)
			dataOut.clear();
		else
			dataOut=new LinkedHashMap<String,Integer>();

		if (body==null) return dataOut;

		final LinkedHashMap<String,Integer> dataOut2=dataOut;

		Arrays.asList(body.split(MAIN_SEPARATOR)).forEach(
				(w)->{
					int index = w.indexOf(SEC_SEPARATOR);
					if (index>=0)
						dataOut2.put(w.substring(0,index),Integer.valueOf(w.substring(index+1)));
				});

		return dataOut2;
	}



}
