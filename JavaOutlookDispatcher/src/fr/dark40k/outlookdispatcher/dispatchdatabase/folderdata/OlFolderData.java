package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.util.ArrayList;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;

/*
 * Exemple fichier xml (sauts de lignes ajoutes pour faciliter comprehension)
 * 	<autodispatch2>
 *
 * 		<keywords excludeTitle="true">
 * 		</keywords>
 *
 * 		<emailAdresses maxSize="0">
 * 			renee.bauguil@airbus.com;frederic.digesu@airbus.com;...
 * 		</emailAdresses>
 *
 * 		<conversationIds maxSize="20">
 * 			01D3D09E6FB1AD654425B3;01D3D0CE06512543CAFB02;...
 * 		</conversationIds>
 *
 * 		<autoKeywords max="5717" total="6194">
 * 			(pr=2601),5717;(rd=0321),1; ...
 * 		</autoKeywords>
 *
 * 		<RegExpFilter regExp="(?i)(copil)" score="10000.0" applyTo="1" multicatch="false"/>
 * 		<RegExpFilter regExp="(?i)(CMRH)" score="10000.0" applyTo="1" multicatch="false"/>
 *
 * 	</autodispatch2>
 */

@Root(name = OlFolderData.XML_NAME, strict=false)
@Default(value=DefaultType.FIELD,required=false)
public class OlFolderData {

	final static String XML_NAME = "autodispatch2";

	private KeywordsData keywords;

	private MultiStringData emailAdresses;

	private MultiStringData conversationIds;

	private CountedMultiStringData autoKeywords;

	@Attribute(required=false)
	private boolean inactiveAutoKeywords;

	@Attribute(required=false)
	private boolean inactiveEmailAdresses;

	@ElementList(inline=true, required=false)
	private ArrayList<RegExpFilter> regExpFilters;

	//
	// Getters / Setters
	//

	public KeywordsData getKeywords() {
		if (keywords == null)
			keywords = new KeywordsData();
		return keywords;
	}

	public CountedMultiStringData getAutoKeywords() {
		if (autoKeywords == null)
			autoKeywords = new CountedMultiStringData();
		return autoKeywords;
	}

	public MultiStringData getEmailAdresses() {
		if (emailAdresses == null)
			emailAdresses = new MultiStringData(EMailAutoDispatcher.properties.maxAdressesNbrParam);
		return emailAdresses;
	}

	public MultiStringData getConversationIds() {
		if (conversationIds == null)
			conversationIds = new MultiStringData(EMailAutoDispatcher.properties.maxConversationNbrParam);
		return conversationIds;
	}

	public ArrayList<RegExpFilter> getRegExpFilters() {
		if (regExpFilters == null)
			regExpFilters = new ArrayList<RegExpFilter>();
		return regExpFilters;
	}

	public boolean getInactiveAutoKeywords() {
		return inactiveAutoKeywords;
	}

	public void setInactiveAutoKeywords(boolean val) {
		inactiveAutoKeywords=val;
	}

	public boolean getInactiveEmailAdresses() {
		return inactiveEmailAdresses;
	}

	public void setInactiveEmailAdresses(boolean val) {
		inactiveEmailAdresses=val;
	}


	//
	// Methods
	//

	public void clearEmailAdresses() {
		emailAdresses = null;
	}

	public void clearConversationIds() {
		conversationIds = null;
	}

	public void clearAutoKeywords() {
		autoKeywords = null;
	}

	public void replaceData(OlFolderData newOlFolderData) {
		if (newOlFolderData.keywords!=null) keywords=newOlFolderData.keywords;
		if (newOlFolderData.emailAdresses!=null) emailAdresses=newOlFolderData.emailAdresses;
		if (newOlFolderData.conversationIds!=null) conversationIds=newOlFolderData.conversationIds;
		if (newOlFolderData.autoKeywords!=null) autoKeywords=newOlFolderData.autoKeywords;
		if (newOlFolderData.regExpFilters!=null) regExpFilters=newOlFolderData.regExpFilters;
		inactiveAutoKeywords=newOlFolderData.inactiveAutoKeywords;
		inactiveEmailAdresses=newOlFolderData.inactiveEmailAdresses;
	}

}
