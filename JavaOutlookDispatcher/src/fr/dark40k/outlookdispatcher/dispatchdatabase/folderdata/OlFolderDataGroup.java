package fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlObject;

@Root
@Default(value=DefaultType.FIELD,required=false)
public class OlFolderDataGroup {

	private final static Logger LOGGER = LogManager.getLogger();

	@ElementList(inline=true)
	private ArrayList<FolderDataBlock> folderBlocks = new ArrayList<FolderDataBlock>();

	@Root
	public static class FolderDataBlock {

		@Attribute(required = false)
		public String name;

		@Attribute(required = false)
		public String path;

		@Attribute(required = true)
		public String entryId;

		@Element(name = OlFolderData.XML_NAME)
		public OlFolderData olFolderData;

		FolderDataBlock() {}

		public FolderDataBlock(OlFolderEMail folderEmail) {
			name=folderEmail.getName();
			path=folderEmail.getPath("\\").toString();
			entryId=folderEmail.getEntryID();
			olFolderData = OlFolderDataUtility.load(folderEmail);
		}

	}

	public void addFolders(OlFolderEMail folderEmail, boolean recursive) {

		folderBlocks.add(new FolderDataBlock(folderEmail));

		if (recursive)
			for (OlFolderEMail subFolderEmail : folderEmail )
				addFolders(subFolderEmail,true);

	}

	public void replaceOlDataFolders() {

		for (FolderDataBlock folderDataBlock : folderBlocks) {

			LOGGER.info("import = "+folderDataBlock.name);

			OlObject object = OlConnector.getOlObject(folderDataBlock.entryId);

			// verifie si l'objet a �t� trouv�
			if (object==null) {
				LOGGER.warn("Skipping, repertoire non trouve = "+folderDataBlock.path+", entryID="+folderDataBlock.entryId);
				continue;
			}

			// verifie si le repertoire est bien du type OlFolderEMail
			if (!(object instanceof OlFolderEMail)) {
				LOGGER.warn("Skipping, objet n'est pas de type Email = "+folderDataBlock.path+", entryID="+folderDataBlock.entryId);
				continue;
			}

			OlFolderEMail folderEmail = (OlFolderEMail) object;

			OlFolderData olFolderData = OlFolderDataUtility.load(folderEmail);

			olFolderData.replaceData(folderDataBlock.olFolderData);

			OlFolderDataUtility.save(olFolderData,folderEmail);

		}

	}

}
