package fr.dark40k.outlookdispatcher.dispatchdatabase;

import java.util.LinkedHashMap;

import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.UndoableAction;

public class UndoableActionSetDestFolder extends UndoableAction {
	
	private class UndoRedoData {
		OlFolderEMail destinationFolder;
		boolean isManual;
		public UndoRedoData(ToSortEmailSingle emailData) {
			destinationFolder = emailData.getDestFolder();
			isManual = emailData.isManualDestination;
		}
		public void restoreData(ToSortEmailSingle emailData) {
			emailData.destFolder = destinationFolder;
			emailData.isManualDestination = isManual;
		}
	}
	
	private final DispatchDatabase database;
	
	private final LinkedHashMap<OlEmail,UndoRedoData> originalData;
	
	
	private final LinkedHashMap<OlEmail,UndoRedoData> newData;
	
	public UndoableActionSetDestFolder(DispatchDatabase database) {
		super("", null, null);
				
		// stocke la base
		this.database=database;
		
		// initialise le stockage des repertoires sources
		originalData = new LinkedHashMap<OlEmail,UndoRedoData>();
		newData = new LinkedHashMap<OlEmail,UndoRedoData>();
		
		// initialise les actions
		undoAction = () -> { return doUndo();};
		redoAction = () -> { return doRedo();};

	}
	
	public void addEmail(ToSortEmailSingle emailData) {
		originalData.put(emailData.getEMail(), new UndoRedoData(emailData));
		updateActionName();
	}
	
	private void updateActionName() {
		actionName = "Modifier destination email ("+originalData.size()+")";
	}
	
	public int getFoldersCount() {
		return originalData.size();
	}

	private boolean doUndo() {
		
		for (ToSortEmailSingle emailData : database.getEMailDataList()) {
			
			if (!originalData.containsKey(emailData.getEMail())) continue;
			
			newData.put(emailData.getEMail(), new UndoRedoData(emailData));
			originalData.get(emailData.getEMail()).restoreData(emailData);;
			
			database.fireDestFolderChangedEvent(emailData);
		}
		
		return true;
	}
	
	private boolean doRedo() {
		
		for (ToSortEmailSingle emailData : database.getEMailDataList()) {
			
			if (!originalData.containsKey(emailData.getEMail())) continue;
			
			newData.get(emailData.getEMail()).restoreData(emailData);;
			
			database.fireDestFolderChangedEvent(emailData);
		}
		
		return true;
	}
	
	
}
