package fr.dark40k.outlookdispatcher.dispatchdatabase;

import java.util.ArrayDeque;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.SwingWorker;

public class BufferedMoveEmailMgr {

	// swing worker qui
	private  SwingWorker<Void,String> sw;

	private final ArrayDeque<UndoableActionMoveEmails> runStack = new ArrayDeque<UndoableActionMoveEmails>();

	private Consumer<String> displayProgress;


	public BufferedMoveEmailMgr(Consumer<String> displayProgress) {
		this.displayProgress=displayProgress;
	}

	public void setProgressDisplay(Consumer<String> displayProgress) {
		this.displayProgress=displayProgress;
	}

	public synchronized void addToRunStack(UndoableActionMoveEmails action) {

		runStack.push(action);

		// Pas la peine d'aller plus loin si le worker tourne d�j�
		if ( (sw!=null) && !sw.isDone()) return;

		sw = new ProcessStack();

		sw.execute();
	}

	public synchronized void removeFromRunStack(UndoableActionMoveEmails action) {
		runStack.remove(action);
	}

	public synchronized UndoableActionMoveEmails getNextAction() {
		return runStack.poll();
	}

	public int getRemainingEmailsCount() {
		int emailCount = 0;
		for (UndoableActionMoveEmails action : runStack)
			emailCount+=action.getMovesCount();
		return emailCount;
	}

	private class ProcessStack extends SwingWorker<Void,String> {

		@Override
		protected Void doInBackground() throws Exception {

			for (UndoableActionMoveEmails nextAction; (nextAction = getNextAction()) !=null; ) {

				final Integer emailCountFinal=getRemainingEmailsCount();

				nextAction.doFirst((count)->{publish("Moves = "+Integer.toString(runStack.size())+" - Emails = "+ Integer.toString(emailCountFinal+count));});

			}

			return null;
		}

		@Override
		protected void process(List<String> strings) {
			if (displayProgress!=null)
				displayProgress.accept(strings.get(strings.size()-1));
		}

		@Override
		protected void done() {
			if (displayProgress!=null)
				displayProgress.accept(null);
		}

	}


}
