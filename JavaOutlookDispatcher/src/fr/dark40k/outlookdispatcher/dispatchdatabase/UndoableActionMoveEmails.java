package fr.dark40k.outlookdispatcher.dispatchdatabase;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;
import fr.dark40k.outlookdispatcher.staticundomgr.UndoableAction;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;

public class UndoableActionMoveEmails extends UndoableAction {

	private final static Logger LOGGER = LogManager.getLogger();

	private final DispatchDatabase database;

	private final ArrayList<MovementData> moves;

	private boolean done = false;

	public UndoableActionMoveEmails(DispatchDatabase database, String title) {

		// initialisation generale
		super("", null, null);

		// stocke la base
		this.database=database;

		// initialise les actions
		undoAction = () -> { return doUndo();};
		redoAction = () -> { return doRedo();};

		// stocke les donn�es initiales du mouvement
		moves = new ArrayList<MovementData>();

		// intialise le titre
		actionName = title+" ("+moves.size()+")";
	}

	public int getMovesCount() {
		return moves.size();
	}

	public void addMovement(ToSortEmailSingle mailData, String sourceFolderId, String destinationFolderId, boolean learnFromMove) {
		moves.add(new MovementData(mailData, sourceFolderId, destinationFolderId, learnFromMove));
	}

	public void addToRunStack() {
		database.bufferedMoves.addToRunStack(this);
	}

	public synchronized boolean doFirst(Consumer<Integer> updateCount) {

		done=true;

		for (int i=0; i<moves.size(); i++) {
			MovementData move = moves.get(i);

			// signale le nombre restant � processer
			updateCount.accept(moves.size()-i);

			OlFolderEMail destinationFolder=(OlFolderEMail) OlConnector.getOlObject(move.destinationFolderId);
			if (move.learnFromMove) {

				// stocke la description avant le d�placement
				move.destinationFolderDescriptionBefore = destinationFolder.getDescription();

				// Met a jour la description du repertoire
				EMailAutoDispatcher.updateFolderDescriptionAfterMove(destinationFolder, move.email);

				// stocke la nouvelle description
				move.destinationFolderDescriptionAfter = destinationFolder.getDescription();

			}

			// Deplace le mail
			move.email.moveEMail(destinationFolder);

		}

		return true;
	}


	private synchronized boolean doRedo() {

		// Si l'action n'a pas �t� faite alors il faut la remettre dans le buffer
		if (!done) {
			database.bufferedMoves.addToRunStack(this);
			return true;
		}

		// prepare barre de progression
		JProgressDialog progressDialog = StaticUndoManager.getProgressDialog();
		if (progressDialog!=null) progressDialog.setManual("Refaire mouvement ("+moves.size()+")", 0, 0, moves.size());

		ArrayList<ToSortEmailSingle> emailDataList = new ArrayList<ToSortEmailSingle>(moves.size());

		for (int i=0; i<moves.size(); i++) {
			MovementData move = moves.get(i);

			OlFolderEMail destinationFolder=(OlFolderEMail) OlConnector.getOlObject(move.destinationFolderId);

			// inverse le mouvement
			LOGGER.info("Deplacer \""+move.email.getSubject()+"\" => "+destinationFolder.getName());
			move.email.moveEMail(destinationFolder);

			// restaure la description
			if (move.destinationFolderDescriptionAfter!=null)
				destinationFolder.setDescription(move.destinationFolderDescriptionAfter);

			// stocke la copie de restauration de EmailData
			emailDataList.add(move.getEmailDataCopy());

			// affiche barre de progression
			if (progressDialog!=null) progressDialog.incManualProgress();
		}

		database.rawRemoveEMailDatasFromDatabase(emailDataList);

		return true;
	}

	private synchronized boolean doUndo() {

		// Si l'action n'a pas �t� faite alors il faut l'enlever du buffer
		if (!done) {
			database.bufferedMoves.removeFromRunStack(this);
			return true;
		}

		// prepare barre de progression
		JProgressDialog progressDialog = StaticUndoManager.getProgressDialog();
		if (progressDialog!=null) progressDialog.setManual("Annulation mouvement ("+moves.size()+")", 0, 0, moves.size());

		ArrayList<ToSortEmailSingle> emailDataList = new ArrayList<ToSortEmailSingle>(moves.size());

		for (int i=moves.size()-1; i>=0; i--) {
			MovementData move = moves.get(i);

			OlFolderEMail sourceFolder=(OlFolderEMail) OlConnector.getOlObject(move.sourceFolderId);
			OlFolderEMail destinationFolder=(OlFolderEMail) OlConnector.getOlObject(move.destinationFolderId);

			// inverse le mouvement
			LOGGER.info("Deplacer \""+move.email.getSubject()+"\" => "+sourceFolder.getName());
			move.email.moveEMail(sourceFolder);

			// restaure la description
			if (move.destinationFolderDescriptionBefore!=null)
				destinationFolder.setDescription(move.destinationFolderDescriptionBefore);

			// stocke la copie de restauration de EmailData
			emailDataList.add(move.getEmailDataCopy());

			// affiche barre de progression
			if (progressDialog!=null) progressDialog.incManualProgress();
		}

		database.rawAddEMailDatasToDatabase(emailDataList);

		return true;
	}

	private class MovementData {

		private OlEmail email;

		public String sourceFolderId;
		public String destinationFolderId;

		public String destinationFolderDescriptionBefore;
		public String destinationFolderDescriptionAfter;

		public Element mailDataElt;

		public boolean learnFromMove;

		public MovementData(ToSortEmailSingle mailData, String sourceFolderId, String destinationFolderId, boolean learnFromMove) {

			this.email = mailData.getEMail();

			this.sourceFolderId = sourceFolderId;
			this.destinationFolderId = destinationFolderId;

			this.learnFromMove=learnFromMove;

			mailDataElt=mailData.exportToXML();
		}

		public ToSortEmailSingle getEmailDataCopy() {
			return new ToSortEmailSingle(database, email, mailDataElt);
		}

	}

}
