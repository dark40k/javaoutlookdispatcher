package fr.dark40k.outlookdispatcher.staticundomgr;


import javax.swing.undo.UndoManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.util.JProgressDialog;

public class StaticUndoManager {

	private final static Logger LOGGER = LogManager.getLogger();
	
	private static UndoManager undoMgr = new UndoManager();

	private static JProgressDialog progressDialog;
	
	
	public static JProgressDialog getProgressDialog() {
		return progressDialog;
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------
	
	// Undo
	
	public static boolean canUndo() {
		return undoMgr.canUndo();
	}

	public static void undo(JProgressDialog dialog) {
		LOGGER.info("Undo = " + undoMgr.getUndoPresentationName());
		StaticUndoManager.progressDialog=dialog;
		if (dialog!=null) dialog.setAuto("Annulation action.");
		undoMgr.undo();
		StaticUndoManager.progressDialog=null;
	}

	// Redo
	
	public static boolean canRedo() {
		return undoMgr.canRedo();
	}

	public static void redo(JProgressDialog dialog) {
		LOGGER.info("Redo = " + undoMgr.getRedoPresentationName());
		StaticUndoManager.progressDialog=dialog;
		if (dialog!=null) dialog.setAuto("Refaire action.");
		undoMgr.redo();
		StaticUndoManager.progressDialog=null;
	}

	// Manage pile
	
	public static void discardAllEdits() {
		undoMgr.discardAllEdits();
	}

	public static String getUndoPresentationName() {
		if (canUndo()) return undoMgr.getUndoPresentationName();
		return "Annuler <vide>";
	}
	
	public static String getRedoPresentationName() {
		if (canRedo()) return undoMgr.getRedoPresentationName();
		return "Refaire <vide>";
	}
	
	// Move group
	
	public static void addUndoableAction(UndoableAction edit) {
		undoMgr.addEdit(edit);
	}
		
}
