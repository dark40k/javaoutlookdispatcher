package fr.dark40k.outlookdispatcher.staticundomgr;

import java.util.function.BooleanSupplier;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UndoableAction implements UndoableEdit {
	
	private final static Logger LOGGER = LogManager.getLogger();
	
	protected String actionName;
	protected BooleanSupplier undoAction;
	protected BooleanSupplier redoAction;
	private boolean canUndo;
	private boolean canRedo;
	
	public UndoableAction(String actionName, BooleanSupplier undoAction, BooleanSupplier redoAction) {
		
		this.actionName=actionName;
		
		this.undoAction=undoAction;
		canUndo=true;
		
		this.redoAction=redoAction;
		canRedo=false;
		
	}

	@Override
	public void undo() throws CannotUndoException {
		
		if (!canUndo()) throw new CannotUndoException();
		
		boolean result=undoAction.getAsBoolean();
		
		if (!result) {
			LOGGER.error("Undo failed.");
			canUndo=false;
			canRedo=false;
			return;
		}
		
		canUndo=false;
		canRedo=true;
		
	}

	@Override
	public boolean canUndo() {
		return canUndo && (undoAction!=null);
	}

	@Override
	public void redo() throws CannotRedoException {
		
		if (!canRedo()) throw new CannotRedoException();
		
		boolean result=redoAction.getAsBoolean();
		
		if (!result) {
			LOGGER.error("Redo failed.");
			canUndo=false;
			canRedo=false;
			return;
		}
		
		canUndo=true;
		canRedo=false;

	}

	@Override
	public boolean canRedo() {
		return canRedo && (redoAction!=null);
	}

	@Override
	public void die() {
	}

	@Override
	public boolean addEdit(UndoableEdit anEdit) {
		return false;
	}

	@Override
	public boolean replaceEdit(UndoableEdit anEdit) {
		return false;
	}

	@Override
	public boolean isSignificant() {
		return true;
	}

	@Override
	public String getPresentationName() {
		return actionName;
	}

	@Override
	public String getUndoPresentationName() {
		return "Annuler "+actionName;
	}

	@Override
	public String getRedoPresentationName() {
		return "Refaire "+actionName;
	}
	

}
