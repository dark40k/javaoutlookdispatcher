package fr.dark40k.outlookdispatcher.outlookconnector;

public enum OlFolderDefaultItemTypeEnum {

	OL_APPOINTMENT_ITEM(1), // Représente un objet AppointmentItem 
	OL_CONTACT_ITEM(2), // Représente un objet ContactItem 
	OL_DISTRIBUTION_LIST_ITEM(7), // Représente un objet ExchangeDistributionList 
	OL_JOURNAL_ITEM(4), // Représente un objet JournalItem 
	OL_MAIL_ITEM(0), // Représente un objet MailItem 
	OL_NOTE_ITEM(5), // Représente un objet NoteItem 
	OL_POST_ITEM(6), // Représente un objet PostItem 
	OL_TASK_ITEM(3); // Représente un objet TaskItem 
	
	long code;

	private OlFolderDefaultItemTypeEnum(long code) {
		this.code = code;
	}

	public static OlFolderDefaultItemTypeEnum valueOfByCode(final long code) {
		for (OlFolderDefaultItemTypeEnum format : values()) {
			if (format.code == code) {
				return format;
			}
		}
		throw new IllegalArgumentException("Ce code ne correspond n'est pas valide. code=" + code);
	}

}
