package fr.dark40k.outlookdispatcher.outlookconnector;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public enum OlObjectClassEnum {

	// Table bas�e sur extraction de https://msdn.microsoft.com/en-us/library/office/ff863329.aspx
	// OlObjectClass Enumeration (Outlook)
	// Office 2013 and later 
	
	olAccount(105,"Account"),
	olAccountRuleCondition(135,"AccountRuleCondition"),
	olAccounts(106,"Accounts"),
	olAction(32,"Action"),
	olActions(33,"Actions"),
	olAddressEntries(21,"AddressEntries"),
	olAddressEntry(8,"AddressEntry"),
	olAddressList(7,"AddressList"),
	olAddressLists(20,"AddressLists"),
	olAddressRuleCondition(170,"AddressRuleCondition"),
	olApplication(0,"Application"),
	olAppointment(26,"AppointmentItem"),
	olAssignToCategoryRuleAction(122,"AssignToCategoryRuleAction"),
	olAttachment(5,"Attachment"),
	olAttachments(18,"Attachments"),
	olAttachmentSelection(169,"AttachmentSelection"),
	olAutoFormatRule(147,"AutoFormatRule"),
	olAutoFormatRules(148,"AutoFormatRules"),
	olCalendarModule(159,"CalendarModule"),
	olCalendarSharing(151,"CalendarSharing"),
	olCategories(153,"Categories"),
	olCategory(152,"Category"),
	olCategoryRuleCondition(130,"CategoryRuleCondition"),
	olClassBusinessCardView(168,"BusinessCardView"),
	olClassCalendarView(139,"CalendarView"),
	olClassCardView(138,"CardView"),
	olClassIconView(137,"IconView"),
	olClassNavigationPane(155,"NavigationPane"),
	olClassPeopleView(183,"PeopleView"),
	olClassTableView(136,"TableView"),
	olClassTimeLineView(140,"TimelineView"),
	olClassTimeZone(174,"TimeZone"),
	olClassTimeZones(175,"TimeZones"),
	olColumn(154,"Column"),
	olColumnFormat(149,"ColumnFormat"),
	olColumns(150,"Columns"),
	olConflict(102,"Conflict"),
	olConflicts(103,"Conflicts"),
	olContact(40,"ContactItem"),
	olContactsModule(160,"ContactsModule"),
	olConversation(178,"Conversation"),
	olConversationHeader(182,"ConversationHeader"),
	olDistributionList(69,"ExchangeDistributionList"),
	olDocument(41,"DocumentItem"),
	olException(30,"Exception"),
	olExceptions(29,"Exceptions"),
	olExchangeDistributionList(111,"ExchangeDistributionList"),
	olExchangeUser(110,"ExchangeUser"),
	olExplorer(34,"Explorer"),
	olExplorers(60,"Explorers"),
	olFolder(2,"Folder"),
	olFolders(15,"Folders"),
	olFolderUserProperties(172,"UserDefinedProperties"),
	olFolderUserProperty(171,"UserDefinedProperty"),
	olFormDescription(37,"FormDescription"),
	olFormNameRuleCondition(131,"FormNameRuleCondition"),
	olFormRegion(129,"FormRegion"),
	olFromRssFeedRuleCondition(173,"FromRssFeedRuleCondition"),
	olFromRuleCondition(132,"ToOrFromRuleCondition"),
	olImportanceRuleCondition(128,"ImportanceRuleCondition"),
	olInspector(35,"Inspector"),
	olInspectors(61,"Inspectors"),
	olItemProperties(98,"ItemProperties"),
	olItemProperty(99,"ItemProperty"),
	olItems(16,"Items"),
	olJournal(42,"JournalItem"),
	olJournalModule(162,"JournalModule"),
	olMail(43,"MailItem"),
	olMailModule(158,"MailModule"),
	olMarkAsTaskRuleAction(124,"MarkAsTaskRuleAction"),
	olMeetingCancellation(54,"MeetingItem object that is meeting cancellation notice."),
	olMeetingForwardNotification(181,"MeetingItem object that is notice about forwarding the meeting request."),
	olMeetingRequest(53,"MeetingItem object that is meeting request."),
	olMeetingResponseNegative(55,"MeetingItem object that is refusal of meeting request."),
	olMeetingResponsePositive(56,"MeetingItem object that is acceptance of meeting request."),
	olMeetingResponseTentative(57,"MeetingItem object that is tentative acceptance of meeting request."),
	olMoveOrCopyRuleAction(118,"MoveOrCopyRuleAction"),
	olNamespace(1,"NameSpace"),
	olNavigationFolder(167,"NavigationFolder"),
	olNavigationFolders(166,"NavigationFolders"),
	olNavigationGroup(165,"NavigationGroup"),
	olNavigationGroups(164,"NavigationGroups"),
	olNavigationModule(157,"NavigationModule"),
	olNavigationModules(156,"NavigationModules"),
	olNewItemAlertRuleAction(125,"NewItemAlertRuleAction"),
	olNote(44,"NoteItem"),
	olNotesModule(163,"NotesModule"),
	olOrderField(144,"OrderField"),
	olOrderFields(145,"OrderFields"),
	olOutlookBarGroup(66,"OutlookBarGroup"),
	olOutlookBarGroups(65,"OutlookBarGroups"),
	olOutlookBarPane(63,"OutlookBarPane"),
	olOutlookBarShortcut(68,"OutlookBarShortcut"),
	olOutlookBarShortcuts(67,"OutlookBarShortcuts"),
	olOutlookBarStorage(64,"OutlookBarStorage"),
	olOutspace(180,"AccountSelector"),
	olPages(36,"Pages"),
	olPanes(62,"Panes"),
	olPlaySoundRuleAction(123,"PlaySoundRuleAction"),
	olPost(45,"PostItem"),
	olPropertyAccessor(112,"PropertyAccessor"),
	olPropertyPages(71,"PropertyPages"),
	olPropertyPageSite(70,"PropertyPageSite"),
	olRecipient(4,"Recipient"),
	olRecipients(17,"Recipients"),
	olRecurrencePattern(28,"RecurrencePattern"),
	olReminder(101,"Reminder"),
	olReminders(100,"Reminders"),
	olRemote(47,"RemoteItem"),
	olReport(46,"ReportItem"),
	olResults(78,"Results"),
	olRow(121,"Row"),
	olRule(115,"Rule"),
	olRuleAction(117,"RuleAction"),
	olRuleActions(116,"RuleActions"),
	olRuleCondition(127,"RuleCondition"),
	olRuleConditions(126,"RuleConditions"),
	olRules(114,"Rules"),
	olSearch(77,"Search"),
	olSelection(74,"Selection"),
	olSelectNamesDialog(109,"SelectNamesDialog"),
	olSenderInAddressListRuleCondition(133,"SenderInAddressListRuleCondition"),
	olSendRuleAction(119,"SendRuleAction"),
	olSharing(104,"SharingItem"),
	olSimpleItems(179,"SimpleItems"),
	olSolutionsModule(177,"SolutionsModule"),
	olStorageItem(113,"StorageItem"),
	olStore(107,"Store"),
	olStores(108,"Stores"),
	olSyncObject(72,"SyncObject"),
	olSyncObjects(73,"SyncObjects"),
	olTable(120,"Table"),
	olTask(48,"TaskItem"),
	olTaskRequest(49,"TaskRequestItem"),
	olTaskRequestAccept(51,"TaskRequestAcceptItem"),
	olTaskRequestDecline(52,"TaskRequestDeclineItem"),
	olTaskRequestUpdate(50,"TaskRequestUpdateItem"),
	olTasksModule(161,"TasksModule"),
	olTextRuleCondition(134,"TextRuleCondition"),
	olUserDefinedProperties(172,"UserDefinedProperties"),
	olUserDefinedProperty(171,"UserDefinedProperty"),
	olUserProperties(38,"UserProperties"),
	olUserProperty(39,"UserProperty"),
	olView(80,"View"),
	olViewField(142,"ViewField"),
	olViewFields(141,"ViewFields"),
	olViewFont(146,"ViewFont"),
	olViews(79,"Views"),
	olNull(-1,"null object"); // added to manage null
	
	final long code;
	final String objectName;

	private OlObjectClassEnum(long code, String objectName) {
		this.code = code;
		this.objectName=objectName;
	}

	public static OlObjectClassEnum valueOfByCode(final long code) {
		for (OlObjectClassEnum format : values()) {
			if (format.code == code) {
				return format;
			}
		}
		System.err.println("OlObjectClassEnum : Code objet inconnu =" + code+"\n\n");
		return OlObjectClassEnum.olNull;
	}

	public static OlObjectClassEnum getClass(Dispatch dispatch) {
		if (dispatch==null) return OlObjectClassEnum.olNull;
		Variant v=Dispatch.call(dispatch, "Class");
		OlObjectClassEnum clnbr = OlObjectClassEnum.valueOfByCode(v.getInt());
		v.safeRelease();
		return clnbr;
	}
	
	@Override
	public String toString() {
		return objectName;
	}
	
}
