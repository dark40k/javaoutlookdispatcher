package fr.dark40k.outlookdispatcher.outlookconnector;

import com.jacob.com.Dispatch;

public abstract class OlObject {

	protected String entryID;

	protected final OlObjectClassEnum type;

	protected Dispatch dispatch;

	protected OlObject(String entryID, Dispatch dispatch, OlObjectClassEnum type) {

		assert entryID != null : "OlObject.OlObject() - Entry ID is null";

		this.entryID = entryID;
		this.dispatch = dispatch;
		this.type = type;

		if (dispatch!=null)
			assert OlObjectClassEnum.getClass(dispatch) == type : "OlObject.OlObject() : Types incoh�rent " + type +" != "+ OlObjectClassEnum.getClass(dispatch);

		OlConnector.addOlObject(this);

		refresh();

	}

	//
	// liberation de l'objet
	//
	public void forceSafeRelease() {
		dispatch.safeRelease();
		OlConnector.removeOlObject(this);
	}

	//
	// Mise � jours du contenu
	//

	public abstract void refresh();

	//
	// Getters
	//

	public String getEntryID() {
		return entryID;
	}

	public Dispatch getDispatch() {
		return dispatch;
	}

	public OlObjectClassEnum getType() {
		return type;
	}

	//
	// Fonctions generales
	//

	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof OlObject)) return false;
	    return entryID.equals(((OlObject) other).entryID);
	}

}
