package fr.dark40k.outlookdispatcher.outlookconnector;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.jacob.com.Dispatch;

public class OlEmail extends OlObject {

	private boolean unRead;
	private String conversationID;
	private OlEMailSubject subject;
	private String from;
	private String to;
	private String cc;
	private LocalDateTime sentOn;
	private String parentName;
	private String parentId;
	private String categories;
	private String fromAdress;
	private ArrayList<String> toAdress;
	private ArrayList<String> ccAdress;

	public final OlEmailStatus status = new OlEmailStatus(this);

	public OlEmail(String entryID) {
		super(entryID, DispUtil.call(OlConnector.getSession(), "GetItemFromID", entryID), OlObjectClassEnum.olMail);
	}

	private OlEmail(Dispatch dispatch) {
		super(DispUtil.getString(dispatch, "entryID"), dispatch, OlObjectClassEnum.olMail);
	}

	//
	// Actions
	//

	@Override
	public void refresh() {

		// param�tres pouvants evoluer rapidement
		if (status!=null) status.updateContent();
		unRead=DispUtil.getBoolean(dispatch,"UnRead");
		categories = DispUtil.getString(dispatch,"Categories");

		// On arr�te la mise � jours si le champ sujet est d�j� renseign�
		if (subject!=null) return;

		// recuperation du dossier contenant le mail
		Dispatch parentD = DispUtil.get(getDispatch(),"Parent");
		parentName = DispUtil.getString(parentD, "Name");
		parentId = DispUtil.getString(parentD, "entryID");
		parentD.safeRelease();

		// update de la conversation
		conversationID = DispUtil.getString(dispatch,"ConversationIndex");
		if (conversationID.length() > 22)
			conversationID = conversationID.substring(0, 22);

		// MaJ des parametres evoluant de mani�re rare

		subject=new OlEMailSubject(DispUtil.getString(dispatch,"Subject"));
		from=DispUtil.getString(dispatch,"SenderName");
		to=DispUtil.getString(dispatch,"To");
		cc=DispUtil.getString(dispatch,"CC");
		sentOn=DispUtil.getLocalDateTime(dispatch,"SentOn");
		fromAdress=DispUtil.getString(dispatch,"SenderEmailAddress");

		// Lecture des adresses des destinataires
		toAdress = new ArrayList<String>();
		ccAdress = new ArrayList<String>();
		Dispatch recipients = DispUtil.get(dispatch,"Recipients");
		int recipientsCount = DispUtil.getInt(recipients, "Count");
		for (int recipientIndex = 1; recipientIndex <= recipientsCount; recipientIndex++ ){
			Dispatch recipient = DispUtil.call(recipients, "Item", recipientIndex);
			String recipientAdress = DispUtil.getString(recipient,"Address");
			switch (OlMailRecipientEnum.fromCode(DispUtil.getInt(recipient, "Type"))) {
				case olTo: toAdress.add(recipientAdress); break;
				case olCC: ccAdress.add(recipientAdress); break;
				default:
			}
			recipient.safeRelease();
		}
		recipients.safeRelease();

	}

	public void showMail() {
		Dispatch.call(dispatch,"Display").safeRelease();
	}

	public void moveEMail(OlFolderEMail olFolder) {

		OlConnector.removeOlObject(this);

		OlFolderEMail currentFolder = (OlFolderEMail) OlConnector.getOlObject(parentId);

		Dispatch oldDispatch = dispatch;

		dispatch = DispUtil.call(oldDispatch,"Move",olFolder.getDispatch());

		oldDispatch.safeRelease();

		entryID = DispUtil.getString(dispatch, "entryID");

		if (currentFolder.emailList!=null) {
			currentFolder.emailList.remove(this);
			if (currentFolder.count!=null) currentFolder.count--;
		}

		if (olFolder.emailList!=null) {
			olFolder.emailList.add(this);
			if (olFolder.count!=null) olFolder.count++;
		}

		refresh();

		OlConnector.addOlObject(this);
	}

	//
	// Fonctions generales
	//

	// cas particulier: les emails peuvent changer d'ID suite � un move. Seul l'objet est maintenu.
	@Override
	public boolean equals(Object other){
	    return (this==other);
	}

	//
	// Setters
	//

	//
	// Getters
	//

	@Override
	public String toString() {
		StringBuilder toString = new StringBuilder();
		toString.append("[Titre=");
		toString.append(subject);
		toString.append(", From=");
		toString.append(from);
		toString.append(", FromAdress=");
		toString.append(fromAdress);
		toString.append(", To=");
		toString.append(to);
		toString.append(", Cc=");
		toString.append(cc);
		toString.append("]");
		return toString.toString();
	}

	public OlEmailBody getBody() {
		return OlEmailBody.getBodyFromOutlook(entryID);
	}

	public boolean isUnRead() {
		return unRead;
	}

	public String getConversationID() {
		return conversationID;
	}

	public OlEMailSubject getSubject() {
		return subject;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public String getCC() {
		return cc;
	}

	public LocalDateTime getSentOn() {
		return sentOn;
	}

	public String getCategories() {
		return categories;
	}

	public String getParentName() {
		return parentName;
	}

	public String getParentId() {
		return parentId;
	}

	public String getFromAdress() {
		return fromAdress;
	}

	public List<String> getToAdresses() {
		return Collections.unmodifiableList(toAdress);
	}

	public List<String> getCcAdresses() {
		return Collections.unmodifiableList(ccAdress);
	}

	public int getToAdressesCount() {
		return toAdress.size();
	}

	public int getCcAdressesCount() {
		return ccAdress.size();
	}


}
