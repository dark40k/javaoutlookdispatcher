package fr.dark40k.outlookdispatcher.outlookconnector;

public class OlEMailSubject implements Comparable<OlEMailSubject> {

	private String subject;
	private String subjectCleaned;

	protected OlEMailSubject(String objet) {
		this.subject=objet;
		this.subjectCleaned=removeLeadingCodes(objet);
	}

	@Override
	public int compareTo(OlEMailSubject objet) {
		return subjectCleaned.compareTo(objet.subjectCleaned);
	}

	@Override
	public String toString() {
		return subject;
	}

	public String getCleaned() {
		return subjectCleaned;
	}

	private static String removeLeadingCodes(String textIn) {
		String text=textIn;
		text=text.replaceAll("(?i)^(\\s*re\\s*:\\s*)+", "");
		text=text.replaceAll("(?i)^(\\s*tr\\s*:\\s*)+", "");
		text=text.replaceAll("(?i)^(\\s*aw\\s*:\\s*)+", "");
		return text;
	}

}
