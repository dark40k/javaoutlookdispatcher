package fr.dark40k.outlookdispatcher.outlookconnector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.com.Dispatch;

public class OlFolderEMail extends OlFolder  {

	private final static Logger LOGGER = LogManager.getLogger();

	private String name;
	private String description;

	protected Integer count = null;

	protected ArrayList<OlEmail> emailList;

	private OlFolderEMail(String entryID, OlFolder parent) {
		super(entryID, DispUtil.call(OlConnector.getSession(), "GetFolderFromID", entryID), parent);
	}

	protected static OlFolderEMail getOrUpdateOlFolderEMail(String entryID, OlFolder parent) {

		OlFolderEMail folder = (OlFolderEMail) OlConnector.getOlObject(entryID);

		if (folder==null) {
			folder = new OlFolderEMail(entryID, parent);
		} else {
			folder.setParentFolder(parent);
			folder.updateSubFoldersList();
		}

		return folder;
	}

	@Override
	public void refresh() {
		// TODO Refresh ne reactualise pas la liste des mails contenus dans le repertoire
		name=DispUtil.getString(dispatch,"Name");

		try {
			description=DispUtil.getString(dispatch,"Description");
		} catch (Exception e) {
			LOGGER.warn("Skipping folder, JACOB cannot get description for path:"+getPath("\\").toString());
		}
	}

	//
	// Donn�es du folder
	//

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name=name;
		Dispatch.put(getDispatch(),"Name",name);
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description=description;
		Dispatch.put(getDispatch(),"Description",description);
	}

	public OlFolderEMail createSubFolder(String newFoldername) {

		// verifie que le nom n'est pas deja utilise
		for (OlFolder subFolder:subFoldersList) {
			if (subFolder.getName().equals(newFoldername)) {
				LOGGER.warn("Le nom est deja utilis�, annulation de la creation du repertoire:"+newFoldername);
				return null;
			}
		}

		Dispatch foldersD;
		Dispatch newFolderD;
		foldersD = DispUtil.get(getDispatch(), "Folders");

		// creation du repertoire
		newFolderD = DispUtil.call(foldersD, "Add", OlFolderDefaultItemTypeEnum.OL_MAIL_ITEM.code);

		// applique le nom
		Dispatch.put(newFolderD,"Name",newFoldername);

		// recupere l'ID
		String newFolderId = DispUtil.getString(newFolderD,"entryID");

		newFolderD.safeRelease();
		foldersD.safeRelease();

		updateSubFoldersList();

		for (OlFolder subFolder:subFoldersList) {
			if (subFolder.entryID.equals(newFolderId))
				return (OlFolderEMail) subFolder;
		}
		throw new RuntimeException("Le repertoire cr�� n'a pas ete retrouv�.");

	}

	//
	// Liste les emails
	//

	public int getItemsCount(boolean recursive) {

		if (count==null) {
		Dispatch itemsD = DispUtil.get(getDispatch(), "Items");
		count = DispUtil.getInt(itemsD, "Count");
		itemsD.safeRelease();
		}

		int sum = count;

		if (recursive)
			for (OlFolderEMail folder : this)
				sum += folder.getItemsCount(recursive);

		return sum;

	}


	public Iterable<OlEmail> listMails() {
		return listMails(null);
	}


	public Iterable<OlEmail> listMails(Consumer<Integer> progress) {

		if (emailList==null) {
			emailList = new ArrayList<OlEmail>();
			updateListMails(progress);
		}

		return emailList;

	}

	protected void updateListMails(Consumer<Integer> progress) {

		if (emailList==null) return;

		emailList.clear();

		try {
			Dispatch itemsD = DispUtil.get(getDispatch(), "Items");

			int count = DispUtil.getInt(itemsD, "Count");

			for (int mailIndex = 1; mailIndex <= count; mailIndex++ ){

				Dispatch mailItemD = DispUtil.call(itemsD, "Item", mailIndex);

				OlObjectClassEnum typeEnum = OlObjectClassEnum.getClass(mailItemD);
				if (typeEnum == OlObjectClassEnum.olMail) {

					String mailEntryID = DispUtil.getString(mailItemD, "entryID");

					OlEmail email = (OlEmail) OlConnector.getOlObject(mailEntryID);

					if (email==null)
						email = new OlEmail(mailEntryID);


					emailList.add(email);

				} else {
					LOGGER.debug("OlEMailFolder.listMails() : Skipping item, object type not recognised : "+typeEnum.objectName);
				}

				mailItemD.safeRelease();

				if (progress!=null) progress.accept(mailIndex);
			}

			itemsD.safeRelease();

		} catch (Exception e) {
			LOGGER.error("Erreure connection",e);
		}

	}

	//
	// Construction des subfolders
	//

	@Override
	protected void updateSubFoldersList() {

		refresh();

//		LOGGER.warning("updateFolderContent() "+this.name+ System.identityHashCode(this)+"*"+this.entryID+"==>"+subFoldersList.size());

		subFoldersList.clear();

		Dispatch subFoldersD = DispUtil.get(getDispatch(), "Folders");

		int count = DispUtil.getInt(subFoldersD, "Count");

		for (int subFolderIndex = 1; subFolderIndex <= count; subFolderIndex++) {

			Dispatch subFolderD = DispUtil.call(subFoldersD, "Item", subFolderIndex);

			try {
				if (DispUtil.getInt(subFolderD, "DefaultItemType")==OlFolderDefaultItemTypeEnum.OL_MAIL_ITEM.code)
					subFoldersList.add(getOrUpdateOlFolderEMail(DispUtil.getString(subFolderD, "entryID"), this));
			} catch (com.jacob.com.ComFailException e) {
				LOGGER.warn("Skipping folder, JACOB cannot list "+getPath("\\").toString()+"\\"+DispUtil.getString(subFolderD, "Name"));
			}

			subFolderD.safeRelease();
		}

		subFoldersD.safeRelease();

		Collections.sort(subFoldersList, new Comparator<OlFolder>() {
			@Override
			public int compare(OlFolder folder1, OlFolder folder2) {
				return folder1.getName().compareTo(folder2.getName());
			}
		});

//		LOGGER.warning("updateFolderContent() "+this.name+ System.identityHashCode(this)+"*"+this.entryID+"==> ==>"+subFoldersList.size());

	}


}
