package fr.dark40k.outlookdispatcher.outlookconnector;

public enum OlFlagStatusEnum {

	olNoFlag(0),
	olFlagComplete(1),
	olFlagMarked(2);
	
	private final int code;
	
	private OlFlagStatusEnum(int code) {
		this.code=code;
	}
	
	public int getCode() {
		return code;
	}
	
	public static OlFlagStatusEnum getFlagStatusFromCode(int searchCode) {
		for (OlFlagStatusEnum flagStatus : values()) {
			if (flagStatus.code == searchCode) {
				return flagStatus;
			}
		}
		throw new IllegalArgumentException("Ce code ne correspond n'est pas valide. code=" + searchCode);
	}
	
}
