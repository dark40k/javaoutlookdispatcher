package fr.dark40k.outlookdispatcher.outlookconnector;

public enum OlEmailBodyFormatEnum {
	
	UNSPECIFIED(0), 
	PLAIN(1), 
	HTML(2), 
	RTF(3);
	
	long code;

	private OlEmailBodyFormatEnum(long code) {
		this.code = code;
	}

	public static OlEmailBodyFormatEnum valueOfByCode(final long code) {
		for (OlEmailBodyFormatEnum format : values()) {
			if (format.code == code) {
				return format;
			}
		}
		throw new IllegalArgumentException("Ce code ne correspond n'est pas valide. code=" + code);
	}
}