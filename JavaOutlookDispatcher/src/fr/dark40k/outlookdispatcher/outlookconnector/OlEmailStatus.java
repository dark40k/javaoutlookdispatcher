package fr.dark40k.outlookdispatcher.outlookconnector;

import com.jacob.com.Dispatch;

/**
 * Regroupe les donn�es autour du status du mail 
 *
 */
public class OlEmailStatus {

	private OlEmail olEmail;

	private OlFlagStatusEnum flag;

//	private LocalDateTime taskStartDate;
//	private LocalDateTime taskDueDate;
//	private LocalDateTime taskCompletedDate;
//	private LocalDateTime toDoTaskOrdinal;
	
	//
	// Constructeur
	//
	
	public OlEmailStatus(OlEmail olEmail) {
		this.olEmail=olEmail;
		updateContent();
	}

	//
	// Mise � jours du contenu
	//

	public void updateContent() {
		flag = OlFlagStatusEnum.getFlagStatusFromCode(DispUtil.getInt(olEmail.dispatch,"FlagStatus"));
//		taskStartDate=DispUtil.getLocalDateTime(olEmail.dispatch,"TaskStartDate");
//		taskDueDate=DispUtil.getLocalDateTime(olEmail.dispatch,"TaskDueDate");
//		taskCompletedDate=DispUtil.getLocalDateTime(olEmail.dispatch,"TaskCompletedDate");
//		toDoTaskOrdinal=DispUtil.getLocalDateTime(olEmail.dispatch,"ToDoTaskOrdinal");
		
	}

	//
	// Setters
	// 
	
	/**
	 * @param newInterval : si null => pas de tache associ�e (envoie un ClearTaskFlag)
	 */
	public void markIntervalAndSetStatusFlag(OlMarkIntervalEnum newInterval) {
		
		if (newInterval == null) {
			Dispatch.call(olEmail.dispatch, "ClearTaskFlag");
			flag = OlFlagStatusEnum.olNoFlag;
		} else if (newInterval == OlMarkIntervalEnum.olMarkComplete) {
			Dispatch.call(olEmail.dispatch, "MarkAsTask", newInterval.getCode());
			flag = OlFlagStatusEnum.olFlagComplete;
		} else {
			Dispatch.call(olEmail.dispatch, "MarkAsTask", newInterval.getCode());
			flag = OlFlagStatusEnum.olFlagMarked;
		}
		
		Dispatch.put(olEmail.dispatch,"FlagStatus",flag.getCode());

		Dispatch.call(olEmail.dispatch, "Save");
		
	}
	
	//
	// Getters
	//
	
	public OlFlagStatusEnum getFlag() {
		return flag;
	}

//	public LocalDateTime getTaskStartDate() {
//		return taskStartDate;
//	}
//
//	public LocalDateTime getTaskDueDate() {
//		return taskDueDate;
//	}
//
//	public LocalDateTime getTaskCompletedDate() {
//		return taskCompletedDate;
//	}
//
//	public LocalDateTime getToDoTaskOrdinal() {
//		return toDoTaskOrdinal;
//	}

	//
	// Setters
	//
	
//	public void setTaskStartDate(LocalDateTime taskStartDate) {
//		this.taskStartDate = taskStartDate;
//	}
//
//	public void setTaskDueDate(LocalDateTime taskDueDate) {
//		this.taskDueDate = taskDueDate;
//	}
//
//	public void setTaskCompletedDate(LocalDateTime taskCompletedDate) {
//		this.taskCompletedDate = taskCompletedDate;
//	}
//
//	public void setToDoTaskOrdinal(LocalDateTime toDoTaskOrdinal) {
//		this.toDoTaskOrdinal = toDoTaskOrdinal;
//	}

	
	
}
