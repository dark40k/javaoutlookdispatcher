package fr.dark40k.outlookdispatcher.outlookconnector;

public enum OlMailRecipientEnum {

	olUnknown(-1),
	olOriginator(0),
	olTo(1),
	olCC(2),
	olBCC(3);
	
	private final int code;
	
	private OlMailRecipientEnum(int code) {
		this.code=code;
	}
	
	public int getCode() {
		return code;
	}
	
	public static OlMailRecipientEnum fromCode(int searchCode) {
		for (OlMailRecipientEnum flagStatus : values()) {
			if (flagStatus.code == searchCode) {
				return flagStatus;
			}
		}
		return OlMailRecipientEnum.olUnknown;
	}

	
}
