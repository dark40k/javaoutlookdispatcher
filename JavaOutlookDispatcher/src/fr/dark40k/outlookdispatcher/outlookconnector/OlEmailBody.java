package fr.dark40k.outlookdispatcher.outlookconnector;

import com.jacob.com.ComFailException;
import com.jacob.com.Dispatch;


public class OlEmailBody {

	public final static OlEmailBody EMPTYBODY = new OlEmailBody("", OlEmailBodyFormatEnum.PLAIN);

	private OlEmailBodyFormatEnum format;
	private String body;

	private OlEmailBody(String body, OlEmailBodyFormatEnum format) {
		this.body = body;
		this.format = format;
	}

	public OlEmailBodyFormatEnum getFormat() {
		return format;
	}

	public String getContent() {
		return body;
	}

	public static OlEmailBody getBodyFromOutlook(String entryID) {

		try {
			Dispatch mailItemD = DispUtil.call(OlConnector.getSession(), "GetItemFromID", entryID);

			OlEmailBodyFormatEnum format = OlEmailBodyFormatEnum.valueOfByCode(DispUtil.getInt(mailItemD, "BodyFormat"));

			String body = DispUtil.getString(mailItemD, (format==OlEmailBodyFormatEnum.HTML)?"HTMLBody":"Body");

			mailItemD.safeRelease();

			return new OlEmailBody(body, format);

		} catch (ComFailException e) {
			return OlEmailBody.EMPTYBODY;
		}
	}


}
