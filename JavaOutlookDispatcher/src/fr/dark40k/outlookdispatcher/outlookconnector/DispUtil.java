package fr.dark40k.outlookdispatcher.outlookconnector;

import java.time.LocalDateTime;
import java.time.ZoneId;

import com.jacob.com.DateUtilities;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class DispUtil {

	public static Dispatch get(Dispatch dispatch, String name) {
		Variant v=Dispatch.get(dispatch,name);
		Dispatch out=v.toDispatch();
		v.safeRelease();
		return out;
	}

	public static String getString(Dispatch dispatch, String name) {
			Variant v=Dispatch.get(dispatch,name);
			String out=v.toString();
			v.safeRelease();
			return out;
	}
	
	public static boolean getBoolean(Dispatch dispatch, String name) {
		Variant v=Dispatch.get(dispatch,name);
		boolean out=v.getBoolean();
		v.safeRelease();
		return out;
	}

	public static int getInt(Dispatch dispatch, String name) {
		Variant v=Dispatch.get(dispatch,name);
		int out=v.getInt();
		v.safeRelease();
		return out;
	}
		
	public static LocalDateTime getLocalDateTime(Dispatch dispatch, String name) {
		Variant v=Dispatch.get(dispatch,name);
		double out=v.getDate();
		v.safeRelease();
		return DateUtilities.convertWindowsTimeToDate(out).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static Dispatch call(Dispatch dispatch, String name, Object... attributes) {
		Variant vout=Dispatch.call(dispatch,name,attributes);
		Dispatch out=vout.toDispatch();
		vout.safeRelease();
		return out;
	}

}
