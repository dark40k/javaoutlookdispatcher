package fr.dark40k.outlookdispatcher.outlookconnector;

public enum OlMarkIntervalEnum {

	 olMarkComplete(5),
	 olMarkNextWeek(3),
	 olMarkNoDate(4),
	 olMarkThisWeek(2),
	 olMarkToday(0),
	 olMarkTomorrow(1);

	private final int code;
	
	private OlMarkIntervalEnum(int code) {
		this.code=code;
	}
	
	public int getCode() {
		return code;
	}
	
	public static OlMarkIntervalEnum fromCode(int searchCode) {
		for (OlMarkIntervalEnum flagStatus : values()) {
			if (flagStatus.code == searchCode) {
				return flagStatus;
			}
		}
		throw new IllegalArgumentException("Ce code ne correspond n'est pas valide. code=" + searchCode);
	}


}
