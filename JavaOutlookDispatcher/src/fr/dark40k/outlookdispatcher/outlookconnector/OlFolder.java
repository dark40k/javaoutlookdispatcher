package fr.dark40k.outlookdispatcher.outlookconnector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.jacob.com.Dispatch;

public abstract class OlFolder extends OlObject implements Iterable<OlFolderEMail> {

	protected final ArrayList<OlFolderEMail> subFoldersList = new ArrayList<OlFolderEMail>();

	private OlFolder parent;

	protected OlFolder(String entryID, Dispatch dispatch, OlFolder parent) {

		super(entryID, dispatch, OlObjectClassEnum.olFolder);

		this.parent=parent;

		updateSubFoldersList();

	}

	//
	// Donn�es du folder
	//

	// TODO Refresh n'est pas impl�ment�, pas de r�actualisation des descendants du repertoire

	public abstract String getName();

	public abstract String getDescription();

	@Override
	public String toString() {
		return getName();
	}

	public StringBuilder getPath(String separator) {
		if (parent==null) return new StringBuilder(separator);
		StringBuilder str = parent.getPath(separator);
		str.append(separator);
		str.append(getName());
		return str;
	}

	//
	// Acces au pere
	//
	public OlFolder getParentFolder() {
		return parent;
	}

	protected void setParentFolder(OlFolder parent) {
		this.parent=parent;
	}

	//
	// Acces aux subFolders
	//

	/**
	 * Cr�e un Stream contenant le repertoire et ses sous-repertoires
	 *
	 * @param includeFolder renvoie true s'il faut inclure le repertoire et ses sous-repertoires
	 * @return Stream contenant le repertoire et les sous-repertoires passant le filtre
	 */
	public Stream<OlFolder> folderAndSubFoldersStream(Predicate<OlFolder> excludeFolder) {

		return (excludeFolder.test(this))
				?
				Stream.empty()
				:
				Stream.concat(
						Stream.of(this),
						subFoldersList.stream().flatMap(
								(Function<OlFolder, Stream<OlFolder>>) folder -> {
									return folder.folderAndSubFoldersStream(excludeFolder);
								}
								));

	}

	/**
	 * Cr�e un Stream contenant le repertoire et ses sous-repertoires
	 *
	 * @return Stream contenant le repertoire et ses sous-repertoires
	 */
	public Stream<OlFolder> folderAndSubFoldersStream() {

		return Stream.concat(
				Stream.of(this),
				subFoldersList.stream().flatMap(
						(Function<OlFolder, Stream<OlFolder>>) folder -> {
							return folder.folderAndSubFoldersStream();
						}
						));

	}

	public int getSubFoldersCount() {
		return getSubFoldersCount(false);
	}

	public int getSubFoldersCount(boolean recursive) {
		int sum = subFoldersList.size();
		if (recursive)
			for (OlFolderEMail folder : this)
				sum += folder.getSubFoldersCount(recursive);
		return sum;
	}

	public OlFolderEMail getSubFolder(int index) {
		return subFoldersList.get(index);
	}

	public int getIndexOfSubFolder(OlFolder folder) {
		return subFoldersList.indexOf(folder);
	}

	@Override
	public Iterator<OlFolderEMail> iterator() {
		return subFoldersList.iterator();
	}

	public Iterable<OlFolderEMail> getSubFolders() {
		return subFoldersList;
	}

	//
	// Construction des subfolders
	//

	protected abstract void updateSubFoldersList();

}
