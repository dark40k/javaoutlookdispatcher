package fr.dark40k.outlookdispatcher.outlookconnector;

import com.jacob.com.Dispatch;

public class OlFolderRoot extends OlFolder {

	protected OlFolderRoot() {
		super("<ROOT>", null, null);
	}

	//
	// Donn�es du folder
	//
	@Override
	public void refresh() {}
		
	@Override
	public String getName() {
		return "Root";
	}

	@Override
	public String getDescription() {
		return "";
	}

	//
	// Construction des subfolders
	//
	
	public void forceUpdate() {
		updateSubFoldersList();
	}
	
	@Override
	protected void updateSubFoldersList() {
		
		subFoldersList.clear();

		Dispatch folders = DispUtil.get(OlConnector.getSession(), "Folders");
		
		int count = DispUtil.getInt(folders, "Count");

		for (int mailIndex = 1; mailIndex <= count; mailIndex++) {
			Dispatch mailItem =  DispUtil.call(folders, "Item", mailIndex);
			if (DispUtil.getInt(mailItem, "DefaultItemType")==OlFolderDefaultItemTypeEnum.OL_MAIL_ITEM.code)
				subFoldersList.add(OlFolderEMail.getOrUpdateOlFolderEMail(DispUtil.getString(mailItem, "entryID"), this));
			mailItem.safeRelease();
		}
		
		folders.safeRelease();
		
	}

}
