package fr.dark40k.outlookdispatcher.outlookconnector;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComFailException;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class OlConnector {

	private final static Logger LOGGER = LogManager.getLogger();

	private final static HashMap<String,OlObject> objects = new HashMap<String,OlObject>(1000);

	private final static ActiveXComponent outlookActiveX;
	private final static Dispatch session;
	private final static OlFolderRoot rootFolder;

	static {

		LOGGER.info("Demarrage connection � Outlook");
		outlookActiveX = connectOutlook();

		Variant sessionV = outlookActiveX.getProperty("Session");
		session = sessionV.toDispatch();
		sessionV.safeRelease();

		rootFolder=new OlFolderRoot();

		LOGGER.info("Demarrage connection termin�.");
	}

	//
	// Fonctions utilisateur
	//

	public static OlFolderRoot getRootFolder() {
		return rootFolder;
	}

	public static OlObject getOlObject(String id) {
		return objects.get(id);
	}

	//
	// Fonctions internes au package
	//

	protected static Dispatch getSession() {
		return session;
	}

	protected static ActiveXComponent getActiveX() {
		return outlookActiveX;
	}

	protected static void addOlObject(OlObject object) {
		objects.put(object.getEntryID(),object);
	}

	protected static void removeOlObject(OlObject object) {
		objects.remove(object.getEntryID());
	}

	public static void refresh() {
		LOGGER.info("D�but rechargement des objets Outlook.");
		for (OlObject olObject : objects.values()) olObject.refresh();
		LOGGER.info("Fin rechargement des objets Outlook.");
	}

	private final static ActiveXComponent connectOutlook() {

		ActiveXComponent tryOutlookActiveX = null;

		try {
			LOGGER.info("Outlook (Outlook.Application)");
			tryOutlookActiveX = new ActiveXComponent("Outlook.Application");
			LOGGER.info("Success");
			return tryOutlookActiveX;
		} catch (ComFailException e) {
			LOGGER.info("Failed, cause = "+e.getLocalizedMessage());
		}

		try {
			LOGGER.info("Outlook 2007 (Outlook.Application.12)");
			tryOutlookActiveX = new ActiveXComponent("Outlook.Application.12");
			LOGGER.info("Success");
			return tryOutlookActiveX;
		} catch (ComFailException e) {
			LOGGER.info("Failed, cause = "+e.getLocalizedMessage());
		}

		throw new ComFailException("Impossible de se connecter � Outlook.");

	}

}
