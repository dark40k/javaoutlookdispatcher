package fr.dark40k.outlookdispatcher;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;
import java.util.function.BooleanSupplier;

import javax.swing.JOptionPane;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.com.ComThread;

import fr.dark40k.outlookdispatcher.console.MessageConsoleMgr;
import fr.dark40k.outlookdispatcher.dispatchdatabase.DispatchDatabase;
import fr.dark40k.outlookdispatcher.dispatchdatabase.ToSortEmailSingle;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.AutoUpdateFolderParam;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.EMailAutoDispatcher;
import fr.dark40k.outlookdispatcher.dispatchdatabase.autodispatcher.MultinomialClassifier.MultinomialClassifier;
import fr.dark40k.outlookdispatcher.dispatchdatabase.folderdata.OlFolderDataUtility;
import fr.dark40k.outlookdispatcher.gui.UpdateFoldersDescriptionsDialog;
import fr.dark40k.outlookdispatcher.gui.editdirectlink.EditDirectLinksJDialog;
import fr.dark40k.outlookdispatcher.gui.folderdescription.EditFolderDescriptionJDialog;
import fr.dark40k.outlookdispatcher.gui.folderlist.EditFolderListDialog;
import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlEmail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFlagStatusEnum;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolder;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;
import fr.dark40k.outlookdispatcher.outlookconnector.OlMarkIntervalEnum;
import fr.dark40k.outlookdispatcher.staticundomgr.StaticUndoManager;
import fr.dark40k.outlookdispatcher.staticundomgr.UndoableAction;
import fr.dark40k.outlookdispatcher.util.ActiveDirectory;
import fr.dark40k.outlookdispatcher.util.JProgressDialog;
import fr.dark40k.util.persistantproperties.LockedPropertiesFile;
import fr.dark40k.util.persistantproperties.LockedPropertiesFile.LockedFileException;
import fr.dark40k.util.persistantproperties.PropertiesList;
import fr.dark40k.util.persistantproperties.dialogs.DialogEditStringArrayField;
import fr.dark40k.util.persistantproperties.dialogs.propertieslist.DialogEditPropertiesList;

public class Dispatcher {

	// TODO : ajouter un filtre sur les entetes, principe REGEXP pour string, min/max pour valeurs?

	// TODO :  verifier pouquoi l'outils propose un repertoire exclu (Boite de reception)

	// TODO :  Ajouter une commande envoyer vers ne pas classer

	// TODO :  pourquoi le score ne change pas pour chaque mail ? Le total est re-distribu� sur les mails de la conversation ?

	// TODO :  amelioration score: indiquer le pourcentage des mails dans un repertoire pour mieux comprendre la r�partition

	// DONE :  arrondir le score dans l'affichage du tableau de selection

	// TODO :  permettre l'affichage du corps de message apr�s nettoyage

	// DONE :  pouvoir desactiver les adresses emails pour un repertoire

	// DONE :  ajouter une colonne avec destination complete pour faciliter le tri

	// DONE :  Faire que si un repertoire est declar� explicitement sur un code (N� dans le titre) il soit prioritaire sur ce code

	// DONE :  stocker le corps apr�s nettoyage pour accelerer reactualisation


	public final static MessageConsoleMgr messageConsoleMgr = new MessageConsoleMgr();
	private final static Logger LOGGER = LogManager.getLogger();

	final static Properties gitProperties = new Properties(); // fichier de propri�t�s du build

	private final static PropertiesList propertiesList = new PropertiesList();
	{
		propertiesList.add(Dispatcher.properties);
		propertiesList.add(DispatchDatabase.properties);
		propertiesList.add(EMailAutoDispatcher.properties);
	}

	public final static DispatcherProperties properties = new DispatcherProperties();

	public static File activeDirectory;

	private final LockedPropertiesFile lockedPropertiesFile = new LockedPropertiesFile();

	private final String title;

	private final DispatcherFrame dispatcherFrame;

	public final DispatchDatabase database;


	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------

	public Dispatcher(File paramFile) {

		try {
			LOGGER.info("Initialisation graphique");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		try {
			LOGGER.info("Chargement du fichier de proprietes (git.properties)");
			final InputStream stream = Dispatcher.class.getClassLoader().getResourceAsStream("git.properties");
			if (stream != null) {
				gitProperties.load(stream);
				stream.close();
			} else
				LOGGER.error("git.properties non trouvees.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// calcul du titre
		title = "Dispatcher de mails <" + ((!gitProperties.isEmpty())?gitProperties.getProperty("git.commit.id.describe-short"):"git.properties missing")+">";

		// demarre le log
		LOGGER.info("");
		LOGGER.info("");
		LOGGER.info("Starting - " + title);
		LOGGER.info("");
		LOGGER.info("");

		// active l'autogarbage collection de JACOB
		System.setProperty("com.jacob.autogc","true");

		// force l'initialisation des repertoires
		OlConnector.getRootFolder();


		// Identifie et charge les parametres
		try {

			LOGGER.info("Chargement fichier parametre:"+paramFile);

			lockedPropertiesFile.loadAndLock(propertiesList, paramFile);

			ActiveDirectory.set(paramFile);

		} catch (LockedFileException e) {
			LOGGER.error("Erreur de chargement du fichier parametres.");
			LOGGER.error(e);
			doExit();
		}

		// Initialisation de la base de donn�es
		database = new DispatchDatabase();

		// Initialisation de l'affichage
		dispatcherFrame = new DispatcherFrame(this);
		dispatcherFrame.setTitle(title);
		dispatcherFrame.setDatabase(database);

		// Connecte l'affichage � l'avancement des transfers avec l'affichage
		database.bufferedMoves.setProgressDisplay((msg)->{dispatcherFrame.setTransferProgressMessage(msg);});

		ToolTipManager.sharedInstance().setInitialDelay(250);
		ToolTipManager.sharedInstance().setReshowDelay(250);

		// Mise � jours de la base de donn�es
		// doUpdateAll();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------

	public void doUpdateAll() {

		JProgressDialog.showProgressDialog(
				dispatcherFrame,
				"Mise � jours donn�es",
				"Lancement",
				(JProgressDialog dialog) -> {doResetDatabase(dialog);}
			);

	}



	public void doResetDatabase(JProgressDialog progressDialog) {

		dispatcherFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		StaticUndoManager.discardAllEdits(); // Vide le buffer Undo/Redo

		database.resetDatabase(properties.selectConversation,progressDialog);

		dispatcherFrame.setCursor(Cursor.getDefaultCursor());
	}

	//
	// Commandes des menus
	//

	public void doSaveAndExit() {

		LOGGER.info("Saving parameters.");

		database.saveConfiguration();

		dispatcherFrame.saveWindowConfiguration();
		dispatcherFrame.dispose();

		lockedPropertiesFile.saveAndRelease();

		doExit();
	}

	public static void doExit() {
		LOGGER.info("Exiting.");
		ComThread.Release();
		System.exit(0);
	}

	protected void doShowEventConsole(boolean abortMode) {
		messageConsoleMgr.setAbortMode(abortMode);
		messageConsoleMgr.setVisible(true);
	}

	protected void doActualiser() {

		database.saveConfiguration();
		doUpdateAll();
	}

	public void doTrainClassifier() {

		String s = JOptionPane.showInputDialog("Profondeur d'apprentissage (100) ?","100");

		int maxDepth;
		try {
			maxDepth = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return;
		}

		MultinomialClassifier classifier = new MultinomialClassifier();

		JProgressDialog.showProgressDialog(
				dispatcherFrame,
				"Mise � jours donn�es",
				"Lancement",
				(JProgressDialog dialog) -> {
					classifier.train(OlConnector.getRootFolder(), maxDepth, dialog);
				});
	}

	public void doActualiserDossiers() {
//		if (JOptionPane.showConfirmDialog(dispatcherFrame, "La r�actualisation de tous les dossiers de destination va \nmodifier leur description dans Outlook.", "Confirmation", JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION) return;

		UpdateFoldersDescriptionsDialog updateDialog = new UpdateFoldersDescriptionsDialog();
		updateDialog.setVisible(true);

		if (updateDialog.getExistStatus()!=true) return;

		AutoUpdateFolderParam updateParam = updateDialog.getAutoUpdateFolderMode();

		JProgressDialog.showProgressDialog(
				dispatcherFrame,
				"Mise � jours donn�es",
				"Lancement",
				(JProgressDialog dialog) -> {EMailAutoDispatcher.updateAllFoldersDefinitions(OlConnector.getRootFolder(), updateParam, dialog);}
			);

	}

	public void doActualiserSousDossiers(OlFolder folder) {
//		if (JOptionPane.showConfirmDialog(dispatcherFrame, "La r�actualisation de tous les dossiers de destination va \nmodifier leur description dans Outlook.", "Confirmation", JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION) return;

		UpdateFoldersDescriptionsDialog updateDialog = new UpdateFoldersDescriptionsDialog();
		updateDialog.setVisible(true);

		if (updateDialog.getExistStatus()!=true) return;

		AutoUpdateFolderParam updateParam = updateDialog.getAutoUpdateFolderMode();

		JProgressDialog.showProgressDialog(
				dispatcherFrame,
				"Mise � jours donn�es",
				"Lancement",
				(JProgressDialog dialog) -> {EMailAutoDispatcher.updateAllFoldersDefinitions(folder, updateParam, dialog);}
			);

	}




	public void doEraseAllFolders() {

		if (JOptionPane.showConfirmDialog(dispatcherFrame, "La r�actualisation de tous les dossiers de destination va \nmodifier leur description dans Outlook.", "Confirmation", JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION) return;

		messageConsoleMgr.setVisible(true);

		OlFolderDataUtility.resetAllFolders();

	}


	protected void doChangeConversationSelectionMode() {
		dispatcherFrame.dispatchFolderViewPanel.setConversationSelectionMode(dispatcherFrame.chckbxmntmSelectionnerConversations.isSelected());
		properties.selectConversation=dispatcherFrame.chckbxmntmSelectionnerConversations.isSelected();
	}

	//
	// Undo / Redo
	//

	public void doUndo() {
		if (StaticUndoManager.canUndo())
			JProgressDialog.showProgressDialog(
					dispatcherFrame,
					"Mise � jours donn�es",
					"Lancement",
					(JProgressDialog dialog) -> {StaticUndoManager.undo(dialog);}
				);
		else
			JOptionPane.showMessageDialog(dispatcherFrame,
				    "Il n'y a pas d'action a defaire.",
				    "Erreur",
				    JOptionPane.WARNING_MESSAGE);

	}


	public void doRedo() {
		if (StaticUndoManager.canRedo())
			JProgressDialog.showProgressDialog(
					dispatcherFrame,
					"Mise � jours donn�es",
					"Lancement",
					(JProgressDialog dialog) -> {StaticUndoManager.redo(dialog);}
				);
		else
			JOptionPane.showMessageDialog(dispatcherFrame,
				    "Il n'y a pas d'action a refaire.",
				    "Erreur",
				    JOptionPane.WARNING_MESSAGE);

	}


	//
	// Deplacement des mails
	//

	public void doMoveSelection() {

		LOGGER.info("Action : Ranger mails selectionn�s.");

		dispatcherFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		final Collection<ToSortEmailSingle> listMails = dispatcherFrame.dispatchFolderViewPanel.getExtendedEmailSelection();

		dispatcherFrame.dispatchFolderViewPanel.fullRefreshPanel();

		JProgressDialog.showProgressDialog(
				dispatcherFrame,
				"Mise � jours donn�es",
				"Lancement",
				(JProgressDialog dialog) -> {database.doMoveEmails(listMails,"Ranger Emails",null,true,dialog);}
			);

		dispatcherFrame.setCursor(Cursor.getDefaultCursor());
	}

	public void doMoveToTrashSelection() {

		LOGGER.info("Action : Supprimer mail selectionn�.");

		Collection<ToSortEmailSingle> listMails = dispatcherFrame.dispatchFolderViewPanel.getEmailSelection();

		if (listMails.size()==0) {
			LOGGER.warn("Pas de selection. Operation annul�e.");
			return;
		}

		OlFolderEMail destFolder=(OlFolderEMail) OlConnector.getOlObject(properties.trashDestinationAddress);
		if (destFolder==null) {
			LOGGER.warn("Repertoire corbeille non trouv�. Operation annul�e.");
			return;
		}

		dispatcherFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		dispatcherFrame.dispatchFolderViewPanel.fullRefreshPanel();

		database.doMoveEmails(listMails,"Supprimer mails", destFolder,false, null );

		dispatcherFrame.setCursor(Cursor.getDefaultCursor());

	}

	//
	// Edition des parametres
	//

	protected void doEditScanDirectories() {
		DialogEditStringArrayField.showDialog("Repertoires de recherche", DispatchDatabase.properties.scanDirectories);
	}

	protected void doEditExcludedWords() {
		DialogEditStringArrayField.showDialog("Table d'exclusion de mots", EMailAutoDispatcher.properties.excludedWordsParam);
	}

	protected void doEditExcludedDirectories() {
		DialogEditStringArrayField.showDialog("Table d'exclusion de repertoires", DispatchDatabase.properties.excludedDirectories);
	}

	protected void doEditExcludedRecDirectories() {
		DialogEditStringArrayField.showDialog("Table d'exclusion de repertoires recursifs", DispatchDatabase.properties.excludedRecDirectories);
	}

	public void doEditExcludedAddresses() {
		DialogEditStringArrayField.showDialog("Table d'exclusion des adresses", EMailAutoDispatcher.properties.excludedAddressesParam);
	}

	public void doEditPermanentDestinationDirectories() {
		DialogEditStringArrayField.showDialog("Table d'exclusion des adresses", properties.permanentDestinationAddresses);
	}

	public void doEditDirectLinks() {
		EditDirectLinksJDialog.showDialog("Editer r�gles globales");
	}

	public void doEditFolders() {
		boolean ret=EditFolderListDialog.showDialog(OlConnector.getRootFolder());
		if (ret)
			dispatcherFrame.folderAssignementPanel.forceRefresh();
	}



	public void doEditTrashDirectory() {

		String s = (String)JOptionPane.showInputDialog(
				dispatcherFrame,
                "Indiquer ID du repertoire:",
                "Definition poubelle",
                JOptionPane.PLAIN_MESSAGE,
                null,
                (Object[]) null,
                properties.trashDestinationAddress);

		if ((s == null) || (s.length() == 0)) {
			return;
		}

		properties.trashDestinationAddress=s;

	}

	//
	// Manipulation des repertoires
	//

	public void doRenameFolder(OlFolder folder) {

		if (!(folder instanceof OlFolderEMail)) {
			JOptionPane.showMessageDialog(dispatcherFrame.dispatchFolderViewPanel,
				    "Le repertoire ne contient pas de mails.",
				    "Erreur de type",
				    JOptionPane.WARNING_MESSAGE);
		}

		String collapsedState = dispatcherFrame.folderAssignementPanel.saveCollapsedState();

		OlFolderEMail folderEMail = (OlFolderEMail) folder;

		String s = (String)JOptionPane.showInputDialog(
				dispatcherFrame.dispatchFolderViewPanel,
                "Indiquer non du nouveau repertoire:",
                "Renommer repertoire",
                JOptionPane.PLAIN_MESSAGE,
                null,
                (Object[]) null,
                folderEMail.getName());

		if ((s != null) && (s.length() > 0)) {
			LOGGER.info("Renommer \""+folderEMail.getName()+"\" => \""+s+"\"");
			folderEMail.setName(s);
			dispatcherFrame.folderAssignementPanel.forceRefresh(collapsedState);
		}

	}

	public OlFolderEMail doCreateSubFolder(OlFolder folder) {

		if (!(folder instanceof OlFolderEMail)) {
			JOptionPane.showMessageDialog(dispatcherFrame.dispatchFolderViewPanel,
				    "Le repertoire ne contient pas de mails.",
				    "Erreur de type",
				    JOptionPane.WARNING_MESSAGE);
			return null;
		}

		OlFolderEMail folderEMail = (OlFolderEMail) folder;

		String s = (String)JOptionPane.showInputDialog(
				dispatcherFrame.dispatchFolderViewPanel,
                "Indiquer non du nouveau repertoire:",
                "Creer repertoire",
                JOptionPane.PLAIN_MESSAGE,
                null,
                (Object[]) null,
                "Nouveau");

		if ((s == null) || (s.length() == 0)) {
			return null;
		}


		LOGGER.info("Creer \""+s+"\" dans \""+folderEMail.getName()+"\"");

		String collapsedState = dispatcherFrame.folderAssignementPanel.saveCollapsedState();

		OlFolderEMail newFolder=folderEMail.createSubFolder(s);

		if (newFolder!=null) {
			LOGGER.info("Creation reussie, MaJ affichage");
			dispatcherFrame.folderAssignementPanel.forceRefresh(collapsedState);
		} else {
			LOGGER.warn("Echec creation.");
			JOptionPane.showMessageDialog(dispatcherFrame.dispatchFolderViewPanel,
				    "Echec de la creation, nom invalide ou existe deja.",
				    "Erreur",
				    JOptionPane.WARNING_MESSAGE);
		}

		return newFolder;
	}

//	public void doEditFolderRawDescription(OlFolder folder) {
//
//		if (!(folder instanceof OlFolderEMail)) {
//			JOptionPane.showMessageDialog(dispatcherFrame.dispatchFolderViewPanel, "Le repertoire ne contient pas de mails.", "Erreur de type", JOptionPane.WARNING_MESSAGE);
//		}
//
//		OlFolderEMail folderEMail = (OlFolderEMail) folder;
//
//		int n = 0;
//		do {
//			JTextArea msg = new JTextArea(folder.getDescription());
//			msg.setLineWrap(true);
//			msg.setWrapStyleWord(true);
//			msg.setEditable(true);
//			JScrollPane scrollPane = new JScrollPane(msg);
//			scrollPane.setPreferredSize(new Dimension(500, 250));
//			Object[] options = { "Mettre � jour", "Appliquer", "Annuler" };
//
//			n = JOptionPane.showOptionDialog(null, scrollPane, " Description de " + folderEMail.getName(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
//
//			if (n == 0) EMailAutoDispatcher.updateFolderDefinition(folder, dispatcherParams);
//
//			if (n == 1) {
//				LOGGER.info("Mise � jours de la d�finition du repertoire: "+folderEMail.getName());
//				folderEMail.setDescription(msg.getText());
//			}
//
//		} while (n == 0);
//
//	}

	public void doEditFolderDescription(OlFolder folder) {

		if (!(folder instanceof OlFolderEMail)) {
			JOptionPane.showMessageDialog(dispatcherFrame.dispatchFolderViewPanel, "Le repertoire ne contient pas de mails.", "Erreur de type", JOptionPane.WARNING_MESSAGE);
		}

		EditFolderDescriptionJDialog.editDescription((OlFolderEMail) folder);

	}

	public void doEditParameters() {
		DialogEditPropertiesList.editPropertiesList(propertiesList,true);
	}

	public static void doSetStatusOfEMail(OlEmail mail, OlFlagStatusEnum newStatus) {

		OlFlagStatusEnum oldStatus = mail.status.getFlag();

		LOGGER.info("Changer du status du mail \""+mail.getSubject()+"\" => "+ newStatus);

		mail.status.markIntervalAndSetStatusFlag(getDefaultInterval(newStatus));

		BooleanSupplier undoAction = () -> {
			mail.status.markIntervalAndSetStatusFlag(getDefaultInterval(oldStatus));
			return true;
		};

		BooleanSupplier redoAction = () -> {
			mail.status.markIntervalAndSetStatusFlag(getDefaultInterval(newStatus));
			return true;
		};

		StaticUndoManager.addUndoableAction(
				new UndoableAction(
						"Changer status",
						undoAction ,
						redoAction
						)
				);

	}
	private static OlMarkIntervalEnum getDefaultInterval(OlFlagStatusEnum status) {
		switch (status) {
			case olFlagComplete:
				return OlMarkIntervalEnum.olMarkComplete;
			case olFlagMarked:
				return OlMarkIntervalEnum.olMarkNoDate;
			case olNoFlag:
				return null;
		}
		throw new RuntimeException("La ligne ne devrait pas etre atteinte: code=" + status);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Lancement de l'application
	//
	// -------------------------------------------------------------------------------------------

	public static void main(String[] args) {

		File startFile = null;

		if (args.length > 0) {
			startFile = new File(args[0]);
		}

		final File paramFile = startFile;

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Dispatcher dispatcher=new Dispatcher(paramFile);
				dispatcher.dispatcherFrame.setVisible(true);
			}
		});
	}

}

