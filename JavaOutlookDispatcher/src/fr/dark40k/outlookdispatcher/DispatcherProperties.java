package fr.dark40k.outlookdispatcher;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.fields.ParamGroupField;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;
import fr.dark40k.util.persistantproperties.tags.PropertyAlias;
import fr.dark40k.util.persistantproperties.tags.PropertyRootClassAlias;

@PropertyRootClassAlias("dispatcherMain")
public final class DispatcherProperties extends Property {

	@PropertyAlias("selectConversation")
	public Boolean selectConversation = true;

	@PropertyAlias("trashDirectory")
	public String trashDestinationAddress = "";

	@PropertyAlias("permanentAdresses")
	public final StringHashSetField permanentDestinationAddresses = new StringHashSetField();

	@PropertyAlias("displayParams")
	public final ParamGroupField displayParams = new ParamGroupField();

}
