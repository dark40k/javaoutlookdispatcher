package fr.dark40k.outlookdispatcher;

import java.util.Arrays;

import fr.dark40k.util.persistantparameters.ParamBoolean;
import fr.dark40k.util.persistantparameters.ParamGroup;
import fr.dark40k.util.persistantparameters.ParamRoot;
import fr.dark40k.util.persistantparameters.ParamString;
import fr.dark40k.util.persistantparameters.ParamStringList;

public class DispatcherParams extends ParamRoot {

	public final ParamStringList scanDirectoriesParam;
	public static final String SCAN_DIRECTORIES_PARAMNAME = "scanDirectoriesID";
	public static final String[] SCAN_DIRECTORIES_DEFAULT = { };

	public final ParamStringList excludedWordsParam;
	public final static String EXCLUSION_WORDS_PARAMNAME="excludedWords";
	public final static String[] EXCLUSION_WORDS_DEFAULT = { "pour","for","des","from","and","with","avec"};

	public final ParamStringList excludedDirectoriesParam;
	public final static String EXCLUSION_DIRECTORIES_PARAMNAME="excludedDirectoriesID";
	public final static String[] EXCLUSION_DIRECTORIES_DEFAULT = { };

	public final ParamStringList excludedRecDirectoriesParam;
	public final static String EXCLUSION_REC_DIRECTORIES_PARAMNAME="excludedRecDirectoriesID";
	public final static String[] EXCLUSION_REC_DIRECTORIES_DEFAULT = { };

	public final ParamStringList excludedAddressesParam;
	public final static String EXCLUSION_ADDRESS_PARAMNAME="excludedAdresses";
	public final static String[] EXCLUSION_ADDRESSES_DEFAULT = { ".*@cedrat-tec.com$"};

	public final ParamStringList permanentDestinationAddressesParam;
	public final static String PERMANENT_DESTINATION_ADDRESSES_PARAMNAME="permanentAdresses";
	public final static String[] PERMANENT_DESTINATION_ADDRESSES_DEFAULT = { };

	public final ParamBoolean selectConversationParam;
	public final static String SELECT_CONVERSATION_PARAMNAME="selectConversation";
	public final static boolean SELECT_CONVERSATION_DEFAULT = true;

	public final ParamString trashDestinationAddressParam;
	public final static String TRASH_DIRECTORY_PARAMNAME="trashDirectory";
	public final static String TRASH_DIRECTORY_DEFAULT = "";

	public final ParamGroup manualDestFoldersParam;
	public final static String DEST_FOLDERS_PARAMNAME="manualDestFolders";

	public final ParamGroup moveToFolderHistory;
	public final static String MOVE_TO_FOLDER_HISTORY_PARAMNAME="destFolderHistory";

	public final ParamString classifierModelFileParam;
	public final static String CLASSIFIER_MODEL_FILE_PARAMNAME="classifierModelFile";
	public final static String CLASSIFIER_MODEL_FILE_DEFAULT = "classifier.model";

	public final ParamString classifierArffFileParam;
	public final static String CLASSIFIER_ARFF_FILE_PARAMNAME="classifierArffFile";
	public final static String CLASSIFIER_ARFF_FILE_DEFAULT = "classifier.arff";


	public DispatcherParams() {

		// initialisaation
		super();

		// Initialisation des parametres

		scanDirectoriesParam = getSubParamStringList(SCAN_DIRECTORIES_PARAMNAME, Arrays.asList(SCAN_DIRECTORIES_DEFAULT));

		selectConversationParam = getSubParamBoolean(SELECT_CONVERSATION_PARAMNAME, SELECT_CONVERSATION_DEFAULT);

		excludedDirectoriesParam = getSubParamStringList(EXCLUSION_DIRECTORIES_PARAMNAME, Arrays.asList(EXCLUSION_DIRECTORIES_DEFAULT));

		excludedRecDirectoriesParam = getSubParamStringList(EXCLUSION_REC_DIRECTORIES_PARAMNAME, Arrays.asList(EXCLUSION_REC_DIRECTORIES_DEFAULT));

		excludedWordsParam = getSubParamStringList(EXCLUSION_WORDS_PARAMNAME, Arrays.asList(EXCLUSION_WORDS_DEFAULT));

		excludedAddressesParam = getSubParamStringList(EXCLUSION_ADDRESS_PARAMNAME, Arrays.asList(EXCLUSION_ADDRESSES_DEFAULT));

		permanentDestinationAddressesParam = getSubParamStringList(PERMANENT_DESTINATION_ADDRESSES_PARAMNAME, Arrays.asList(PERMANENT_DESTINATION_ADDRESSES_DEFAULT));

		trashDestinationAddressParam = getSubParamString(TRASH_DIRECTORY_PARAMNAME, TRASH_DIRECTORY_DEFAULT);

		manualDestFoldersParam = getSubParamGroup(DEST_FOLDERS_PARAMNAME);

		moveToFolderHistory = getSubParamGroup(MOVE_TO_FOLDER_HISTORY_PARAMNAME);

		classifierModelFileParam = getSubParamString(CLASSIFIER_MODEL_FILE_PARAMNAME, CLASSIFIER_MODEL_FILE_DEFAULT);

		classifierArffFileParam = getSubParamString(CLASSIFIER_ARFF_FILE_PARAMNAME, CLASSIFIER_ARFF_FILE_DEFAULT);

	}

}
