package fr.dark40k.outlookdispatcher.util;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ActiveDirectory {

	private final static Logger LOGGER = LogManager.getLogger();

	private static File activeDir = new File(System.getProperty("user.dir"));{
		LOGGER.debug("Active dir = "+activeDir.getAbsolutePath());
	}

	public static void set(File newDirectory) {

		if (newDirectory==null) {
			LOGGER.debug("Active dir = "+activeDir.getAbsolutePath());
			return;
		}

		if (newDirectory.isFile()) {
			set(newDirectory.getParentFile());
			return;
		}

		if (!newDirectory.exists()) {
			LOGGER.debug("Cancelling - Active dir does not exist = "+activeDir.getAbsolutePath());
			return;
		}

		activeDir=newDirectory.getAbsoluteFile();
		LOGGER.debug("Active dir = "+activeDir.getAbsolutePath());
	}

	public static File get() {
		return activeDir;
	}

	public static File getAbsolute(File fileIn) {

		if (fileIn.isAbsolute()) return fileIn;

		return new File(activeDir.getAbsolutePath(),fileIn.getPath());

	}

//	public static void main(String[] args) {
//
//		System.out.println(ActiveDirectory.getAbsolute(new File("./tutu/toto.txt")));
//
//
//	}

}
