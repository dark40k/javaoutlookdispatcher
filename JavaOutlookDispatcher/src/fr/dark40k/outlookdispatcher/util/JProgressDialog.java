package fr.dark40k.outlookdispatcher.util;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JProgressDialog {

	private final static Logger LOGGER = LogManager.getLogger();

	private final JDialog dlgProgress;
	private final JLabel lblStatus;
	private final JProgressBar pbProgressAuto;
	private final JProgressBar pbProgressManual;

	private boolean updatePending;

	private boolean effModeAuto;

	private boolean modeAuto;
	private int min;
	private int max;
	private int val;
	private String status;

	private JProgressDialog(Frame frame,String dialogTitle, String initStatus)  {

		dlgProgress = new JDialog(frame, dialogTitle, true);//true means that the dialog created is modal

		lblStatus = new JLabel(initStatus); // this is just a label in which you can indicate the state of the processing

		updatePending = false;

		modeAuto = true;
		min=0;
		max=1;
		val=0;
		status="";

		effModeAuto = true;

		pbProgressAuto = new JProgressBar();
		pbProgressAuto.setIndeterminate(true); //we'll use an indeterminate progress bar

		pbProgressManual = new JProgressBar(0,1);
		pbProgressManual.setIndeterminate(false);

		dlgProgress.setLocationRelativeTo(null);
		dlgProgress.setSize(400, 90);
		dlgProgress.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE); // prevent the user from closing the dialog

		dlgProgress.add(BorderLayout.NORTH, lblStatus);
		dlgProgress.add(BorderLayout.CENTER, pbProgressAuto);

	}

	private void updateDialog() {

		if (updatePending) return;
	    updatePending=true;

	    SwingUtilities.invokeLater(new Runnable() {
	        @Override
			public void run() {

	        	// LOGGER.debug("ProgressDialog update display");

	    		if (effModeAuto!=modeAuto) {

	    			if (modeAuto) {
	    				dlgProgress.remove(pbProgressManual);
	    				dlgProgress.add(BorderLayout.CENTER, pbProgressAuto);
	    			} else {
	    				dlgProgress.remove(pbProgressAuto);
	    				dlgProgress.add(BorderLayout.CENTER, pbProgressManual);
	    			}

	    			effModeAuto=modeAuto;
	    		}

	    		if (!modeAuto) {
	    			pbProgressManual.setMinimum(min);
	    			pbProgressManual.setMaximum(max);
	    			pbProgressManual.setValue(val);
	    		}

	    		lblStatus.setText(status);

	    		dlgProgress.repaint();

	    		updatePending=false;

	        }
	      });
	}

	public void set(String status) {
		this.status=status;
		updateDialog();
	}

	public void setAuto() {
		modeAuto=true;
		updateDialog();
	}

	public void setAuto(String status) {
		modeAuto=true;
		this.status=status;
		updateDialog();
	}

//	public void setProgress(int n) {
//		modeAuto=false;
//		this.val=n;
//		updateDialog();
//	}

//	public void setManual(String status, int n) {
//		modeAuto=false;
//		this.status=status;
//		this.val=n;
//		updateDialog();
//	}

	public void setManual(String status, int n, int min, int max) {
		modeAuto=false;
		this.status=status;
		this.min=min;
		this.max=max;
		this.val=n;
		updateDialog();
	}

	public void setVal(int newVal) {
		if (newVal>max) newVal=max;
		if (newVal<min) newVal=min;
		
		if (newVal==val) return;
		
		val=newVal;
		updateDialog();
	}
	
	public void incManualProgress() {
		if (!modeAuto) val+=1;
		updateDialog();
	}

	public static void showProgressDialog(Frame frame,String dialogTitle, String initStatus, Consumer<JProgressDialog> r) {

		final JProgressDialog dialog = new JProgressDialog(frame, dialogTitle, initStatus);
		final AtomicReference<String> errorMsg = new AtomicReference<String>(null);

		SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				LOGGER.info("starting.");
				try {
					r.accept(dialog);
				} catch (Exception e) {
					LOGGER.error("SwingWorker failed:",e);
					errorMsg.set(String.format("Unexpected problem: %s", e.toString()));
				}
				LOGGER.info("done.");
				return null;
			}

			@Override
			protected void done() {
				LOGGER.info("done.");
				dialog.dlgProgress.dispose();// close the modal dialog
			}
		};

		sw.execute(); // this will start the processing on a separate thread
		dialog.dlgProgress.setVisible(true); //this will block user input as long as the processing task is working

		if (errorMsg.get()!=null)
			throw new RuntimeException(errorMsg.get());

	}


}
