package fr.dark40k.outlookdispatcher.util;

import java.util.HashMap;

public class StringCounter {

	private final HashMap<String,Integer> counter;
	private final int maxCount;

	public StringCounter(int maxCount) {
		this(maxCount,100);
	}

	public StringCounter(int maxCount, int size) {
		this.maxCount=maxCount;
		counter = new HashMap<String,Integer>(size);
	}

	public boolean add(String string) {

		Integer count = counter.get(string);

		if (count==null) {
			counter.put(string,1);
			return false;
		}

		count++;
		counter.put(string,count);
		return (count>maxCount);

	}

}
