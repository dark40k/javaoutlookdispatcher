package fr.dark40k.outlookdispatcher.util;

import java.awt.Component;
import java.awt.Graphics;
import java.util.Vector;

import javax.swing.Icon;

public class CompoundIcon implements Icon {
	 
    private Vector<Icon> _icons = new Vector<Icon>();
    private int _spaceSize = 2;
 
    public CompoundIcon() {}
    
    public CompoundIcon(Icon[] icons) {
        for (int i = 0; i < icons.length; i++) {
            if (icons[i] != null) _icons.add(icons[i]);
        }
    }
 
    @Override
    public int getIconHeight() {
        int result = 0;
        for (Icon icon : getIcons()) {
            result = Math.max(result, icon.getIconHeight());
        }
        return result;
    }
 
    @Override
    public int getIconWidth() {
        int result = 0;
        for (Icon icon : getIcons()) {
            result += icon.getIconWidth();
            result += getSpaceSize();
        }
        return result;
    }
 
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int h = getIconHeight();
        int offset = 0;
 
        for (Icon icon : getIcons()) {
            icon.paintIcon(c, g, x + offset, y + (h - icon.getIconHeight()) / 2);
            offset += icon.getIconWidth();
            offset += getSpaceSize();
        }
    }
 
    public int getSpaceSize() {
        return _spaceSize;
    }
 
    public void setSpaceSize(int spaceSize) {
        this._spaceSize = spaceSize;
    }
 
    public void add(Icon icon) {
        _icons.add(icon);
    }
 
    public Vector<Icon> getIcons() {
        return _icons;
    }
}