package fr.dark40k.outlookdispatcher.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.outlookdispatcher.outlookconnector.OlConnector;
import fr.dark40k.outlookdispatcher.outlookconnector.OlFolderEMail;

public class ClearFolderData {

		private final static Logger LOGGER = LogManager.getLogger();

		public static void resetAllFolders() {
			for (OlFolderEMail subFolderEmail : OlConnector.getRootFolder() ) 
				resetFolder(subFolderEmail,true);
		}
		
		public static void resetFolder(OlFolderEMail folderEmail, boolean recursive) {
			
			resetFolderBlock(folderEmail,"autodispatch.*");
			resetFolderBlock(folderEmail,"conversation");
			
			if (recursive)
				for (OlFolderEMail subFolderEmail : folderEmail ) 
					resetFolder(subFolderEmail,true);
			
		}

		private static void resetFolderBlock(OlFolderEMail folderEmail,String title) {
			
			LOGGER.info("Scanning <"+title+"> in "+folderEmail.getName());
			
			String oldDescription=folderEmail.getDescription();
			String newDescription=oldDescription.replaceAll("(?s)<"+title+">.*</"+title+">", "");
			
			// applique la modification de la description (si elle a chang�)
			if (!oldDescription.equals(newDescription)) {
				LOGGER.info("Clear description : "+folderEmail.getName()+" => "+newDescription);
				folderEmail.setDescription(newDescription);
			}

		}

}
