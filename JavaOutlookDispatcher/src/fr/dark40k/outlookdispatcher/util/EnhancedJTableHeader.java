package fr.dark40k.outlookdispatcher.util;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class EnhancedJTableHeader extends JTableHeader {

	private static final int ADDITIONAL_MARGIN = 5;

	private final ArrayList<String> toolTips = new ArrayList<String>();

	public EnhancedJTableHeader(TableColumnModel cm, JTable table) {
		super(cm);
		setTable(table);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				doMouseClicked(e);
			}
		});
	}

	public void doMouseClicked(MouseEvent e) {
		if (getResizingAllowed() && (e.getClickCount() == 2)) {
			TableColumn column = getResizingColumn(e.getPoint(), columnAtPoint(e.getPoint()));
			if (column != null) doAutoResize(column);
		}
	}

	public void doAutoResize(TableColumn column) {
		int oldMinWidth = column.getMinWidth();
		column.setMinWidth(getRequiredColumnWidth(column));
		setResizingColumn(column);
		table.doLayout();
		column.setMinWidth(oldMinWidth);
	}

	public void doAutoResizeAll() {
		for (int col=0; col < table.getColumnCount(); col++)
			doAutoResize(table.getColumnModel().getColumn(col));
	}

	private int getRequiredColumnWidth(TableColumn column) {
		int modelIndex = column.getModelIndex();
		TableCellRenderer renderer;
		Component component;
		int requiredWidth = 0;
		int rows = table.getRowCount();
		for (int i = 0; i < rows; i++) {
			renderer = table.getCellRenderer(i, modelIndex);
			Object valueAt = table.getValueAt(i, modelIndex);
			component = renderer.getTableCellRendererComponent(table, valueAt, false, false, i, modelIndex);
			requiredWidth = Math.max(requiredWidth, component.getPreferredSize().width + 2);
		}
		return requiredWidth + ADDITIONAL_MARGIN;
	}

	private TableColumn getResizingColumn(Point p, int column) {
		if (column == -1) {
			return null;
		}
		Rectangle r = getHeaderRect(column);
		r.grow(-3, 0);
		if (r.contains(p)) {
			return null;
		}
		int midPoint = r.x + r.width / 2;
		int columnIndex;
		if (getComponentOrientation().isLeftToRight()) {
			columnIndex = (p.x < midPoint) ? column - 1 : column;
		} else {
			columnIndex = (p.x < midPoint) ? column : column - 1;
		}
		if (columnIndex == -1) {
			return null;
		}
		return getColumnModel().getColumn(columnIndex);
	}

	@Override
	public String getToolTipText(MouseEvent e) {
		int col = columnAtPoint(e.getPoint());
		int modelCol = getTable().convertColumnIndexToModel(col);
		String retStr;
		try {
			retStr = toolTips.get(modelCol);
		} catch (NullPointerException ex) {
			retStr = null;
		} catch (IndexOutOfBoundsException ex) {
			retStr = null;
		}
		if (retStr == null) {
			retStr = super.getToolTipText(e);
		}
		return retStr;
	}

	public void setToolTipForColumn(int col, String toolTip) {
		toolTips.add(col, toolTip);
	}

}