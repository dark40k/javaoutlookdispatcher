package fr.dark40k.outlookdispatcher.util;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.Scrollable;

public class ScrollablePanel extends JPanel implements Scrollable{
	
    @Override
	public Dimension getPreferredScrollableViewportSize() {
        return super.getPreferredSize(); //tell the JScrollPane that we want to be our 'preferredSize' - but later, we'll say that vertically, it should scroll.
    }

    @Override
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 1;
    }

    @Override
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 1;
    }

    @Override
	public boolean getScrollableTracksViewportWidth() {
        return true; //track the width, and re-size as needed.
    }

    @Override
	public boolean getScrollableTracksViewportHeight() {
        return true; //track the height, and re-size as needed.
    }
    
}