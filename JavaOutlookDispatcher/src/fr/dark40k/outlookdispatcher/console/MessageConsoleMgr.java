package fr.dark40k.outlookdispatcher.console;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyledDocument;

import org.apache.logging.log4j.LogManager;

import fr.dark40k.outlookdispatcher.console.MessageConsole.ConsoleOutputStream;

@SuppressWarnings("resource")
public class MessageConsoleMgr {

//	private final static Logger LOGGER = LogManager.getLogger();

	private MessageConsoleDialog msgDialog;

	private boolean abortMode;

	private StyledDocument document;

	private class interceptStream extends OutputStream {
		@Override
		public void write(int b) throws IOException {
			if (msgDialog==null) createDialog();
			if (!msgDialog.isVisible()) msgDialog.setVisible(true);
		}
	}

	public MessageConsoleMgr() {

		abortMode = false;

		document = new DefaultStyledDocument();

		// ajoute le gestionnaire d'affichage des erreurs non g�r�es
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread aThread, Throwable aThrowable) {
				LogManager.getLogger().error(aThread + " throws exception: " + aThrowable,aThrowable);
				setAbortMode(false);
				if (msgDialog==null) createDialog();
				msgDialog.setVisible(true);
			}
		});

		//
		// Log messages into console
		//

		MessageConsole mc = new MessageConsole(document);

		ConsoleOutputStream cosOut = mc.new ConsoleOutputStream(null, null);
		MultiOutputStream multiOut= new MultiOutputStream(System.out, cosOut);
		PrintStream stdout= new PrintStream(multiOut,true);
		System.setOut(stdout);

		ConsoleOutputStream cosErr = mc.new ConsoleOutputStream(Color.RED, null);
		MultiOutputStream multiErr= new MultiOutputStream(System.err, cosErr, new interceptStream());
		PrintStream stderr= new PrintStream(multiErr,true);
		System.setErr(stderr);

		mc.setMessageLines(5000);

	}

	public void setAbortMode(boolean abortMode) {
		if (msgDialog!=null)
			msgDialog.setAbortMode(abortMode);
		else
			this.abortMode=abortMode;
	}

	public void createDialog()  {
		msgDialog = new MessageConsoleDialog(abortMode, document);
	}

	public void setVisible(boolean b) {
		if (msgDialog==null) createDialog();
		msgDialog.setVisible(b);
	}

}
