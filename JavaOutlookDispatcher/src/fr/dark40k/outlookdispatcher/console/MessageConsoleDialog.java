package fr.dark40k.outlookdispatcher.console;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyledDocument;

public class MessageConsoleDialog extends JDialog {
	
//	private final static Logger LOGGER = LogManager.getLogger();

	private final JPanel contentPanel = new JPanel();
	private final JPanel buttonPane;
	private final JButton closeButton;
	private final JButton abortButton;

	public MessageConsoleDialog(boolean abortMode, StyledDocument document) {
				
		setTitle("Console output");
		setBounds(100, 100, 1200, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPanel.add(scrollPane, BorderLayout.CENTER);
		
		JTextPane textPane = new JTextPane(document);
		textPane.setFont(new Font("Consolas", Font.PLAIN, 13));
		textPane.setEditable(false);
		
		JPanel noWrapPanel = new JPanel( new BorderLayout() );
		noWrapPanel.add( textPane );
		scrollPane.setViewportView(noWrapPanel);

		buttonPane = new JPanel();
		buttonPane.setLayout(new BorderLayout());
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MessageConsoleDialog.this.setVisible(false);
				MessageConsoleDialog.this.setModal(false);
			}
		});
		buttonPane.add(closeButton, BorderLayout.EAST);
		
		abortButton = new JButton("Abort");
		abortButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		buttonPane.add(abortButton, BorderLayout.WEST);
		
		getRootPane().setDefaultButton(closeButton);
		
		setAbortMode(abortMode);
		
	}

	public void setAbortMode(boolean abortMode) {
		setModal(abortMode);
		closeButton.setEnabled(!abortMode);
	}
	
}
