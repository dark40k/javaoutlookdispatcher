package fr.dark40k.util.persistantproperties.fields;

import java.util.Collection;

import org.jdom2.CDATA;
import org.jdom2.Content;

public class StringHashSetField extends GenericImmutableHashSetField<String> {

	public StringHashSetField() {
		super();
	}

	public StringHashSetField(Collection<String> defaultString ) {
		super(defaultString);
	}

	public StringHashSetField(String[] defaultString) {
		super(defaultString);
	}

	@Override
	String importContent(Content content) {
		if (!(content instanceof CDATA)) return null;
		return ((CDATA)content).getText();
	}

	@Override
	Content exportContent(String property) {
		return new CDATA(property);
	}

}
