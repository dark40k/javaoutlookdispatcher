package fr.dark40k.util.persistantproperties.fields;

import org.jdom2.Element;

import fr.dark40k.util.persistantparameters.ParamPersistentGroup;
import fr.dark40k.util.persistantproperties.PropertyImportExport;

public class ParamPersistentGroupField extends ParamPersistentGroup implements PropertyImportExport {

	//
	// Constructeur
	//

	public ParamPersistentGroupField() {
		super(null,"paramgroup");
	}

	//
	// Import / Export
	//

	@Override
	public boolean importElement(Element fatherElt, String elementName) {
		Element elt = fatherElt.getChild(elementName);
		if (elt!=null) load(elt);
		return true;
	}

	@Override
	public Element exportElement(String newElementName) {
		Element elt = new Element(newElementName);
		save(elt);
		return elt;
	}

}
