package fr.dark40k.util.persistantproperties.fields;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.jdom2.Content;
import org.jdom2.Element;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.events.PropertyUpdateEvent;
import fr.dark40k.util.persistantproperties.events.PropertyUpdateListener;

public abstract class GenericHashMapField<T extends Property> extends GenericField implements PropertyUpdateListener {

	private final LinkedHashMap<String,T> propHashMap = new LinkedHashMap<String,T>();

	//
	// Constructeur
	//

	public GenericHashMapField() {
	}

	//
	// Data
	//

	public boolean containsValue(Object value) {
		return propHashMap.containsValue(value);
	}

	public void clear() {
		for (T values : propHashMap.values())
			values.removeUpdateListener(this);
		propHashMap.clear();
		fireUpdateEvent();
	}

	public int size() {
		return propHashMap.size();
	}

	public Collection<T> values() {
		return propHashMap.values();
	}

	public boolean containsKey(Object key) {
		return propHashMap.containsKey(key);
	}

	public T get(Object key) {
		return propHashMap.get(key);
	}

	public T put(String key, T value) {
		value.addUpdateListener(this);
		T ret = propHashMap.put(key, value);
		if (ret!=null) ret.removeUpdateListener(this);
		fireUpdateEvent();
		return ret;
	}

	public Set<Entry<String, T>> entrySet() {
		return propHashMap.entrySet();
	}

	public T remove(Object key) {
		T ret = propHashMap.remove(key);
		if (ret!=null) ret.removeUpdateListener(this);
		fireUpdateEvent();
		return ret;
	}


	//
	// Load/Save
	//

	abstract T importNewItem(Element elt);
	abstract Content exportItem(String key, T property);

	@Override
	public boolean importElement(Element fatherElt, String xmlName) {

		propHashMap.clear();

		Element elt = fatherElt.getChild(xmlName);
		if (elt==null) {
			fireUpdateEvent();
			return true;
		}

		List<Element> children = elt.getChildren();
		if (children.isEmpty()) {
			fireUpdateEvent();
			return true;
		}

		for (Element subElt : children) {
			T prop = importNewItem(subElt);
			propHashMap.put(subElt.getName(),prop);
			prop.addUpdateListener(this);
		}
		fireUpdateEvent();
		return true;
	}

	@Override
	public Element exportElement(String xmlName) {

		Element fatherElt = new Element(xmlName);

		for ( Entry<String, T> entry : propHashMap.entrySet())
			fatherElt.addContent(exportItem(entry.getKey(),entry.getValue()));

		return fatherElt;
	}

	//
	// Evenement d'objets enfants
	//

	@Override
	public void propertiesUpdated(PropertyUpdateEvent evt) {
		fireUpdateEvent();
	}


}