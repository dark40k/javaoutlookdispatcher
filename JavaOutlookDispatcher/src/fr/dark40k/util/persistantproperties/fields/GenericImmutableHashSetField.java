package fr.dark40k.util.persistantproperties.fields;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.jdom2.Content;
import org.jdom2.Element;

public abstract class GenericImmutableHashSetField<T> extends GenericField implements Iterable<T> {

	private final LinkedHashSet<T> propertiesList = new LinkedHashSet<T>();

	//
	// Constructeur
	//

	public GenericImmutableHashSetField() {
	}

	public GenericImmutableHashSetField(Collection<? extends T> defaultStringListIn) {
		set(defaultStringListIn);
	}

	public GenericImmutableHashSetField(T[] properties) {
		set(properties);
	}

	//
	// Data
	//

	@Override
	public Iterator<T> iterator() {
		return propertiesList.iterator();
	}

	public boolean contains(T property) {
		return propertiesList.contains(property);
	}

	public void clear() {
		propertiesList.clear();
		fireUpdateEvent();
	}

	public void set(Collection<? extends T> list) {
		propertiesList.clear();
		for (T property : list) {
			propertiesList.add(property);
		}
		fireUpdateEvent();
	}

	private void set(T[] properties) {
		propertiesList.clear();
		for (T property : properties) {
			propertiesList.add(property);
		}
		fireUpdateEvent();
	}

	public void add(T property) {
		propertiesList.add(property);
		fireUpdateEvent();
	}

	public void remove(T property) {
		propertiesList.remove(property);
		fireUpdateEvent();
	}

	public void switchParam(T property) {
		if (propertiesList.contains(property))
			remove(property);
		else
			add(property);
	}

	//
	// Load/Save
	//

	abstract T importContent(Content content);
	abstract Content exportContent(T property);

	@Override
	public boolean importElement(Element fatherElt, String xmlName) {

		propertiesList.clear();

		Element elt = fatherElt.getChild(xmlName);
		if (elt==null) {
			fireUpdateEvent();
			return true;
		}

		List<Content> contentList = elt.getContent();
		if (contentList.isEmpty()) {
			fireUpdateEvent();
			return true;
		}

		for (Content content : contentList) {
			T prop = importContent(content);
			if (prop!=null) propertiesList.add(prop);
		}
		fireUpdateEvent();
		return true;
	}

	@Override
	public Element exportElement(String xmlName) {

		Element fatherElt = new Element(xmlName);

		for (T property : propertiesList)
			fatherElt.addContent(exportContent(property));

		return fatherElt;
	}

}