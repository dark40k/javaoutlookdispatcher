package fr.dark40k.util.persistantproperties.fields;

import javax.swing.event.EventListenerList;

import fr.dark40k.util.persistantproperties.PropertyImportExport;
import fr.dark40k.util.persistantproperties.events.PropertyUpdateEvent;
import fr.dark40k.util.persistantproperties.events.PropertyUpdateListener;

public abstract class GenericField implements PropertyImportExport {

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public void addUpdateListener(PropertyUpdateListener listener) {
		listenerList.add(PropertyUpdateListener.class, listener);
	}

	public void removeUpdateListener(PropertyUpdateListener listener) {
		listenerList.remove(PropertyUpdateListener.class, listener);
	}

	public void fireUpdateEvent() {
		PropertyUpdateEvent evt = new PropertyUpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == PropertyUpdateListener.class) {
				((PropertyUpdateListener) listeners[i + 1]).propertiesUpdated(evt);
			}
		}
	}

}
