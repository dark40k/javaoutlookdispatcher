package fr.dark40k.util.persistantproperties.fields;

import org.jdom2.Element;

import fr.dark40k.util.persistantparameters.ParamGroup;
import fr.dark40k.util.persistantproperties.PropertyImportExport;

public class ParamGroupField extends ParamGroup implements PropertyImportExport {

	//
	// Constructeur
	//

	public ParamGroupField() {
		super(null,"paramgroup");
	}

	//
	// Import / Export
	//

	@Override
	public boolean importElement(Element fatherElt, String elementName) {
		Element elt = fatherElt.getChild(elementName);
		if (elt!=null) load(elt);
		return true;
	}

	@Override
	public Element exportElement(String newElementName) {
		Element elt = new Element(newElementName);
		save(elt);
		return elt;
	}

}
