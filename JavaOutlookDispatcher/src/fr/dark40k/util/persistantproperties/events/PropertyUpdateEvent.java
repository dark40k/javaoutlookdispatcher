package fr.dark40k.util.persistantproperties.events;

import java.util.EventObject;

public class PropertyUpdateEvent extends EventObject {
	public PropertyUpdateEvent(Object source) {
		super(source);
	}
}

