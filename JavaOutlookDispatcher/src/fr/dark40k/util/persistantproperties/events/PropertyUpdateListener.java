package fr.dark40k.util.persistantproperties.events;

import java.util.EventListener;

public interface PropertyUpdateListener extends EventListener {
	public void propertiesUpdated(PropertyUpdateEvent evt);
}
