package fr.dark40k.util.persistantproperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.output.XMLOutputter;

public class LockedPropertiesFile {

	private final static Logger LOGGER = LogManager.getLogger();

	private PropertiesList propertiesList;

	private RandomAccessFile paramFile;
	private FileChannel paramFileChannel;
	private FileLock paramFileLock;

	//
	// Sauvegarde g�n�rale
	//

	public void loadAndLock(PropertiesList globalproperties, File file) throws LockedFileException {

		if (file == null) throw new LockedFileException("Parameter file is null.");

		this.propertiesList=globalproperties;

		//
		// Chargement du fichier de parametres
		//

		LOGGER.info("Loading parameters, file = " + file.getAbsolutePath());

		Element rootElt = null;

		if (file.exists()) {
			SAXBuilder sxb = new SAXBuilder();
			try {
				rootElt = sxb.build(file).getRootElement();
			} catch (Exception e) {
				LOGGER.error("Le fichier parametre n'a pas put etre charg� : " + e.getMessage());
				JOptionPane.showMessageDialog(null, "Le fichier parametre n'a pas put etre charg�.\nL'application fonctionne deja?", "Loading error",
						JOptionPane.ERROR_MESSAGE);
				throw new LockedFileException("Le fichier parametre n'a pas put etre charg� : " + e.getMessage());
			}
		} else {
			LOGGER.info("Parameter File does not exist. Skipping.");
		}

		//
		// Chargement des parametres
		//
		propertiesList.importElement(rootElt);


		//
		// Creation (si necessaire) et Verrouillage du fichier parametres
		//
		LOGGER.info("Verrouillage du fichier parametres = " + file.getAbsolutePath());

		try {
			paramFile = new RandomAccessFile(file, "rw");
			paramFileChannel = paramFile.getChannel();
		} catch (FileNotFoundException e1) {
			LOGGER.error("Echec de la creation du fichier parametre: " + e1.getMessage());
			closeLock();
			throw new LockedFileException("Echec de la creation du fichier parametre: " + e1.getMessage());
		}

		try {
			paramFileLock = paramFileChannel.tryLock(0, Long.MAX_VALUE, false);
		} catch (OverlappingFileLockException | IOException e) {
			LOGGER.error("Echec du verrrouillage (application fonctionne deja?): " + e.getMessage());
			closeLock();
			throw new LockedFileException("Echec du verrrouillage (application fonctionne deja?).");
		}

		// ajoute un deverrouillage automatique � la fin de l'exection de la JVM
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (paramFileLock == null) return;
				System.out.println("Shutdown release of file : " + file.getAbsolutePath());
				closeLock();
			}
		});

	}

	@SuppressWarnings("resource")
	public void saveAndRelease() {

		Element rootElement = propertiesList.exportElement();

		Format f = Format.getPrettyFormat();
		f.setTextMode(TextMode.PRESERVE);

		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());

		OutputStream fileOut = null;

		try {
			fileOut = Channels.newOutputStream(paramFileChannel);
			sortie.output(rootElement, fileOut);
			fileOut.flush();
		} catch (IOException e) {
			LOGGER.warn("File could not be saved : " + e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error: File could not be saved : \n" + e.getMessage(), "Saving error", JOptionPane.ERROR_MESSAGE);
		}

		if (fileOut != null)
			try {
			paramFile.setLength(paramFile.getFilePointer());
			paramFile.close();
			} catch (IOException e) {
			LOGGER.error("Stream could not be closed : " + e.getMessage());
			}

		closeLock();

	}

	private void closeLock() {

		try {
			paramFileLock.release();
		} catch (Exception e) {
		}
		paramFileLock = null;

		try {
			paramFileChannel.close();
		} catch (Exception e) {
		}
		paramFileChannel = null;

	}

	//
	// Sous-classe pour retour d'erreur
	//

	public static class LockedFileException extends Exception
	{
	    //Parameterless Constructor
	    public LockedFileException() {}

	    //Constructor that accepts a message
	    public LockedFileException(String message)
	    {
	       super(message);
	    }

	}


}
