package fr.dark40k.util.persistantproperties;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.time.format.DateTimeParseException;

import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.util.persistantproperties.events.PropertyUpdateEvent;
import fr.dark40k.util.persistantproperties.events.PropertyUpdateListener;
import fr.dark40k.util.persistantproperties.tags.PropertyAlias;

public abstract class Property implements PropertyImportExport, PropertyUpdateListener {

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public void addUpdateListener(PropertyUpdateListener listener) {
		listenerList.add(PropertyUpdateListener.class, listener);
	}

	public void removeUpdateListener(PropertyUpdateListener listener) {
		listenerList.remove(PropertyUpdateListener.class, listener);
	}

	public void fireUpdateEvent() {
		PropertyUpdateEvent evt = new PropertyUpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == PropertyUpdateListener.class) {
				((PropertyUpdateListener) listeners[i + 1]).propertiesUpdated(evt);
			}
		}
	}

	//
	// Evenement d'objets enfants
	//

	@Override
	public void propertiesUpdated(PropertyUpdateEvent evt) {
		fireUpdateEvent();
	}

	//
	// Import / Export
	//

	@Override
	public boolean importElement(Element elt, String xmlName) {

		// status de l'importation, bascule sur false en cas de probl�me � l'importation
		boolean importStatus = true;

		// recupere le LOGGER
		Logger LOGGER = LogManager.getLogger();

		// recupere l'objet
		Element subElt = elt.getChild(xmlName);

		// on sort si pas trouve l'objet XML de stockage
		if (subElt == null) {
			LOGGER.warn("Champ <"+xmlName+"> non trouv� dans fichier XML, importation impossible, classe = " + this.getClass().getName());
			return false;
		}

		// balaie les champs pour trouver les informations
		for (Field field : this.getClass().getDeclaredFields()) {

			LOGGER.debug("Processing "+field.getName()+" in "+this.getClass().getName());

			// l'objet est statique donc d�j� correctement intialis�, pas besoin de le sauvegarder => passe au suivant
			if (Modifier.isStatic(field.getModifiers())) continue;

			// l'objet est marqu� transient (pas utiliser en serialisation) => passe au suivant
			if (Modifier.isTransient(field.getModifiers())) continue;

			// autoriser l'acces si le champ est priv�
			if (Modifier.isPrivate(field.getModifiers())) field.setAccessible(true);

			// importe l'objet
			try {

				// recupere l'objet � importer
				Object fieldObject = field.get(this);

				// l'objet n'existe pas donc pas possible de s'en sortir => passe au suivant
				if (fieldObject == null) {
					LOGGER.warn("Objet = null, importation impossible. Champ = "+field.getName() + ", classe = " + this.getClass().getName());
					importStatus=false;
					continue;
				}

				// verifie si le champ a une annotation
				String name = field.getName();

				// verifie s'il y a une annotation d'Alias
				if (field.getAnnotation(PropertyAlias.class)!=null)
					name = field.getAnnotation(PropertyAlias.class).value();

				// ajoute l'element � la suite s'il est compatible importExportXML
				if (fieldObject instanceof PropertyImportExport) {
					importStatus &= ((PropertyImportExport) fieldObject).importElement(subElt,name);
					continue;
				}

				// charge les donn�es XML
				String data = subElt.getAttributeValue(name);

				// si les donn�es n'ont pas �t� trouv�es => passe au suivant
				if (data==null) continue;

				// balaie les types d'importation
				if (field.getType() == String.class) { field.set(this, data); continue;	}
				if (field.getType() == Integer.class) { field.set(this, Integer.parseInt(data)); continue; }
				if (field.getType() == Integer.TYPE) { field.setInt(this, Integer.parseInt(data)); continue; }
				if (field.getType() == Double.class) { field.set(this, Double.parseDouble(data)); continue; }
				if (field.getType() == Boolean.class) { field.set(this, Boolean.parseBoolean(data)); continue; }
				if (field.getType() == Duration.class) { field.set(this, Duration.parse(elt.getAttributeValue(name))); continue; }

				LOGGER.warn("Type non g�r� "+ field.getType().getName()+" pour le parametre " + field.getName() + " pour la classe " + this.getClass().getName());
				importStatus = false;

			} catch (IllegalArgumentException | IllegalAccessException | DateTimeParseException e) {
				LOGGER.warn(e.getClass().getName() +", Parametre " + field.getName() + " pour la classe " + this.getClass().getName() + " => " + e.getMessage());
				continue;
			}

		}

		return importStatus;

	}

	@Override
	public Element exportElement(String xmlName) {

		// recupere le LOGGER
		Logger LOGGER = LogManager.getLogger();

		// genere l'element de base de sortie
		Element elt = new Element(xmlName);


		for (Field field : this.getClass().getDeclaredFields()) {

			LOGGER.debug("Processing "+field.getName()+" in "+this.getClass().getName());

			// l'objet est statique donc d�j� correctement intialis�, pas besoin de le sauvegarder => passe au suivant
			if (Modifier.isStatic(field.getModifiers())) continue;

			// l'objet est marqu� transient (pas utiliser en serialisation) => passe au suivant
			if (Modifier.isTransient(field.getModifiers())) continue;

			// autoriser l'acces si le champ est priv�
			if (Modifier.isPrivate(field.getModifiers())) field.setAccessible(true);

			// exporte l'objet
			try {

				// recupere l'objet � sauvegarder
				Object o = field.get(this);

				// l'objet n'existe pas donc pas possible de le sauvegarder => passe au suivant
				if (o == null) continue;

				// verifie si le champ a une annotation
				String name = field.getName();

				// verifie s'il y a une annotation d'Alias
				if (field.getAnnotation(PropertyAlias.class)!=null)
					name = field.getAnnotation(PropertyAlias.class).value();

				// ajoute l'element � la suite s'il est compatible importExportXML
				if (o instanceof PropertyImportExport) {
					elt.addContent(((PropertyImportExport) o).exportElement(name));
					continue;
				}

				// type Duration
				if (field.getType() == Duration.class) {
					elt.setAttribute(name, ((Duration) o).toString());
					continue;
				}

				// exporte avec toString()
				elt.setAttribute(name, o.toString());

			} catch (IllegalArgumentException | IllegalAccessException e) {
				LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + this.getClass().getName() + " => " + e.getMessage(), e);
				continue;
			}

		}

		return elt;

	}

}
