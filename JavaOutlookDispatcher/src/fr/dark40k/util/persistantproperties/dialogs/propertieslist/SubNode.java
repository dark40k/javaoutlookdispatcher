package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.tags.PropertyDescription;

class SubNode implements TreeNode {

	private final static Logger LOGGER = LogManager.getLogger();

	private final TreeNode parent;

	private final ArrayList<SubNode> subProperties;
	private final ArrayList<Field> editableFields;

	private Property property;

	private String description;


	SubNode(Property property, String description, boolean includeStatic, TreeNode parent) {

		LOGGER.debug("description="+description);

		this.property=property;
		this.description=description;
		this.parent = parent;


		subProperties = new ArrayList<>();
		editableFields = new ArrayList<>();

		for (Field field : property.getClass().getDeclaredFields())

			try {

				// le champ est marqu� transient (suppos� non editable directement) => passe au suivant
				if (Modifier.isTransient(field.getModifiers())) continue;

				// le champ est statique (ne l'affiche que si on inclu les variables statiques) => passe au suivant
				if ((!includeStatic) && (Modifier.isStatic(field.getModifiers()))) continue;

				// autoriser l'acces si le champ est priv�
				if (Modifier.isPrivate(field.getModifiers())) field.setAccessible(true);

				// recupere l'objet � sauvegarder
				Object o = field.get(property);

				// l'objet n'existe pas donc pas possible � editer => passe au suivant
				if (o == null) continue;

				// verifie si le champ a une annotation
				String subFieldDescription = field.getName();

				// verifie s'il y a une description
				if (field.getAnnotation(PropertyDescription.class)!=null)
					subFieldDescription = field.getAnnotation(PropertyDescription.class).value();

				// il s'agit d'un objet groupe de propri�t� => le stocke comme sous-propri�t�
				if (o instanceof Property) {
					subProperties.add(new SubNode( (Property) o, subFieldDescription, includeStatic, this ));
					continue;
				}

				// stocke le champ editable
				editableFields.add(field);

			} catch (IllegalArgumentException | IllegalAccessException e) {
				LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
				continue;
			}

	}

	//
	// Getters / Setters
	//

	public Property getProperty() {
		return property;
	}

	public Iterable<Field> getFields() {
		return editableFields;
	}

	//
	// Interface TreeNode
	//

	@Override
	public SubNode getChildAt(int childIndex) {
		return subProperties.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return subProperties.size();
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public int getIndex(TreeNode searchNode) {
		if (!(searchNode instanceof SubNode)) throw new IllegalArgumentException("Instance is not PropertyNode, class = "+searchNode.getClass().getName());
		return subProperties.indexOf(searchNode);
	}

	@Override
	public boolean getAllowsChildren() {
		return subProperties.size()>0;
	}

	@Override
	public boolean isLeaf() {
		return subProperties.size()==0;
	}

	@Override
	public Enumeration<SubNode> children() {
		return Collections.enumeration(subProperties);
	}

	@Override
	public String toString() {
		return description;
	}

}
