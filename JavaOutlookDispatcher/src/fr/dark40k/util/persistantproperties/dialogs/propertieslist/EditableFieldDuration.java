package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.lang.reflect.Field;
import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.persistantproperties.Property;

class EditableFieldDuration extends EditableField {

	private final static Logger LOGGER = LogManager.getLogger();

	protected JTextField textField = new JTextField();

	public EditableFieldDuration(Field field, Property property) {

		super(field,property);

		textField.setText(formatDuration((Duration)originalValue,""));
		addEditorComponent(textField);

	}

	@Override
	public boolean hasPendingChanges() {

		if (getValue()==null) return false;

		return !getValue().equals(originalValue);

	}

	@Override
	public boolean applyChanges() {

		Duration newValue = getValue();
		if (newValue==null) return false;

		try {
			field.set(property,newValue);
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new RuntimeException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}

	}

	private Duration getValue() {
		return parseDuration(textField.getText(),(Duration)null);
	}

	//
	// Duration
	//

	// formule sans les codes : ((?<hh>\d+)\s*h)?(\s*)((?<mm>\d+)\s*m)?(\s*)((?<ss>\d+(.\d*)?)\s*s)?(\s*)
	private final static Pattern regExp = Pattern.compile("((?<hh>\\d+)\\s*h)?(\\s*)((?<mm>\\d+)\\s*m)?(\\s*)((?<ss>\\d+(.\\d*)?)\\s*s)?(\\s*)");

	/**
	 * Convertit une dur�e au format "256h 38m 26s"
	 *
	 * @param duration
	 * @param defaultString : renvoy� si duration est null
	 * @return
	 */
	private static String formatDuration(Duration duration, String defaultString) {
		if (duration == null) return defaultString;
		long seconds = duration.getSeconds();
		long absSeconds = Math.abs(seconds);
		String positive = String.format("%dh %02dm %02ds", absSeconds / 3600, (absSeconds % 3600) / 60, absSeconds % 60);
		return seconds < 0 ? "-" + positive : positive;
	}

	/**
	 * Lit une dur�e bas�e sur le format "256h 38m 26s"
	 *
	 * Chaque groupe est optionnel
	 * Les espaces entre les groupes sont ignor�s.
	 * Les espaces entre la valeur et le symbole d'unit�s sont ignor�s
	 *
	 * @param value
	 * @return
	 *
	 * @exception  NumberFormatException if the {@code String} does not contain a parsable {@code Duration}.
	 *
	 */
	private static Duration parseDuration(String string, Duration defaultDuration) {
		try {
			return parseDuration(string);
		} catch (Exception e) {
			return defaultDuration;
		}
	}

	private static Duration parseDuration(String value) throws NumberFormatException {

		Matcher m = regExp.matcher(value);

		if (!m.find()) throw new NumberFormatException("Format illisible : "+value);

		int hh = Integer.parseInt(m.group("hh"));
		int mm = Integer.parseInt(m.group("mm"));
		int ss = Integer.parseInt(m.group("ss"));

		return Duration.ofHours(hh).plusMinutes(mm).plusSeconds(ss);

	}

}
