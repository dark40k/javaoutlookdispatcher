package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import fr.dark40k.util.persistantproperties.PropertiesList;

class RootNode implements TreeNode {

	private ArrayList<SubNode> subProperties = new ArrayList<SubNode>();

	public RootNode(PropertiesList propertiesList, boolean includeStatic) {
		subProperties.clear();
		propertiesList.applyToAll(
				entry -> {
					subProperties.add(new SubNode(entry.getValue(),entry.getKey(),includeStatic,this));
					});
	}

	//
	// Interface TreeNode
	//

	@Override
	public SubNode getChildAt(int childIndex) {
		return subProperties.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return subProperties.size();
	}

	@Override
	public TreeNode getParent() {
		return null;
	}

	@Override
	public int getIndex(TreeNode searchNode) {
		if (!(searchNode instanceof RootNode)) throw new IllegalArgumentException("Instance is not PropertyNode, class = "+searchNode.getClass().getName());
		return subProperties.indexOf(searchNode);
	}

	@Override
	public boolean getAllowsChildren() {
		return subProperties.size()>0;
	}

	@Override
	public boolean isLeaf() {
		return subProperties.size()==0;
	}

	@Override
	public Enumeration<SubNode> children() {
		return Collections.enumeration(subProperties);
	}

	@Override
	public String toString() {
		return "root";
	}

}
