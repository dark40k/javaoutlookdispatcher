package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JTextArea;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.fields.StringHashSetField;

class EditableFieldStringHashSetProperty extends EditableField {

//	private final static Logger LOGGER = LogManager.getLogger();

	protected JTextArea textArea = new JTextArea();

	private String originalValueFlattened;

	public EditableFieldStringHashSetProperty(Field field, Property property) {

		super(field,property);

		StringBuilder builder = new StringBuilder();
		for(String s : (StringHashSetField) originalValue) {
		    builder.append(s);
		    builder.append("\n");
		}
		originalValueFlattened = builder.toString();

		textArea.setText(originalValueFlattened);

		addEditorComponent(textArea);

	}

	@Override
	public boolean hasPendingChanges() {

		return !textArea.getText().equals(originalValueFlattened);

	}

	@Override
	public boolean applyChanges() {

		if (!hasPendingChanges()) return false;

		ArrayList<String> outList = new ArrayList<String>();
		for (String string : textArea.getText().split("\n+")) {
			string = string.trim();
			if (string.length() > 0) outList.add(string);
		}

		((StringHashSetField)originalValue).set(outList);
		return true;

	}

}
