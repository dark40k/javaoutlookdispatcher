package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.awt.BorderLayout;
import java.awt.Component;
import java.lang.reflect.Field;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.persistantproperties.Property;
import fr.dark40k.util.persistantproperties.tags.PropertyDescription;
import fr.dark40k.util.persistantproperties.tags.PropertyHelp;

abstract class EditableField extends JPanel {

	private final static Logger LOGGER = LogManager.getLogger();

	protected final Field field;
	protected final Property property;

	protected final Object originalValue;
	protected final JPanel panel = new JPanel();

	public EditableField(Field field, Property property) {

		LOGGER.debug("Ajout parametre " + field.getName() + " pour la classe " + property.getClass().getName());

		this.field = field;
		this.property = property;

		initGUI();

		String borderTitle = (field.getAnnotation(PropertyDescription.class) != null)
				? field.getAnnotation(PropertyDescription.class).value()
				: field.getName();

		borderTitle +=" ( "+field.getType().getSimpleName()+" )";
				
		setBorder(new TitledBorder(null, borderTitle, TitledBorder.LEADING, TitledBorder.TOP, null, null));

		setToolTipText(
				(field.getAnnotation(PropertyHelp.class) != null)
			?
				textToHTML(field.getAnnotation(PropertyHelp.class).value())
			:
				null
			);
		setLayout(new BorderLayout(0, 0));

		try {
			originalValue = field.get(property);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new IllegalArgumentException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}

	}

	private static String textToHTML(String string) {
		return "<html><code>" + string.replace("\n", "<br>").replace(" ", "&nbsp;") + "</html>";
	}

	public abstract boolean hasPendingChanges();

	public abstract boolean applyChanges();

	protected void addEditorComponent(Component comp) {
		add(comp, BorderLayout.CENTER);
	}

	private void initGUI() {


	}

}
