package fr.dark40k.util.persistantproperties.dialogs.propertieslist;

import java.lang.reflect.Field;

import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.persistantproperties.Property;

class EditableFieldInteger extends EditableField {

	private final static Logger LOGGER = LogManager.getLogger();

	protected JTextField textField = new JTextField();

	public EditableFieldInteger(Field field, Property property) {

		super(field,property);

		textField.setText(((Integer)originalValue).toString());

		addEditorComponent(textField);

	}

	@Override
	public boolean hasPendingChanges() {

		if (getValue()==null) return false;

		return !getValue().equals(originalValue);

	}

	@Override
	public boolean applyChanges() {

		Integer newValue = getValue();
		if (newValue==null) return false;

		try {
			field.set(property,newValue);
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new RuntimeException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}

	}

	private Integer getValue() {
		try {
			return Integer.parseInt(textField.getText());
		} catch (NumberFormatException e) {
			return null;
		}
	}

}
