package fr.dark40k.util.persistantproperties.dialogs;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import fr.dark40k.util.persistantproperties.fields.StringHashSetField;

public class DialogEditStringArrayField extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final JTextArea textArea = new JTextArea();

	private String finalValue;
	private String defaultValue;

	//
	// Fast access command
	//

	public static void showDialog(String title, StringHashSetField stringHashSetField) {

		DialogEditStringArrayField dialog = new DialogEditStringArrayField();

		dialog.setTitle(title);

		dialog.defaultValue=flattenList(stringHashSetField);
		dialog.textArea.setText(flattenList(stringHashSetField));

		dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		dialog.setVisible(true);

		if (dialog.finalValue==null) return;

		ArrayList<String> outList = new ArrayList<String>();
		for (String string : dialog.finalValue.split("\n+")) {
			string = string.trim();
			if (string.length() > 0) outList.add(string);
		}
		stringHashSetField.set(outList);

	}

	//
	// Constructor
	//

	public DialogEditStringArrayField() {
		initGUI();
	}

	//
	// Commands
	//

	protected void doCancel() {
		finalValue=null;
		setVisible(false);
	}

	protected void doOk() {
		finalValue=textArea.getText();
		setVisible(false);
	}

	protected void doDefault() {
		textArea.setText(defaultValue);
	}

	//
	// Utilities
	//

	private static String flattenList(Iterable<String> stringList) {
		StringBuilder builder = new StringBuilder();
		for(String s : stringList) {
		    builder.append(s);
		    builder.append("\n");
		}
		return builder.toString();
	}

	//
	// GUI
	//

	private void initGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new TitledBorder(null, "Liste des param�tres (1 param�tre par ligne)", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane, BorderLayout.CENTER);
			scrollPane.setViewportView(textArea);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{0, 312, 47, 65, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnDefault = new JButton("Default");
				btnDefault.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doDefault();
					}
				});
				GridBagConstraints gbc_btnDefault = new GridBagConstraints();
				gbc_btnDefault.anchor = GridBagConstraints.WEST;
				gbc_btnDefault.insets = new Insets(0, 0, 0, 5);
				gbc_btnDefault.gridx = 0;
				gbc_btnDefault.gridy = 0;
				buttonPane.add(btnDefault, gbc_btnDefault);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doOk();
					}
				});
				GridBagConstraints gbc_okButton = new GridBagConstraints();
				gbc_okButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_okButton.insets = new Insets(0, 0, 0, 5);
				gbc_okButton.gridx = 2;
				gbc_okButton.gridy = 0;
				buttonPane.add(okButton, gbc_okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						doCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				GridBagConstraints gbc_cancelButton = new GridBagConstraints();
				gbc_cancelButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_cancelButton.gridx = 3;
				gbc_cancelButton.gridy = 0;
				buttonPane.add(cancelButton, gbc_cancelButton);
			}
		}
	}

}
