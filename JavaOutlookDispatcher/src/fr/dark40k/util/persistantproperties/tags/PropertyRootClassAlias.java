package fr.dark40k.util.persistantproperties.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Permet de definir un alias pour le nom du bloc XML contenant le champ.
 *
 * @author Francois
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyRootClassAlias {
	String value();
}
