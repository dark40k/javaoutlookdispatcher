package fr.dark40k.util.persistantproperties;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.util.persistantproperties.tags.PropertyRootClassAlias;

public class PropertiesList {

	private final static Logger LOGGER = LogManager.getLogger();

	private final LinkedHashMap<String,Property> rootGroupList = new LinkedHashMap<String,Property>();

	//
	// Enregistrement des racines
	//

	public void add(Property property) {

		String name = property.getClass().getName();

		// verifie s'il y a une annotation d'Alias
		if (property.getClass().getAnnotation(PropertyRootClassAlias.class)!=null)
			name = property.getClass().getAnnotation(PropertyRootClassAlias.class).value();

		rootGroupList.put(name,property);

	}

	public void importElement(Element rootElt) {

		if (rootElt==null) return;

		LOGGER.info("Parsing parameters data.");

		for ( Entry<String, Property> rootGroupEntry : rootGroupList.entrySet())
			rootGroupEntry.getValue().importElement(rootElt,rootGroupEntry.getKey());

	}

	public Element exportElement() {
		Element rootElement = new Element("Root");

		for ( Entry<String, Property> rootGroupEntry : rootGroupList.entrySet())
			rootElement.addContent(rootGroupEntry.getValue().exportElement(rootGroupEntry.getKey()));

		return rootElement;
	}

	public void applyToAll(Consumer<Entry<String, Property>> todo) {
		for ( Entry<String, Property> rootGroupEntry : rootGroupList.entrySet())
			todo.accept(rootGroupEntry);;
	}

}
