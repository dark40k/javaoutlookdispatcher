package fr.dark40k.util.persistantproperties;

import org.jdom2.Element;

public interface PropertyImportExport {

	public boolean importElement(Element fatherElt, String elementName);

	public Element exportElement(String newElementName);
	

}
