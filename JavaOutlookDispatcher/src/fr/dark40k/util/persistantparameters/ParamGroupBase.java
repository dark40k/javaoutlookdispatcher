package fr.dark40k.util.persistantparameters;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.jdom2.Element;

public abstract class ParamGroupBase extends Param {

	protected Element loadElt;

	protected final LinkedHashMap<String,Param> subParams = new LinkedHashMap<String,Param>();
	
	public ParamGroupBase(Param father, String name) {
		super(father, name);
	}

	//
	// Data
	//
	
	public void clear() {
		
		// supprime les donn�es de chargement residuelles qui n'ont pas �t� d�clar�s � ce stade
		loadElt = null;
		
		// vide la liste des sous-objets
		subParams.clear();
		
		// signal le changement
		incVersion();
	}
	
	public Iterable<String> listSubParamsNames(boolean includeUnloadedParams) {
		
		LinkedHashSet<String> listNames = new  LinkedHashSet<String>();
		
		if ((loadElt!=null) && includeUnloadedParams)
			for (Element elt : loadElt.getChildren())
				listNames.add(elt.getName());
		
		for (String name : subParams.keySet())
			listNames.add(name);

		return listNames;
		
	}

	public Iterable<Param> listDeclaredSubParams() {
		return subParams.values();
	}
	
	
	// SubParams - Group
	
	public ParamGroup getSubParamGroup(String name) {
		ParamGroup subParam = (ParamGroup) subParams.get(name);
		if (subParam!=null) return subParam;
		subParam = new ParamGroup(this, name);
		subParams.put(name, subParam);
		return subParam;
	}
	
	// SubParams - Persistent Group
	
	public ParamPersistentGroup getSubParamPersistentGroup(String name) {
		ParamPersistentGroup subParam = (ParamPersistentGroup) subParams.get(name);
		if (subParam!=null) return subParam;
		subParam = new ParamPersistentGroup(this, name);
		subParams.put(name, subParam);
		return subParam;
	}
	
	// SubParams - Integer
	// la valeur par defaut n'est modifi�e que si elle est diff�rente de null
	
	public ParamInteger getSubParamInteger(String name, Integer defaultValue) {
		ParamInteger subParam = (ParamInteger) subParams.get(name);
		if (subParam!=null) {
			if (defaultValue!=null) subParam.setDefaultValue(defaultValue);
			return subParam;
		}
		subParam = new ParamInteger(this, name, defaultValue);
		subParams.put(name, subParam);
		return subParam;
	}
	
	// SubParams - Double
	// la valeur par defaut n'est modifi�e que si elle est diff�rente de null
	
	public ParamDouble getSubParamDouble(String name, Double defaultValue) {
		ParamDouble subParam = (ParamDouble) subParams.get(name);
		if (subParam!=null) {
			if (defaultValue!=null) subParam.setDefaultValue(defaultValue);
			return subParam;
		}
		subParam = new ParamDouble(this, name, defaultValue);
		subParams.put(name, subParam);
		return subParam;
	}
	
	// SubParams - Boolean
	// la valeur par defaut n'est modifi�e que si elle est diff�rente de null
	
	public ParamBoolean getSubParamBoolean(String name, Boolean defaultValue) {
		ParamBoolean subParam = (ParamBoolean) subParams.get(name);
		if (subParam!=null) {
			if (defaultValue!=null) subParam.setDefaultValue(defaultValue);
			return subParam;
		}
		subParam = new ParamBoolean(this, name, defaultValue);
		subParams.put(name, subParam);
		return subParam;
	}
	
	// SubParams - String
	// la valeur par defaut n'est modifi�e que si elle est diff�rente de null
	
	public ParamString getSubParamString(String name, String defaultValue) {
		ParamString subParam = (ParamString) subParams.get(name);
		if (subParam!=null) {
			if (defaultValue!=null) subParam.setDefaultValue(defaultValue);
			return subParam;
		}
		subParam = new ParamString(this, name, defaultValue);
		subParams.put(name, subParam);
		return subParam;
	}

	// SubParams - String List
	// la valeur par defaut n'est modifi�e que si elle est diff�rente de null
	
	public ParamStringList getSubParamStringList(String name, Collection<? extends String> defaultValue) {
		ParamStringList subParam = (ParamStringList) subParams.get(name);
		if (subParam!=null) {
			if (defaultValue!=null) subParam.setDefaultValue(defaultValue);
			return subParam;
		}
		subParam = new ParamStringList(this, name, defaultValue);
		subParams.put(name, subParam);
		return subParam;
	}
	
	//
	// Load/Save
	//
	
	@Override
	public void restoreDefault() {
		
		// supprime les donn�es de chargement residuelles qui n'ont pas �t� d�clar�s � ce stade
		loadElt = null;
		
		// reinitialise les parametres d�j� declar�s
		for (Param subParam : subParams.values() ) subParam.restoreDefault();
		
		//incremente la version
		incVersion();
	}

	@Override
	protected void load(Element fatherElt) {
		
		// initialise le loadElt pour les d�clarations de parametres futures
		loadElt = fatherElt.getChild(xmlName);
		
		// quitte si pas de parametres � charger (pas de parametres d�clar�s ou pas d'elements conteneur)
		if ((subParams==null) || (loadElt==null)) return;
				
		// charge les parametres
		for (Param subParam : subParams.values() ) subParam.load(loadElt);
				
		//incremente la version
		incVersion();
				
	}

	@Override
	protected abstract void save(Element fatherElt);
	
}
