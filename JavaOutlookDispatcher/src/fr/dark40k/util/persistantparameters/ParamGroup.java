package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamGroup extends ParamGroupBase {

	public ParamGroup(Param father, String name) {
		super(father, name);
	}


	@Override
	protected void save(Element fatherElt) {
		
		// supprime les donn�es de chargement residuelles qui n'ont pas �t� d�clar�s � ce stade
		loadElt = null;
		
		// cree un loadElt vide pour stocker les sous-parametres
		Element elt = new Element(xmlName);
		
		// sauve les parametres declar�s
		for (Param subParam : subParams.values() ) subParam.save(elt);
		
		// ajoute les donnees au pere si elles existent
		if (elt.hasAttributes() || (elt.getContentSize()>0)) {
			fatherElt.removeChildren(xmlName);
			fatherElt.addContent(elt);
		}
		
	}


}
