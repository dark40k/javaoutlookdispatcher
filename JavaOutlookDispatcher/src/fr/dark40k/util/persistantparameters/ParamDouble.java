package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamDouble extends ParamRawType<Double> {

	public ParamDouble(Param father, String xmlName, Double defaultValue) {
		super(father, xmlName, defaultValue);
	}

	//
	// Load/Save
	//
	
	@Override
	protected void load(Element fatherElt) {
		
		String attr = fatherElt.getAttributeValue(xmlName);
		
		if (attr==null) return;
		
		value = Double.valueOf(attr);
		incVersion();

	}

}
