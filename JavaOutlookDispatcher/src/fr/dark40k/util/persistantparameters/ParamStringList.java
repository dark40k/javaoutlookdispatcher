package fr.dark40k.util.persistantparameters;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.jdom2.CDATA;
import org.jdom2.Element;

public class ParamStringList extends Param implements Iterable<String> {
	
	private LinkedHashSet<String> stringList;
	private LinkedHashSet<String> defaultStringList;
	
	public ParamStringList(ParamGroupBase father, String xmlName, Collection<? extends String> defaultStringList) {
		super(father,xmlName);
		if (defaultStringList!=null) setDefaultValue(defaultStringList);
	}
	
	//
	// Data
	//
	
	@Override
	public Iterator<String> iterator() {
		if (stringList==null) { 
			return (defaultStringList!=null) ? defaultStringList.iterator() : Collections.emptyIterator();
		}
		return stringList.iterator();
	}

	public boolean contains(String string) {
		assert defaultStringList!=null : "DefaultStringList is not defined.";
		if (stringList==null) return defaultStringList.contains(string);
		return stringList.contains(string);
	}
	
	public boolean containsSubString(String string) {
		assert defaultStringList!=null : "DefaultStringList is not defined.";
		LinkedHashSet<String> searchedList = (stringList!=null)?stringList:defaultStringList;
		
		for (String searchedString : searchedList)
			if (searchedString.contains(string))
				return true;
		
		return false;
	}
	
	public void set(Collection<? extends String> list) {
		assert list!=null : "Trying to set a null list.";
		
		if (stringList==null) 
			stringList = new LinkedHashSet<String>();
		else
			stringList.clear();
		
		stringList.addAll(list);
		
		incVersion();
	}

	public void add(String string) {
		assert string!=null : "Trying to add a null string";
		if (stringList==null) buildFromDefault();
		stringList.add(string);
		incVersion();
	}
	
	public void remove(String string) {
		assert string!=null : "Trying to remove a null string";
		if (stringList==null) buildFromDefault();
		stringList.remove(string);
		incVersion();
	}

	public void switchParam(String string) {
		assert string!=null : "Trying to switch a null string";
		if (stringList==null) buildFromDefault();
		if (stringList.contains(string)) 
			stringList.remove(string);
		else
			stringList.add(string);
		incVersion();
	}

	private void buildFromDefault() {
		stringList = new LinkedHashSet<String>();
		stringList.addAll(defaultStringList);
	}
	
	//
	// Default
	//
	
	public Iterable<String> getDefault() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return (defaultStringList!=null) ? defaultStringList.iterator() : Collections.emptyIterator();
			}
		};
	}
	
	public void setDefaultValue(Collection<? extends String> defaultStringList) {
		
		assert defaultStringList!=null : "Trying to set a null defaultValue.";
		
		this.defaultStringList=new LinkedHashSet<String>();
		this.defaultStringList.addAll(defaultStringList);
		
	}
	
	@Override
	public void restoreDefault() {
		stringList=null;
		incVersion();
	}
	
	//
	// Load/Save
	//
	

	@Override
	protected void load(Element fatherElt) {
		
		List<Element> children = fatherElt.getChildren(xmlName);
		
		if (children.isEmpty()) return;
		
		stringList= new LinkedHashSet<String>();
		for (Element elt : fatherElt.getChildren(xmlName))
			stringList.add(elt.getValue());
		incVersion();
	}

	@Override
	protected void save(Element fatherElt) {
		
		if ((stringList == null) || (stringList.equals(defaultStringList))) {
			fatherElt.removeChildren(xmlName);
			return;
		}
		
		for (String string : stringList) {
			Element elt = new Element(xmlName);
			elt.setContent(new CDATA(string));
			fatherElt.addContent(elt);
		}
		
	}

}
