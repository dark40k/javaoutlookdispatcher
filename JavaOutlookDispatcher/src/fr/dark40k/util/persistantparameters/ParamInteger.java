package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamInteger extends ParamRawType<Integer> {

	public ParamInteger(Param father, String xmlName, Integer defaultValue) {
		super(father, xmlName, defaultValue);
	}

	//
	// Load/Save
	//
	
	@Override
	protected void load(Element fatherElt) {
		
		String attr = fatherElt.getAttributeValue(xmlName);
		
		if (attr==null) return;
		
		value = Integer.valueOf(attr);
		incVersion();
		
	}

}
