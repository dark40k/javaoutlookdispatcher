package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public abstract class ParamRawType<T> extends Param {

	protected T value;
	protected T defaultValue;
	
	public ParamRawType(Param father, String xmlName, T defaultValue) {
		super(father,xmlName);
		if (defaultValue!=null) setDefaultValue(defaultValue);
	}
	
	//
	// Data
	//
	
	public T getValue() {
		if (value==null) {
			return defaultValue;
		}
		return value;
	}
	
	public void setValue(T value) {
		this.value=value;
		incVersion();
	}

	//
	// Default
	//
	
	public void setDefaultValue(T defaultValue) {
		this.defaultValue=defaultValue;
	}
	
	@Override
	public void restoreDefault() {
		value=null;
		incVersion();
	}

	//
	// Load/Save
	//
	
	@Override
	protected abstract void load(Element fatherElt);

	@Override
	protected void save(Element fatherElt) {
		if ((value==null)||(value.equals(defaultValue))) {
			fatherElt.removeAttribute(xmlName);
			return;
		}
		fatherElt.setAttribute(xmlName, value.toString());
	}
	

}
