package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamBoolean extends ParamRawType<Boolean> {

	public ParamBoolean(Param father, String xmlName, Boolean defaultValue) {
		super(father, xmlName, defaultValue);
	}

	//
	// Load/Save
	//
	
	@Override
	protected void load(Element fatherElt) {
		
		String attr = fatherElt.getAttributeValue(xmlName);
		
		if (attr==null) return;
		
		value = Boolean.valueOf(attr);
		incVersion();

	}

}
