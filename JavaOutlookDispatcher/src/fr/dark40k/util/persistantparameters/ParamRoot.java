package fr.dark40k.util.persistantparameters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class ParamRoot extends ParamGroup {

	private final static Logger LOGGER = LogManager.getLogger();

	private boolean hasChanged = false;

	private RandomAccessFile paramFile;
	private FileChannel paramFileChannel;
	private FileLock paramFileLock;


	//
	// Creation
	//

	public ParamRoot() {
		super(null, "Root");
	}

	//
	// Flag Sauvegarde
	//

	@Override
	public void incVersion() {
		super.incVersion();
		hasChanged=true;
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	//
	// Sauvegarde du contenu
	//

	@Override
	protected void load(Element elt) {
		throw new RuntimeException("unused method.");
	}

	@Override
	protected void save(Element elt) {
		throw new RuntimeException("unused method.");
	}

	@Override
	public void restoreDefault() {
		super.restoreDefault();
	}

	//
	// Sauvegarde g�n�rale
	//

	public void loadAndLock(File file) throws ParamRootException {

		if (file==null) throw new ParamRootException("Parameter file is null.");

        //
        // Chargement du fichier de parametres
        //

		LOGGER.info("Loading parameters, file = "+file.getAbsolutePath());

		if (file.exists()) {
			SAXBuilder sxb = new SAXBuilder();
			try {
				loadElt = sxb.build(file).getRootElement();
			} catch (Exception e) {
				LOGGER.error("Le fichier parametre n'a pas put etre charg� : " + e.getMessage());
				JOptionPane.showMessageDialog(null, "Le fichier parametre n'a pas put etre charg�.\nL'application fonctionne deja?", "Loading error", JOptionPane.ERROR_MESSAGE);
				throw new ParamRootException("Le fichier parametre n'a pas put etre charg� : " + e.getMessage());
			}
		} else {
			LOGGER.info("Parameter File does not exist. Skipping.");
		}

        //
        // Chargement des parametres
        //

		if (loadElt!=null) {
			for (Param subParam : listDeclaredSubParams())
				subParam.load(loadElt);
			incVersion();
		}

		//
		// Creation (si necessaire) et Verrouillage du fichier parametres
		//
		LOGGER.info("Verrouillage du fichier parametres = "+file.getAbsolutePath());

		try {
			paramFile =  new RandomAccessFile(file, "rw");
			paramFileChannel =paramFile.getChannel();
		} catch (FileNotFoundException e1) {
        	LOGGER.error("Echec de la creation du fichier parametre: "+e1.getMessage());
            closeLock();
            throw new ParamRootException("Echec de la creation du fichier parametre: "+e1.getMessage());
		}

        try {
            paramFileLock = paramFileChannel.tryLock(0,Long.MAX_VALUE,false);
        }
        catch (OverlappingFileLockException | IOException e) {
        	LOGGER.error("Echec du verrrouillage (application fonctionne deja?): "+e.getMessage());
            closeLock();
            throw new ParamRootException("Echec du verrrouillage (application fonctionne deja?).");
        }

        // ajoute un deverrouillage automatique � la fin de l'exection de la JVM
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
			public void run() {
            	if (paramFileLock==null) return;
            	System.out.println("Shutdown release of file : "+file.getAbsolutePath());
                closeLock();
            }
        });


	}

	@SuppressWarnings("resource")
	public void saveAndRelease() {

		if (hasChanged) {

			Element elt = new Element(xmlName);
			for (Param subParam : listDeclaredSubParams())
				subParam.save(elt);

			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());

			OutputStream fileOut = null;

			try {
				fileOut = Channels.newOutputStream(paramFileChannel);
				sortie.output(elt, fileOut);
				fileOut.flush();
			} catch (IOException e) {
				LOGGER.warn("File could not be saved : " + e.getMessage(),e);
				JOptionPane.showMessageDialog(null, "Error: File could not be saved : \n" + e.getMessage(), "Saving error", JOptionPane.ERROR_MESSAGE);
				return;
			} finally {
				if (fileOut != null)
					try {
						paramFile.setLength(paramFile.getFilePointer());
						paramFile.close();
					} catch (IOException e) {
						LOGGER.error("Stream could not be closed : " + e.getMessage());
					}
			}

			hasChanged=false;

		}

		closeLock();

	}

	private void closeLock() {

		try {
			paramFileLock.release();
		} catch (Exception e) {
		}
		paramFileLock=null;
		try {
			paramFileChannel.close();
		} catch (Exception e) {
		}
		paramFileChannel=null;
	}

	//
	// Sous-classe pour retour d'erreur
	//

	public static class ParamRootException extends Exception
	{
	    //Parameterless Constructor
	    public ParamRootException() {}

	    //Constructor that accepts a message
	    public ParamRootException(String message)
	    {
	       super(message);
	    }

	}

}
