package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

/**
 * @author Francois
 *
 */
abstract class Param {

	protected final Param father;
	protected final String xmlName;
	
	private int version;
	
	public Param(Param father, String xmlName) {
		this.father=father;
		this.xmlName=xmlName;
		this.version=-1;
		
		// si le pere contient des donn�es, alors les charger dans le nouveau sous-parametre
		if (father instanceof ParamGroupBase) 
			if (((ParamGroupBase)father).loadElt!=null) 
				load(((ParamGroupBase)father).loadElt);
	}
	
	protected void incVersion() {
		version++;
		if (father!=null) father.incVersion();
	}
	
	public int getVersion() {
		return version;
	}
	
	//
	// Default
	//
	
	abstract public void restoreDefault();

	//
	// Load/Save
	//
	
	abstract protected void load(Element fatherElt);
	
	abstract protected void save(Element fatherElt);
	
}
