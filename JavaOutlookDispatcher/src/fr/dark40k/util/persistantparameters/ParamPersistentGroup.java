package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamPersistentGroup extends ParamGroupBase{

	public ParamPersistentGroup(Param father, String name) {
		super(father, name);
	}

	@Override
	protected void save(Element fatherElt) {
		
		// cree un elt contenant les donn�es d'origine
		Element elt = (loadElt!=null)? loadElt.clone() : new Element(xmlName);
		
		// sauve (ajoute ou ecrase si deja pr�sent) les parametres declar�s
		for (Param subParam : subParams.values() ) subParam.save(elt);
		
		// ajoute les donnees au pere si elles existent
		if (elt.hasAttributes() || (elt.getContentSize()>0)) {
			fatherElt.removeChildren(xmlName);
			fatherElt.addContent(elt);
		}
		
	}

}
