package fr.dark40k.util.persistantparameters;

import org.jdom2.Element;

public class ParamString extends ParamRawType<String> {

	public ParamString(Param father, String xmlName, String defaultValue) {
		super(father, xmlName, defaultValue);
	}

	//
	// Load/Save
	//
	
	@Override
	protected void load(Element fatherElt) {
		
		value = fatherElt.getAttributeValue(xmlName);
		
		if (value==null) return;
		
		incVersion();
		
	}

}
